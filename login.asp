<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include virtual="/inc/Language.asp"-->
<!DOCTYPE html>
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if gt IE 9]> <html lang="en" class="ie"> <![endif]-->
<!--[if !IE]><!-->
<html dir="ltr" lang="en">
	<!--<![endif]-->

	<head>
		<meta charset="utf-8">
		<title>EZ pay</title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />

		<script>
			function login_ok2()
			{
				
				var wallet_no = $("#wallet_no2").val();
				var password = $("#Password2").val();
				var remember = $("#remember2").val();
				
				if (wallet_no.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_63%>',
						text: '<%=EZ_LAN_64%>',
						callback: function(result) {
							$("#wallet_no").focus();
						}

					});
					
				}else if (password.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_63%>',
						text: '<%=EZ_LAN_52%>',
						callback: function(result) {
							$("#password").focus();
						}
					});
					
				}else{
					$.ajax({
						 url: "login_ok.asp",
						 type: "POST",
						 data: { wallet_no: wallet_no, password: password, remember: remember },  

						 success: function(data){
							 
							 if(data == "YES"){						 
								location.href="index.asp";							
							 }else if(data == "NO"){							 
								modal({
									type: 'error',
									title: '<%=EZ_LAN_63%>',
									text: '<%=EZ_LAN_65%>',
									callback: function(result) {
										$("#wallet_no").focus();
									}
								});				
							 }else if(data == "ERROR"){							 
								modal({
									type: 'error',
									title: '<%=EZ_LAN_63%>',
									text: '<%=EZ_LAN_9%>',
									callback: function(result) {
										$("#wallet_no").focus();
									}
								});				
							 }
						 },
			 
						 complete: function(data){
						//	location.href="index.asp";
						 }
					});
				}	
			}
			function tog(flag){
				var tmp = flag.value;
				tmp = tmp.toUpperCase();
				flag.value = tmp;
			}
		</script>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<!-- "gradient-background-header": applies gradient background to header -->
	<!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
	<body class="no-trans    ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!--#include file="header.asp"-->
		
			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active">Login</li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- main-container start -->
			<!-- ================ -->
			<!--div class="main-container dark-translucent-bg" style="background-image:url('template/images/background-img-6.jpg');"-->
			<div class="main-container dark-translucent-bg" >
				<div class="container">
					<div class="row">
						<!-- main start -->
						<!-- ================ -->
						<div class="main object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
							<div class="form-block center-block p-30 light-gray-bg border-clear">
								<h2 class="title">Log In</h2>
								<form class="form-horizontal" role="form" method="POST">
									
									<div class="form-group has-feedback">
										<label for="inputEmail" class="col-sm-3 control-label"><%=EZ_LAN_70%> <span class="text-danger small">*</span></label>
										<div class="col-sm-8">
											<input type="text" class="form-control" name="wallet_no2" id="wallet_no2" placeholder="wallet_no" onblur="javascript:tog(this);" required>
											<i class="fa fa-envelope form-control-feedback"></i>
										</div>
									</div>
									<div class="form-group has-feedback">
										<label for="inputPassword" class="col-sm-3 control-label"><%=EZ_LAN_71%> <span class="text-danger small">*</span></label>
										<div class="col-sm-8">
											<input type="password" class="form-control" name="Password2" id="Password2" placeholder="Password" required>
											<i class="fa fa-lock form-control-feedback"></i>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-8">
											<div class="checkbox">
												<label>
													<input type="checkbox" name="remember2" id="remember2"> <%=EZ_LAN_72%>
												</label>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-8">
											<button type="button" class="btn btn-group btn-default btn-animated" onclick="login_ok2();">Log In <i class="fa fa-check"></i></button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- main end -->
					</div>
				</div>
			</div>
			<!-- main-container end -->
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>
		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>
		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>
		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>
		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>
		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>

	</body>
</html>
