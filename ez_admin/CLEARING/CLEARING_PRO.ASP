<%@Language="VBScript" CODEPAGE="65001"%>
<%Response.ContentType="text/html;charset=UTF-8"%>
<!--#include virtual="/EZ_ADMIN/INC/DB_OPEN.INC" -->
<!--#include virtual="/EZ_ADMIN/CALL_QUERY/CLEARING.INC" -->
<%
SAVE_FLAG	=	REQUEST("SAVE_FLAG")
IDX			=	REQUEST("CLEARING_IDX")
MEMO		=	REQUEST("CLEARING_MEMO")

If IDX = "" Then
	Response.WRITE "ERROR"
	Response.redirect "/EZ_ADMIN/CLEARING/CLEARING_LIST.ASP"
Else
	If SAVE_FLAG = "DEL" Then
		Call CLEARING_DEL(IDX)
	ELSE
		Call CLEARING_CHK(IDX, MEMO)
	End If
End If
%>