<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_top_common_utf8.asp" -->
<!--#include virtual="/inc/Language.asp"-->
<script runat="server" Language="Javascript" src="./inc/JSON_JS.asp"></script>

<%


on error resume next

Dim sResult : sResult = "N"
Dim nResultCnt : nResultCnt = 0

Dim send_wallet : send_wallet = fnKeywordFilter(request("send_wallet"))
Dim receive_wallet : receive_wallet = fnKeywordFilter(request("receive_wallet"))
Dim gcoin : gcoin = fnKeywordFilter(request("gcoin"))
Dim mode : mode = fnKeywordFilter(request("mode"))
Dim SS_premium : SS_premium = Session("SS_premium")

'If send_wallet = "M1008" Or receive_wallet = "M1008" Then
'	msg = "사용하실 수 없습니다!"
'	Response.write msg
'Else

	Dim now_gcoin,franchise_check,change_gcoin,fee,fee_coin, recomm, recomm_coin
	Dim s_count,ssCheck,h_check
	s_count=0
	fee_coin=0
	now_gcoin=0

	fnDBConn()

	sCheck = "NO"
	ssCheck = "NO"

		If Left(send_wallet,1)<>"M" And Left(receive_wallet,1)<>"M" Then

			'지금 핀이 사용가능한 핀인지
			strSQL =  "  select count(send_wallet) as m_cnt from gcoin_transfer where send_wallet='" & send_wallet & "' and left(receive_wallet,1) <> 'M'"

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				s_count = rs("m_cnt")
	'			If CInt(s_count) >= 5 Then
	'				ssCheck = "NO"
	'				sResult = "S"
	'			Else
					ssCheck = "YES"
	'			End if
			end If
			
			rs.close
		Else

			ssCheck = "YES"

		End If

		If (ssCheck = "YES") then
			'지금 지갑이 사용가능한 지갑인지
			strSQL =  "  select wallet_no,gcoin,USD,JPY,CNY,PHP,HKD,send_fee,h_check from member where wallet_no='" & send_wallet & "'"

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) Then
				If mode="EZ" then
					now_gcoin = rs("gcoin")
					fee_coin= CCur(rs("gcoin"))*(rs("send_fee")*0.01)
				ElseIf mode="USD" Then
					now_gcoin = rs("USD")
				ElseIf mode="JPY" Then
					now_gcoin = rs("JPY")
				ElseIf mode="CNY" Then
					now_gcoin = rs("CNY")
				ElseIf mode="PHP" Then
					now_gcoin = rs("PHP")
				ElseIf mode="HKD" Then
					now_gcoin = rs("HKD")
				ElseIf mode="SGD" Then
					now_gcoin = rs("SGD")
				ElseIf mode="MYR" Then
					now_gcoin = rs("MYR")
				Else
					now_gcoin = rs("gcoin")
				End If

				h_check = rs("h_check")

				fee_coin= CCur(gcoin)*(CCur(rs("send_fee"))*0.01)
				
				If h_check = "정상" Then
				
					If CCur(now_gcoin) >= CCur(gcoin) Then
						'Response.write "True"
						sCheck = "YES"
					Else
						'Response.write "False"
						sCheck = "NO"
						sResult = "0"
						msg = EZ_LAN_148
					End If
				
				Else

					sCheck = "NO"
					sResult = "0"
					msg = EZ_LAN_149
				End If
				
			end If

			'Response.end
			
			rs.close


			If sCheck = "YES" then

				strSQL =  "  select wallet_no,gcoin from member where wallet_no='" & receive_wallet & "'"

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					sCheck = "YES"
				Else
					sCheck = "NO"
					sResult = "0"
					msg = EZ_LAN_149
				end If
				
				rs.close

			End If

			If(sCheck="YES") And (Left(send_wallet,1)<>"M" And Left(receive_wallet,1)<>"M") Then

				strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_settlement where wallet_no='" & send_wallet & "' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					g_sum = rs("g_sum")
					
				end If
				
				rs.close


				strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_settlement2 where wallet_no='" & send_wallet & "' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					g_sum = g_sum + rs("g_sum")
					
				end If
				
				rs.close


				strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_transfer where send_wallet='" & send_wallet & "' and receive_wallet not like 'M%' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					g_sum = g_sum + rs("g_sum")
					
				end If
				
				rs.close

				'특정 지갑회원
				If send_wallet = "521004" Or send_wallet="407000" Or wallet_no="M1008"  Or wallet_no="M1009" Then
					sCheck = "YES"
				else
				'특정 지갑회원
					If SS_premium <> "Y" Then
						If CCur(g_sum)+CCur(gcoin) <= 5000000 Then
							sCheck = "YES"
						Else
							sCheck = "NO"
							msg = EZ_LAN_150 & 5000000-CCur(g_sum) & EZ_LAN_150_1
						End If
					End If
				End If 
			End if	

			'특정 지갑회원
			If send_wallet = "521004" Or send_wallet="407000" Or wallet_no="M1008"  Or wallet_no="M1009" Then
				sCheck = "YES"
			else
			'특정 지갑회원
				If sCheck="YES" And SS_premium <> "Y" then
					If(sCheck="YES") And (Left(send_wallet,1)<>"M" And Left(receive_wallet,1)<>"M") Then	
						If CCur(gcoin) <= 2000000 Then
							sCheck = "YES"
						Else
							sCheck = "NO"
							msg = EZ_LAN_151 & 2000000-CCur(g_sum) & EZ_LAN_151_1
						end If	
					End If
				ElseIf sCheck="YES" And SS_premium = "Y" then
					If CCur(gcoin) <= 30000000 Then
						sCheck = "YES"
					Else
						sCheck = "NO"
						msg = EZ_LAN_152
					end If	
				End If
			End If
		
			If sCheck = "YES" then
			
				If Left(receive_wallet,1)="M" Then

					'지금 지갑이 사용가능한 지갑인지
					strSQL =  "  select fee, recomm from gcoin_franchise where wallet_no='" & receive_wallet & "'"

					cmd.CommandText = strSQL
					Set rs = cmd.execute()

					if not(rs.eof or rs.bof) then
						fee=rs("fee")
						fee_coin=(CCur(gcoin)*(fee*0.01))
						change_gcoin = CCur(gcoin)-CCur(fee_coin)

						'가맹점 추천지갑 체크
						SQL_ = "SELECT wallet_no from MEMBER WHERE recomm ='"& receive_wallet &"'"
						cmd.CommandText = SQL_
						Set rs_ = cmd.execute()

						if not(rs_.eof or rs_.bof) Then
							recomm = rs("recomm")
							recomm_coin = (CCur(gcoin)*(recomm*0.01))
							change_gcoin = CCur(gcoin)-CCur(fee_coin)-CCur(recomm_coin)
						Else
							recomm_coin = 0
							change_gcoin = change_gcoin
						End If
						rs_.close
					Else
						change_gcoin = gcoin
						sCheck = "NO"
					end If
					
					rs.close

				Else
					
					change_gcoin = CCur(gcoin)
	'				change_gcoin = gcoin

				End If
			
			End If



			If(sCheck="YES") Then

				strSQL=""
			
				dbConn.BeginTrans

				strSQL = strSQL & "	insert into gcoin_transfer(send_wallet,receive_wallet,gcoin,mode,fee, recomm) values"
				strSQL = strSQL & "	( '" & send_wallet &"','" & receive_wallet &"','" & gcoin &"','" & mode &"','" & fee_coin &"','" & recomm_coin &"')"

				If mode = "EZ" then
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_gcoin&"')  where wallet_no='" & receive_wallet & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & send_wallet & "'"	
				ElseIf mode = "USD" Then
					strSQL = strSQL & "	update  member set USD=convert(money,USD)+convert(money,'"&change_gcoin&"')  where wallet_no='" & receive_wallet & "'"	
					strSQL = strSQL & "	update  member set USD=convert(money,USD)-convert(money,'"&gcoin&"')  where wallet_no='" & send_wallet & "'"
				ElseIf mode = "JPY" Then
					strSQL = strSQL & "	update  member set JPY=convert(money,JPY)+convert(money,'"&change_gcoin&"')  where wallet_no='" & receive_wallet & "'"	
					strSQL = strSQL & "	update  member set JPY=convert(money,JPY)-convert(money,'"&gcoin&"')  where wallet_no='" & send_wallet & "'"
				ElseIf mode = "PHP" Then
					strSQL = strSQL & "	update  member set PHP=convert(money,PHP)+convert(money,'"&change_gcoin&"')  where wallet_no='" & receive_wallet & "'"	
					strSQL = strSQL & "	update  member set PHP=convert(money,PHP)-convert(money,'"&gcoin&"')  where wallet_no='" & send_wallet & "'"
				ElseIf mode = "CNY" Then
					strSQL = strSQL & "	update  member set CNY=convert(money,CNY)+convert(money,'"&change_gcoin&"')  where wallet_no='" & receive_wallet & "'"	
					strSQL = strSQL & "	update  member set CNY=convert(money,CNY)-convert(money,'"&gcoin&"')  where wallet_no='" & send_wallet & "'"
				ElseIf mode = "HKD" Then
					strSQL = strSQL & "	update  member set HKD=convert(money,HKD)+convert(money,'"&change_gcoin&"')  where wallet_no='" & receive_wallet & "'"	
					strSQL = strSQL & "	update  member set HKD=convert(money,HKD)-convert(money,'"&gcoin&"')  where wallet_no='" & send_wallet & "'"
				ElseIf mode = "SGD" Then
					strSQL = strSQL & "	update  member set SGD=convert(money,SGD)+convert(money,'"&change_gcoin&"')  where wallet_no='" & receive_wallet & "'"	
					strSQL = strSQL & "	update  member set SGD=convert(money,SGD)-convert(money,'"&gcoin&"')  where wallet_no='" & send_wallet & "'"
				ElseIf mode = "MYR" Then
					strSQL = strSQL & "	update  member set MYR=convert(money,MYR)+convert(money,'"&change_gcoin&"')  where wallet_no='" & receive_wallet & "'"	
					strSQL = strSQL & "	update  member set MYR=convert(money,MYR)-convert(money,'"&gcoin&"')  where wallet_no='" & send_wallet & "'"
				Else
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_gcoin&"')  where wallet_no='" & receive_wallet & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & send_wallet & "'"	
				End if

				cmd.CommandText = strSQL
				cmd.Execute nResultCnt, , adExecuteNoRecords

				if nResultCnt <> 1 or Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
					dbConn.RollBackTrans
					fnErrSave()
					msg = EZ_LAN_9
				else
					dbConn.CommitTrans
					sResult = "Y"
				'	fnLogSave()
				end If

			End If

		End If
		
		

	fnDBClose()
	%>

	<% 
	If sResult = "Y" Then
		Response.write "YES"
	Else
		Response.write msg
	End If

'End If
%>