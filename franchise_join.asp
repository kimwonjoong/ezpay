<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">

		<title>EZ pay | <%=EZ_LAN_51%></title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />

		<script type="text/javascript">
	
	
			function join_ok()
			{

				var wallet_no = $("#wallet_no").val();
				var now_wallet = $("#now_wallet").val();
				var password1 = $("#password1").val();
				var password2 = $("#password2").val();			
				var company = $("#company").val();
				var tel = $("#tel").val();
				var email = $("#email").val();

				if (password1.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_51%>',
						text: '<%=EZ_LAN_52%>',
						callback: function(result) {
							$("#password1").focus();
						}
					});
					
				}else if (password2.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_51%>',
						text: '<%=EZ_LAN_52%>',
						callback: function(result) {
							$("#password2").focus();
						}
					});
					
				}else if (password1 != password2){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_51%>',
						text: '<%=EZ_LAN_53%>',
						callback: function(result) {
							$("#password1").focus();
						}
					});
					
				}else if (company.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_51%>',
						text: '<%=EZ_LAN_54%>',
						callback: function(result) {
							$("#company").focus();
						}
					});
					
				}else if (tel.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_51%>',
						text: '<%=EZ_LAN_55%>',
						callback: function(result) {
							$("#tel").focus();
						}
					});
					
				}else if (email.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_51%>',
						text: '<%=EZ_LAN_56%>',
						callback: function(result) {
							$("#email").focus();
						}
					});
					
				}else{
					$.ajax({
						 url: "franchise_join_ok.asp",
						 type: "POST",
						 data: { wallet_no: wallet_no, password: password1,tel: tel,company: company,email: email ,now_wallet: now_wallet },  

						 success: function(data){
							 if(data == "YES"){						 
								modal({
									type: 'success',
									title: '<%=EZ_LAN_51%>',
									text: '<%=EZ_LAN_57%>',
									callback: function(result) {
										location.href="index.asp";
									}
								});		
							 }else{								 
								modal({
									type: 'error',
									title: '<%=EZ_LAN_51%>',
									text: '<%=EZ_LAN_58%>',
									callback: function(result) {
										location.href="franchise_join.asp";
									}
								});				
							 }
						 },
			 
						 complete: function(data){
						//	location.href="index.asp";
						 }
					});
				}	
			}
			
		</script>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<!-- "gradient-background-header": applies gradient background to header -->
	<!-- "page-loader-1 ... page-loader-6": add a page loader to the page (more info @components-page-loaders.html) -->
	<body class="no-trans    ">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!--#include file="header.asp"-->
		
			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active"><%=EZ_LAN_51%></li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- main-container start -->
			<!-- ================ -->
			<!--div class="main-container dark-translucent-bg" style="background-image:url('template/images/background-img-6.jpg');"-->
			<div class="main-container dark-translucent-bg" >
				<div class="container">
					<div class="row">
						<!-- main start -->
						<!-- ================ -->
						<div class="main object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
							<div class="form-block center-block p-30 light-gray-bg border-clear">
								<h2 class="title"><%=EZ_LAN_51%></h2>
								<form class="form-horizontal" role="form" name="joinform" id="joinform"  method="POST">
								<input type="hidden" name="now_wallet" id="now_wallet" value="<%=Session("SS_USERID")%>">
									<div class="form-group has-feedback">
										<label for="inputName" class="col-sm-3 control-label">가맹점지갑번호 <span class="text-danger small">*</span></label>
										<div class="col-sm-8">
											<%
												fnDBConn()


												'지금 핀이 사용가능한 핀인지
												strSQL =  "  select wallet_no from gcoin_franchise order by wallet_no desc"

												cmd.CommandText = strSQL
												Set rs = cmd.execute()

												if not(rs.eof or rs.bof) Then
													wallet_no = rs("wallet_no")
													sCheck = "YES"
												Else
													wallet_no = "M" & "1001"
												end If
												
												rs.close
												
												If(sCheck="YES") Then

													wallet_no = "M" & CLng(right(wallet_no,4)) + 1

												End if
												
												fnDBClose()
											%>
											<input type="text" readonly id="wallet_no" name="wallet_no" class="form-control margin-bottom-10" value="<%=wallet_no%>" required>
											<i class="fa fa-pencil form-control-feedback"></i>
										</div>
									</div>
									<div class="form-group has-feedback">
										<label for="inputLastName" class="col-sm-3 control-label"><%=EZ_LAN_71%><span class="text-danger small">*</span></label>
										<div class="col-sm-8">
											<input type="password" name="password1" id="password1" class="form-control margin-bottom-20" required>
											<i class="fa fa-key form-control-feedback"></i>
										</div>
									</div>
									<div class="form-group has-feedback">
										<label for="inputLastName" class="col-sm-3 control-label"><%=EZ_LAN_59%><span class="text-danger small">*</span></label>
										<div class="col-sm-8">
											<input type="password" name="password2" id="password2" class="form-control margin-bottom-20" required>
											<i class="fa fa-key form-control-feedback"></i>
										</div>
									</div>
									<div class="form-group has-feedback">
										<label for="inputUserName" class="col-sm-3 control-label"><%=EZ_LAN_60%><span class="text-danger small">*</span></label>
										<div class="col-sm-8">
											<input type="text" name="company" id="company" class="form-control margin-bottom-20" required>
											<i class="fa fa-user form-control-feedback"></i>
										</div>
									</div>
									<div class="form-group has-feedback">
										<label for="inputUserName" class="col-sm-3 control-label"><%=EZ_LAN_61%><span class="text-danger small">*</span></label>
										<div class="col-sm-8">
											<input type="tel" name="tel" id="tel" class="form-control margin-bottom-20" required>
											<i class="fa fa-tel form-control-feedback"></i>
										</div>
									</div>
									<div class="form-group has-feedback">
										<label for="inputEmail" class="col-sm-3 control-label">Email <span class="text-danger small">*</span></label>
										<div class="col-sm-8">
											<input type="email" name="email" id="email" class="form-control margin-bottom-20" required>
											<i class="fa fa-email form-control-feedback"></i>
										</div>
									</div>
									
									<div class="form-group">
										<div class="col-sm-offset-3 col-sm-8">
											<button type="button" class="btn btn-group btn-default btn-animated" onclick="join_ok();"><%=EZ_LAN_62%><i class="fa fa-check"></i></button>
										</div>
									</div>
								</form>
							</div>
						</div>
						<!-- main end -->
					</div>
				</div>
			</div>
			<!-- main-container end -->
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>
		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>
		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>
		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>
		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>
		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>

	</body>
</html>
