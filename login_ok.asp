<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib.asp"-->

<%


on error resume next

Dim sUserID, sPassword, sLoginChk
Dim sIDSaveChk
Dim sGroup, sUserName, sStoreCode, nStoreIdx, sWallet

sUserID = Trim(Request("wallet_no"))
sPassword = Trim(Request("password"))
sIDSaveChk = Trim(Request("remember"))


	fnDBConn()

	sLoginChk = "NO"

	'아이디 및 비밀번호 체크
	sql = " Select * from member where wallet_no=? and password=?  and status='정상' and h_check='정상' "			
	cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParaminput, 50, sUserID)
	cmd.Parameters.Append cmd.CreateParameter("@password", adVarChar, adParaminput, 50, sPassword)
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	If not(rs.eof or rs.bof) Then
		sLoginChk = "YES"
		sUserName = fnNullInit(rs("s_name"))	
		sPrimeum = fnNullInit(rs("primeum"))	
	End If	
	rs.close	
	Set rs = nothing	
	ParamInit()
	
	if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
		sLoginChk = "ERROR"
	else
		if sLoginChk = "YES" then
			'로그인 세션 설정
			
			Session("SS_USERID") = sUserID
			Session("SS_USERNAME") = sUserName
			Session("SS_WALLET") = sUserID
			Session("SS_premium") = sPrimeum
			
			'ID저장 쿠키 체크
			if remember = "on" then
				Response.Cookies("saveID") = sUserID
				Response.Cookies("saveIDChk") = "on"
			else
				Response.Cookies("saveID") = ""
				Response.Cookies("saveIDChk") = "off"
			end if
			
			Response.Cookies("saveID").Expires = DateAdd("m", 1, Now())
			Response.Cookies("saveIDChk").Expires = DateAdd("m", 1, Now())		

		

		end if
	end if

	fnDBClose()

	Response.write sLoginChk

%>
