<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!-- #include virtual="./inc/json2.asp" -->
<!--#include virtual="/inc/Language.asp"-->
<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 
%>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY | <%=EZ_LAN_24%></title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

	
		
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active"><%=EZ_LAN_24%></li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->
			<%
				Function getSiteSourcePost( siteURL, params )
				 Set httpObj = Server.CreateObject("WinHttp.WinHttpRequest.5.1")
				 httpObj.open "POST" , siteURL, False
				 httpObj.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded"
				 '포스트 방식시 위의 라인을 추가해 주어야 한다.
				  
				 httpObj.Send params
				 '포스트의 파라미터는 Send 호출시 같이 값을 넘겨 주어야 한다.
				 httpObj.WaitForResponse
				 If httpObj.Status = "200" Then
				  getSiteSourcePost = httpObj.ResponseText
				 Else
				  getSiteSourcePost = null
				 End If
				End Function
				'호출 URL

				Dim urlPath : urlPath = "http://fx.kebhana.com/FER1101M.web"
				'페이지로드Post방식

				'response.write(Mid(getSiteSourcePost("http://fx.keb.co.kr/FER1101M.web",""),27))
				fnDBConn()

					'지금 핀이 사용가능한 핀인지
					strSQL =  "  select exchange_name,rate from gcoin_exchange"

					cmd.CommandText = strSQL
					Set rs = cmd.execute()

					if not(rs.eof or rs.bof) then
						nData = fnRs2JsonArr(aData, rs)
					end If
					
					rs.close

				fnDBClose()

				' 2017. 08. 03 if nData >= 0 then 수정함!
				if nData > 0 then
					for i=0 to nData
						If aData("exchange_name")(i) = "USD" Then
							dUSD = aData("rate")(i)*0.01
						ElseIf aData("exchange_name")(i) = "JPY" Then
							dJPY = aData("rate")(i)*0.01
						ElseIf aData("exchange_name")(i) = "CNY" Then
							dCNY = aData("rate")(i)*0.01
						ElseIf aData("exchange_name")(i) = "PHP" Then
							dPHP = aData("rate")(i)*0.01
						ElseIf aData("exchange_name")(i) = "HKD" Then
							dHKD = aData("rate")(i)*0.01
						ElseIf aData("exchange_name")(i) = "SGD" Then
							dSGD = aData("rate")(i)*0.01
						ElseIf aData("exchange_name")(i) = "MYR" Then
							dMYR = aData("rate")(i)*0.01
						End if
					Next
				End if

			

				dim exView : set exView = JSON.parse(Mid(getSiteSourcePost("http://fx.kebhana.com/FER1101M.web",""),27))
				'Response.write(exView & vbNewline) 
				%>

			<!-- banner start -->
				<!-- section start -->
			
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title"><%=EZ_LAN_24%></h1>
					<div class="separator-2"></div>
					<table class="table table-striped table-colored">
						<thead>
							<tr>
								<th><%=EZ_LAN_25%></th>
								<th><%=EZ_LAN_26%></th>
								<th><%=EZ_LAN_27%></th>
								<th><%=EZ_LAN_28%></th>
								
							</tr>
						</thead>
							<tbody>
					<%

					dim key : for each key in exView.keys()

						If key="리스트" Then

					%>
							<tr>
								<td><%=EZ_LAN_30%></td>								
								<td><%=Round(exView.[리스트].[0].[매매기준율]+exView.[리스트].[0].[매매기준율]*CDbl(dUSD),2)%></td>								
								<td><%=Round(exView.[리스트].[0].[현찰파실때]+exView.[리스트].[0].[매매기준율]*CDbl(dUSD),2)%></td>								
								<td><%=Round(exView.[리스트].[0].[현찰사실때]+exView.[리스트].[0].[매매기준율]*CDbl(dUSD),2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_32%></td>								
								<td><%=Round(exView.[리스트].[1].[매매기준율]+exView.[리스트].[1].[매매기준율]*CDbl(dJPY),2)%></td>								
								<td><%=Round(exView.[리스트].[1].[현찰파실때]+exView.[리스트].[1].[매매기준율]*CDbl(dJPY),2)%></td>								
								<td><%=Round(exView.[리스트].[1].[현찰사실때]+exView.[리스트].[1].[매매기준율]*CDbl(dJPY),2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_33%></td>								
								<td><%=Round(exView.[리스트].[3].[매매기준율]+exView.[리스트].[3].[매매기준율]*CDbl(dCNY),2)%></td>								
								<td><%=Round(exView.[리스트].[3].[현찰파실때]+exView.[리스트].[3].[매매기준율]*CDbl(dCNY),2)%></td>								
								<td><%=Round(exView.[리스트].[3].[현찰사실때]+exView.[리스트].[3].[매매기준율]*CDbl(dCNY),2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_36%></td>								
								<td><%=Round(exView.[리스트].[4].[매매기준율]+exView.[리스트].[4].[매매기준율]*CDbl(dHKD),2)%></td>								
								<td><%=Round(exView.[리스트].[4].[현찰파실때]+exView.[리스트].[4].[매매기준율]*CDbl(dHKD),2)%></td>								
								<td><%=Round(exView.[리스트].[4].[현찰사실때]+exView.[리스트].[4].[매매기준율]*CDbl(dHKD),2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_37%></td>								
								<td><%=Round(exView.[리스트].[7].[매매기준율]+exView.[리스트].[7].[매매기준율]*CDbl(dPHP),2)%></td>								
								<td><%=Round(exView.[리스트].[7].[현찰파실때]+exView.[리스트].[7].[매매기준율]*CDbl(dPHP),2)%></td>								
								<td><%=Round(exView.[리스트].[7].[현찰사실때]+exView.[리스트].[7].[매매기준율]*CDbl(dPHP),2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_38%></td>								
								<td><%=Round(exView.[리스트].[8].[매매기준율]+exView.[리스트].[8].[매매기준율]*CDbl(dSGD),2)%></td>								
								<td><%=Round(exView.[리스트].[8].[현찰파실때]+exView.[리스트].[8].[매매기준율]*CDbl(dSGD),2)%></td>								
								<td><%=Round(exView.[리스트].[8].[현찰사실때]+exView.[리스트].[8].[매매기준율]*CDbl(dSGD),2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_39%></td>								
								<td><%=Round(exView.[리스트].[13].[매매기준율]+exView.[리스트].[13].[매매기준율]*CDbl(dMYR),2)%></td>								
								<td><%=Round(exView.[리스트].[13].[현찰파실때]+exView.[리스트].[13].[매매기준율]*CDbl(dMYR),2)%></td>								
								<td><%=Round(exView.[리스트].[13].[현찰사실때]+exView.[리스트].[13].[매매기준율]*CDbl(dMYR),2)%></td>								
							</tr>
							
							<tr>
								<td><%=EZ_LAN_40%></td>								
								<td><%=Round(exView.[리스트].[2].[매매기준율],2)%></td>								
								<td><%=Round(exView.[리스트].[2].[현찰파실때],2)%></td>								
								<td><%=Round(exView.[리스트].[2].[현찰사실때],2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_41%></td>								
								<td><%=Round(exView.[리스트].[11].[매매기준율],2)%></td>								
								<td><%=Round(exView.[리스트].[11].[현찰파실때],2)%></td>								
								<td><%=Round(exView.[리스트].[11].[현찰사실때],2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_42%></td>								
								<td><%=Round(exView.[리스트].[12].[매매기준율],2)%></td>								
								<td><%=Round(exView.[리스트].[12].[현찰파실때],2)%></td>								
								<td><%=Round(exView.[리스트].[12].[현찰사실때],2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_43%></td>								
								<td><%=Round(exView.[리스트].[28].[매매기준율],2)%></td>								
								<td><%=Round(exView.[리스트].[28].[현찰파실때],2)%></td>								
								<td><%=Round(exView.[리스트].[28].[현찰사실때],2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_44%></td>								
								<td><%=Round(exView.[리스트].[9].[매매기준율],2)%></td>								
								<td><%=Round(exView.[리스트].[9].[현찰파실때],2)%></td>								
								<td><%=Round(exView.[리스트].[9].[현찰사실때],2)%></td>								
							</tr>
							
							<tr>
								<td><%=EZ_LAN_45%></td>								
								<td><%=Round(exView.[리스트].[6].[매매기준율],2)%></td>								
								<td><%=Round(exView.[리스트].[6].[현찰파실때],2)%></td>								
								<td><%=Round(exView.[리스트].[6].[현찰사실때],2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_46%></td>								
								<td><%=Round(exView.[리스트].[5].[매매기준율],2)%></td>								
								<td><%=Round(exView.[리스트].[5].[현찰파실때],2)%></td>								
								<td><%=Round(exView.[리스트].[5].[현찰사실때],2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_47%></td>								
								<td><%=Round(exView.[리스트].[10].[매매기준율],2)%></td>								
								<td><%=Round(exView.[리스트].[10].[현찰파실때],2)%></td>								
								<td><%=Round(exView.[리스트].[10].[현찰사실때],2)%></td>								
							</tr>
							<tr>
								<td><%=EZ_LAN_48%></td>								
								<td><%=Round(exView.[리스트].[36].[매매기준율],2)%></td>								
								<td><%=Round(exView.[리스트].[36].[현찰파실때],2)%></td>								
								<td><%=Round(exView.[리스트].[36].[현찰사실때],2)%></td>								
							</tr>
					<%

						End If
						
					next


					%>
						</tbody>
					</table>
				</div>
			</div>
			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
