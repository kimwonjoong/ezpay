<?xml version="1.0" encoding="UTF-8"?><%@CodePage = "65001" %>
<!--#include file="../inc/inc_top_common_utf8.asp" -->
<script runat="server" Language="Javascript" src="../inc/JSON_JS.asp"></script>
<%
session.CodePage = 65001
response.Charset = "UTF-8"
response.ContentType = "text/xml"



Function Get_AbString( strSource, chrSource )

	Dim strDest, strTemp

	strDest = Split( strSource, chrSource )
	strTemp = ""

	For i = 0 To Ubound(strDest) 
	strTemp = strTemp & strDest(i)
	Next

	Get_AbString = strTemp

End Function


Dim sResult : sResult = "N"
Dim nResultCnt : nResultCnt = 0

Dim send_wallet : send_wallet = fnKeywordFilter(request("send_wallet"))
Dim receive_wallet : receive_wallet = fnKeywordFilter(request("receive_wallet"))
Dim gcoin : gcoin = fnKeywordFilter(request("gcoin"))
Dim mode : mode = fnKeywordFilter(request("mode"))

Dim mode_value : mode_value = "EZ"

Dim now_gcoin,franchise_check,change_gcoin,fee,fee_coin
Dim s_count,ssCheck,h_check
s_count=0
fee_coin=0
now_gcoin=0

fnDBConn()

sCheck = "NO"
ssCheck = "NO"

	If Left(send_wallet,1)<>"9" And Left(receive_wallet,1)<>"9" Then

		'지금 핀이 사용가능한 핀인지
		strSQL =  "  select count(send_wallet) as m_cnt from gcoin_transfer where send_wallet='" & send_wallet & "' and left(receive_wallet,1) <> '9'"

		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) then
			s_count = rs("m_cnt")
'			If CInt(s_count) >= 5 Then
'				ssCheck = "NO"
'				sResult = "S"
'			Else
				ssCheck = "YES"
'			End if
		end If
		
		rs.close
	Else

		ssCheck = "YES"

	End if
	'Response.write ssCheck
	If (ssCheck = "YES") then
		'지금 지갑이 사용가능한 지갑인지
		strSQL =  "  select wallet_no,gcoin,USD,JPY,CNY,PHP,HKD,send_fee,h_check from member where wallet_no='" & send_wallet & "'"

		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) Then
			If mode="B" then
				now_gcoin = rs("gcoin")
				fee_coin= CCur(rs("gcoin"))*(rs("send_fee")*0.01)
				mode_value="EZ"
			ElseIf mode="S" Then
				now_gcoin = rs("USD")
				mode_value="USD"
			ElseIf mode="Y" Then
				now_gcoin = rs("JPY")
				mode_value="JPY"
			ElseIf mode="C" Then
				now_gcoin = rs("CNY")
				mode_value="CNY"
			ElseIf mode="P" Then
				now_gcoin = rs("PHP")
				mode_value="PHP"
			ElseIf mode="H" Then
				now_gcoin = rs("HKD")
				mode_value="HKD"
			Else
				now_gcoin = rs("gcoin")
				mode_value="EZ"
			End If

			h_check = rs("h_check")

			fee_coin= CCur(gcoin)*(CCur(rs("send_fee"))*0.01)
			
			If h_check = "정상" Then
			
				If CCur(now_gcoin) >= CCur(gcoin) Then
					'Response.write "True"
					sCheck = "YES"
				Else
					'Response.write "False"
					sCheck = "NO"
					sResult = "0"
					msg = "잔액이 부족합니다. 잔액 확인후 다시 신청하시기 바랍니다."
				End If
			
			Else

				sCheck = "NO"
				sResult = "0"

			End If
			
		end If

		'Response.end
		
		rs.close
		Response.write sCheck
		Response.write sResult
		If sCheck = "YES" then

			strSQL =  "  select wallet_no,gcoin from member where wallet_no='" & receive_wallet & "'"

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				sCheck = "YES"
			Else
				sCheck = "NO"
				sResult = "0"
			end If
			
			rs.close

		End If
		Response.write sCheck
		If(sCheck="YES" And Left(send_wallet,1)<>"9" And Left(receive_wallet,1)<>"9") Then

			strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_settlement where wallet_no='" & send_wallet & "' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				g_sum = rs("g_sum")
				
			end If
			
			rs.close


			strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_settlement2 where wallet_no='" & send_wallet & "' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				g_sum = g_sum + rs("g_sum")
				
			end If
			
			rs.close


			strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_transfer where send_wallet='" & send_wallet & "' and receive_wallet not like '9%' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				g_sum = g_sum + rs("g_sum")
				
			end If
			
			rs.close


			If CCur(g_sum)+CCur(gcoin) <= 3000000 Then
				sCheck = "YES"
			Else
				sCheck = "NO"
				msg = "하루 송금+청산+이체금액 300만원을 넘었습니다. 송금가능금액은 " & 3000000-CCur(g_sum) & "원입니다."
			End if

		End if	
		Response.write sCheck
		If(sCheck="YES") And (Left(send_wallet,1)<>"9" And Left(receive_wallet,1)<>"9") Then	
			If CCur(gcoin) <= 3000000 Then
				sCheck = "YES"
			Else
				sCheck = "NO"
				msg = "하루 송금+청산+이체금액 300만원을 넘었습니다. 송금가능금액은 " & 3000000-CCur(g_sum) & "원입니다."
			end If	
		End if	
		Response.write sCheck
		If sCheck = "YES" then
		
			If Left(receive_wallet,1)="9" Then

				'지금 지갑이 사용가능한 지갑인지
				strSQL =  "  select fee from gcoin_franchise where wallet_no='" & receive_wallet & "'"

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					fee=rs("fee")
					fee_coin=(CCur(gcoin)*(fee*0.01))
					change_gcoin = CCur(gcoin)-CCur(fee_coin)
				Else
					change_gcoin = gcoin
					sCheck = "NO"
				end If
				
				rs.close

			Else
				
				change_gcoin = CCur(gcoin) - CCur(fee_coin)
'				change_gcoin = gcoin

			End If
		
		End If
		Response.write sCheck
		If(sCheck="YES") Then

			sResult = "Y"	

		End If

	End if
	
fnDBClose()

If sResult <> "Y" Then
	
	sResult = msg

End If



%>



<%
response.write "<xml>" & vbcrlf
response.write "<body>" & vbcrlf


response.write "		<item>" & vbcrlf
response.write "			<result>"
response.write "				<![CDATA[" & sResult& "]]>"
response.write "			</result>"& vbcrlf
response.write "		</item>" & vbcrlf
%>					
<%
response.write "</body>" & vbcrlf
response.write "</xml>"
%>