<?xml version="1.0" encoding="UTF-8"?><%@CodePage = "65001" %>
<!--#include file="../inc/inc_top_common_utf8.asp" -->
<%
session.CodePage = 65001
response.Charset = "UTF-8"
response.ContentType = "text/xml"

Dim sResult : sResult = "N"
Dim sPrice_sum,aPrice_sum
Dim nResultCnt : nResultCnt = 0
Dim wallet_no : wallet_no = fnKeywordFilter(request("wallet_no"))
Dim category : category = fnKeywordFilter(request("category"))

fnDBConn()

	If category = "all" Then

		strSQL = " select 'charge' as category,wallet_no,gcoin,result,mode,regist_date from gcoin_buy where wallet_no='" & wallet_no & "' union  select 'send' as type,receive_wallet,gcoin,result,mode,regist_date from gcoin_transfer where send_wallet='" & wallet_no & "' union  select 'receive' as type,send_wallet,gcoin,result,mode,regist_date from gcoin_transfer where receive_wallet='" & wallet_no & "' union  select 'settlement' as type,wallet_no,gcoin,result,mode,regist_date from gcoin_settlement where wallet_no='" & wallet_no & "'  union  select 'transfer' as type,wallet_no,gcoin,result,mode,regist_date from gcoin_settlement2 where wallet_no='" & wallet_no & "' order by regist_date desc"

	ElseIf category = "charge" Then

		strSQL = " select 'charge' as category,wallet_no,gcoin,result,mode,regist_date from gcoin_buy where wallet_no='" & wallet_no & "' order by regist_date desc"

	ElseIf category = "send" Then

		strSQL = " select 'send' as category,receive_wallet,gcoin,result,mode,regist_date from gcoin_transfer where send_wallet='" & wallet_no & "' order by regist_date desc"

	ElseIf category = "receive" Then

		strSQL = " select 'receive' as category,receive_wallet,gcoin,result,mode,regist_date from gcoin_transfer where receive_wallet='" & wallet_no & "' order by regist_date desc"

	ElseIf category = "settlement" Then

		strSQL = " select 'settlement' as category,wallet_no,gcoin,result,mode,regist_date from gcoin_settlement where wallet_no='" & wallet_no & "' order by regist_date desc"

	ElseIf category = "transfer" Then	

		strSQL = " select 'transfer' as category,wallet_no,gcoin,result,mode,regist_date from gcoin_settlement2 where wallet_no='" & wallet_no & "' order by regist_date desc"

	End if
	
	'Response.write strSQL


	cmd.CommandText = strSQL
	Set rs = cmd.execute()
	if not(rs.eof or rs.bof) then
		nData = fnRs2JsonArrR(aData, rs)
	end if
	rs.close
	Set rs = nothing


fnDBClose()

response.write "<xml>" & vbcrlf
response.write "<body>" & vbcrlf

for i=0 to nData
	response.write "		<item>" & vbcrlf
	
	For each sItemKey in aData(i).Collection
		response.write "			<" & sItemKey & ">" 
		response.write "				<![CDATA["& aData(i)(sItemKey) & "]]>"
		response.write "			</" & sItemKey & ">" & vbcrlf
	next

	response.write "		</item>" & vbcrlf
next

response.write "</body>" & vbcrlf
response.write "</xml>"
%>
