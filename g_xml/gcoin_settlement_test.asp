<?xml version="1.0" encoding="UTF-8"?><%@CodePage = "65001" %>
<!--#include file="../inc/inc_top_common_utf8.asp" -->
<%
session.CodePage = 65001
response.Charset = "UTF-8"
response.ContentType = "text/xml"


Dim sResult : sResult = "N"
Dim nResultCnt : nResultCnt = 0
Dim g_sum : g_sum = 0

bank_name = "신한은행"
bank_code = "088"
wallet_no = "883500"
account_no = "11111111111111"
input_name = "테스트"
gcoin = "200000"

Dim bank_code

If bank_name = "우리은행" Or bank_name = "우리" Then
	bank_code = "020"
ElseIf bank_name = "국민은행" Or bank_name = "국민" Then
	bank_code = "004"
ElseIf bank_name = "기업은행" Or bank_name = "기업" Then
	bank_code = "003"
ElseIf bank_name = "농협" Then
	bank_code = "011"
ElseIf bank_name = "SC" Then
	bank_code = "023"
ElseIf bank_name = "우체국" Then
	bank_code = "071"
ElseIf bank_name = "신한은행" Or bank_name = "신한" Then
	bank_code = "088"
ElseIf bank_name = "하나은행" Or bank_name = "하나" Then
	bank_code = "081"
ElseIf bank_name = "KDB산업" Or bank_name = "산업" Then
	bank_code = "002"
ElseIf bank_name = "새마을금고" Then
	bank_code = "045"
ElseIf bank_name = "경남" Or bank_name = "경남은행" Then
	bank_code = "039"
ElseIf bank_name = "부산" Or bank_name = "부산은행" Then
	bank_code = "032"
ElseIf bank_name = "외환" Or bank_name = "외환은행" Then
	bank_code = "005"
ElseIf bank_name = "광주" Or bank_name = "광주은행" Then
	bank_code = "034"
ElseIf bank_name = "씨티" Or bank_name = "씨티은행" Then
	bank_code = "027"
ElseIf bank_name = "대구" Or bank_name = "대구은행" Then
	bank_code = "031"
ElseIf bank_name = "제주" Or bank_name = "제주은행" Then
	bank_code = "035"
ElseIf bank_name = "신협" Then
	bank_code = "048"
ElseIf bank_name = "수협" Then
	bank_code = "007"
ElseIf bank_name = "전북은행" Or bank_name = "전북" Then
	bank_code = "037"
ElseIf bank_name = "산업" Then
	bank_code = "002"	
End if

fnDBConn()

sCheck = "NO"
ssCheck = "YES"

	'지금 핀이 사용가능한 핀인지
	strSQL =  "  select wallet_no,gcoin from member where wallet_no='" & wallet_no & "'"

	cmd.CommandText = strSQL
	Set rs = cmd.execute()

	if not(rs.eof or rs.bof) then
		now_gcoin = rs("gcoin")
		If CCur(now_gcoin) >= CCur(gcoin) Then
			sCheck = "YES"
		Else 
			sCheck = "NO"
			msg = "잔액이 부족합니다. 잔액 확인후 다시 신청하시기 바랍니다."
		End if
	end If
	
	rs.close

	If(sCheck="YES") Then

		strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_settlement where wallet_no='" & wallet_no & "' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) then
			g_sum = CCur(rs("g_sum"))
			
		end If
		
		rs.close


		strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_settlement2 where wallet_no='" & wallet_no & "' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) then
			g_sum = g_sum + CCur(rs("g_sum"))
			
		end If
		
		rs.close


		strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_transfer where send_wallet='" & wallet_no & "' and receive_wallet not like '9%' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) then
			g_sum = g_sum + CCur(rs("g_sum"))
			
		end If
		
		rs.close


		If CCur(g_sum)+CCur(gcoin) <= 3000000 Then
			sCheck = "YES"
		Else
			sCheck = "NO"
			msg = "하루 송금+청산+이체금액 300만원을 넘었습니다. 이체가능금액은 " & 3000000-CCur(g_sum) & "원입니다."
		End if

	End if	

	If(sCheck="YES") Then		
		If CCur(gcoin) <= 3000000 Then
			sCheck = "YES"
		Else
			sCheck = "NO"
			msg = "하루 송금+청산+이체금액 300만원을 넘었습니다. 이체가능금액은 " & 3000000-CCur(g_sum) & "원입니다."
		end If	
	End if	

	If(sCheck="YES") Then
		
		strSQL =  "  select count(*) as cnt from gcoin_buy where wallet_no ='" & wallet_no & "' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, -1, GETDATE()),  23) AS DATETIME) "

		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) then
			b_cnt = rs("cnt")
			If b_cnt = 0 Then
				sCheck = "YES"
			Else
				sCheck = "NO"
				msg = "청산 체크중입니다. 관리자에게 문의바랍니다."
			End if
		end If
		
		rs.close



		If(sCheck="NO") Then

			'지금 핀이 사용가능한 핀인지
			strSQL =  "  select count(*) as cnt from gcoin_transfer where send_wallet ='" & wallet_no & "' or receive_wallet='" & wallet_no & "'"

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				s_cnt = rs("cnt")
				If s_cnt > 0 Then
					sCheck = "YES"
				Else
					sCheck = "NO"
					ssCheck = "NO"
					msg = "청산 체크중입니다. 관리자에게 문의바랍니다."
				End if
			end If
			
			rs.close

		End if

	End if
	
	If(sCheck="YES") Then		
		sResult = "Y"
	End If

fnDBClose()

If sResult <> "Y" Then
	
	sResult = msg

End if

%>
<%
response.write "<xml>" & vbcrlf
response.write "<body>" & vbcrlf


response.write "		<item>" & vbcrlf
response.write "			<result>"
response.write "				<![CDATA[" & sResult& "]]>"
response.write "			</result>"& vbcrlf
response.write "		</item>" & vbcrlf
%>					
<%
response.write "</body>" & vbcrlf
response.write "</xml>"
%>