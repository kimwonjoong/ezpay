<!-- #include virtual="/inc/json2.asp" -->
<!--#include file="../inc/inc_top_common_utf8.asp" -->
<%
session.CodePage = 65001
response.Charset = "UTF-8"
'response.ContentType = "text/xml"
%>
<%
Function getSiteSourcePost( siteURL, params )
 Set httpObj = Server.CreateObject("WinHttp.WinHttpRequest.5.1")
 httpObj.open "POST" , siteURL, False
 httpObj.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded"
 '포스트 방식시 위의 라인을 추가해 주어야 한다.
  
 httpObj.Send params
 '포스트의 파라미터는 Send 호출시 같이 값을 넘겨 주어야 한다.
 httpObj.WaitForResponse
 If httpObj.Status = "200" Then
  getSiteSourcePost = httpObj.ResponseText
 Else
  getSiteSourcePost = null
 End If
End Function
'호출 URL

Dim urlPath : urlPath = "http://fx.keb.co.kr/FER1101M.web"
'페이지로드Post방식

'response.write(Mid(getSiteSourcePost("http://fx.keb.co.kr/FER1101M.web",""),27))
fnDBConn()

	'지금 핀이 사용가능한 핀인지
	strSQL =  "  select exchange_name,rate from gcoin_exchange"

	cmd.CommandText = strSQL
	Set rs = cmd.execute()

	if not(rs.eof or rs.bof) then
		nData = fnRs2JsonArr(aData, rs)
	end If
	
	rs.close

fnDBClose()

if nData >= 0 then
	for i=0 to nData
		If aData("exchange_name")(i) = "USD" Then
			dUSD = aData("rate")(i)*0.01
		ElseIf aData("exchange_name")(i) = "JPY" Then
			dJPY = aData("rate")(i)*0.01
		ElseIf aData("exchange_name")(i) = "CNY" Then
			dCNY = aData("rate")(i)*0.01
		ElseIf aData("exchange_name")(i) = "PHP" Then
			dPHP = aData("rate")(i)*0.01
		ElseIf aData("exchange_name")(i) = "HKD" Then
			dHKD = aData("rate")(i)*0.01
		End if
	Next
End if

'Response.write dUSD&"<br>"
'Response.write dJPY&"<br>"
'Response.write dCNY&"<br>"
'Response.write dPHP&"<br>"
'Response.write dHKD&"<br>"

dim exView : set exView = JSON.parse(Mid(getSiteSourcePost("http://fx.keb.co.kr/FER1101M.web",""),27))
'Response.write(exView & vbNewline) 

dim key : for each key in exView.keys()

	If key="리스트" Then

		response.write "<xml>" & vbcrlf
		response.write "<body>" & vbcrlf


		response.write "		<item>" & vbcrlf
		response.write "			<USD_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[0].[매매기준율]+exView.[리스트].[0].[매매기준율]*CDbl(dUSD),2) & "]]>"
		response.write "			</USD_basic>"& vbcrlf
		response.write "			<USD_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[0].[현찰파실때]+exView.[리스트].[0].[매매기준율]*CDbl(dUSD),2) & "]]>"
		response.write "			</USD_sale>"& vbcrlf
		response.write "			<USD_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[0].[현찰사실때]+exView.[리스트].[0].[매매기준율]*CDbl(dUSD),2) & "]]>"
		response.write "			</USD_buy>"& vbcrlf
		response.write "			<JPY_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[1].[매매기준율]+exView.[리스트].[1].[매매기준율]*CDbl(dJPY),2) & "]]>"
		response.write "			</JPY_basic>"& vbcrlf
		response.write "			<JPY_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[1].[현찰파실때]+exView.[리스트].[1].[매매기준율]*CDbl(dJPY),2) & "]]>"
		response.write "			</JPY_sale>"& vbcrlf
		response.write "			<JPY_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[1].[현찰사실때]+exView.[리스트].[1].[매매기준율]*CDbl(dJPY),2) & "]]>"
		response.write "			</JPY_buy>"& vbcrlf
		response.write "			<CNY_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[3].[매매기준율]+exView.[리스트].[3].[매매기준율]*CDbl(dCNY),2) & "]]>"
		response.write "			</CNY_basic>"& vbcrlf
		response.write "			<CNY_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[3].[현찰파실때]+exView.[리스트].[3].[매매기준율]*CDbl(dCNY),2) & "]]>"
		response.write "			</CNY_sale>"& vbcrlf
		response.write "			<CNY_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[3].[현찰사실때]+exView.[리스트].[3].[매매기준율]*CDbl(dCNY),2) & "]]>"
		response.write "			</CNY_buy>"& vbcrlf
		response.write "			<HKD_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[4].[매매기준율]+exView.[리스트].[4].[매매기준율]*CDbl(dHKD),2) & "]]>"
		response.write "			</HKD_basic>"& vbcrlf
		response.write "			<HKD_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[4].[현찰파실때]+exView.[리스트].[4].[매매기준율]*CDbl(dHKD),2) & "]]>"
		response.write "			</HKD_sale>"& vbcrlf
		response.write "			<HKD_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[4].[현찰사실때]+exView.[리스트].[4].[매매기준율]*CDbl(dHKD),2) & "]]>"
		response.write "			</HKD_buy>"& vbcrlf
		response.write "			<PHP_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[7].[매매기준율]+exView.[리스트].[7].[매매기준율]*CDbl(dPHP),2) & "]]>"
		response.write "			</PHP_basic>"& vbcrlf
		response.write "			<PHP_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[7].[현찰파실때]+exView.[리스트].[7].[매매기준율]*CDbl(dPHP),2) & "]]>"
		response.write "			</PHP_sale>"& vbcrlf
		response.write "			<PHP_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[7].[현찰사실때]+exView.[리스트].[7].[매매기준율]*CDbl(dPHP),2) & "]]>"
		response.write "			</PHP_buy>"& vbcrlf
		response.write "			<EUR_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[2].[매매기준율],2) & "]]>"
		response.write "			</EUR_basic>"& vbcrlf
		response.write "			<EUR_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[2].[현찰파실때],2) & "]]>"
		response.write "			</EUR_sale>"& vbcrlf
		response.write "			<EUR_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[2].[현찰사실때],2) & "]]>"
		response.write "			</EUR_buy>"& vbcrlf
		response.write "			<GBP_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[11].[매매기준율],2) & "]]>"
		response.write "			</GBP_basic>"& vbcrlf
		response.write "			<GBP_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[11].[현찰파실때],2) & "]]>"
		response.write "			</GBP_sale>"& vbcrlf
		response.write "			<GBP_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[11].[현찰사실때],2) & "]]>"
		response.write "			</GBP_buy>"& vbcrlf
		response.write "			<CAD_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[12].[매매기준율],2) & "]]>"
		response.write "			</CAD_basic>"& vbcrlf
		response.write "			<CAD_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[12].[현찰파실때],2) & "]]>"
		response.write "			</CAD_sale>"& vbcrlf
		response.write "			<CAD_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[12].[현찰사실때],2) & "]]>"
		response.write "			</CAD_buy>"& vbcrlf
		response.write "			<CHF_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[28].[매매기준율],2) & "]]>"
		response.write "			</CHF_basic>"& vbcrlf
		response.write "			<CHF_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[28].[현찰파실때],2) & "]]>"
		response.write "			</CHF_sale>"& vbcrlf
		response.write "			<CHF_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[28].[현찰사실때],2) & "]]>"
		response.write "			</CHF_buy>"& vbcrlf
		response.write "			<AUD_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[9].[매매기준율],2) & "]]>"
		response.write "			</AUD_basic>"& vbcrlf
		response.write "			<AUD_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[9].[현찰파실때],2) & "]]>"
		response.write "			</AUD_sale>"& vbcrlf
		response.write "			<AUD_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[9].[현찰사실때],2) & "]]>"
		response.write "			</AUD_buy>"& vbcrlf
		response.write "			<MYR_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[13].[매매기준율],2) & "]]>"
		response.write "			</MYR_basic>"& vbcrlf
		response.write "			<MYR_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[13].[현찰파실때],2) & "]]>"
		response.write "			</MYR_sale>"& vbcrlf
		response.write "			<MYR_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[13].[현찰사실때],2) & "]]>"
		response.write "			</MYR_buy>"& vbcrlf
		response.write "			<TWD_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[6].[매매기준율],2) & "]]>"
		response.write "			</TWD_basic>"& vbcrlf
		response.write "			<TWD_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[6].[현찰파실때],2) & "]]>"
		response.write "			</TWD_sale>"& vbcrlf
		response.write "			<TWD_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[6].[현찰사실때],2) & "]]>"
		response.write "			</TWD_buy>"& vbcrlf
		response.write "			<THB_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[5].[매매기준율],2) & "]]>"
		response.write "			</THB_basic>"& vbcrlf
		response.write "			<THB_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[5].[현찰파실때],2) & "]]>"
		response.write "			</THB_sale>"& vbcrlf
		response.write "			<THB_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[5].[현찰사실때],2) & "]]>"
		response.write "			</THB_buy>"& vbcrlf
		response.write "			<VND_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[10].[매매기준율],2) & "]]>"
		response.write "			</VND_basic>"& vbcrlf
		response.write "			<VND_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[10].[현찰파실때],2) & "]]>"
		response.write "			</VND_sale>"& vbcrlf
		response.write "			<VND_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[10].[현찰사실때],2) & "]]>"
		response.write "			</VND_buy>"& vbcrlf
		response.write "			<IDR_basic>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[36].[매매기준율],2) & "]]>"
		response.write "			</IDR_basic>"& vbcrlf
		response.write "			<IDR_sale>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[36].[현찰파실때],2) & "]]>"
		response.write "			</IDR_sale>"& vbcrlf
		response.write "			<IDR_buy>"
		response.write "				<![CDATA[" & Round(exView.[리스트].[36].[현찰사실때],2) & "]]>"
		response.write "			</IDR_buy>"& vbcrlf
		response.write "		</item>" & vbcrlf

		response.write "</body>" & vbcrlf
		response.write "</xml>"
		

	End If
	
next


%>



