<?xml version="1.0" encoding="UTF-8"?><%@CodePage = "65001" %>
<!--#include file="../inc/inc_top_common_utf8.asp" -->
<script runat="server" Language="Javascript" src="../inc/JSON_JS.asp"></script>
<%
session.CodePage = 65001
response.Charset = "UTF-8"
response.ContentType = "text/xml"



Function Get_AbString( strSource, chrSource )

	Dim strDest, strTemp

	strDest = Split( strSource, chrSource )
	strTemp = ""

	For i = 0 To Ubound(strDest) 
	strTemp = strTemp & strDest(i)
	Next

	Get_AbString = strTemp

End Function


Dim sResult : sResult = "N"
Dim nResultCnt : nResultCnt = 0

Dim wallet_no : wallet_no = fnKeywordFilter(request("wallet_no"))
Dim gcoin : gcoin = fnKeywordFilter(request("gcoin"))
Dim change_coin : change_coin = fnKeywordFilter(request("change_coin"))
Dim s_mode : s_mode = fnKeywordFilter(request("s_mode"))
Dim r_mode : r_mode = fnKeywordFilter(request("r_mode"))

Dim now_gcoin,franchise_check,change_gcoin,fee,fee_coin
Dim s_count,ssCheck
s_count=0
fee_coin=0
now_gcoin=0

fnDBConn()

sCheck = "NO"

		strSQL =  "  select wallet_no,gcoin,USD,JPY,CNY,PHP,HKD from member where wallet_no='" & wallet_no & "'"

		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) Then

			If s_mode="B" then
				now_gcoin = rs("gcoin")
			ElseIf s_mode="S" Then
				now_gcoin = rs("USD")
			ElseIf s_mode="Y" Then
				now_gcoin = rs("JPY")
			ElseIf s_mode="C" Then
				now_gcoin = rs("CNY")
			ElseIf s_mode="P" Then
				now_gcoin = rs("PHP")
			ElseIf s_mode="H" Then
				now_gcoin = rs("HKD")
			Else
				now_gcoin = rs("gcoin")
			End If
			
		
			If CCur(now_gcoin) >= CCur(gcoin) Then
			    'Response.write "True"
				sCheck = "YES"
			Else
			    'Response.write "False"
				sCheck = "NO"
				sResult = "0"
			End if
			
		
		end If
		
		rs.close
		
		If(sCheck="YES") Then

			strSQL=""
		
			dbConn.BeginTrans

			strSQL = strSQL & "	insert into gcoin_transfer_log(wallet_no,gcoin,change_coin,mode,s_mode) values"
			strSQL = strSQL & "	( '" & wallet_no &"','" & gcoin &"','" & change_coin &"','" & r_mode &"','" & s_mode &"')"


			If s_mode = "B" Then
			
				If r_mode = "B" then
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
				ElseIf r_mode = "S" Then
					strSQL = strSQL & "	update  member set USD=convert(money,USD)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "Y" Then
					strSQL = strSQL & "	update  member set JPY=convert(money,JPY)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "P" Then
					strSQL = strSQL & "	update  member set PHP=convert(money,PHP)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "C" Then
					strSQL = strSQL & "	update  member set CNY=convert(money,CNY)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "H" Then
					strSQL = strSQL & "	update  member set HKD=convert(money,HKD)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				End If

			ElseIf s_mode = "S" Then

				If r_mode = "B" Then
					strSQL = strSQL & "	update  member set USD=convert(money,USD)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
				
			ElseIf s_mode = "Y" Then

				If r_mode = "B" Then
					strSQL = strSQL & "	update  member set JPY=convert(money,JPY)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
			
			ElseIf s_mode = "P" Then

				If r_mode = "B" Then
					strSQL = strSQL & "	update  member set PHP=convert(money,PHP)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
				
			ElseIf s_mode = "C" Then

				If r_mode = "B" Then
					strSQL = strSQL & "	update  member set CNY=convert(money,CNY)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
				
			ElseIf s_mode = "H" Then

				If r_mode = "B" Then
					strSQL = strSQL & "	update  member set HKD=convert(money,HKD)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
				
			End if
			
			'Response.write strSQL
			If sResult <> "0" Then

				cmd.CommandText = strSQL
				cmd.Execute nResultCnt, , adExecuteNoRecords

				if nResultCnt <> 1 or Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
					dbConn.RollBackTrans
					fnErrSave()
				else
					dbConn.CommitTrans
					sResult = "Y"
				'	fnLogSave()
				end If

			End if

		End If


	
%>


<%
response.write "<xml>" & vbcrlf
response.write "<body>" & vbcrlf


response.write "		<item>" & vbcrlf
response.write "			<result>"
response.write "				<![CDATA[" & sResult& "]]>"
response.write "			</result>"& vbcrlf
response.write "		</item>" & vbcrlf
%>					
<%
response.write "</body>" & vbcrlf
response.write "</xml>"
%>