<?xml version="1.0" encoding="UTF-8"?><%@CodePage = "65001" %>
<!--#include file="../inc/inc_top_common_utf8.asp" -->
<%
session.CodePage = 65001
response.Charset = "UTF-8"
response.ContentType = "text/xml"


Dim sResult : sResult = "N"
Dim nResultCnt : nResultCnt = 0

Dim wallet_no : wallet_no = fnKeywordFilter(request("wallet_no"))
Dim gcoin : gcoin = fnKeywordFilter(request("gcoin"))
Dim bank_name : bank_name = fnKeywordFilter(request("bank_name"))
Dim account_no : account_no = fnKeywordFilter(request("account_no"))
Dim input_name : input_name = fnKeywordFilter(request("input_name"))

Dim bank_code

If bank_name = "우리은행" Then
	bank_code = "020"
ElseIf bank_name = "국민은행" Then
	bank_code = "004"
ElseIf bank_name = "기업은행" Then
	bank_code = "003"
ElseIf bank_name = "농협" Then
	bank_code = "011"
ElseIf bank_name = "SC" Then
	bank_code = "023"
ElseIf bank_name = "우체국" Then
	bank_code = "071"
ElseIf bank_name = "신한은행" Then
	bank_code = "088"
ElseIf bank_name = "하나은행" Then
	bank_code = "081"
ElseIf bank_name = "KDB산업" Then
	bank_code = "002"
ElseIf bank_name = "새마을금고" Then
	bank_code = "045"
ElseIf bank_name = "경남" Then
	bank_code = "039"
ElseIf bank_name = "부산" Then
	bank_code = "032"
ElseIf bank_name = "외환" Then
	bank_code = "005"
ElseIf bank_name = "광주" Then
	bank_code = "034"
ElseIf bank_name = "씨티" Then
	bank_code = "027"
ElseIf bank_name = "대구" Then
	bank_code = "031"
ElseIf bank_name = "제주" Then
	bank_code = "035"
ElseIf bank_name = "신협" Then
	bank_code = "048"
ElseIf bank_name = "수협" Then
	bank_code = "007"
ElseIf bank_name = "전북은행" Then
	bank_code = "037"
ElseIf bank_name = "산업" Then
	bank_code = "002"	
End if

fnDBConn()

sCheck = "NO"


	'지금 핀이 사용가능한 핀인지
	strSQL =  "  select wallet_no,gcoin from member where wallet_no='" & wallet_no & "'"

	cmd.CommandText = strSQL
	Set rs = cmd.execute()

	if not(rs.eof or rs.bof) then
		now_gcoin = rs("gcoin")
		If CCur(now_gcoin) >= CCur(gcoin) Then
			sCheck = "YES"
		End if
	end If
	
	rs.close
	
	If(sCheck="YES") Then

		strSQL=""
	
		dbConn.BeginTrans

		strSQL = strSQL & "	insert into gcoin_settlement(wallet_no,gcoin,bank_name,bank_code,account_no,input_name) values"
		strSQL = strSQL & "	( '" & wallet_no &"','" & gcoin &"','" & bank_name &"','" & bank_code &"','" & account_no &"','" & input_name &"')"

		strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	

		Dim now_date,now_time
		now_date = Replace(date, "-", "") 
		now_time = Year(Now)&Month(Now)&Day(Now)&Hour(Now)&Minute(Now)&Second(Now)

		strSQL = strSQL & "	insert into TB_BANK_TRAN(TR_DATE,SEQ,BANK_CD,ORG_CD,OUT_BANK_CD,OUT_ACCT_NO,IN_BANK_CD,IN_ACCT_NO,TR_AMT,OUT_REMARK,IN_REMARK) values"
		strSQL = strSQL & "	( '" & now_date &"',dbo.rtn_seq(),'004','20034231','004','53200101278247','"& bank_code &"','"& account_no &"',"& gcoin &",'"& input_name &"','EZ pay')"

		
		cmd.CommandText = strSQL
		cmd.Execute nResultCnt, , adExecuteNoRecords

		if nResultCnt <> 1 or Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
			dbConn.RollBackTrans
			fnErrSave()
		else
			dbConn.CommitTrans
			sResult = "Y"
		'	fnLogSave()
		end If

	End if
	


fnDBClose()


%>
<%
response.write "<xml>" & vbcrlf
response.write "<body>" & vbcrlf


response.write "		<item>" & vbcrlf
response.write "			<result>"
response.write "				<![CDATA[" & sResult& "]]>"
response.write "			</result>"& vbcrlf
response.write "		</item>" & vbcrlf
%>					
<%
response.write "</body>" & vbcrlf
response.write "</xml>"
%>