<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->
<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 
	Dim SHOPPING_POINT
fnDBConn()
	strSQL = " SELECT WALLET_NO, SHOPPING_POINT FROM MEMBER WHERE WALLET_NO = '"& Session("SS_USERID") &"' "
	cmd.CommandText = strSQL
	Set rs = cmd.execute()
SHOPPING_POINT = RS("SHOPPING_POINT")
	rs.close
	
fnDBClose()
%>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY | <%=EZ_LAN_156%></title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<script type="text/javascript">
		<!--
		//[] <--문자 범위 [^] <--부정 [0-9] <-- 숫자  
		//[0-9] => \d , [^0-9] => \D
		var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
		var rgx2 = /(\d+)(\d{3})/; 

		function getNumber(obj) {
			 var num01;
			 var num02;
			 num01 = obj.value;
			 num02 = num01.replace(rgx1,"");
			 num01 = setComma(num02);
			 obj.value =  num01;
		}

		function setComma(inNum) {
			 var outNum;
			 outNum = inNum; 
			 while (rgx2.test(outNum)) {
				  outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
			  }
			 return outNum;
		}
		//-->
		</script>

		<script type="text/javascript">
			function send_ok() {			
				var use_point = $("#use_point").val();
				//var receive_wallet = $("#receive_wallet").val();
				var send_wallet = $("#send_wallet").val();

				//alert(mode);
				/*
				if (receive_wallet.length == 0){

					modal({
						type: 'info',
						title: '쇼핑포인트 송금하기',
						text: '받는지갑번호를 입력해주세요.',
						callback: function(result) {
							$("#receive_wallet").focus();
						}

					});
					
				}else 
				*/
				if (use_point.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_156%>',
						text: '<%=EZ_LAN_157%>',
						callback: function(result) {
							$("#use_point").focus();
						}
					});
					
				}else if (use_point < 10000){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_158%>',
						text: '<%=EZ_LAN_159%>',
						callback: function(result) {
							$("#use_point").focus();
						}
					});
					
				}else{
					$.ajax({
						 url: "shopping_ok.asp",
						 type: "POST",
						 data: {use_point: use_point,send_wallet: send_wallet },  

						 success: function(data){
							 if(data == "YES"){						 
								modal({
									type: 'success',
									title: '<%=EZ_LAN_156%>',
									text: '<%=EZ_LAN_160%>',
									callback: function(result) {
										location.href="shopping_send.asp";
									}
								});		
							 }else if(data == "ERROR"){							 
								modal({
									type: 'error',
									title: '<%=EZ_LAN_158%>',
									text: '<%=EZ_LAN_9%>',
									callback: function(result) {
										location.href="shopping_send.asp";
									}
								});				
							 }else{								 
								modal({
									type: 'error',
									title: '<%=EZ_LAN_161%>',
									text: '<%=EZ_LAN_162%>',
									callback: function(result) {
										location.href="shopping_send.asp";
									}
								});				
							 }
						 },
			 
						 complete: function(data){
						//	location.href="index.asp";
						 }
					});
				}	
			}

			function insert_money(money) {
				$("#use_point").val(money);
			}
			
		</script>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active"><%=EZ_LAN_156%></li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- banner start -->
				<!-- section start -->
			
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title"><%=EZ_LAN_156%></h1>
					<div class="separator-2"></div>
					<form role="form">
					<input type="hidden" name="send_wallet" id="send_wallet" value="<%=Session("SS_USERID")%>">
						<%
						If SHOPPING_POINT >= "1000" Then
							SHOPPING_POINT_P = FormatNumber(SHOPPING_POINT,0)
						Else
							SHOPPING_POINT_P = SHOPPING_POINT
						End If
						%>
						<div class="form-group">
							<label for="exampleInputPassword1">MY POINT</label>
							<input type="text" class="form-control" name="my_point" id="my_point" VALUE="<%=SHOPPING_POINT_P%>" READONLY>
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_163%></label>
							<input type="money" class="form-control" id="use_point" name="use_point" required>
						</div>
						
						<div class="alert alert-info alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<b>SHOPPING POINT -> <%=EZ_LAN_164%></b>
						</div>
					
						<button type="button" class="btn btn-default"  onclick="send_ok();"><%=EZ_LAN_165%></button>
					</form>
				</div>
			</div>
			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
