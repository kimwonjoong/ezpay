<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Dim sKColumn, sKeyword, sKPushType, sKComm, sKHosu
Dim aData1, nData1
Dim aData2, nData2
Dim aData3, nData3
Dim aDong, nDong : nDong = -1
Dim aHosu, nHosu : nHosu = -1
Dim aComm, nComm : nComm = -1

MENU_IDX_ = 7
sMenuCode = "admin"
PAGE_CODE_ = "push"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sKPushType = fnKeywordFilter(Request("optPushType"))
sKComm = fnKeywordFilter(Request("optKComm"))
sKHosu = fnKeywordFilter(Request("optKHosu"))
sQueryString = "optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&optPushType="&sKPushType
sFilter = ""

fnDBConn()

if SS_GROUP = "A" then
	'sFilter = sFilter & " and comm_id=? "
	'cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, sKComm)
elseif SS_GROUP = "S" then
	sFilter = sFilter & " and m_userid=? "
	cmd.Parameters.Append cmd.CreateParameter("@m_userid", adVarChar, adParamInput, 50, SS_USER_ID)
end if

if sKeyword <> "" then
	if sKColumn = "contents" then
		sFilter = sFilter & " and contents like ? "
		cmd.Parameters.Append cmd.CreateParameter("@contents", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "location" then
		sFilter = sFilter & " and location like ? "
		cmd.Parameters.Append cmd.CreateParameter("@location", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	end if
end if

sqlTable = "select * from member_msg_data where msg_type='p' " & sFilter

'게시물 개수
sql = " select count(*) cnt from (" & sqlTable & ")a "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nRecordCount = trim(rs("cnt"))
end if
rs.close

'게시물 리스트	
sqlTable = " SELECT ROW_NUMBER() OVER(ORDER BY idx desc) AS RowNum, * FROM (" & sqlTable & ")a "	
sql = " ;with tbl as(" &  sqlTable & ") "
sql = sql & " SELECT TOP " & nPageSize & " idx, msg_cnt, msg_send_cnt, msg_type, send_chk, contents, m_userid, insert_date FROM tbl"
sql = sql & " WHERE RowNum >= (SELECT MAX(RowNum)RowNum FROM (SELECT TOP " & (nPageNO-1)*nPageSize+1 & " RowNum FROM tbl ORDER BY RowNum ASC) a) ORDER BY RowNum ASC "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	aData1 = rs.getRows()
end if
rs.close
Set rs = nothing

fnDBClose()

if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 

nData1 = -1

if isArray(aData1) then
	nData1 = ubound(aData1, 2)
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_ajax_cond.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
// 전체선택/해제
function fnSelectAll(obj)
{
	var bFlag = obj.checked;
	var objs = document.getElementsByName("chkSelect");
	for(var i=0; i < (objs.length==null?1:objs.length); i++)
	{
		objs[i].checked = bFlag;
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<div class="lyMainTitle"><img src="/g_admin/images/common/subtitle02.png" height="39" alt="알림(푸쉬)"></div>
<div class="lyTab">
	<!--span class="button tab"><a href="push_list.asp">알림메세지 전송</a></span-->
	<span class="button tab"><a href="push_list.asp?pty=p">PUSH메세지 전송</a></span>
	<span class="button tab_over"><a href="push_msg_list.asp">메세지 전송현황 보기</a></span>
</div>
<div class="lyCSch">
	<form name="frmSch" id="frmSch" action="push_msg_list.asp" method="post" onsubmit="return fnSchChk(this);">
	<select name="optColumn" id="optColumn">
		<option value="contents" <%if sKColumn="contents" then%>selected<%end if%>>메세지</option>
	</select>
	<input type="text" class="txtKey" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<input type="image" src="/g_admin/images/common/btn_sch.jpg" width="62" height="23" align="absmiddle" title="검색하기" style="width:62px;height:23px">
	</form>
</div>
<%=fnRecordCnt(nRecordCount, nPageNO, nPageCount)%>
<div class="lyMainTbl">
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="27%">메세지</th>
		<th width="10%">요청건수</th>
		<th width="10%">전송건수</th>
		<th width="13%">상태</th>
		<th width="13%">발송자</th>
		<th width="19%">등록일</th>
	</tr>
	<%
	if nData1 >= 0 then
		for i=0 to nData1
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td class="al"><a href="push_msg_view.asp?idx=<%=trim(aData1(0,i))%>&<%=sQueryString%>"><%=fnStrCut(trim(aData1(5,i)), 30, "...")%></a></td>
		<td><%=trim(aData1(1,i))%></td>
		<td><%=trim(aData1(2,i))%></td>
		<td><%=fnSendStatus(aData1(4,i))%></td>
		<td><%=trim(aData1(6,i))%></td>
		<td><%=trim(aData1(7,i))%></td>
	</tr>
	<%
		next
	%>

	<%else%>
	<tr class="msg145">
		<td colspan="8">알림메세지 전송내역이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>
<%=fnPaging("push_msg_list.asp", nPageNO, nBlockSize, nPageCount, sQueryString)%>
<br/>
<br/>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>