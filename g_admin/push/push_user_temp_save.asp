<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")
on error resume next

Dim sSessionID
Dim sSelectMode, nSelectCnt, sKPushChk, sKColumn, sKeyword, sKComm, sKHosu
Dim sPushKey, sType
Dim comm_id : comm_id = "CBCOIN"
sKComm = "CBCOIN"

MENU_IDX_ = 7
sMenuCode = "admin"
PAGE_CODE_ = "push"
sListFileName = PAGE_CODE_ & "_list.asp"

sSessionID = fnKeywordFilter(Request.form("hdSessionID"))
sSelectMode = fnKeywordFilter(Request.form("hdSelectMode"))
nSelectCnt = fnKeywordFilter(Request.form("hdSelectCnt"))
sKPushChk = fnKeywordFilter(Request.form("hdPushChk"))
sKColumn = fnKeywordFilter(Request.form("hdColumn"))
sKeyword = fnKeywordFilter(Request.form("hdKeyword"))
sPushKey = fnKeywordFilter(Request.form("hdPushKey"))
sType = "p"

if sSessionID <> SESSION_ID_ then
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('세션정보가 일치하지 않습니다. 다시 시도해주세요.');"
	Response.Write "parent.parent.location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if


'실제 메세지 전송시 푸쉬설정이 on인 것만 처리함
'sKPushChk = "on"

if sSelectMode = "all" then
	fnDBConn()
	dbConn.BeginTrans
	
	'현재 세션으로 등록된 임시 데이터 삭제
	sql = " delete member_msg_list_tmp where session_id=? "
	cmd.Parameters.Append cmd.CreateParameter("@session_id", adVarChar, adParamInput, 255, sSessionID)
	cmd.CommandText = sql
	cmd.Execute , , adExecuteNoRecords
	ParamInit()
	
	if sKeyword <> "" then
		if sKColumn = "user_phone" then
			sFilter = sFilter & " and handphone like ? "
			cmd.Parameters.Append cmd.CreateParameter("@handphonoe", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
		end if
	end if

	sqlTable = " select * from member a where isnull(a.reg_id,'') <> '' and isnull(a.reg_id,'') <> 'null' " & sFilter

	'카운터 체크
	sql = " select count(*) cnt from (" & sqlTable & ")a "
	cmd.CommandText = sql
	Set rs = cmd.execute(sql)
	if not(rs.eof or rs.bof) then
		nRecordCount = trim(rs("cnt"))
	end if
	rs.close

	if isNumeric(nRecordCount) then
		nRecordCount = CLng(nRecordCount)
	else
		nRecordCount = 0
	end if

	if nRecordCount >= 1 then
		sql = " insert into member_msg_list_tmp (idx, comm_id, session_id, m_userid, handphone, insert_date, push_key, msg_type) "
		sql = sql & " select ROW_NUMBER()over(order by idx)idx, '" & sKComm &"' comm_id, '" &SESSION_ID_ &"' session_id, '" &SS_USER_ID &"' m_userid "
		sql = sql & " , handphone user_phone, getdate(), '" & sPushKey & "' push_key, '" & sType & "' msg_type from  (" & sqlTable & ")a "
		cmd.CommandText = sql
		cmd.Execute , , adExecuteNoRecords
	end if
		
	if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
		dbConn.RollBackTrans
		fnErrSave()
		
		Response.Write "<script type='text/javascript'>"
		Response.Write "alert('오류가 발생하여 설정에 실패하였습니다.');"
		Response.Write "location.href='/g_admin/temp.asp';"
		'Response.Write "history.back();"
		Response.Write "</script>"
	else			
		dbConn.CommitTrans
		if nRecordCount >= 1 then
			Response.Write "<script type='text/javascript'>"
			'Response.Write "alert('전송대상이 설정되었습니다.');"
			Response.Write "parent.parent.ifrPushMsg.document.frmPushSend.hdPushKey.value='" & sPushKey & "';"
			Response.Write "parent.parent.ifrPushMsg.document.frmPushSend.hdKComm.value='" & sKComm & "';"
			Response.Write "parent.parent.ifrPushMsg.document.frmPushSend.txtUser.value='[검색결과전체 설정됨]\n- 선택한 휴대폰번호 : " & nSelectCnt & "건\n- 전송가능한 휴대폰번호 : " & nRecordCount & "건';"
			Response.Write "parent.parent.ifrPushMsg.document.frmPushSend.txtMsgCnt.value='" & nRecordCount & "';"
			Response.Write "location.href='/g_admin/temp.asp';"
			Response.Write "</script>"
		else
			Response.Write "<script type='text/javascript'>"
			Response.Write "alert('PUSH전송가능한 휴대폰이 없습니다.');"
			'Response.Write "location.href='"&sListFileName&"';"
			Response.Write "location.href='/g_admin/temp.asp';"
			Response.Write "</script>"
		end if
	end if

	fnDBClose()
elseif sSelectMode = "select" then
	fnDBConn()
	dbConn.BeginTrans
	
	'현재 세션으로 등록된 임시 데이터 삭제
	sql = " delete member_msg_list_tmp where session_id=? "
	cmd.Parameters.Append cmd.CreateParameter("@session_id", adVarChar, adParamInput, 255, sSessionID)
	cmd.CommandText = sql
	cmd.Execute , , adExecuteNoRecords
	ParamInit()
	
	Dim sPhoneList : sPhoneList = ""
	Dim nPhoneCnt : nPhoneCnt = 0
	Dim sSecPhoneList : sSecPhoneList = ""
	nPhoneCnt = request.form("chkSelect").count
	
	sql = ""
	for i=1 to nPhoneCnt
		if sPhoneList <> "" then
			sPhoneList = sPhoneList & ","
			sSecPhoneList = sSecPhoneList & ","
		end if
		sPhoneList = sPhoneList & trim(request.form("chkSelect")(i))

		sSecPhoneList = sSecPhoneList & fnRDataSec(trim(request.form("chkSelect")(i)),4)
		sql = sql & " insert into member_msg_list_tmp (idx, comm_id, session_id, m_userid, handphone, insert_date, push_key, msg_type) "
		sql = sql & " values(" & i &", ?, '" &SESSION_ID_ &"', '"&SS_USER_ID&"', ?, getdate(), '"&sPushKey&"','" & sType & "')  "
		cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, sKComm)
		cmd.Parameters.Append cmd.CreateParameter("@handphone", adVarChar, adParamInput, 50, trim(request.form("chkSelect")(i)))
	next	
	cmd.CommandText = sql
	cmd.Execute , , adExecuteNoRecords
		
	if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
		dbConn.RollBackTrans
		fnErrSave()
		
		Response.Write "<script type='text/javascript'>"
		Response.Write "alert('오류가 발생하여 설정에 실패하였습니다.');"
		'Response.Write "location.href='/admin/temp.asp';"
		'Response.Write "history.back();"
		Response.Write "</script>"
	else			
		dbConn.CommitTrans
		Response.Write "<script type='text/javascript'>"
		Response.Write "parent.parent.ifrPushMsg.document.frmPushSend.hdPushKey.value='" & sPushKey & "';"
		Response.Write "parent.parent.ifrPushMsg.document.frmPushSend.hdKComm.value='" & sKComm & "';"
		if SS_GROUP="A" then
			Response.Write "parent.parent.ifrPushMsg.document.frmPushSend.txtUser.value='" & sPhoneList & "';"
		else
			Response.Write "parent.parent.ifrPushMsg.document.frmPushSend.txtUser.value='" & sSecPhoneList & "';"
		end if
		Response.Write "parent.parent.ifrPushMsg.document.frmPushSend.txtMsgCnt.value='" & nPhoneCnt & "';"
		'Response.Write "alert('전송대상이 설정되었습니다.');"
		Response.Write "location.href='/g_admin/temp.asp';"
		Response.Write "</script>"
	end if

	fnDBClose()

else
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('전송대상 선택정보가 없습니다.');"
	Response.Write "parent.parent.location.href='"&sListFileName&"';"
	Response.Write "</script>"
end if
%>