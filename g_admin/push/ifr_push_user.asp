<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Dim sPushKey
Dim sKPushChk, sKColumn, sKeyword, sKComm, sKHosu
Dim aData1, nData1
Dim sType : sType = "p"
Dim aComm, nComm : nComm = -1
Dim aHosu, nHosu : nHosu = -1

sMenuCode = "admin"
PAGE_CODE_ = "push"
nPageSize = 15
sListFileName = PAGE_CODE_ & "_list.asp"

nPageNO = fnPagingInit(Request("pageNO"))
sKPushChk = fnKeyworeFilter(Request("optPushChk"))
sKColumn = fnKeyworeFilter(Request("optColumn"))
sKeyword = fnKeyworeFilter(Request("txtKeyword"))
sPushKey = fnKeyworeFilter(Request("pushKey"))
sKComm = fnKeywordFilter(Request("optKComm"))

sKComm = "CBCOIN"

sQueryString = "optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&pushKey="&sPushKey
sFilter = ""

fnDBConn()

if sKeyword <> "" then
	if sKColumn = "user_phone" then
		sFilter = sFilter & " and handphone like ? "
		cmd.Parameters.Append cmd.CreateParameter("@handphonoe", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	end if
end if

sqlTable = " select * from member a where isnull(a.reg_id,'') <> '' and isnull(a.reg_id,'') <> 'null'  " & sFilter

'게시물 개수
sql = " select count(*) cnt from (" & sqlTable & ")a "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nRecordCount = trim(rs("cnt"))
end if
rs.close

'게시물 리스트	
sqlTable = " SELECT ROW_NUMBER() OVER(ORDER BY idx desc) AS RowNum, * FROM (" & sqlTable & ")a "	
sql = " ;with tbl as(" &  sqlTable & ") "
sql = sql & " SELECT TOP " & nPageSize & " * FROM tbl"
sql = sql & " WHERE RowNum >= (SELECT MAX(RowNum)RowNum FROM (SELECT TOP " & (nPageNO-1)*nPageSize+1 & " RowNum FROM tbl ORDER BY RowNum ASC) a) ORDER BY RowNum ASC "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing

fnDBClose()

if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_ajax_cond.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
// 전체선택/해제
function fnSelectAll(obj)
{
	var bFlag = obj.checked;
	var objs = document.getElementsByName("chkSelect");
	for(var i=0; i < (objs.length==null?1:objs.length); i++)
	{
		objs[i].checked = bFlag;
	}
}
function fnMsgUserSelect(sArgSelectMode)
{
	var sMsg = "";
	var nSelect = 0;
	if(sArgSelectMode == "all")
	{
		sMsg = "조회결과 전체(<%=nRecordCount%>건)를 전송대상으로 선택하시겠습니까?";
		nSelect = <%=nRecordCount%>;
	}
	else
	{
		//선택한 쿠폰 체크
		var objs = document.getElementsByName("chkSelect");
		
		for(var i=0; i < (objs.length==null?1:objs.length); i++)
		{
			if(objs[i].checked) nSelect ++;
		}

		if(nSelect == 0)
		{
			alert("전송대상을 선택해주세요.");
			return false;
		}
		else
		{
			sMsg = "체크한 대상("+nSelect + "건)을 전송대상으로 선택하시겠습니까?";
		}
	}

	if(confirm(sMsg))
	{
		var frm = document.getElementById("frmMsgUserSelect");
		frm.hdSelectMode.value = sArgSelectMode;
		frm.hdSelectCnt.value = nSelect;
		frm.submit();
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<div class="lyCBtn">
	<form name="frmSch" id="frmSch" action="ifr_push_user.asp" method="post" onsubmit="return fnSchChk(this);">
	<input type="hidden" name="pushKey" id="pushKey" value="<%=sPushKey%>">
	<input type="hidden" name="pty" id="pty" value="<%=sType%>">
	<select name="optColumn" id="optColumn">
		<option value="user_phone" <%if sKColumn="user_phone" then%>selected<%end if%>>휴대폰번호</option>
	</select>
	<input type="text" class="txtKey" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<input type="image" src="/g_admin/images/common/btn_sch.jpg" width="62" height="23" align="absmiddle" title="검색하기" style="width:62px;height:23px">
	</form>
</div>
<%=fnRecordCnt(nRecordCount, nPageNO, nPageCount)%>
<div class="lyMainTbl">
<form name="frmMsgUserSelect" id="frmMsgUserSelect" method="post" action="push_user_temp_save.asp" target="ifrHidden">
<input type="hidden" name="hdSelectMode" id="hdSelectMode" value="">
<input type="hidden" name="hdSelectCnt" id="hdSelectCnt" value="">
<input type="hidden" name="hdColumn" id="hdColumn" value="<%=sKColumn%>">
<input type="hidden" name="hdKeyword" id="hdKeyword" value="<%=sKeyword%>">
<input type="hidden" name="hdPushChk" id="hdPushChk" value="<%=sKPushChk%>">
<input type="hidden" name="hdSessionID" id="hdSessionID" value="<%=SESSION_ID_%>">
<input type="hidden" name="hdPushKey" id="hdPushKey" value="<%=sPushKey%>">
<input type="hidden" name="hdKComm" id="hdKComm" value="<%=sKComm%>">
<input type="hidden" name="pty" id="pty" value="<%=sType%>">
<table class="tblMain" style="width:98%">
	<tr>
		<th width="15%">번호</th>
		<th width="30%">휴대폰번호</th>
		<th width="35%">등록일자</th>
		<th width="15%"><input type="checkbox" name="chkSelectAll" id="chkSelectAll" value="all" onclick="fnSelectAll(this);"></th>
	</tr>
	<%
	if nData >= 0 then
		for i=0 to nData
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td><%=aData("handphone")(i)%></td>
		<td class="al"><%=fnFormatDateTime(aData("reg_date")(i))%></td>
		<td><input type="checkbox" name="chkSelect" id="chkSelect<%=i%>" value="<%=aData("handphone")(i)%>"></td>
	</tr>
	<%
		next
	%>

	<%else%>
	<tr class="msg145">
		<td colspan="5">조회된 데이터가 없습니다.</td>
	</tr>
	<%end if%>
</table>
<%if SS_GROUP="A" then%>
<!--div class="lyLMsg">* PUSH메세지는 <span class="spBlue">PUSH설정</span>이 <span class="spRed">[on]</span>인 휴대폰으로만 전송가능합니다.</div-->
<%end if%>
</div>
<%if nRecordCount > 0 then%>
<div class="lyRBtn">
	<span class="button black"><input type="button" value="검색결과(<%=nRecordCount%>건) 전체선택" onclick="javascript:fnMsgUserSelect('all');return false;"/></span>
	<span class="button black"><input type="button" value="체크항목 선택" onclick="fnMsgUserSelect('select');return false;"/></span>
</div>
<%end if%>
<%=fnPaging("ifr_push_user.asp", nPageNO, nBlockSize, nPageCount, sQueryString)%>
</form>
<iframe width="900" height="400" frameborder="0" name="ifrHidden" id="ifrHidden" scrolling="no"></iframe>
</body>
</html>