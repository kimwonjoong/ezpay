<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Dim sKColumn, sKeyword, nIdx,sWallet
Dim fromDate,toDate,difDate
Dim aCharge, nCharge: nCharge = -1
Dim aSend, nSend: nSend = -1
Dim aReceive, nReceive: nReceive = -1
Dim aSettlement, nSettlement: nSettlement = -1
Dim aSettlement2, nSettlement2: nSettlement2 = -1

MENU_IDX_ = 8
PAGE_CODE_ = "settlement"
sListFileName = PAGE_CODE_ & "_list.asp"
nIdx = fnIdxInit(request("idx"))
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sWallet = Request("wallet")
sQueryString = "pageNO="&nPageNO&"&optColumn="&sKColumn&"&txtKeyword="&sKeyword

sFilter = ""

if sWallet = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('회원 정보가 없습니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if

fnDBConn()

sql = " select * from member where wallet_no=? "
cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParamInput, 50, sWallet)
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2Json(aData, rs)
else
	nIdx = ""
end if
rs.close
ParamInit()

if sWallet <> "" then
	sql = " select * from gcoin_buy where wallet_no=? order by regist_date desc"
	cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParaminput, 50, sWallet)
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nCharge = fnRs2JsonArr(aCharge, rs)
	end if
	rs.close
	ParamInit()

	sql = " select * from gcoin_transfer where send_wallet=? order by regist_date desc"
	cmd.Parameters.Append cmd.CreateParameter("@send_wallet", adVarChar, adParaminput, 50, aData("wallet_no"))
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nSend = fnRs2JsonArr(aSend, rs)
	end if
	rs.close
	ParamInit()


	sql = " select * from gcoin_transfer where receive_wallet=? order by regist_date desc"
	cmd.Parameters.Append cmd.CreateParameter("@receive_wallet", adVarChar, adParaminput, 50, aData("wallet_no"))
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nReceive = fnRs2JsonArr(aReceive, rs)
	end if
	rs.close
	ParamInit()


	sql = " select * from gcoin_settlement where wallet_no=? order by regist_date desc"
	cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParaminput, 50, sWallet)
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nSettlement = fnRs2JsonArr(aSettlement, rs)
	end if
	rs.close
	ParamInit()

	sql = " select * from gcoin_settlement2 where wallet_no=? order by regist_date desc"
	cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParaminput, 50, aData("wallet_no"))
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nSettlement2 = fnRs2JsonArr(aSettlement2, rs)
	end if
	rs.close
	ParamInit()


end if
Set rs = nothing

fnDBClose()

if sWallet = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('등록되지 않은 회원입니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}

function fnUpdateChk(sMode)
{
	var sMsg = "";
	var sAction = "";
	if(sMode == "delete")
	{
		sMsg = "삭제";
		sAction = "<%=PAGE_CODE_%>_update_ok.asp";

		if(confirm("회원정보를 " + sMsg + " 하시겠습니까?"))
		{
			var frm = document.getElementById("frmEdit");
			frm.hdMode.value = sMode;
			frm.action = sAction;
			frm.submit();
		}
	}
	else
	{
		alert("잘못된 접근입니다.");
		return false;
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%" onload="intervalCall();">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<div class="lyMainTitle"><img src="/g_admin/images/common/icon_04.png" alt="<%=PAGE_TITLE_%>"></div>
<div class="lyMainTbl">
<table class="tblContent">
	<tr class="top">
		<th width="20%">지갑주소</th>
		<td colspan="3"><%=aData("wallet_no")%></td>
	</tr>
	
	<tr>
		<th>휴대폰</th>
		<td colspan="3"><%Dim aTel : aTel = fnGetTel(getJson(aData, "handphone"))%>
			<%=aTel(0)%>-<%=aTel(1)%>-<%=aTel(2)%></td>
	</tr>
	
	<tr>
		<th>EZ페이</th>
		<td colspan="3"><%=fnFormatNumber(aData("gcoin"),0,0)%> EZ</td>
	</tr>
</table>
</div>
<div class="lyRBtn">
	<form name="frmEdit" id="frmEdit" method="post" action="member_update.asp">
	<input type="hidden" name="hdIdx" id="hdIdx" value="<%=nIdx%>">
	<input type="hidden" name="hdMode" id="hdMode" value="">
	<input type="hidden" name="pageNO" id="pageNO" value="<%=nPageNO%>">
	<input type="hidden" name="optColumn" id="optColumn" value="<%=sKColumn%>">
	<input type="hidden" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<%if SS_GROUP="A" then%>
		<!--span class="button base"><input type="button" value="수정" onclick="fnUpdateChk('modify');"></span>
		<span class="button base"><input type="button" value="삭제" onclick="fnUpdateChk('delete');"></span-->
	<%end if%>
	<span class="button base"><a href="<%=sListFileName%>?<%=sQueryString%>">목록</a></span>
	</form>
</div>

<div class="csCaption1">충전하기</div>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="30%">충전EZ페이</th>
		<th width="30%">진행상태</th>
		<th width="30%">충전일자</th>
	</tr>
	<%
	if nCharge >= 0 then
		for i=0 to nCharge
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><%=fnFormatNumber(aCharge("gcoin")(i),0,0)%> EZ</td>
		<td><% If aCharge("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aCharge("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aCharge("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aCharge("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
		<td><%=aCharge("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 충전정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>
<div class="csCaption1">보낸내역</div>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="35%">보낸지갑주소</th>
		<th width="15%">EZ페이</th>
		<th width="15%">진행상태</th>
		<th width="25%">전송일자</th>
	</tr>
	<%
	if nSend >= 0 then
		for i=0 to nSend
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><a href="member_view.asp?wallet_no=<%=aSend("receive_wallet")(i)%>&pageNO=<%=nPageNO%>&sQueryString"><%=aSend("receive_wallet")(i)%></a></td>
		<td><%=fnFormatNumber(aSend("gcoin")(i),0,0)%> EZ</td>
		<td><% If aSend("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aSend("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aSend("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aSend("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
		<td><%=aSend("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 송금정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>

<div class="csCaption1">받은내역</div>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="35%">받은지갑주소</th>
		<th width="15%">EZ페이</th>
		<th width="15%">진행상태</th>
		<th width="25%">전송일자</th>
	</tr>
	<%
	if nReceive >= 0 then
		for i=0 to nReceive
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><a href="member_view.asp?wallet_no=<%=aReceive("send_wallet")(i)%>&pageNO=<%=nPageNO%>&sQueryString"><%=aReceive("send_wallet")(i)%></a></td>
		<td><%=fnFormatNumber(aReceive("gcoin")(i),0,0)%> EZ</td>
		<td><% If aReceive("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aReceive("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aReceive("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aReceive("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
		<td><%=aReceive("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 송금정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>

<div class="csCaption1">청산하기</div>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="35%">지갑주소</th>
		<th width="15%">청산EZ페이</th>
		<th width="15%">진행상태</th>
		<th width="25%">청산일자</th>
	</tr>
	<%
	if nSettlement >= 0 then
		for i=0 to nSettlement
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><%=aSettlement("wallet_no")(i)%></td>
		<td><%=fnFormatNumber(aSettlement("gcoin")(i),0,0)%> EZ</td>
		<td><% If aSettlement("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aSettlement("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aSettlement("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aSettlement("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
		<td><%=aSettlement("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 청산정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>

<div class="csCaption1">이체하기</div>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="35%">지갑주소</th>
		<th width="15%">이체EZ페이</th>
		<th width="15%">진행상태</th>
		<th width="25%">이체일자</th>
	</tr>
	<%
	if nSettlement2 >= 0 then
		for i=0 to nSettlement2
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><a href="member_view.asp?wallet_no=<%=aSettlement2("wallet_no")(i)%>&pageNO=<%=nPageNO%>&sQueryString"><%=aSettlement2("wallet_no")(i)%></a></td>
		<td><%=fnFormatNumber(aSettlement2("gcoin")(i),0,0)%> EZ</td>
		<td><% If aSettlement2("result")(i) = "0" Then %>
		이체대기중
		<%ElseIf aSettlement2("result")(i) = "1" Then%><font color="blue">처리완료</font>
		<%ElseIf aSettlement2("result")(i) = "2" Then%><font color="red">취소</font>
		<%Else%>
		<%
		
			fnDBConn()
			Dim strSQL,error_name
			strSQL =  ""
			strSQL = " select ERR_MSG from TB_CTBX_ERRCD where ERR_CD='"& aSettlement2("result")(i) &"'"
			'Response.write strSQL
			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) Then		
				error_name = rs("ERR_MSG")
			end if
			rs.close
			Set rs = nothing
			fnDBClose()
		%>
		<font color="red"><%=error_name%></font>
		<%End If%></td>
		<td><%=aSettlement2("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 이체정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>