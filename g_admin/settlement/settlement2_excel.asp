<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

Response.Buffer = True
Response.ContentType = "appllication/vnd.ms-excel" 
Response.CacheControl = "public"

Response.AddHeader "Content-Disposition","attachment; filename=이체리스트.xls"

Dim sKColumn, sKeyword, sKStatus

Dim fromDate,toDate,difDate

Dim sKSDate : sKSDate = fnNullInit(request("txtSDate"))
Dim sKEDate : sKEDate = fnNullInit(request("txtEDate"))
Dim aCompany, nCompany : nCompany = -1

MENU_IDX_ = 8
sMenuCode = "settlement2"
PAGE_CODE_ = "settlement2"
PAGE_TITLE_ = "이체관리"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sKStatus = fnKeywordFilter(Request("optStatus"))
sQueryString = "optStatus="&sKStatus&"&optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&txtSDate="&sKSDate&"&txtEDate="&sKEDate


if isDate(sKSDate) and isDate(sKEDate) then
	if DateDiff("d", sKSDate, sKEDate) < 0 then
		sKEDate = sKSDate
	end if
else
	sKSDate = ""
	sKEDate = ""
end if

sFilter = ""

fnDBConn()

if sKeyword <> "" then
	if sKColumn = "wallet_no" then
		sFilter = sFilter & " and wallet_no like ? "
		cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "handphone" then
		sFilter = sFilter & " and handphone like ? "
		cmd.Parameters.Append cmd.CreateParameter("@handphone", adVarChar, adParamInput, 250, "%" & sKeyword & "%")	
	end if
end If

if sKStatus <> "" Then
	sFilter = sFilter & " and result = ? "
	cmd.Parameters.Append cmd.CreateParameter("@result", adVarChar, adParamInput, 50, sKStatus )	
End if

if isDate(sKSDate) and isDate(sKEDate) then
	sFilter = sFilter & " and convert(date, regist_date) between ? and ? "
	cmd.Parameters.Append cmd.CreateParameter("@reg_date1", adDate, adParamInput, , sKSDate)
	cmd.Parameters.Append cmd.CreateParameter("@reg_date2", adDate, adParamInput, , sKEDate)
end if

'sqlTable = "select a.*, app_name from store a left join app_info b on(a.app_id=b.app_id) where 1=1 " & sFilter
sqlTable = "select * from gcoin_settlement where 1=1 " & sFilter & " ORDER BY idx desc"
 
cmd.CommandText = sqlTable
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close

Set rs = nothing

fnDBClose()

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<div class="lyMainTbl">
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="10%">지갑번호</th>
		<th width="15%">EZ페이</th>
		<th width="10%">은행명</th>
		<th width="10%">계좌번호</th>
		<th width="10%">입금자명</th>
		<th width="15%">이체신청일자</th>
		<th width="10%">진행상태</th>
	</tr>
	<%
	Dim gcoin_sum
	gcoin_sum = 0
	if nData >= 0 then
		for i=0 to nData
	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><%=aData("wallet_no")(i)%></td>
		<td><%=fnFormatNumber(aData("gcoin")(i),0,0)%>EZ</td>
		<td><%=aData("bank_name")(i)%></td>
		<td><%=aData("account_no")(i)%></td>
		<td><%=aData("input_name")(i)%></td>
		<td><%=aData("regist_date")(i)%></td>
		<td><% If aData("result")(i) = "0" Then %>이체대기중<%ElseIf aData("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aData("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aData("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
	</tr>
	<%
		gcoin_sum = gcoin_sum + aData("gcoin")(i)
		next
	%>
	<tr>
		<td><font color=red>합계</font></td><td></td><td><font color=red><%=fnFormatNumber(gcoin_sum,0,0)%>EZ</font></td><td colspan="5"></td>
	</tr>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 이체내역이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>


</body>
</html>