<!--#include file="../inc/inc_top_common.asp"-->
<!--#include file="../inc/inc_data_chk.asp"-->
<%
sbMemberAuthChk("S")
on error resume next

Dim nIdx
Dim sKColumn, sKeyword
Dim nResultCnt : nResultCnt = 0
Dim sUserID
Dim handphone : handphone = ""
Dim sWallet : sWallet = ""

MENU_IDX_ = 8
PAGE_CODE_ = "settlement"
sListFileName = PAGE_CODE_ & "_list.asp"
nIdx = fnIdxInit(request("idx"))
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "pageNO="&nPageNO&"&optColumn="&sKColumn&"&txtKeyword="&sKeyword


if nIdx = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('삭제 정보가 없습니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if

fnDBConn()

dbConn.BeginTrans

sql = sql & " delete from gcoin_settlement where idx=? "
cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParaminput, 8, nIdx)
cmd.CommandText = sql
cmd.Execute , , adExecuteNoRecords

if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
	dbConn.RollBackTrans

	'fnErrChk()
	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('오류가 발생하여 삭제에 실패하였습니다.');"
	Response.Write "history.back();"
	Response.Write "</script>"
else			
	dbConn.CommitTrans

	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('삭제가 완료되었습니다.');"
	Response.Write "location.href='"&sListFileName&"?" & sQueryString & "';"
	Response.Write "</script>"
end if

fnDBClose()


%>