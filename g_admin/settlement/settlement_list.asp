<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

Dim sKColumn, sKeyword, sKStatus

Dim fromDate,toDate,difDate

Dim sKSDate : sKSDate = fnNullInit(request("txtSDate"))
Dim sKEDate : sKEDate = fnNullInit(request("txtEDate"))

MENU_IDX_ = 8
sMenuCode = "settlement"
PAGE_CODE_ = "settlement"
PAGE_TITLE_ = "청산관리"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKStatus = fnKeywordFilter(Request("optStatus"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "optStatus="&sKStatus&"&optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&txtSDate="&sKSDate&"&txtEDate="&sKEDate

if isDate(sKSDate) and isDate(sKEDate) then
	if DateDiff("d", sKSDate, sKEDate) < 0 then
		sKEDate = sKSDate
	end if
else
	sKSDate = ""
	sKEDate = ""
end if

sFilter = ""

fnDBConn()

if sKeyword <> "" then
	if sKColumn = "wallet_no" then
		sFilter = sFilter & " and wallet_no like ? "
		cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "handphone" then
		sFilter = sFilter & " and handphone like ? "
		cmd.Parameters.Append cmd.CreateParameter("@handphone", adVarChar, adParamInput, 250, "%" & sKeyword & "%")	
	end if
end If

if sKStatus <> "" Then
	sFilter = sFilter & " and result = ? "
	cmd.Parameters.Append cmd.CreateParameter("@result", adVarChar, adParamInput, 1, sKStatus )	
End if

if isDate(sKSDate) and isDate(sKEDate) then
	sFilter = sFilter & " and convert(date, regist_date) between ? and ? "
	cmd.Parameters.Append cmd.CreateParameter("@reg_date1", adDate, adParamInput, , sKSDate)
	cmd.Parameters.Append cmd.CreateParameter("@reg_date2", adDate, adParamInput, , sKEDate)
end if

'sqlTable = "select a.*, app_name from store a left join app_info b on(a.app_id=b.app_id) where 1=1 " & sFilter
sqlTable = "select b.*,(select handphone from member where wallet_no=b.wallet_no) as handphone from gcoin_settlement as b where 1=1  " & sFilter

'게시물 개수
sql = " select count(*) cnt from (" & sqlTable & ")a "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nRecordCount = trim(rs("cnt"))
end if
rs.close

'게시물 리스트	
sqlTable = " SELECT ROW_NUMBER() OVER(ORDER BY regist_date desc) AS RowNum, * FROM (" & sqlTable & ")a "	
sql = " ;with tbl as(" &  sqlTable & ") "
sql = sql & " SELECT TOP " & nPageSize & " * FROM tbl "
sql = sql & " WHERE RowNum >= " & (nPageNO-1)*nPageSize+1 & " and RowNum >= (SELECT MAX(RowNum)RowNum FROM (SELECT TOP " & (nPageNO-1)*nPageSize+1 & " RowNum FROM tbl ORDER BY RowNum ASC) a) ORDER BY RowNum ASC "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing

fnDBClose()

if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script type="text/javascript">
function input_ok(idx)
{

	sMsg = "청산완료처리하시겠습니까?";
	
	if(confirm(sMsg))
	{		
		var frm = document.getElementById("frmReg");
		
		frm.idx.value = idx;
		frm.action = "settlement_send_ok.asp";
		frm.submit();
	}
}

function check_ok(idx)
{

	sMsg = "청산처리하시겠습니까?";
	
	if(confirm(sMsg))
	{		
		var frm = document.getElementById("frmReg");
		
		frm.idx.value = idx;
		frm.action = "settlement_check_ok.asp";
		frm.submit();
	}
}
function fnSchChk(frm)
{
	return true;
}

function clickDel(idx){
	if(confirm("정말로 삭제 하시겠습니까? \n삭제하면 모든 잔여코인이\n모두 사라지며 복구가\n불가능합니다.")){
		document.myform.idx.value = idx;
		document.myform.action = "settlement_delete.asp" ;
		document.myform.submit();		
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<div class="lyMainTitle"><img src="/g_admin/images/common/icon_04.png" alt="<%=PAGE_TITLE_%>"></div>
<br>
<div class="lyCSch">
	<form name="frmSch" id="frmSch" action="<%=sListFileName%>" method="post" onsubmit="return fnSchChk(this);">
	<span class="txtLabel">청산상태</span>
	<select name="optStatus" id="optStatus">
		<option value="" <%if sKStatus="" then%>selected<%end if%>>전체</option>
		<option value="0" <%if sKStatus="0" then%>selected<%end if%>>청산대기</option>
		<option value="1" <%if sKStatus="1" then%>selected<%end if%>>청산완료</option>
		<option value="2" <%if sKStatus="2" then%>selected<%end if%>>취소</option>
	</select>
	&nbsp;
	<span class="txtLabel">청산신청일자</span>
	<input name="txtSDate" type="text" id="txtSDate" size="10" value="<%=sKSDate%>" onclick="Calendar_D(this);" class="txtDate" /> ~
	<input name="txtEDate" type="text" id="txtEDate" size="10" value="<%=sKEDate%>" onclick="Calendar_D(this);" class="txtDate"  />
	<select name="optColumn" id="optColumn">
		<option value="wallet_no" <%if sKColumn="wallet_no" then%>selected<%end if%>>지갑번호</option>
		<option value="handphone" <%if sKColumn="handphone" then%>selected<%end if%>>휴대폰</option>
	</select>
	<input type="text" class="txtKey" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<input type="image" src="/g_admin/images/common/btn_sch.jpg" width="62" height="23" align="absmiddle" title="검색하기" style="width:62px;height:23px">
	</form>
</div>
<form name="frmReg" id="frmReg"  method="post">
<input type="hidden" name="idx" id="idx" value="">
</form>
<form name="myform" method="post">
<input type="hidden" name="idx">
</form>
<div class="lyMainTbl">
<%=fnRecordCnt(nRecordCount, nPageNO, nPageCount)%>
<table class="tblMain">
	<tr>
		<th width="7%">번호</th>
		<th width="9%">휴대폰번호</th>
		<th width="9%">지갑번호</th>
		<th width="12%">EZ PAY</th>
		<th width="10%">은행명</th>
		<th width="10%">계좌번호</th>
		<th width="9%">입금자명</th>
		<th width="*">청산신청일자</th>
		<th width="12%">진행상태</th>
		<th width="9%">삭제</th>
	</tr>
	<%
	Dim gcoin_sum
	gcoin_sum = 0

	if nData >= 0 then
		for i=0 to nData

	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td><%=aData("handphone")(i)%></td>
		<td><a href="settlement_view.asp?wallet=<%=aData("wallet_no")(i)%>&pageNO=<%=nPageNO%>&sQueryString"><%=aData("wallet_no")(i)%></td>
		<td><%=fnFormatNumber(aData("gcoin")(i),0,0)%> EZ</td>
		<td><%=aData("bank_name")(i)%></td>
		<td><%=aData("account_no")(i)%></td>
		<td><%=aData("input_name")(i)%></td>
		<td><%=aData("regist_date")(i)%></td>
		<td>
		<% If aData("result")(i) = "0" Then %>
		<span class="button base"><a href="javascript:input_ok(<%=aData("idx")(i)%>);">청산대기중</a></span>
		<%ElseIf aData("result")(i) = "1" Then%><font color="blue">처리완료</font>
		<%ElseIf aData("result")(i) = "3" Then%><span class="button base"><a href="javascript:check_ok(<%=aData("idx")(i)%>);">청산체크</a></span>
		<%ElseIf aData("result")(i) = "2" Then%><font color="red">취소</font>
		<%Else%>
		<%
		
			fnDBConn()
			Dim strSQL,error_name
			strSQL =  ""
			strSQL = " select ERR_MSG from TB_CTBX_ERRCD where ERR_CD='"& aData("result")(i) &"'"
			'Response.write strSQL
			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) Then		
				error_name = rs("ERR_MSG")
			end if
			rs.close
			Set rs = nothing
			fnDBClose()
		%>
		<font color="red"><%=error_name%></font>
		<%End If%>
		</td>
		<td><span class="button base"><a href="javascript:clickDel(<%=aData("idx")(i)%>);">삭제</a></span></td>
	</tr>
	<%
		gcoin_sum = gcoin_sum + aData("gcoin")(i)
		next
	%>
	<tr>
		<td></td><td><font color=red>합계</font></td><td></td><td><font color=red><%=fnFormatNumber(gcoin_sum,0,0)%>EZ</font></td><td colspan="5"></td>
	</tr>
	<%else%>
	<tr class="msg145">
		<td colspan="10">등록된 청산내역이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>
<div class="lyRMsg"><span class="button base"><a href="settlement_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<%=fnPaging(sListFileName, nPageNO, nBlockSize, nPageCount, sQueryString)%>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>