<!--#include file="../inc/inc_top_common.asp"-->
<%
Dim aFranchise, nFranchise : nFranchise = -1
Dim idx, wallet_no, gcoin, fee, company, tel, regist_date, result, charge_fee, recomm

wallet_no = request("wallet_no")

MENU_IDX_ = 8
sMenuCode = "franchise"
PAGE_CODE_ = "franchise"
PAGE_TITLE_ = "가맹점관리"
sListFileName = PAGE_CODE_ & "_list.asp"


fnDBConn()

sql = "select f.*,m.gcoin as coin_sum from gcoin_franchise as f,member as m where m.wallet_no =f.wallet_no and f.wallet_no ='"&wallet_no&"'"
cmd.CommandText = sql
	Set rs = cmd.Execute()


if not(rs.eof or rs.bof) Then
	idx = rs("idx")
	wallet_no = rs("wallet_no")
	gcoin = rs("coin_sum")
	fee = rs("fee")
	charge_fee = rs("charge_fee")
	recomm = rs("recomm")
	company = rs("company")
	tel = rs("tel")
	regist_date = rs("regist_date")
	result = rs("result")
End If
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript" src="/g_admin/editor/SE2.3.1.O9858/js/HuskyEZCreator.js" charset="utf-8"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script src="../js/script_ajax_cond.js" type="text/javascript"></script>
<script type="text/javascript">

function sendit(){
	if(document.frmEdit.company.value == "") {
		alert("가맹점명을 입력해주세요");
		document.frmEdit.company.focus();
		return false;
	}
	if(document.frmEdit.gcoin.value == "") {
		alert("G코인을 입력해주세요");
		document.frmEdit.gcoin.focus();
		return false;
	}
	if(document.frmEdit.fee.value == "") {
		alert("수수료를 입력해주세요");
		document.frmEdit.fee.focus();
		return false;
	}
	if(document.frmEdit.charge_fee.value == "") {
		alert("가맹점충전수수료를 입력해주세요");
		document.frmEdit.charge_fee.focus();
		return false;
	}
	if(document.frmEdit.tel.value == "") {
		alert("연락처를 입력해주세요");
		document.frmEdit.tel.focus();
		return false;
	}
	document.frmEdit.submit();
}

function OnlyNumber() {
	if((event.keyCode<48)||(event.keyCode>57))
	if(event.preventDefault) {
		event.preventDefault();
    } else {
		event.returnValue = false;
    }
}

</script>
</head> 
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->

<div class="lyTitle"><%=PAGE_TITLE_%></div>
<form name="frmEdit" id="frmEdit" method="post" action="franchise_update_ok.asp">
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="wallet_no" value="<%=wallet_no%>">
<div class="lyMainTbl">
<table class="tblContent">

	<tr class="top">
		<th width="20%">가맹점명</th>
		<td><input type="text" size="25" name="company" value="<%=company%>"></td>
	</tr>
	<tr>
		<th width="20%">지갑번호</th>
		<td><%=wallet_no%></td>
	</tr>
	<tr>
		<th width="20%">EZ페이</th>
		<td><input type="text" size="15" name="gcoin" value="<%=fnFormatNumber(gcoin,0,0)%>">EZ</td>
	</tr>
	<tr>
		<th width="20%">수수료</th>
		<td>
		<input type="text" size="4" name="fee" value="<%=fee%>">%
		</td>
	</tr>
	<tr>
		<th width="20%">가맹점충전수수료</th>
		<td>
		<input type="text" size="4" name="charge_fee" value="<%=charge_fee%>">%
		</td>
	</tr>
	<tr>
		<th width="20%">추천인 수수료</th>
		<td>
		<input type="text" size="4" name="recomm" value="<%=recomm%>">%
		</td>
	</tr>
	<tr>
		<th width="20%">연락처</th>
		<td><input type="text" name="tel" value="<%=tel%>"></td>
	</tr>
	<tr>
		<th width="20%">승인여부</th>
		<td><select name="result">
		<option value="">====선택====</option>
		<option value="0" <% If result = "0" then%>selected<%End if%>>미승인</option>
		<option value="1" <% If result = "1" then%>selected<%End if%>>승인</option>
		</select></td>
	</tr>
	<tr>
		<th width="20%">가입일</th>
		<td><%=regist_date%></td>
	</tr>	
	

</table>
</div>
<div class="lyCBtn">

		<span class="button base"><input type="button" value="수정" onclick="sendit();"></span>
		<span class="button base"><input type="button" value="취소" onclick="history.back();"></span>

</div>
</form>

<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>