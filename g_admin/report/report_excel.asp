<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

Response.Buffer = True
Response.ContentType = "appllication/vnd.ms-excel" 
Response.CacheControl = "public"

Response.AddHeader "Content-Disposition","attachment; filename=미션확인.xls"

Dim sKColumn, sKeyword, sKCompany,sKCbcoin

Dim fromDate,toDate,difDate

Dim sKSDate : sKSDate = fnNullInit(request("txtSDate"))
Dim sKEDate : sKEDate = fnNullInit(request("txtEDate"))
Dim aCompany, nCompany : nCompany = -1

MENU_IDX_ = 8
sMenuCode = "report"
PAGE_CODE_ = "report"
PAGE_TITLE_ = "미션내역관리"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sKCompany = fnKeywordFilter(Request("optCompany"))
sKCbcoin = fnKeywordFilter(Request("optCbcoin"))
sQueryString = "optCbcoin="&sKCbcoin&"&optCompany="&sKCompany&"&optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&txtSDate="&sKSDate&"&txtEDate="&sKEDate

if isDate(sKSDate) and isDate(sKEDate) then
	if DateDiff("d", sKSDate, sKEDate) < 0 then
		sKEDate = sKSDate
	end if
else
	sKSDate = ""
	sKEDate = ""
end if

sFilter = ""

fnDBConn()

if sKeyword <> "" then
	if sKColumn = "handphone" then
		sFilter = sFilter & " and handphone like ? "
		cmd.Parameters.Append cmd.CreateParameter("@handphone", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "app_name" then
		sFilter = sFilter & " and app_name like ? "
		cmd.Parameters.Append cmd.CreateParameter("@app_name", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "tag" then
		sFilter = sFilter & " and tag like ? "
		cmd.Parameters.Append cmd.CreateParameter("@tag", adVarChar, adParamInput, 250, "%" & sKeyword & "%")	
	end if
end if

if isDate(sKSDate) and isDate(sKEDate) then
	sFilter = sFilter & " and convert(date, reg_date) between ? and ? "
	cmd.Parameters.Append cmd.CreateParameter("@reg_date1", adDate, adParamInput, , sKSDate)
	cmd.Parameters.Append cmd.CreateParameter("@reg_date2", adDate, adParamInput, , sKEDate)
end if



sqlTable = "select * from mission where 1=1 " & sFilter

cmd.CommandText = sqlTable
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close

Set rs = nothing

fnDBClose()

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<div class="lyMainTbl">
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="15%">휴대폰</th>
		<th width="30%">미션명</th>
		<th width="10%">성공여부</th>
		<th width="10%">미션경로</th>
		<th width="25%">미션일자</th>
	</tr>
	<%
	if nData >= 0 then
		for i=0 to nData
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td class="al"><%=aData("handphone")(i)%></td>
		<td><%=aData("app_name")(i)%></td>
		<td><%=aData("succeed")(i)%></td>
		<td><%=aData("tag")(i)%></td>
		<td><%=aData("reg_date")(i)%></td>

	</tr>
	<%
		next
	%>

	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 미션이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>


</body>
</html>