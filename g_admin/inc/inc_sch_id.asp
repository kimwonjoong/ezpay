<!--#include file="../inc/inc_top_common.asp"-->
<%
Dim sKeyword : sKeyword = fnKeywordFilter(request("txtKeyword"))

if sKeyword <> "" then
	fnDBConn()

	sql = "select * from member where id = ? "
	cmd.Parameters.Append cmd.CreateParameter("@id", adVarChar, adParaminput, 50, sKeyword)
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nData = fnRs2Json(aData, rs)
	end if
	rs.close
	Set rs = nothing

	fnDBClose()
end if
%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	if(fnDataChk(frm.txtKeyword, 'USERID') == false){return false;}
	return true;
}
function fnIDChkOK()
{
	var obj = parent.document;
	obj.frmEdit.hdIDChk.value="YES";
	obj.frmEdit.txtUserID.readOnly = true;
	obj.frmEdit.txtUserID.value = "<%=sKeyword%>"; 
	parent.fnHiddenIDSchClose();
}
function fnIDChkFail()
{
	var obj = parent.document;
	obj.frmEdit.hdIDChk.value="NO"; 
	obj.frmEdit.txtUserID.value = ""; 
	parent.fnHiddenIDSchClose();
}
</script>
</HEAD>
<BODY leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow-x:hidden;overflow-y:hidden;">
<div class="lyCSch">
<form name="frmSch" id="frmSch" action="inc_sch_id.asp" method="post" onsubmit="return fnSchChk(this);">
<input type="text" class="txtKey" name="txtKeyword" id="txtKeyword" size="20" value="<%=sKeyword%>">
<input type="image" src="/admin/images/common/btn_sch.jpg" width="62" height="23" align="absmiddle" title="검색하기" style="width:62px;height:23px">
</form>
</div>
<%if sKeyword = "" then%>
	<div class="lyCMsg">조회 할 아이디 입력 후 [검색하기]버튼을 눌러주세요.</div>
<%elseif nData = -1 then%>
	<div class="lyCMsg">
		<p><span class="spBlue"><%=sKeyword%></span> 는(은) 사용가능한 아이디입니다.</p>
		<p><span class="spBlue"><%=sKeyword%></span> 를(을) 아이디로 사용하시겠습니까?<br>
		<!-- 버튼:확인--><span class="button beige "><input type="button" onclick="fnIDChkOK();" value="아이디사용"></span>
		<!-- 버튼:취소--><span class="button beige"><input type="button" onclick="fnIDChkFail();" value="취소하기"></span>
	</div>
<%else%>
	<div class="lyCMsg">
		<p><span class="spBlue"><%=sKeyword%></span> 는(은) 사용할 수 없는 아이디입니다.</p>
	           <p>다른 아이디를 입력해주세요.<br></p>
	           <Script type="text/javascript"> 
	           	var obj = parent.document;
	           	obj.frmEdit.hdIDChk.value="NO"; 
	           	obj.frmEdit.txtUserID.value = ""; 
	           </Script>
	</div>
<%end if%>
</div>
</body>
</html>