<!--#include file="inc_top_common.asp"-->
<!--#include file="inc_img_class.asp"-->
<%Call sbPopMemberAuthChk("")%>
<%
Response.Buffer = False

Dim FSO
Dim sFilePath, sFileName, nNum, sImgSrc
Dim Image, iType, img_size, img_width, img_height, bg_width, bg_height
	
sFilePath = fnNullInit(Request("src"))

If sFilePath = "" Then
	fnErrScript "이미지 정보가 없습니다.", "self.close();"
Else		
	sImgSrc = replace(sFilePath, "\", "/")
	sFilePath = server.MapPath("\") & "" & replace(sImgSrc, "/", "\")

	Set FSO = CreateObject("Scripting.FileSystemObject")
	If FSO.FileExists(sFilePath) then
		Set Image = new ImageClass
			With Image
				   .LoadFilePath(sFilePath)
				   .ImageRead
				   iType = .ImageType
				   img_width = .Width
				   img_height = .Height
			End With
			Set Image = Nothing 
	
		'이미지 사이즈
		if img_width = 0 or img_height = 0 then
			set img_size = LoadPicture(sFilePath)
			img_width = CLng(CDbl(img_Size.Width)*24/635) 
			img_height = CLng(CDbl(img_Size.Height)*24/635)
			set img_size = nothing
		end if
		
		if img_width > 0 and img_height > 0 then

			'최대 크기
			if img_width > 10000 then
				img_height = CLng(img_height * (10000/img_width))
				img_width = 10000
			end if
			
			if img_height > 10000 then
				img_width= CLng(img_width * (10000/img_height))
				img_height = 10000
			end if 
			
			bg_width = img_width
			bg_height = img_height	
			
			if img_height > img_width then
				if img_width > 0 and img_width < 200 then
					bg_height = CLng(img_height * (200/img_width))
					bg_width = 200
				end if
			else
				if img_height > 0 and img_height < 200 then
					bg_width= CLng(img_width * (200/img_height))
					bg_height = 200
				end if
			end If
		else
			img_width = 200
			img_height = 200
		end if
	Else
		bg_width = 200
		bg_height = 200
		fnErrScript "잘못된 이미지경로입니다.", "self.close();"
	End If	
	Set FSO = nothing	
End If
%>
<html>
<head>
<script type="text/javascript">
self.resizeTo(<%=bg_width%>+50,<%=bg_height%>+50);
</script>
</head>
<body topmargin="0" leftmargin="0" width="100%" height="100%"  align="center" valign="middle">
<table cellpadding="0" cellspacing="0" border="0" width="100%" height="100%" align="center" valign="middle">
	<tr valign="middle">
		<td align="center" valign="middle"><img src="<%=sImgSrc%>" width="<%=img_width%>" height="<%=img_height%>" border="0" alt="클릭하면 창이 닫힙니다" style="cursor:pointer" onclick="self.close();"></td>
	</tr>
</table>
</body>
</html>