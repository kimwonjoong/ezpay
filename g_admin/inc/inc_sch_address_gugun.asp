<!--#include file="../inc/inc_top_common.asp"-->
<%
Dim sKeyword : sKeyword = fnKeywordFilter(request.form("txtKeyword"))

if sKeyword <> "" then
	fnDBConn()

	sql = "select * from zipcode where dong like ? order by sido, gugun, dong, ri, bunji, seq "
	cmd.Parameters.Append cmd.CreateParameter("@dong", adVarChar, adParaminput, 50, "%" & sKeyword & "%")
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nData = fnRs2JsonArr(aData, rs)
	end if
	rs.close
	Set rs = nothing

	fnDBClose()
end if
%>    
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
function fnGetGeoCode(sSiGuDongAddress, sSiGuDongLiAddress, sAddress, sAreaCode)
{
	var frm = document.getElementById("frmGeoCode");
	
	frm.hdSiGuDongAddress.value = sSiGuDongAddress;
	frm.hdSiGuDongLiAddress.value = sSiGuDongLiAddress;
	frm.hdAddress.value = sAddress;
	frm.hdAreaCode.value = sAreaCode;
	frm.submit();
}
</script>
</HEAD>
<BODY leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow-x:hidden;overflow-y:scroll;padding:5px">
<div style="width:98%">
<div class="lyCSch">
<form name="frmSch" id="frmSch" action="inc_sch_address_gugun.asp" method="post" onsubmit="return fnSchChk(this);">
<input type="text" class="txtKey" name="txtKeyword" id="txtKeyword" size="20" value="<%=sKeyword%>">
<input type="image" src="/admin/images/common/btn_sch.jpg" width="62" height="23" align="absmiddle" title="검색하기" style="width:62px;height:23px">
</form>
</div>
<div class="lyMainTbl">
<table class="tblMain">
	<tr class="top">
		<th width="85%">주소</th>
		<th width="15%">선택</th>
	</tr>	
	<%if sKeyword <> "" and nData < 0 then%>
	<tr class="msg145">
		<td colspan="2">검색결과가 없습니다.</td>
	</tr>
	<%elseif nData < 0 then%>
	<tr class="msg145">
		<td colspan="2">읍.면.동을 입력하고 검색하세요.</td>
	</tr>
	<%else%>
		<%
		Dim sAddress, sSiGuDongLiAddress, sSiGuDongAddress
		for i=0 to nData
			sSiGuDongAddress = aData("sido")(i)&" " & aData("gugun")(i)&" " & aData("dong")(i)
			sSiGuDongLiAddress = sSiGuDongAddress&" " & aData("ri")(i)
			sAddress = sSiGuDongLiAddress &" " & aData("bunji")(i)

			if instr(aData("bunji")(i), "~") >= 1 then
				sAddress = sSiGuDongLiAddress
			end if
		%>
		<tr>
			<td class="al">
				<a href="#" onclick="fnGetGeoCode('<%=sSiGuDongAddress%>', '<%=sSiGuDongLiAddress%>', '<%=sAddress%>', '<%=aData("gugun_code")(i)%>');">
					[<%=aData("zipcode")(i)%>] <%=sAddress%>
				</a>
			</td>
			<td><span class="button small mustard"><input type="button" value="선택" onclick="fnGetGeoCode('<%=sSiGuDongAddress%>','<%=sSiGuDongLiAddress%>', '<%=sAddress%>', '<%=aData("gugun_code")(i)%>');"></span></td>
		</tr>
		<%next%>
	<%end if%>
</table>
</div>
<form name="frmGeoCode" id="frmGeoCode" action="inc_get_geocode.asp" method="post" target="ifrGeoCode">
<input type="hidden" name="hdSiGuDongAddress" id="hdSiGuDongAddress" value="">
<input type="hidden" name="hdSiGuDongLiAddress" id="hdSiGuDongLiAddress" value="">
<input type="hidden" name="hdAddress" id="hdAddress" value="">
<input type="hidden" name="hdAreaCode" id="hdAreaCode" value="">
</form>
<iframe src="" width="0" height="0" frameborder="0" name="ifrGeoCode" id="ifrGeoCode"></iframe>
</div>
</body>
</html>