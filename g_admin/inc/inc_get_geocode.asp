<!--#include file="../inc/inc_top_common.asp"-->
<%
Dim sResult : sResult = ""
Dim jsonResult : Set jsonResult = jsObject()
Dim sSiGuDongLiAddress : sSiGuDongLiAddress = fnKeywordFilter(request.form("hdSiGuDongLiAddress"))
Dim sAddress : sAddress = fnKeywordFilter(request.form("hdAddress"))
Dim sAreaCode : sAreaCode = fnKeywordFilter(request.form("hdAreaCode"))
Dim sSiGuDongAddress: sSiGuDongAddress = fnKeywordFilter(request.form("hdSiGuDongAddress"))

sAddress = sSiGuDongLiAddress

if sSiGuDongAddress <> "" and sAddress <> "" and sAreaCode <> "" then

	'시.구군.동.리.번지 검색
	'sResult = getGoogleGetCode(sAddress, jsonResult, "NAVER")
	'if sResult <> "OK" then
	'	sResult = getGoogleGetCode(sAddress, jsonResult, "GOOGLE")
	'	if sResult <> "OK" then
	'		sResult = getGoogleGetCode(sAddress, jsonResult, "DAUM")
	'	end if
	'end if

	'시.구군.동.리 검색
	if sResult <> "OK" then
		sResult = getGoogleGetCode(sAddress, jsonResult, "NAVER")
		if sResult <> "OK" then
			sResult = getGoogleGetCode(sAddress, jsonResult, "GOOGLE")
			if sResult <> "OK" then
				sResult = getGoogleGetCode(sAddress, jsonResult, "DAUM")
					'시.구군.동 검색
					if sResult <> "OK" then
						sResult = getGoogleGetCode(sSiGuDongAddress, jsonResult, "NAVER")
						if sResult <> "OK" then
							sResult = getGoogleGetCode(sSiGuDongAddress, jsonResult, "GOOGLE")
							if sResult <> "OK" then
								sResult = getGoogleGetCode(sSiGuDongAddress, jsonResult, "DAUM")
							end if
						end if
					end if

			end if
		end if
	end if
	
	if sResult = "OK" then
		response.write "<script type='text/javascript'>"
		response.write "parent.parent.document.getElementById('frmEdit').hdAreaCode.value='" & sAreaCode & "';"
		response.write "parent.parent.document.getElementById('frmEdit').txtLat.value='" & jsonResult("lat") & "';"
		response.write "parent.parent.document.getElementById('frmEdit').txtLon.value='" & jsonResult("lon") & "';"
		response.write "parent.parent.document.getElementById('frmEdit').txtAddress.value='" & sSiGuDongLiAddress & "';"
		response.write "parent.parent.document.getElementById('frmEdit').txtLat.readOnly=true;"
		response.write "parent.parent.document.getElementById('frmEdit').txtLon.readOnly=true;"
		'response.write "parent.parent.document.getElementById('frmEdit').txtAddress.readOnly=true;"
		response.write "parent.parent.document.getElementById('lyGeoCode').className='csReadOnly';"
		response.write "parent.parent.fnHiddenClose();"
		response.write "</script>"
	else
		response.write "<script type='text/javascript'>"
		response.write "alert('주소 좌표를 얻는데 실패하였습니다.\n좌표를 직접 입력해주세요.');"
		response.write "parent.parent.document.getElementById('frmEdit').hdAreaCode.value='" & sAreaCode & "';"
		response.write "parent.parent.document.getElementById('frmEdit').txtLat.value='';"
		response.write "parent.parent.document.getElementById('frmEdit').txtLon.value='';"
		response.write "parent.parent.document.getElementById('frmEdit').txtAddress.value='" & sSiGuDongLiAddress & "';"
		response.write "parent.parent.document.getElementById('frmEdit').txtLat.readOnly=false;"
		response.write "parent.parent.document.getElementById('frmEdit').txtLon.readOnly=false;"
		response.write "parent.parent.document.getElementById('lyGeoCode').className='csReadOnlyOff';"
		response.write "parent.parent.fnHiddenClose();"
		response.write "</script>"
	end if
	Set jsonResult = nothing
end if

'구글API
Function getGoogleGetCode(sAddress, ByRef jsonResult, sMode)
	on error resume next
	Dim json1 : Set json1 = jsObject()
	Dim xmlHTTP, xmlDom
	Dim sResult : sResult = "FAIL"

	'//xml연결
	Set xmlHTTP = Server.CreateObject("Msxml2.ServerXMLHTTP")       

	Dim classKeyValue : classKeyValue = "http://maps.googleapis.com/maps/api/geocode/xml?address="& sAddress &"&sensor=false&language=ko"		'구글
	
	if sMode = "NAVER" then 
		classKeyValue = "http://maps.naver.com/api/geocode.php?key=de7f6eb84d7e93b0a08679c8c2657a22&query="& sAddress &"&coord=latlng"		'네이버 mbcticket.cafe24.com		
	elseif sMode ="DAUM" then
		classKeyValue = "http://apis.daum.net/local/geo/addr2coord?apikey=ef0cefecc2e1e678cc1493b868d950f58b0042ec&q="& URLEncodeUTF8(sAddress) &"&output=xml"		'다음
		'xmlHTTP.setRequestHeader "Content-Type"  , "text/xml; charset=utf-8" 
	end if

	xmlHTTP.open "GET", classKeyValue, False      
	xmlHTTP.send   

	'//로드
	Set xmlDom = Server.CreateObject("Microsoft.XMLDOM")       
	xmlDom.async = False      
	xmlDom.load xmlHTTP.responseBody 
	
	jsonResult("lat") = null
	jsonResult("lon") = null

	Dim responseYN
	Dim rCode

	if sMode = "NAVER" then
		If xmlDom.selectNodes("//total")(0).text > 0 Then
			sResult = "OK"
			jsonResult("lat") = xmlDom.selectNodes("//y")(0).text	'위도
			jsonResult("lon") = xmlDom.selectNodes("//x")(0).text	'경도
		end if
	elseif sMode ="DAUM" then
		Set responseYN = xmlDom.selectNodes("//result")
		if isObject(responseYN) and responseYN(0).childNodes(0).text >= 1 then
			sResult = "OK"
			'Set rCode = xmlDom.selectNodes("//lng")		'xmlDom.documentElement.SelectSingleNode("/rcode")
			'if isObject(rCode) then
				jsonResult("lat") = xmlDom.selectNodes("//lat")(0).childNodes(0).text
				jsonResult("lon") = xmlDom.selectNodes("//lng")(0).childNodes(0).text
			'end if
			'Set rCode = nothing
		end if
		'response.write jsonResult("lat") & responseYN(0).childNodes(0).text
		'response.end
	'구글
	else
		Set responseYN = xmlDom.selectNodes("//status")
		If responseYN(0).childNodes(0).text = "OK" Then	
			sResult = "OK"
			Set rCode = xmlDom.getElementsByTagName("location")(0)		'xmlDom.documentElement.SelectSingleNode("/rcode")
			if isObject(rCode) then
				jsonResult("lat") = rCode.childNodes(0).text
				jsonResult("lon") = rCode.childNodes(1).text
			end if
			Set rCode = nothing
		end if
	end if
	Set xmlDom = nothing
	Set xmlHTTP = nothing

	getGoogleGetCode = sResult
End Function
%>    