<!-- 페이징 START -->
<%
	nCurrentBlockpage = fnCeiling(nPageNO/nBlockSize)				'현재 페이지의 블럭카운트
	nTempPage = CLng(nBlockSize*(nCurrentBlockpage-1)+1)		'현재 블럭의 첫번째 페이지카운트
%>
<table width="98%" border="0" cellspacing="0" cellpadding="0">
	<tr>
		<td class="ac ft11">
		<%
		'<<(-10) : nCurrentBlockpage > 1
		if nTempPage > 1 then
			response.write "<a href='" & sListFileName & "?PageNO=" & (nTempPage-nBlockSize) & sQueryString & "'><img src='../images/common/mc_ic_05on.gif' width='12' height='12'></a>&nbsp;"
		else
			response.write "<img src='../images/common/mc_ic_05off.gif' width='12' height='12'>&nbsp;"
		end if

		' <(-1) : nCurrentBlockpage < blocksize
		if nPageNO > 1 then
			response.write "<a href='" & sListFileName & "?PageNO=" & (nPageNO-1) & sQueryString & "'><img src='../images/common/mc_ic_04on.gif' width='12' height='12'></a>&nbsp;&nbsp;"
		else
			response.write "<img src='../images/common/mc_ic_04off.gif' width='12' height='12'>&nbsp;&nbsp;"
		end if

		' [n1,n2,3,n4,...] : (blocksize*current_blockpage) ~ nBlockSize
		i = 1
		Do until i > nBlockSize or nTempPage > nPageCount
			if nTempPage = nPageNO then
				response.write "<a href='" & sListFileName & "?PageNO=" & nTempPage & sQueryString &  "' class='ft_12'>" & nTempPage & "</a>&nbsp;"
			else
				response.write "<a href='" & sListFileName & "?PageNO=" & nTempPage & sQueryString &  "'>" & nTempPage & "</a>&nbsp;"
			end if
			nTempPage = nTempPage + 1
			i = i + 1
		Loop

		' >(+1) : nCurrentBlockpage < blocksize 
		if nPageNO < nPageCount then
			response.write "&nbsp;<a href='" & sListFileName & "?PageNO=" & (nPageNO+1) & sQueryString & "'><img src='../images/common/mc_ic_02on.gif' width='12' height='12'></a>&nbsp;"
		else
			response.write "&nbsp;<img src='../images/common/mc_ic_02off.gif' width='12' height='12'>&nbsp;"
		end if

		' >>(+10) : nTempPage < last_blockpage 
		if nTempPage <= nPageCount then
			response.write "<a href='" & sListFileName & "?PageNO=" & nTempPage & sQueryString &  "'><img src='../images/common/mc_ic_03on.gif' width='12' height='12'></a> "
		else
			response.write "<img src='../images/common/mc_ic_03off.gif' width='12' height='12'>"
		end if
		%>   	 
		</td>
	</tr>
</table>
<!-- 페이징 END -->