<%
Dim MENU_CNT_ : MENU_CNT_ = 0
Dim nMenuCnt_member : nMenuCnt_member = MENU_CNT_
Dim nMenuCnt_charge : nMenuCnt_charge = MENU_CNT_
Dim nMenuCnt_send : nMenuCnt_send = MENU_CNT_
Dim nMenuCnt_settlement : nMenuCnt_settlement = MENU_CNT_
Dim nMenuCnt_fee : nMenuCnt_fee = MENU_CNT_
Dim nMenuCnt_franchise : nMenuCnt_franchise = MENU_CNT_
Dim nMenuCnt_admin : nMenuCnt_admin = MENU_CNT_

Dim nMenuCnt_input : nMenuCnt_input = MENU_CNT_
Dim nMenuCnt_gcharge : nMenuCnt_gcharge = MENU_CNT_
Dim nMenuCnt_output : nMenuCnt_output = MENU_CNT_
Dim nMenuCnt_suik : nMenuCnt_suik = MENU_CNT_
Dim nMenuCnt_gamang : nMenuCnt_gamang = MENU_CNT_



%>
<script type="text/javascript">
var aWidth = new Array();

function fnSubMenuOn(sArgID, nMenuCnt)
{
	$("[id^=lySubMenu_]").css("display", "none");
	$("#lySubMenu_"+sArgID).css("display", "block");
	var nCnt = $("#lySubMenu_"+sArgID).find("ul").eq(0).find("li").length;

	var nLeft = aWidth[sArgID];
	var nLeftAdd = 0;

	if(isNaN(nLeft) || nLeft == null || nLeft == "")
	{
		if(nCnt >= 3)
		{
			var nCnt1 = (nCnt/2);
			nCnt = nCnt1+1;
			nLeftAdd = 20*nCnt1;
		}
		var nWidth = eval(nCnt*140);
		var nWidthHalf = nWidth/2;
		nLeft = eval(nMenuCnt)*140+50-nWidthHalf-100+nLeftAdd;

		if(nLeft < 0){nLeft = 0;}
		else if((nWidth + nLeft) > 1024){nLeft = 1024-nWidth-50;}
		aWidth[sArgID] = nLeft;
	}
	$("#lySubMenu_"+sArgID).css("padding-left", nLeft+"px");
}
</script>
<div style="text-align:center;width:100%;">
	<div style="width:1024px;height:157px;text-align:center;margin:auto; ">		
		<table width="1024" height="157" border="0" cellpadding="0" cellspacing="0" style="table-lyaout:fixed">
			<tr height="60" >
				<td width="1024" colspan="2" height="60" style="background:#F1CF37;">
					<!--img src="images/main_01.jpg" width="1024" height="62" alt=""-->
					<%if SS_GROUP="A" then%>
					<div style="float:left"><a href="<%=HOME_URL_%>/g_admin/main.asp" title="홈으로"><img src="<%=HOME_URL_%>/g_admin/images/common/ver2_logo.jpg"></a></div>
					<%else%>
					<div style="float:left"><a href="<%=HOME_URL_%>/g_admin/g_main.asp" title="홈으로"><img src="<%=HOME_URL_%>/g_admin/images/common/ver2_logo.jpg"></a></div>
					<%End if%>
					<div style="float:right;padding:15px 15px 0px 15px;color:#fefefe;">
						(<%=SS_GROUP%>) <%=SS_USER_NAME%>님 반갑습니다. &nbsp;&nbsp;
						<span class="button black"><a href="<%=HOME_URL_%>/g_admin/member_logout.asp">로그아웃</a></span>
					</div>
				</td>
			</tr>
			<tr height="13">
				<td width="1024" height="13" colspan="2" valign="top" ><!--img src="<%=HOME_URL_%>/admin/images/menu/menu_top.jpg"--></td>
			</tr>
			<tr height="62" >
				<td width="985" style="background:url('<%=HOME_URL_%>/g_admin/images/menu/menu_bg.jpg') repeat-x;" valign="top">
					<ul class="ulMenu">
						<li style="width:19px;height:62px;background:#ffffff"></li>
						<li style="width:18px;height:62px"><img src="<%=HOME_URL_%>/g_admin/images/menu/top_menu_left.jpg"></li>
						<%if SS_GROUP="A" then%>
						<li style="width:59px;height:62px"><a href="<%=HOME_URL_%>/g_admin/main.asp"><img src="<%=HOME_URL_%>/g_admin/images/menu/menu_home.jpg"></a></li>
						<%else%>
						<li style="width:59px;height:62px"><a href="<%=HOME_URL_%>/g_admin/g_main.asp"><img src="<%=HOME_URL_%>/g_admin/images/menu/menu_home.jpg"></a></li>
						<%End if%>
						
						
						
						<%if SS_GROUP="A" then%>

						<a href="/g_admin/member/member_list.asp" class="menu<%if sMenuCode="admin" then%>On<%end if%>" onmouseover="fnSubMenuOn('admin', '<%=fnMenuCnt(MENU_CNT_)%>');">							
							<li class="liLeft"></li>
							<li class="liCenter">
								<span class="spMenu">회원 관리</span>
							</li>
							<li class="liRight"></li>
						</a>				
						<%nMenuCnt_admin = MENU_CNT_%>

						<a href="/g_admin/charge/charge_list.asp" class="menu<%if sMenuCode="charge" then%>On<%end if%>" onmouseover="fnSubMenuOn('charge', '<%=fnMenuCnt(MENU_CNT_)%>');">							
							<li class="liLeft"></li>
							<li class="liCenter">
								<span class="spMenu">충전 관리</span>
							</li>
							<li class="liRight"></li>
						</a>				
						<%nMenuCnt_charge = MENU_CNT_%>

						
						<a href="/g_admin/send/send_list.asp" class="menu<%if sMenuCode="send" then%>On<%end if%>" onmouseover="fnSubMenuOn('send', '<%=fnMenuCnt(MENU_CNT_)%>');">							
								<li class="liLeft"></li>
								<li class="liCenter">
									<span class="spMenu">송금 관리</span>
								</li>
								<li class="liRight"></li>
							</a>				
							<%nMenuCnt_send = MENU_CNT_%>

							<a href="/g_admin/settlement/settlement_list.asp" class="menu<%if sMenuCode="settlement" then%>On<%end if%>" onmouseover="fnSubMenuOn('settlement', '<%=fnMenuCnt(MENU_CNT_)%>');">							
								<li class="liLeft"></li>
								<li class="liCenter">
									<span class="spMenu">청산 관리</span>
								</li>
								<li class="liRight"></li>
							</a>				
							<%nMenuCnt_settlement = MENU_CNT_%>

							

							<a href="/g_admin/franchise/franchise_list.asp" class="menu<%if sMenuCode="franchise" then%>On<%end if%>" onmouseover="fnSubMenuOn('franchise', '<%=fnMenuCnt(MENU_CNT_)%>');">							
								<li class="liLeft"></li>
								<li class="liCenter">
									<span class="spMenu">가맹점 관리</span>
								</li>
								<li class="liRight"></li>
							</a>
						<%nMenuCnt_franchise = MENU_CNT_%>


						<a href="/g_admin/fee/day_stat.asp" class="menu<%if sMenuCode="fee" then%>On<%end if%>" onmouseover="fnSubMenuOn('fee', '<%=fnMenuCnt(MENU_CNT_)%>');">							
							<li class="liLeft"></li>
							<li class="liCenter">
								<span class="spMenu">수수료수익</span>
							</li>
							<li class="liRight"></li>
						</a>				
						<%nMenuCnt_fee = MENU_CNT_%>
						<%end if%>

						<%if SS_GROUP="P" then%>
							<a href="/g_admin/input/input_list.asp" class="menu<%if sMenuCode="input" then%>On<%end if%>" onmouseover="fnSubMenuOn('input', '<%=fnMenuCnt(MENU_CNT_)%>');">							
								<li class="liLeft"></li>
								<li class="liCenter">
									<span class="spMenu">입금확인</span>
								</li>
								<li class="liRight"></li>
							</a>				
							<%nMenuCnt_input = MENU_CNT_%>

							<a href="/g_admin/gcharge/charge_list.asp" class="menu<%if sMenuCode="gcharge" then%>On<%end if%>" onmouseover="fnSubMenuOn('gcharge', '<%=fnMenuCnt(MENU_CNT_)%>');">							
								<li class="liLeft"></li>
								<li class="liCenter">
									<span class="spMenu">충전확인</span>
								</li>
								<li class="liRight"></li>
							</a>				
							<%nMenuCnt_gcharge = MENU_CNT_%>

							<a href="/g_admin/output/output_list.asp" class="menu<%if sMenuCode="output" then%>On<%end if%>" onmouseover="fnSubMenuOn('output', '<%=fnMenuCnt(MENU_CNT_)%>');">							
								<li class="liLeft"></li>
								<li class="liCenter">
									<span class="spMenu">출금확인</span>
								</li>
								<li class="liRight"></li>
							</a>				
							<%nMenuCnt_output = MENU_CNT_%>

							<a href="/g_admin/suik/suik_list.asp" class="menu<%if sMenuCode="suik" then%>On<%end if%>" onmouseover="fnSubMenuOn('suik', '<%=fnMenuCnt(MENU_CNT_)%>');">							
								<li class="liLeft"></li>
								<li class="liCenter">
									<span class="spMenu">수수료확인</span>
								</li>
								<li class="liRight"></li>
							</a>				
							<%nMenuCnt_suik = MENU_CNT_%>

							<a href="/g_admin/gamang/gamang_list.asp" class="menu<%if sMenuCode="gamang" then%>On<%end if%>" onmouseover="fnSubMenuOn('gamang', '<%=fnMenuCnt(MENU_CNT_)%>');">							
								<li class="liLeft"></li>
								<li class="liCenter">
									<span class="spMenu">가맹점청산관리</span>
								</li>
								<li class="liRight"></li>
							</a>				
							<%nMenuCnt_gamang = MENU_CNT_%>
							
						<%end if%>
					</ul>
				</td>
				<td style="width:38px;height:62px;background:#ffffff;text-align:left;"><img src="<%=HOME_URL_%>/g_admin/images/menu/top_menu_right.jpg" width="18" height="62"></td>
			</tr>
			<tr height="23">
				<td width="1024" height="23" colspan="2" style="background:url('<%=HOME_URL_%>/g_admin/images/menu/sub_menu_bg.jpg') no-repeat;" valign="top">
					
					<div id="lySubMenu_admin" class="lySubMenu<%if sMenuCode="admin" then%>On<%end if%>">
						<ul class="ulSubMenu">
							<li><a href="/g_admin/member/member_list.asp">회원 관리</a></li>
							<li>|</li>
							<li><a href="/g_admin/push/push_list.asp">PUSH 관리</a></li>
						</ul>
					</div>
					<%if sMenuCode="admin" then%><script type="text/javascript">fnSubMenuOn('admin', '<%=nMenuCnt_admin%>');</script><%end if%>


					<div id="lySubMenu_charge" class="lySubMenu<%if sMenuCode="charge" then%>On<%end if%>">
						<ul class="ulSubMenu">
							<li><a href="/g_admin/charge/charge_list.asp">충전 관리</a></li>
							<li>|</li>
							<li><a href="/g_admin/charge/charge_over_list.asp">Over List</a></li>
							<li>|</li>
							<li><a href="/g_admin/charge/charge_account.asp">충전계좌 관리</a></li>
						</ul>
					</div>
					<%if sMenuCode="charge" then%><script type="text/javascript">fnSubMenuOn('charge', '<%=nMenuCnt_charge%>');</script><%end if%>


					<div id="lySubMenu_send" class="lySubMenu<%if sMenuCode="send" then%>On<%end if%>">
						<ul class="ulSubMenu">
							<li><a href="/g_admin/send/send_list.asp">송금관리</a></li>
							<li>|</li>
							<li><a href="/g_admin/bitcoin/bitcoin_list.asp">비트코인송금관리</a></li>
							<li>|</li>
							<li><a href="/g_admin/bitcoin/bitcoin_transfer_list.asp">비트코인전환관리</a></li>
						</ul>
					</div>
					<%if sMenuCode="send" then%><script type="text/javascript">fnSubMenuOn('send', '<%=nMenuCnt_send%>');</script><%end if%>


					<div id="lySubMenu_settlement" class="lySubMenu<%if sMenuCode="settlement" then%>On<%end if%>">
						<ul class="ulSubMenu">
							<li><a href="/g_admin/settlement/settlement_list.asp">청산 관리</a></li>
							<li>|</li>
							<li><a href="/g_admin/settlement/settlement2_list.asp">이체 관리</a></li>
						</ul>
					</div>
					<%if sMenuCode="settlement" then%><script type="text/javascript">fnSubMenuOn('settlement', '<%=nMenuCnt_settlement%>');</script><%end if%>

					<div id="lySubMenu_franchise" class="lySubMenu<%if sMenuCode="franchise" then%>On<%end if%>">
						<ul class="ulSubMenu">
							<li><a href="/g_admin/franchise/franchise_list.asp">가맹점관리</a></li>
							<li>|</li>
							<li><a href="/g_admin/franchise/franchise_settlement.asp">가맹점청산</a></li>
							<li>|</li>
							<li><a href="/g_admin/exchange/exchange_list.asp">외국환관리</a></li>
						</ul>
					</div>
					<%if sMenuCode="franchise" then%><script type="text/javascript">fnSubMenuOn('franchise', '<%=nMenuCnt_franchise%>');</script><%end if%>

					<div id="lySubMenu_fee" class="lySubMenu<%if sMenuCode="fee" then%>On<%end if%>">
						<ul class="ulSubMenu">
							<li><a href="/g_admin/fee/day_stat.asp">수수료수익</a></li>
							<li>|</li>
							<li><a href="/g_admin/fee/recomm_list.asp">추천인</a></li>
						</ul>
					</div>
					<%if sMenuCode="fee" then%><script type="text/javascript">fnSubMenuOn('fee', '<%=nMenuCnt_fee%>');</script><%end if%>




					
					
					

				</td>
			</tr>
		</table>
	</div>
	<div style="width:1024px;text-align:center;margin:auto;">
	<%if PAGE_CODE_ = "main" then%><br/>
	<table id="__02" width="1024"  border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="5" width="1024" height="43"><img src="<%=HOME_URL_%>/g_admin/images/main_03.jpg" width="1024" height="43" alt=""></td>
		</tr>
		<tr>
			<td style="background:url('<%=HOME_URL_%>/g_admin/images/main_04.jpg') repeat-y">
				<img src="<%=HOME_URL_%>/g_admin/images/main_04.jpg" width="67" height="227" alt="">
			</td>
			<td width="418" height="227" valign="top">
	<%else%>
	<table id="__02" width="1024"  border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td colspan="3"	 width="1024" height="25"><br/>
				<img src="<%=HOME_URL_%>/g_admin/images/main_10.jpg" width="1024" height="25" alt=""></td>
		</tr>
		<tr>
			<td style="background:url('<%=HOME_URL_%>/g_admin/images/main_11.jpg') repeat-y;" width="67">
				<img src="<%=HOME_URL_%>/g_admin/images/main_11.jpg" width="67" height="279" alt=""></td>
			<td width="888" height="279" valign="top">

	<%end if%>