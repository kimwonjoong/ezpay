<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Dim sKColumn, sKeyword, nIdx
Dim fromDate,toDate,difDate
Dim aCharge, nCharge: nCharge = -1
Dim aCharge2, nCharge2: nCharge2 = -1
Dim aSend, nSend: nSend = -1
Dim aSettlement, nSettlement: nSettlement = -1

Dim wallet_no,m_year,m_month,m_day

MENU_IDX_ = 8
sMenuCode = "fee"
PAGE_CODE_ = "day"
PAGE_TITLE_ = "수수료관리"
sListFileName = PAGE_CODE_ & "_stat.asp"

wallet_no = fnKeywordFilter(request("wallet_no"))
m_year = fnKeywordFilter(request("m_year"))
m_month = fnKeywordFilter(request("m_month"))
m_day = fnKeywordFilter(request("m_day"))

sQueryString = "wallet_no="&wallet_no&"&m_year="&m_year&"&m_month="&m_month&"&m_day="&m_day

sFilter = ""

if wallet_no = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('지갑 정보가 없습니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if

fnDBConn()



if wallet_no <> "" then
	

	sql = " select * from gcoin_transfer where send_wallet=? and DATEPART(yyyy, regist_date) ='"&m_year&"'  and  DATEPART(mm, regist_date) ='"&m_month&"' and  DATEPART(dd, regist_date) ='"&m_day&"' "
	cmd.Parameters.Append cmd.CreateParameter("@send_wallet", adVarChar, adParaminput, 50, wallet_no)
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nCharge = fnRs2JsonArr(aCharge, rs)
	end if
	rs.close
	ParamInit()


	sql = " select * from gcoin_transfer where receive_wallet=? and DATEPART(yyyy, regist_date) ='"&m_year&"'  and  DATEPART(mm, regist_date) ='"&m_month&"' and  DATEPART(dd, regist_date) ='"&m_day&"' "
	cmd.Parameters.Append cmd.CreateParameter("@receive_wallet", adVarChar, adParaminput, 50, wallet_no)
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nSend = fnRs2JsonArr(aSend, rs)
	end if
	rs.close
	ParamInit()

	sql = " select * from gcoin_buy where wallet_no=? and DATEPART(yyyy, regist_date) ='"&m_year&"'  and  DATEPART(mm, regist_date) ='"&m_month&"' and  DATEPART(dd, regist_date) ='"&m_day&"' "
	cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParaminput, 50, wallet_no)
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nCharge2 = fnRs2JsonArr(aCharge2, rs)
	end if
	rs.close
	ParamInit()


	
end if
Set rs = nothing

fnDBClose()

if wallet_no = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('등록되지 않은 회원입니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}

</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->

<div class="lyMainTitle"><img src="/g_admin/images/common/icon_06.png" alt="공지사항"></div>
<br>
<div class="lyMainTbl">
<table class="tblContent">
	<tr class="top">
		<th width="20%">지갑주소</th>
		<td colspan="3"><%=wallet_no%></td>
	</tr>
	
	
	
</table>
</div>
<div class="lyRBtn">
	
	<span class="button base"><a href="<%=sListFileName%>?<%=sQueryString%>">목록</a></span>
	</form>
</div>




<div class="csCaption1">이체내역</div>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="15%">받은지갑주소</th>
		<th width="15%">신청금액</th>
		<th width="10%">이체수수료</th>
		<th width="15%">지급코인</th>
		<th width="15%">진행상태</th>
		<th width="25%">출금일자</th>
	</tr>
	<%
	if nSend >= 0 then
		for i=0 to nSend
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><%=aSend("send_wallet")(i)%></td>
		<td><%=fnFormatNumber(CCur(aSend("gcoin")(i)),0,0)%> EZ</td>
		<td><%=fnFormatNumber(aSend("fee")(i),0,0)%>원</td>
		<td><%=fnFormatNumber(aSend("gcoin")(i)-CCur(aSend("fee")(i)),0,0)%> EZ</td>
		<td><% If aSend("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aSend("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aSend("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aSend("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
		<td><%=aSend("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 이체정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>
<div class="lyRMsg"><span class="button base"><a href="transfer_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<div class="csCaption1">출금내역</div>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="35%">보낸지갑주소</th>
		<th width="15%">G코인</th>
		<th width="15%">진행상태</th>
		<th width="25%">전송일자</th>
	</tr>
	<%
	if nCharge >= 0 then
		for i=0 to nCharge
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><%=aCharge("receive_wallet")(i)%></td>
		<td><%=fnFormatNumber(aCharge("gcoin")(i),0,0)%> EZ</td>
		<td><% If aCharge("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aCharge("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aCharge("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aCharge("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
		<td><%=aCharge("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 충전정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>
<div class="lyRMsg"><span class="button base"><a href="charge_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<div class="csCaption1">가맹점충전내역</div>
<table class="tblMain">
	<tr>
		<th width="15%">번호</th>
		<th width="20%">신청금액</th>
		<th width="10%">충전수수료</th>
		<th width="20%">지급코인</th>
		<th width="10%">진행상태</th>
		<th width="25%">충전일자</th>
	</tr>
	<%
	if nCharge2 >= 0 then
		for i=0 to nCharge2
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><%=fnFormatNumber(aCharge2("gcoin")(i),0,0)%> EZ</td>
		<td><%=fnFormatNumber(aCharge2("fee")(i),0,0)%>원</td>
		<td><%=fnFormatNumber(CCur(aCharge2("gcoin")(i))-CCur(aCharge2("fee")(i)),0,0)%> EZ</td>
		<td><% If aCharge2("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aCharge2("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aCharge2("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aCharge2("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
		<td><%=aCharge2("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 가맹점충전정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>
<div class="lyRMsg"><span class="button base"><a href="franchise_charge_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>