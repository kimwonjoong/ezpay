<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Response.Buffer = True
Response.ContentType = "appllication/vnd.ms-excel" 
Response.CacheControl = "public"

Response.AddHeader "Content-Disposition","attachment; filename=일별가맹점충전내역.xls"

Dim sKColumn, sKeyword, nIdx
Dim fromDate,toDate,difDate
Dim aCharge, nCharge: nCharge = -1
Dim aCharge2, nCharge2: nCharge2 = -1
Dim aSend, nSend: nSend = -1
Dim aSettlement, nSettlement: nSettlement = -1

Dim wallet_no,m_year,m_month,m_day


wallet_no = fnKeywordFilter(request("wallet_no"))
m_year = fnKeywordFilter(request("m_year"))
m_month = fnKeywordFilter(request("m_month"))
m_day = fnKeywordFilter(request("m_day"))

sQueryString = "wallet_no="&wallet_no&"&m_year="&m_year&"&m_month="&m_month&"&m_day="&m_day



fnDBConn()



if wallet_no <> "" then
	

	sql = " select * from gcoin_buy where wallet_no=? and DATEPART(yyyy, regist_date) ='"&m_year&"'  and  DATEPART(mm, regist_date) ='"&m_month&"' and  DATEPART(dd, regist_date) ='"&m_day&"' "
	cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParaminput, 50, wallet_no)
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nCharge2 = fnRs2JsonArr(aCharge2, rs)
	end if
	rs.close
	ParamInit()

	
end if
Set rs = nothing

fnDBClose()


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">

<div class="csCaption1">가맹점충전내역</div>
<table class="tblMain">
	<tr>
		<th width="15%">번호</th>
		<th width="20%">신청금액</th>
		<th width="10%">충전수수료</th>
		<th width="20%">지급코인</th>
		<th width="10%">진행상태</th>
		<th width="25%">충전일자</th>
	</tr>
	<%
	if nCharge2 >= 0 then
		for i=0 to nCharge2
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><%=fnFormatNumber(aCharge2("gcoin")(i),0,0)%></td>
		<td><%=fnFormatNumber(aCharge2("fee")(i),0,0)%></td>
		<td><%=fnFormatNumber(CCur(aCharge2("gcoin")(i))-CCur(aCharge2("fee")(i)),0,0)%></td>
		<td><% If aCharge2("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aCharge2("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aCharge2("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aCharge2("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
		<td><%=aCharge2("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 가맹점충전정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>

</body>
</html>