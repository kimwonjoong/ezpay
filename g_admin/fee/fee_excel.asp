<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

Response.Buffer = True
Response.ContentType = "appllication/vnd.ms-excel" 
Response.CacheControl = "public"

Response.AddHeader "Content-Disposition","attachment; filename=수수료정산.xls"

Dim sKColumn, sKeyword,sKColumn2,sKColumn3

Dim fromDate,toDate,difDate
Dim aOutcoin, nOutcoin: nOutcoin = -1
Dim aIncoin, nIncoin: nIncoin = -1
Dim aChargecoin, nChargecoin: nChargecoin = -1
Dim aSettlementcoin, nSettlementcoin: nSettlementcoin = -1

nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKColumn2 = fnKeywordFilter(Request("optColumn2"))
sKColumn3 = fnKeywordFilter(Request("optColumn3"))

If sKColumn = "" Then
	sKColumn = year(date)
End If
If sKColumn2 = "" Then
	sKColumn2 = month(date)
End If
If sKColumn3 = "" Then
	sKColumn3 = day(date)
End If

sQueryString = "optColumn="&sKColumn&"&optColumn2="&sKColumn2&"&optColumn3="&sKColumn3

sFilter = ""

Dim r_check
r_check="0"


fnDBConn()

sql ="select company,wallet_no,convert(float,fee)*0.01 as g_fee,convert(float,charge_fee)*0.01 as c_fee from gcoin_franchise where result ='1' order by company asc"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<div class="lyMainTbl">

<table class="tblMain">
	<tr>
		<th width="10%">가맹점명</th>
		<th width="6%">지갑주소</th>
		<th width="6%">이체<br>수수료율</th>
		<th width="6%">충전<br>수수료율</th>
		<th width="10%">이체금액</th>
		<th width="10%">이체수수료</th>
		<th width="10%">충전금액</th>
		<th width="10%">충전수수료</th>
		<th width="10%">출금금액</th>
		<th width="10%">청산금액</th>
		<th width="10%">잔액</th>
	</tr>
	<%
	Dim t_charge,t_suik,t_fee1,t_fee2,t_jichul,t_chak,t_settlement,o_sum,c_sum,o_sum2,c_sum2,c_sum3,s_sum
	t_suik=0
	t_jichul=0
	t_chak=0
	t_charge=0
	t_fee1=0
	t_fee2=0
	t_settlement=0
	o_sum=0
	o_sum2=0
	c_sum=0
	c_sum2=0
	c_sum3=0
	s_sum=0
	if nData >= 0 then
		for i=0 to nData

			sql=""
			sql="select sum(convert(money,b.gcoin)) as o_sum,sum(convert(money,b.fee)) as c_sum from gcoin_transfer as b,gcoin_franchise as f where b.receive_wallet = '"&aData("wallet_no")(i)&"' and  DATEPART(yyyy, b.regist_date) ='"&sKColumn&"'  and  DATEPART(mm, b.regist_date) ='"&sKColumn2&"' and  DATEPART(dd, b.regist_date) ='"&sKColumn3&"'		group by f.wallet_no,f.fee"
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nOutcoin = fnRs2Json(aOutcoin, rs)
				o_sum =aOutcoin("o_sum")
				c_sum =aOutcoin("c_sum")
			Else
				o_sum =0
				c_sum =0
			
			end if
			rs.close
			ParamInit()


			sql=""
			sql="select sum(convert(money,b.gcoin)) as o_sum2 from gcoin_transfer as b,gcoin_franchise as f where b.send_wallet = '"&aData("wallet_no")(i)&"' and  DATEPART(yyyy, b.regist_date) ='"&sKColumn&"'  and  DATEPART(mm, b.regist_date) ='"&sKColumn2&"' and  DATEPART(dd, b.regist_date) ='"&sKColumn3&"'		group by f.wallet_no,f.fee"
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nIncoin = fnRs2Json(aIncoin, rs)
				o_sum2 =aIncoin("o_sum2")
			Else
				o_sum2 =0
		
			end if
			rs.close
			ParamInit()

			sql=""
			sql="select sum(convert(money,gcoin)) as c_sum2 from gcoin_buy where wallet_no = '"&aData("wallet_no")(i)&"' and  DATEPART(yyyy, regist_date) ='"&sKColumn&"'  and  DATEPART(mm, regist_date) ='"&sKColumn2&"' and  DATEPART(dd, regist_date) ='"&sKColumn3&"' and result='1'		group by wallet_no,fee"
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nChargecoin = fnRs2Json(aChargecoin, rs)
				c_sum3 = CCur(aChargecoin("c_sum2"))
				c_sum2 = CCur(aChargecoin("c_sum2"))*CCur(aData("c_fee")(i))
			Else
				c_sum2 = 0
				c_sum3 = 0
			end if
			rs.close
			ParamInit()

			sql=""
			sql = "select sum(convert(money,gcoin)) as s_sum from gcoin_settlement where wallet_no = '"&aData("wallet_no")(i)&"' and  DATEPART(yyyy, regist_date) ='"&sKColumn&"'  and  DATEPART(mm, regist_date) ='"&sKColumn2&"' and  DATEPART(dd, regist_date) ='"&sKColumn3&"' and result='1' group by wallet_no"

			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nSettlementcoin = fnRs2Json(aSettlementcoin, rs)
				s_sum = CCur(aSettlementcoin("s_sum"))
			Else
				s_sum = 0
			end if
			rs.close
			ParamInit()

			t_suik=t_suik+CCur(o_sum)+CCur(c_sum)
			t_jichul=t_jichul+CCur(o_sum2)
			t_fee1=t_fee1+CCur(c_sum)
			t_fee2=t_fee2+CCur(c_sum2)
			t_charge=t_charge+CCur(c_sum3)
			t_settlement=t_settlement+CCur(s_sum)
			t_chak=t_chak+(CCur(o_sum)+CCur(c_sum3))-(CCur(c_sum2)+CCur(o_sum2)+CCur(s_sum))
		
		

	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><a href="transfer_view.asp?wallet_no=<%=aData("wallet_no")(i)%>&m_year=<%=sKColumn%>&m_month=<%=sKColumn2%>&m_day=<%=sKColumn3%>"><%=aData("company")(i)%></td>
		<td><a href="transfer_view.asp?wallet_no=<%=aData("wallet_no")(i)%>&m_year=<%=sKColumn%>&m_month=<%=sKColumn2%>&m_day=<%=sKColumn3%>"><%=aData("wallet_no")(i)%></a></td>
		<td><%=aData("g_fee")(i)%>%</td>
		<td><%=aData("c_fee")(i)%>%</td>
		<td><%=fnFormatNumber(CCur(o_sum)+CCur(c_sum),0,0)%></td>
		<td><%=fnFormatNumber(CCur(c_sum),0,0)%></td>
		<td><%=fnFormatNumber(CCur(c_sum3),0,0)%></td>
		<td><%=fnFormatNumber(CCur(c_sum2),0,0)%></td>
		<td><%=fnFormatNumber(CCur(o_sum2),0,0)%></td>
		<td><%=fnFormatNumber(CCur(s_sum),0,0)%></td>
		<td><%=fnFormatNumber((CCur(o_sum)+CCur(c_sum3))-(CCur(c_sum2)+CCur(o_sum2)+CCur(s_sum)),0,0)%></td>
	
	</tr>
	<%
		next
	%>

	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 수수료내역이 없습니다.</td>
	</tr>
	<%end if%>

	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><font color="blue">total</font></td>
		<td></td>
		<td></td>
		<td></td>
		<td><font color="blue"><%=fnFormatNumber(t_suik,0,0)%></font></td>
		<td><font color="red"><%=fnFormatNumber(t_fee1,0,0)%></font></td>
		<td><font color="blue"><%=fnFormatNumber(t_charge,0,0)%></font></td>
		<td><font color="red"><%=fnFormatNumber(t_fee2,0,0)%></font></td>
		<td><font color="blue"><%=fnFormatNumber(t_jichul,0,0)%></font></td>
		<td><font color="blue"><%=fnFormatNumber(t_settlement,0,0)%></font></td>
		<td><font color="blue"><%=fnFormatNumber(t_chak,0,0)%></font></td>
	</tr>
</table>
</div>

<% fnDBClose()%>
</body>
</html>