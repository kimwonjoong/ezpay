<!--#include file="../inc/inc_top_common.asp"-->

<%
sbMemberAuthChk("A")

Dim sKColumn, sKeyword,bank_name,account_no,input_name

MENU_IDX_ = 8
sMenuCode = "charge"
PAGE_CODE_ = "charge"
PAGE_TITLE_ = "충전관리"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "optColumn="&sKColumn&"&txtKeyword="&sKeyword

sFilter = ""


fnDBConn()

sql = " select * from gcoin_bank where mode='kr' "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) Then
	bank_name = rs("bank_name")
	account_no = rs("account_no")
	input_name = rs("input_name")	
	nData = fnRs2Json(aData, rs)
else
	nIdx = ""
end if
rs.close
ParamInit()

fnDBClose()

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}

function fnUpdateChk(sMode)
{
	var sMsg = "";
	var sAction = "";
	if(sMode == "modify")
	{
		sMsg = "수정";
		sAction = "account_update_ok.asp";

		if(confirm("계좌정보를 " + sMsg + " 하시겠습니까?"))
		{
			var frm = document.getElementById("frmEdit");
			frm.hdMode.value = sMode;
			frm.action = sAction;
			frm.submit();
		}
	}
	else
	{
		alert("잘못된 접근입니다.");
		return false;
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<form name="frmEdit" id="frmEdit" method="post" action="account_update.asp">
<div class="lyMainTitle"><img src="/g_admin/images/common/icon_02.png" alt="<%=PAGE_TITLE_%>"></div>
<br>
<div class="lyMainTbl">
<table class="tblContent">
	<tr class="top">
		<th width="20%">은행명</th>
		<td colspan="3"><input type="text" name="bank_name" value="<%=bank_name%>"></td>
	</tr>
	
	<tr>
		<th>계좌번호</th>
		<td colspan="3">
					<input type="text" name="account_no" value="<%=account_no%>"></td>
	</tr>
	
	<tr>
		<th>입금자명</th>
		<td colspan="3"><input type="text" name="input_name" value="<%=input_name%>"></td>
	</tr>
	
</table>
</div>
<div class="lyRBtn">
	
	<input type="hidden" name="hdMode" id="hdMode" value="">
	<input type="hidden" name="pageNO" id="pageNO" value="<%=nPageNO%>">
	<input type="hidden" name="optColumn" id="optColumn" value="<%=sKColumn%>">
	<input type="hidden" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<%if SS_GROUP="A" then%>
		<span class="button base"><input type="button" value="수정" onclick="fnUpdateChk('modify');"></span>
		<!--span class="button base"><input type="button" value="삭제" onclick="fnUpdateChk('delete');"></span-->
	<%end if%>
	
	
</div>
</form>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>
