<% @CODEPAGE="65001" language="vbscript" %>
<!--#include file="../inc/inc_top_common.asp"-->
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<%
sbMemberAuthChk("P")

MENU_IDX_ = 8
sMenuCode = "gamang"
PAGE_CODE_ = "gamang"
PAGE_TITLE_ = "가맹점출금신청"
sListFileName = PAGE_CODE_ & "_output.asp"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function fnUpdateChk(frm)
{
	if(confirm("출금신청을 하시겠습니까?"))
	{
		return true;
	}
	return false;
}
</script>
</head> 
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<div class="lyTitle"><%=PAGE_TITLE_%></div>

<div class="csCaption1" style="float:left;width:200px;">가맹점출금신청</div>
<form name="frmEdit" id="frmEdit" method="get" action="../../g_xml/bank_insert.asp" onsubmit="return fnUpdateChk(this);">
<input type="hidden" name="hdMode" id="hdMode" value="write">
<div class="lyMainTbl">
<table class="tblContent">
	<tr class="top">
		<th>가맹점지갑번호</th>
		<td><input type="text" readonly name="Amount" id="Amount" size="4" value="1000"></td>
	</tr>
	<tr>
		<th>G코인</th>
		<td><input type="text" name="CustomerName" id="CustomerName" size="15" value="김승기"></td>
	</tr>
	
</table>

<div class="lyCBtn">
	<span class="button base"><input type="submit" value="등록"></span>
	<span class="button base"><input type="button" value="취소" onclick="history.back();"></span>
</div>
</form>

<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>