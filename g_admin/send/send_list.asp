<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

Dim sKColumn, sKeyword

Dim fromDate,toDate,difDate

Dim sKSDate : sKSDate = fnNullInit(request("txtSDate"))
Dim sKEDate : sKEDate = fnNullInit(request("txtEDate"))

MENU_IDX_ = 8
sMenuCode = "send"
PAGE_CODE_ = "send"
PAGE_TITLE_ = "송금관리"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&txtSDate="&sKSDate&"&txtEDate="&sKEDate

if isDate(sKSDate) and isDate(sKEDate) then
	if DateDiff("d", sKSDate, sKEDate) < 0 then
		sKEDate = sKSDate
	end if
else
	sKSDate = ""
	sKEDate = ""
end if

sFilter = ""

fnDBConn()

if sKeyword <> "" then
	if sKColumn = "send_wallet" then
		sFilter = sFilter & " and send_wallet like ? "
		cmd.Parameters.Append cmd.CreateParameter("@send_wallet", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "receive_wallet" Then
		sFilter = sFilter & " and receive_wallet like ? "
		cmd.Parameters.Append cmd.CreateParameter("@receive_wallet", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "handphone" then
		sFilter = sFilter & " and handphone like ? "
		cmd.Parameters.Append cmd.CreateParameter("@handphone", adVarChar, adParamInput, 250, "%" & sKeyword & "%")	
	end if
end if

if isDate(sKSDate) and isDate(sKEDate) then
	sFilter = sFilter & " and convert(date, regist_date) between ? and ? "
	cmd.Parameters.Append cmd.CreateParameter("@reg_date1", adDate, adParamInput, , sKSDate)
	cmd.Parameters.Append cmd.CreateParameter("@reg_date2", adDate, adParamInput, , sKEDate)
end if

'sqlTable = "select a.*, app_name from store a left join app_info b on(a.app_id=b.app_id) where 1=1 " & sFilter
sqlTable = "select b.*,(select handphone from member where wallet_no=b.send_wallet) as handphone from gcoin_transfer as b where 1=1 " & sFilter

'게시물 개수
sql = " select count(*) cnt from (" & sqlTable & ")a "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nRecordCount = trim(rs("cnt"))
end if
rs.close

'게시물 리스트	
sqlTable = " SELECT ROW_NUMBER() OVER(ORDER BY regist_date desc) AS RowNum, * FROM (" & sqlTable & ")a "	
sql = " ;with tbl as(" &  sqlTable & ") "
sql = sql & " SELECT TOP " & nPageSize & " * FROM tbl "
sql = sql & " WHERE RowNum >= " & (nPageNO-1)*nPageSize+1 & " and RowNum >= (SELECT MAX(RowNum)RowNum FROM (SELECT TOP " & (nPageNO-1)*nPageSize+1 & " RowNum FROM tbl ORDER BY RowNum ASC) a) ORDER BY RowNum ASC "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing

fnDBClose()

if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<div class="lyMainTitle"><img src="/g_admin/images/common/icon_03.png" alt="<%=PAGE_TITLE_%>"></div>
<br>
<div class="lyCSch">
	<form name="frmSch" id="frmSch" action="<%=sListFileName%>" method="post" onsubmit="return fnSchChk(this);">
	<span class="txtLabel">송금일자</span>
	<input name="txtSDate" type="text" id="txtSDate" size="10" value="<%=sKSDate%>" onclick="Calendar_D(this);" class="txtDate" /> ~
	<input name="txtEDate" type="text" id="txtEDate" size="10" value="<%=sKEDate%>" onclick="Calendar_D(this);" class="txtDate"  />
	<select name="optColumn" id="optColumn">
		<option value="send_wallet" <%if sKColumn="send_wallet" then%>selected<%end if%>>보낸지갑번호</option>
		<option value="receive_wallet" <%if sKColumn="receive_wallet" then%>selected<%end if%>>받은지갑번호</option>
	</select>
	<input type="text" class="txtKey" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<input type="image" src="/g_admin/images/common/btn_sch.jpg" width="62" height="23" align="absmiddle" title="검색하기" style="width:62px;height:23px">
	</form>
</div>
<div class="lyMainTbl">
<%=fnRecordCnt(nRecordCount, nPageNO, nPageCount)%>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="10%">휴대폰번호</th>
		<th width="10%">보낸지갑번호</th>
		<th width="10%">받은지갑번호</th>
		<th width="20%">EZ페이</th>
		<th width="20%">전송일자</th>
		<th width="20%">진행상태</th>
	</tr>
	<%
	Dim gcoin_sum
	gcoin_sum = 0
	if nData >= 0 then
		for i=0 to nData
	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td><%=aData("handphone")(i)%></td>
		<td><%=aData("send_wallet")(i)%></td>
		<td><%=aData("receive_wallet")(i)%></td>
		<td><%=fnFormatNumber(aData("gcoin")(i),0,0)%><% If aData("mode")(i)="B" then%>EZ<%ElseIf aData("mode")(i)="S" then%>달러<%ElseIf aData("mode")(i)="Y" then%>엔화<%ElseIf aData("mode")(i)="C" then%>위안화<%ElseIf aData("mode")(i)="P" then%>페소<%ElseIf aData("mode")(i)="H" then%>홍콩달러<%Else%>EZ<%End if%></td>
		<td><%=aData("regist_date")(i)%></td>
		<td><% If aData("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aData("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aData("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aData("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
	</tr>
	<%
		gcoin_sum = gcoin_sum + aData("gcoin")(i)
		next
	%>
	<tr>
		<td></td><td><font color=red>합계</font></td><td></td><td></td><td><font color=red><%=fnFormatNumber(gcoin_sum,0,0)%>EZ</font></td><td colspan="4"></td>
	</tr>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 전송내역이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>
<div class="lyRMsg"><span class="button base"><a href="send_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<%=fnPaging(sListFileName, nPageNO, nBlockSize, nPageCount, sQueryString)%>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>