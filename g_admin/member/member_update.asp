<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Dim sKColumn, sKeyword, nIdx
Dim fromDate,toDate,difDate,status
Dim primeum, Recomm
Dim aCharge, nCharge: nCharge = -1
Dim aSend, nSend: nSend = -1
Dim aReceive, nReceive: nReceive = -1
Dim aSettlement, nSettlement: nSettlement = -1

Dim USD,JPY,CNY,PHP,HKD,buy_fee,send_fee,settlement_fee,h_check

MENU_IDX_ = 8
PAGE_CODE_ = "member"
sListFileName = PAGE_CODE_ & "_list.asp"
nIdx = fnIdxInit(request("idx"))
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "pageNO="&nPageNO&"&optColumn="&sKColumn&"&txtKeyword="&sKeyword

sFilter = ""

if nIdx = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('회원 정보가 없습니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if

fnDBConn()

sql = " select * from member where idx=? "
cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParamInput, 4, nIdx)
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) Then
	USD = rs("USD")
	JPY = rs("JPY")
	CNY = rs("CNY")
	PHP = rs("PHP")
	HKD = rs("HKD")
	buy_fee = rs("buy_fee")
	send_fee = rs("send_fee")
	settlement_fee = rs("settlement_fee")
	status = rs("status")
	h_check = rs("h_check")
	primeum = rs("primeum")
	Recomm	= rs("Recomm")
	nData = fnRs2Json(aData, rs)
else
	nIdx = ""
end if
rs.close
ParamInit()


fnDBClose()

if nIdx = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('등록되지 않은 회원입니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}

function fnUpdateChk(sMode)
{
	var sMsg = "";
	var sAction = "";
	if(sMode == "modify") {
		sMsg = "수정";
		sAction = "member_update_ok.asp";

		if(confirm("회원정보를 " + sMsg + " 하시겠습니까?")) {
			var frm = document.getElementById("frmEdit");
			frm.hdMode.value = sMode;
			frm.action = sAction;
			frm.submit();
		}
	} else {
		alert("잘못된 접근입니다.");
		return false;
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<form name="frmEdit" id="frmEdit" method="post" action="member_update.asp">
<div class="lyMainTitle"><img src="/g_admin/images/common/title_member.jpg" alt="공지사항"></div>
<div class="lyMainTbl">
<table class="tblContent">
	<tr class="top">
		<th width="20%">지갑주소</th>
		<td colspan="3"><%=aData("wallet_no")%></td>
	</tr>
	
	<tr>
		<th>휴대폰</th>
		<td colspan="3"><%Dim aTel : aTel = fnGetTel(getJson(aData, "handphone"))%>
					<input type="text" name="handphone" value="<%=aTel(0)%><%=aTel(1)%><%=aTel(2)%>"></td>
	</tr>
	
	<tr>
		<th>EZ페이</th>
		<td colspan="3"><input type="text" name="gcoin" value="<%=aData("gcoin")%>" readonly style="background-color:#cecece; color:#828282;"> EZ</td>
	</tr>
	<tr>
		<th>달러</th>
		<td colspan="3"><input type="text" name="USD" value="<%=USD%>" readonly style="background-color:#cecece; color:#828282;"> USD</td>
	</tr>
	<tr>
		<th>엔화</th>
		<td colspan="3"><input type="text" name="JPY" value="<%=JPY%>" readonly style="background-color:#cecece; color:#828282;"> JPY</td>
	</tr>
	<tr>
		<th>위안화</th>
		<td colspan="3"><input type="text" name="CNY" value="<%=CNY%>" readonly style="background-color:#cecece; color:#828282;"> CNY</td>
	</tr>
	<tr>
		<th>페소</th>
		<td colspan="3"><input type="text" name="PHP" value="<%=PHP%>" readonly style="background-color:#cecece; color:#828282;"> PHP</td>
	</tr>
	<tr>
		<th>홍콩달러</th>
		<td colspan="3"><input type="text" name="HKD" value="<%=HKD%>" readonly style="background-color:#cecece; color:#828282;"> HKD</td>
	</tr>
	<tr>
		<th>충전수수료</th>
		<td colspan="3"><input type="text" name="buy_fee" value="<%=buy_fee%>">%</td>
	</tr>
	<tr>
		<th>송금수수료</th>
		<td colspan="3"><input type="text" name="send_fee" value="<%=send_fee%>">%</td>
	</tr>
	<tr>
		<th>청산수수료</th>
		<td colspan="3"><input type="text" name="settlement_fee" value="<%=settlement_fee%>">%</td>
	</tr>
	<tr>
		<th>프리미엄 회원</th>
		<td colspan="3"><input type="checkbox" name="primeum" value="Y" <%If primeum = "Y" then%> checked <%Else%><%End If%>> 프리미엄 회원</td>
	</tr>

	<tr>
		<th>추천</th>
		<td colspan="3">
		<%
		fnDBConn()
		SQL = " SELECT wallet_no, company FROM gcoin_franchise WHERE result = '1' ORDER BY company "
		Set RS = dbConn.EXECUTE(SQL)
		%>
		<select id="Recomm" name="Recomm" STYLE="TEXT-ALIGN:CENTER;">
		<OPTION VALUE=""> ▒▒▒ 선택 ▒▒▒ </OPTION>
		<% If RS.EOF Or RS.BOF Then %>
		<OPTION VALUE="">등록된 회원이 없습니다.</OPTION>
		<%
		Else
		Do Until RS.EOF
		%>
		<OPTION VALUE="<%=RS("wallet_no")%>" <%If Recomm = RS("wallet_no") Then%>SELECTED<%Else%><%End If%>><%=RS("wallet_no")%>&nbsp;[<%=RS("company")%>]</OPTION>
		<%
		RS.MOVENEXT
		Loop
		End If
		%>
		</select>
		<%
		fnDBClose()
		%>
		</td>
	</tr>
	
	<tr>
		<th>회원상태</th>
		<td colspan="3">
		<select name="status">
			<option value="정상" <%If status="정상" Then%>selected<%End If%>>정상</option>
			<option value="대기" <%If status="대기" Then%>selected<%End If%>>대기</option>
			<option value="차단" <%If status="차단" Then%>selected<%End If%>>차단</option>
		</select>
		</td>
	</tr>
	<tr>
		<th>휴대폰상태</th>
		<td colspan="3">
		<select name="h_check">
			<option value="정상" <%If h_check="정상" Then%>selected<%End If%>>정상</option>
			<option value="체크" <%If h_check="체크" Then%>selected<%End If%>>체크</option>
			<option value="차단" <%If h_check="차단" Then%>selected<%End If%>>차단</option>
		</select>
		</td>
	</tr>
	
	<tr>
		<th>메모</th>
		<td colspan="3"><input type="text" name="memo" size="80" value="<%=aData("memo")%>"></td>
	</tr>
</table>
</div>
<div class="lyRBtn">
	
	<input type="hidden" name="hdIdx" id="hdIdx" value="<%=nIdx%>">
	<input type="hidden" name="hdMode" id="hdMode" value="">
	<input type="hidden" name="pageNO" id="pageNO" value="<%=nPageNO%>">
	<input type="hidden" name="optColumn" id="optColumn" value="<%=sKColumn%>">
	<input type="hidden" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<%if SS_GROUP="A" then%>
		<span class="button base"><input type="button" value="수정" onclick="fnUpdateChk('modify');"></span>
		<!--span class="button base"><input type="button" value="삭제" onclick="fnUpdateChk('delete');"></span-->
	<%end if%>
	<span class="button base"><a href="<%=sListFileName%>?<%=sQueryString%>">목록</a></span>
	
</div>
</form>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>
