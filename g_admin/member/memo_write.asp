<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Dim sKColumn, sKeyword, wallet_no
Dim fromDate,toDate,difDate
Dim aCharge, nCharge: nCharge = -1
Dim aSend, nSend: nSend = -1
Dim aReceive, nReceive: nReceive = -1
Dim aSettlement, nSettlement: nSettlement = -1

MENU_IDX_ = 8
PAGE_CODE_ = "member"
sListFileName = PAGE_CODE_ & "_list.asp"
wallet_no = fnKeywordFilter(Request("wallet_no"))

sFilter = ""

if wallet_no = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('회원 정보가 없습니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if

%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}

function fnUpdateChk(sMode)
{
	var sMsg = "";
	var sAction = "";
	sAction = "memo_write_ok.asp";

	if(confirm("메모를 저장 하시겠습니까?"))
	{
		var frm = document.getElementById("frmEdit");
		frm.action = sAction;
		frm.submit();
	}
	else
	{
		alert("잘못된 접근입니다.");
		return false;
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<form name="frmEdit" id="frmEdit" method="post" action="memo_write_ok.asp">
<div class="lyMainTitle"><img src="/g_admin/images/common/title_member.jpg" alt="공지사항"></div>
<div class="lyMainTbl">
<table class="tblContent">
	<tr class="top">
		<th width="20%">지갑주소</th>
		<td colspan="3"><%=wallet_no%></td>
	</tr>
	<tr>
		<th>메모</th>
		<td colspan="3"><input type="text" name="memo" size="80" value=""></td>
	</tr>
</table>
</div>
<div class="lyRBtn">
	
	<input type="hidden" name="wallet_no" id="wallet_no" value="<%=wallet_no%>">
	<span class="button base"><input type="button" value="저장" onclick="fnUpdateChk('modify');"></span>
	<span class="button base"><a href="<%=sListFileName%>?<%=sQueryString%>">목록</a></span>
	
</div>
</form>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>