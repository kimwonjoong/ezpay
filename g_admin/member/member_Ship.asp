<!--#include file="../inc/inc_top_common.asp"-->
<!--#include file="../inc/inc_data_chk.asp"-->
<%
sbMemberAuthChk("S")
on error resume next

Dim nIdx, primeum
Dim sKColumn, sKeyword
Dim nResultCnt : nResultCnt = 0
Dim sUserID
Dim handphone : handphone = ""

MENU_IDX_ = 8
PAGE_CODE_ = "member"
sListFileName = PAGE_CODE_ & "_list.asp"
nIdx = fnIdxInit(request("idx"))
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "pageNO="&nPageNO&"&optColumn="&sKColumn&"&txtKeyword="&sKeyword
primeum	=	"Y"

if nIdx = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('회원 정보가 없습니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if

fnDBConn()

dbConn.BeginTrans

		sql = " update member set primeum=? "
		cmd.Parameters.Append cmd.CreateParameter("@primeum", adVarChar, adParaminput, 50, primeum)
		
		sql = sql & " WHERE idx=? "
		cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParaminput, 4, nIdx)

		cmd.CommandText = sql
		cmd.Execute , , adExecuteNoRecords

if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
	dbConn.RollBackTrans

	'fnErrChk()
	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('오류가 발생하여 수락에 실패하였습니다.');"
	Response.Write "history.back();"
	Response.Write "</script>"
else			
	dbConn.CommitTrans

	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('수락 완료되었습니다.');"
	Response.Write "location.href='"&sListFileName&"?" & sQueryString & "';"
	Response.Write "</script>"
end if

fnDBClose()


%>