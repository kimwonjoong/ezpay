//숫자체크
function fnNumChk(obj) 
{
	var replaceNum = FormatUnNumber(obj.value);
	var preNum = replaceNum;
	var sPattern = /[^0-9]/;
	var sRe = /[^0-9]/gi;
	if(sPattern.test(replaceNum)) 
	{
		alert("0-9 사이의 정수만 입력이 가능합니다.");
		obj.value = replaceNum.replace(sRe, "");
	}
}

//금액체크
function fnAmtChk(obj) 
{
	var replaceNum = (obj.value);
	var preNum = replaceNum;
	var sPattern = /[^0-9,]/;
	var sRe = /[^0-9,]/gi;
	if(sPattern.test(replaceNum)) 
	{
		//alert("0-9 사이의 정수만 입력이 가능합니다.");
		obj.value = replaceNum.replace(sRe, "");
	}
}

function fnSaveAs(sArgMode)
{
	var frm = document.frmSaveAs;
	frm.hdVMode.value = sArgMode;
	frm.target = "ifrSaveAs";
	//document.getElementById("ifrSaveAs").contentWindow.focus();
	frmSaveAs.focus();
	frm.submit();
}

function fnIframePrint(sArgLayout)
{
	document.getElementById("ifrSaveAs").contentWindow.document.fnPrint(sArgLayout);
}

function fnIframeFocus(sArgLayout)
{
	document.getElementById("ifrSaveAs").contentWindow.focus();
	fnIframePrint(sArgLayout);
}

//검색조건 - 사용처 전체선택/해제
function fnSelectAll(frm)
{
	if(frm)
	{
		var bFlag = true;
		if(frm.hdSelectAll.value!='N'){var bFlag = false;frm.hdSelectAll.value='N';}
		else{frm.hdSelectAll.value='Y';}
		if(frm.chkSeller)
		{	
			var obj = frm.chkSeller;
			for(var i=0; i < (obj.length==null?1:obj.length); i++)
				frm("chkSeller", i).checked = bFlag;
		}
	}
}

/* 아이디 중복체크 */
function fnUserIDChk() {
	var frm = document.frmJoin;
	if(fnDataChk(frm.txtUserID, "USERID") == false){return false;}
	else
	{
		var sChkUserID = frm.txtUserID.value;
		var popIDChk = window.open('/popup/pop_id_confirm.php?chkUserID=' + sChkUserID, 'popUserIDChk', 'left=300px,top=150px,width=400px,height=225px,resizable=no,status=no,scrollbars=no');
	}
	return false;
}

/* 우편번호 검색창 */
function fnZipcodeSch() {
	var popZipcode = window.open('/popup/pop_zipcode_sch.asp', 'popZipcodeSch', 'left=300px,top=150px,width=400px,height=330px,resizable=no,status=no,scrollbars=no');
}

/* 메일주소 자동입력 */
function fnInputEmail(obj)
{
	if(obj.value == "0")
	{
		document.getElementById("txtEmail2").value = "";
		document.getElementById("txtEmail2").readOnly = false;
	}
	else
	{
		document.getElementById("txtEmail2").readOnly = true;
		document.getElementById("txtEmail2").value = obj.value;
	}
}

//아이프레임 리사이즈
function fnIframeResize(obj)
{ 
 obj.style.height = "auto";
 var nHeight = obj.contentWindow.document.body.scrollHeight;
 //var nHeight = obj.contentWindow.document.documentElement.scrollHeight;
 obj.style.height = nHeight + 'px';
 
}
//문자열공백제거
function fnTrim(str)
{
	return str.replace(/(^[ \f\n\r\t]*)|([ \f\n\r\t]*$)/g, "");
}

//문자열 바이트수 계산
function fnGetByte(str)
{
	var sum = 0;
    var len = str.length;

    for(var i=0; i<len; i++) 
	{
        var temp_chr = escape(str.substring(i, i + 1));
        if(temp_chr.length <= 4){sum++;}
        else{sum += 2;}
    }
    return sum;
}

//지정한 자릿수 만큼 입력시 자동 포커스 이동
function fnMoveFocus(nMaxLen,obj1,obj2)
{
	var nLength = obj1.value.length;
	if(nLength == nMaxLen){obj2.focus();}
}

//자동 포커스
function fnFocus(objID)
{
	var obj = document.getElementById(objID);
	if(obj == "[object]")
	{
		obj.focus();
	}
}

//숫자에 ","넣기
function FormatNumber(value)
{
	if(!isNaN(value))
	{
		valueS = value.toString();
		var len = valueS.length;
		var valueN = "";
  
		for(var i=len;i>0;i--)
		{
  
			if(i!=len && i%3 == len%3)
			{
				valueN = ","+valueN;
				valueN = valueS.substring(i,i-1).concat(valueN);
			}
			else
				valueN = valueS.substring(i,i-1).concat(valueN);
		}
	}
	return valueN;
}

//숫자에 ","빼기
function FormatUnNumber(value)
{
	var sValue = value;
	if(sValue==null || sValue == "[undefined]")
	{
		sValue = "";
	}
	else
	{
		sValue = value.toString();
	}
 	if (sValue != '')
 	{
 		 return  sValue.replace(/,/g,''); 
	}
	else
  	{
  		return "";
  	}
}
//파일 확장자 필터링
function fnXlsFileTypeChk(sArgFileName)
{
	var sFileName = sArgFileName;

	if(sFileName == "")
	{
		alert("업로드 할 파일을 선택해주세요.");
		return false;
	}
	extArray = new Array(".xls",".xlsx");

	allowSubmit = false;
	while (sFileName.indexOf("\\") != -1)
	{
		sFileName = sFileName.slice(sFileName.indexOf("\\") + 1);
		ext = sFileName.slice(sFileName.indexOf(".")).toLowerCase();
		for (var i = 0; i < extArray.length; i++)
		{
			if (extArray[i] == ext) 
			{ 
				allowSubmit = true; 
				break; 
			}
		}
	}
	if(!allowSubmit)
	{
		alert("입력하신 파일은 업로드 할 수 없습니다!");
		return false;
	}
	return true;
}

//파일 확장자 필터링
function fnFileTypeChk(sArgFileName)
{
	var sFileName = sArgFileName;
	extArray = new Array(".htm", ".html", ".jsp", ".cgi", ".php", ".asp", ".aspx", ".exe", ".com");

	allowSubmit = false;
	while (sFileName.indexOf("\\") != -1)
	{
		sFileName = sFileName.slice(sFileName.indexOf("\\") + 1);
		ext = sFileName.slice(sFileName.indexOf(".")).toLowerCase();
		for (var i = 0; i < extArray.length; i++)
		{
			if (extArray[i] == ext) 
			{ 
				allowSubmit = true; 
				break; 
			}
		}
	}
	if(!allowSubmit)
	{
		alert("입력하신 파일은 업로드 할 수 없습니다!");
		return false;
	}
	return true;
}
//파일 확장자 필터링
function fnImgFileTypeChk(sArgFileName)
{
	var sFileName = sArgFileName;
	extArray = new Array(".jpg", ".jpeg", ".bmp", ".png", ".gif");

	allowSubmit = false;
	while (sFileName.indexOf("\\") != -1)
	{
		sFileName = sFileName.slice(sFileName.indexOf("\\") + 1);
		ext = sFileName.slice(sFileName.indexOf(".")).toLowerCase();
		for (var i = 0; i < extArray.length; i++)
		{
			if (extArray[i] == ext) 
			{ 
				allowSubmit = true; 
				break; 
			}
		}
	}
	if(!allowSubmit)
	{
		alert("이미지 파일을 선택해주세요.");
		return false;
	}
	return true;
}
/*******************************************************
	
	 ## fnDataChk(obj, type) : 유효성 체크
			
*******************************************************/
function fnDataChk(obj, sType)
{
	if(!obj)
	{
		alert('존재하지 않는 개체입니다.');
		return false;
	}
	var sPattern, sRe; 
	var sValue = fnTrim(obj.value);
	var nByte = fnGetByte(sValue);
	var nLength = (sValue).length;

	/* 아이디 : 사업자번호 */
	if(sType=="REGNO")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;

		if(obj.value == "")
		{
			alert("사업자번호를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("사업자번호는 숫자만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength != 10 || nByte != 10)
		{
			alert("사업자번호 10자리를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}/* 아이디 : 사업자번호1 */
	else if(sType=="REGNO1")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;

		if(obj.value == "")
		{
			alert("사업자번호 앞 3자리를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("사업자번호는 숫자만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength != 3 || nByte !=  3)
		{
			alert("사업자번호 앞 3자리를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 아이디 : 사업자번호2 */
	else if(sType=="REGNO2")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;

		if(obj.value == "")
		{
			alert("사업자번호 가운데 2자리를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("사업자번호는 숫자만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength != 2 || nByte !=  2)
		{
			alert("사업자번호 가운데 2자리를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 아이디 : 사업자번호3 */
	else if(sType=="REGNO3")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;

		if(obj.value == "")
		{
			alert("사업자번호 끝자리 5자리를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("사업자번호는 숫자만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength != 5 || nByte != 5)
		{
			alert("사업자번호 끝자리 5자리를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 사용자 아이디 */
	else if(sType=="USERID")
	{
		sPattern = /[^0-9a-zA-Z_]/;
		sRe = /[^0-9a-zA-Z_]/gi;
		if(obj.value == "")
		{
			alert("아이디를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("아이디는 영문 또는 숫자만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 14 || nByte > 14)
		{
			alert("아이디를 영문/숫자조합 2~14자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 사용자 아이디(로그인용) */
	else if(sType=="LOGINUSERID")
	{
		sPattern = /[^0-9a-zA-Z]/;
		sRe = /[^0-9a-zA-Z]/gi;
		if(obj.value == "")
		{
			alert("아이디를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("아이디는 영문 또는 숫자만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 14 || nByte > 14)
		{
			alert("아이디를 영문/숫자조합 2~14자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 사용자 비밀번호 */
	else if(sType=="PASSWORD")
	{
		sPattern = /[^0-9a-zA-Z._@!#$%^&*()~]/;
		sRe = /[^0-9a-zA-Z._@!#$%^&*()~]/gi;
		if(obj.value == "")
		{
			alert("비밀번호를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("비밀번호는 영문 또는 숫자만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 14 || nByte > 14)
		{
			alert("비밀번호를 영문/숫자조합 2~14자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 사용자 비밀번호(로그인용) */
	else if(sType=="LOGINPASSWORD")
	{
		sPattern = /[^0-9a-zA-Z._@!#$%^&*()~]/;
		sRe = /[^0-9a-zA-Z._@!#$%^&*()~]/gi;
		if(obj.value == "")
		{
			alert("비밀번호를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("비밀번호는 영문 또는 숫자만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 3 || nLength > 14 || nByte > 14)
		{
			alert("비밀번호를 영문/숫자조합 2~14자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 사용자명 */
	else if(sType=="USERNAME")
	{
		sPattern = /[^가-힝A-Za-z0-9 ]/;
		sRe = /[^가-힝A-Za-z0-9 ]/gi;
		if(obj.value == "")
		{
			alert("사용자명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("사용자명은 한글/영문만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 25)
		{
			alert("사용자명은 2자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 담당자명 */
	else if(sType=="MASTER")
	{
		sPattern = /[^ㄱ-ㅎ가-힝A-Za-z0-9() ]/;
		sRe = /[^ㄱ-ㅎ가-힝A-Za-z0-9() ]/gi;
		if(obj.value == "")
		{
			alert("담당자명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("담당자명은 한글/영문만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 25)
		{
			alert("담당자명은 2자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 예금주 */
	else if(sType=="ACCOUNT_NAME")
	{
		sPattern = /[^ㄱ-ㅎ가-힝A-Za-z0-9() ]/;
		sRe = /[^ㄱ-ㅎ가-힝A-Za-z0-9() ]/gi;
		if(obj.value == "")
		{
			alert("예금주명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("예금주명은 한글/영문만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 25)
		{
			alert("예금주명은 2자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 계좌번호 */
	else if(sType=="ACCOUNT_NO")
	{
		sPattern = /[^0-9-]/;
		sRe = /[^0-9-]/gi;		
		if(obj.value == "")
		{
			alert("계좌번호를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("계좌번호 형식이 잘못되었습니다."); 
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte > 50)
		{
			alert("계좌번호를 50자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 대표자명 */
	else if(sType=="CEO")
	{
		sPattern = /[^ㄱ-ㅎ가-힝A-Za-z0-9() ]/;
		sRe = /[^ㄱ-ㅎ가-힝A-Za-z0-9() ]/gi;
		if(obj.value == "")
		{
			alert("대표자명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("대표자명은 한글/영문만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 25)
		{
			alert("대표자명은 2자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 회사명 */
	else if(sType=="STORE_NAME")
	{
		sPattern = /[^가-힝 A-Za-z0-9ㄱ-ㅎ]/;
		sRe = /[^가-힝 A-Za-z0-9ㄱ-ㅎ]/gi;		
		if(obj.value == "")
		{
			alert("업체명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nLength > 25)
		{
			alert("업체명을 1자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	else if(sType=="COUPON_NAME")
	{
		sPattern = /[^가-힝 A-Za-z0-9ㄱ-ㅎ%+=_』『\[\]*()]/;
		sRe = /[^가-힝 A-Za-z0-9ㄱ-ㅎ%+=_』『\[\]*()]/gi;		
		if(obj.value == "")
		{
			alert("쿠폰명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("쿠폰명에 입력불가능한 문자가 있어 제거되었습니다.");
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nByte > 256)
		{
			alert("쿠폰명을 1자 이상 128자 이하로 입력해주세요.");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 담당자명 */
	else if(sType=="CONTACT_NAME")
	{
		sPattern = /[^가-힝A-Za-z0-9ㄱ-ㅎ]/;
		sRe = /[^가-힝A-Za-z0-9ㄱ-ㅎ]/gi;
		if(obj.value == "")
		{
			alert("담당자명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("담당자명은 한글/영문만 입력가능합니다.");
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nLength > 25)
		{
			alert("담당자명은 1자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 사용자명 */
	else if(sType=="USERNAME")
	{
		sPattern = /[^가-힝A-Za-z0-9 ]/;
		sRe = /[^가-힝A-Za-z0-9 ]/gi;
		if(obj.value == "")
		{
			alert("사용자명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("사용자명은 한글/영문만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 25)
		{
			alert("사용자명은 2자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 앱명 */
	else if(sType=="APPNAME")
	{
		sPattern = /[^가-힝A-Za-z0-9_ ]/;
		sRe = /[^가-힝A-Za-z0-9_ ]/gi;
		if(obj.value == "")
		{
			alert("앱명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		/*else if(sPattern.test(obj.value))
		{ 
			alert("앱명은 한글/영문만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}*/
		else if(nLength < 2 || nByte > 50)
		{
			alert("앱명은 2자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 사용자명 */
	else if(sType=="RECOMM_NAME")
	{
		sPattern = /[^ㄱ-ㅎ가-힝A-Za-z0-9 ]/;
		sRe = /[^ㄱ-ㅎ가-힝A-Za-z0-9 ]/gi;
		if(obj.value == "")
		{
			alert("추천인이름을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("추천인이름은 한글/영문만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 25)
		{
			alert("추천인이름을 2자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 직책 */
	else if(sType=="DUTY")
	{
		sPattern = /[^가-힝A-Za-z0-9]/;
		sRe = /[^가-힝A-Za-z0-9]/gi;
		if(obj.value == "")
		{
			alert("직책을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("직책은 한글/영문만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 10)
		{
			alert("직책은 2자 이상 10자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 지역 */
	else if(sType=="ADDR")
	{
		sPattern = /[^0-9a-zA-Z가-힝-@*&^%~/(),._ ]/;
		sRe = /[^0-9a-zA-Z가-힝-@*&^%~/(),._ ]/gi;
		if(obj.value == "")
		{
			/*
			alert("지역을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
			*/
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("지역에 입력불가능한 특수문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte > 50)
		{
			alert("지역을 25자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 주소 */
	else if(sType=="ADDRESS")
	{
		sPattern = /[^ㄱ-ㅎ0-9a-zA-Z가-힝-@*&^%~/(),._ ]/;
		sRe = /[^ㄱ-ㅎ0-9a-zA-Z가-힝-@*&^%~/(),._ ]/gi;
		if(obj.value == "")
		{
			alert("주소를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("주소에 입력불가능한 특수문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte > 100)
		{
			alert("주소를 50자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 상세주소 */
	else if(sType=="ADDRESS2")
	{
		sPattern = /[^ㄱ-ㅎ0-9a-zA-Z가-힝-!@*&^%~/(),._ ]/;
		sRe = /[^ㄱ-ㅎ0-9a-zA-Z가-힝-!@*&^%~/(),._ ]/gi;
		if(obj.value == "")
		{
			return true;
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("상세주소에 입력불가능한 특수문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte > 100)
		{
			alert("상세주소를 50자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 우편번호 */
	else if(sType=="ZIPCODE")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;		
		if(obj.value == "")
		{
			alert("우편번호를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("우편번호는 숫자만 입력가능합니다."); 
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte != 3)
		{
			alert("우편번호를 3자리로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 전화번호1 */
	else if(sType=="TEL1")
	{
		sPattern = /[0-9]/;
		sRe = /[^0-9]/gi;		
		if(obj.value == "")
		{
			alert("전화번호를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(!sPattern.test(obj.value))
		{ 
			alert("전화번호 형식이 잘못되었습니다."); 
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte < 2 || nByte > 4)
		{
			alert("전화번호를 2자리 이상 4자리 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 전화번호2 */
	else if(sType=="TEL2")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;		
		if(obj.value == "")
		{
			alert("전화번호를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("전화번호는 숫자만 입력가능합니다."); 
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte < 3 || nByte > 4)
		{
			alert("전화번호를 3자리 이상 4자리 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 전화번호3 */
	else if(sType=="TEL3")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;		
		if(obj.value == "")
		{
			alert("전화번호를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("전화번호는 숫자만 입력가능합니다."); 
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte < 4 || nByte > 4)
		{
			alert("전화번호를 4자리로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 추천인번호 */
	else if(sType=="RECOMM_PHONE")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;		
		if(obj.value == "")
		{
			alert("추천인번호를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("추천인번호는 숫자만 입력가능합니다."); 
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte < 3 || nByte > 4)
		{
			alert("추천인번호 형식이 잘못되었습니다."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 이메일 */
	else if(sType=="EMAIL")
	{
		sPattern = /[^0-9a-zA-Z@*^%~/()._-ㄱ-ㅎ가-힣]/;
		sRe = /[^0-9a-zA-Z@*^%~/()._-]ㄱ-ㅎ가-힣/gi;
		if(obj.value == "")
		{
			alert("이메일 주소를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("이메일 주소에 입력불가능한 특수문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte > 20)
		{
			alert("이메일 주소를 20자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}	
	/* 이메일0 */
	else if(sType=="EMAIL0")
	{
		sPattern = /[^0-9a-zA-Z@*^%~/()._-]/;
		sRe = /[^0-9a-zA-Z@*^%~/()._-]/gi;
		if(obj.value == "")
		{
			alert("이메일 주소를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("이메일 주소에 입력불가능한 특수문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nByte > 40)
		{
			alert("이메일 주소를 40자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 제목 */
	else if(sType=="TITLE")
	{
		sPattern = /[^0-9a-zA-Zㄱ-ㅎ가-힝@*&^%~/(),._ -]/;
		sRe = /[^0-9a-zA-Zㄱ-ㅎ가-힝@*&^%~/(),._ -]/gi;

		if(obj.value == "")
		{
			alert("제목을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("제목에 입력불가능한 문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nLength > 64 || nByte > 128)
		{
			alert("제목을 2자 이상, 40자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}	
	/* 내용 */
	else if(sType=="CONTENT")
	{
		sPattern = /[^0-9a-zA-Z가-힝!@\'*&^%~/(),.\r\n\"<>:=?; _-]/;
		sRe = /[^0-9a-zA-Z가-힝!@\'*&^%~/(),.\r\n<>\":=?; _-]/gi;

		if(obj.value == "")
		{
			alert("내용을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("내용에 입력불가능한 문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nLength > 2000 || nByte > 4000)
		{
			alert("내용을 한글 2000자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 내용 */
	else if(sType=="CONTENT_HTML")
	{
		sPattern = /[^0-9a-zA-Z가-힝!@\'*&^%~/[](),.\r\n\"<>:=?; _-]/;
		sRe = /[^0-9a-zA-Z가-힝!@\'*&^%~/[](),.\r\n<>\":=?; _-]/gi;

		if(obj.value == "")
		{
			alert("내용을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("내용에 입력불가능한 문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nLength > 2000 || nByte > 4000)
		{
			alert("내용을 한글 2000자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* MEMO_ANS */
	else if(sType=="MEMO_ANS")
	{
		sPattern = /[^0-9a-zA-Zㄱ-ㅎ가-힝@\'*&^%~/(),.\r\n\"<>:=?; _-]/;
		sRe = /[^0-9a-zA-Zㄱ-ㅎ가-힝@\'*&^%~/(),.\r\n<>\":=?; _-]/gi;

		if(obj.value == "")
		{
			/*
			alert("답변을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
			*/
			return true;
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("답변에 입력불가능한 문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nByte > 255)
		{
			alert("답변을 한글 125자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* MEMO */
	else if(sType=="MEMO")
	{
		sPattern = /[^0-9a-zA-Zㄱ-ㅎ가-힝@\'*&^%~/(),.\r\n\"<>:=?; _-]/;
		sRe = /[^0-9a-zA-Zㄱ-ㅎ가-힝@\'*&^%~/(),.\r\n<>\":=?; _-]/gi;

		if(obj.value == "")
		{
			alert("내용을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("내용에 입력불가능한 문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nByte > 255)
		{
			alert("내용을 한글 125자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* MEMO */
	else if(sType=="MENU_NAME")
	{
		sPattern = /[^0-9a-zA-Zㄱ-ㅎ가-힝@\'*&^%~/(),.\r\n\"<>:=?; _-]/;
		sRe = /[^0-9a-zA-Zㄱ-ㅎ가-힝@\'*&^%~/(),.\r\n<>\":=?; _-]/gi;

		if(obj.value == "")
		{
			alert("메뉴명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("메뉴명에 입력불가능한 문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nLength > 50 || nByte > 50)
		{
			alert("메뉴명을 한글 25자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* PUSH_MSG */
	else if(sType=="PUSH_MSG")
	{
		sPattern = /[^0-9a-zA-Z가-힝@\'*&^%~/(),.\r\n\"<>:=?; _-]/;
		sRe = /[^0-9a-zA-Z가-힝@\'*&^%~/(),.\r\n<>\":=?; _-]/gi;

		if(obj.value == "")
		{
			alert("내용을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("내용에 입력불가능한 문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 1 || nLength > 64 || nByte > 128)
		{
			alert("내용을 한글 60자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* RATE */
	else if(sType=="RATE")
	{
		sPattern = /[^0-9.]/;
		sRe = /[^0-9.]/gi;

		if(obj.value == "")
		{
			alert("수수료를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("수수료는 숫자만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(eval(obj.value) > 100)
		{
			alert("수수료를 100 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 대분류코드 */
	else if(sType=="BCODE")
	{
		sPattern = /[^A-Za-z0-9_]/;
		sRe = /[^A-Za-z0-9_]/gi;

		if(obj.value == "")
		{
			alert("대분류코드를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value) || nLength > 2)
		{ 
			alert("대분류코드를 (영문, 숫자) 2자리 이내로 입력해주세요.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}		else		{			return true;		}
	}
	/* 소분류코드 */
	else if(sType=="SCODE")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;

		if(obj.value == "")
		{
			alert("소분류코드를 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value) || nLength > 10 || obj.value > 2147483647)
		{ 
			alert("소분류코드는 숫자(2147483647 이하)로 입력해주세요.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}		else		{			return true;		}
	}
	/* 코드명 */
	else if(sType=="CODE_NAME")
	{
		sPattern = /[^0-9a-zA-Z가-힝@*&^%~/(),._ -]/;
		sRe = /[^0-9a-zA-Z가-힝@*&^%~/(),._ -]/gi;

		if(obj.value == "")
		{
			alert("코드명을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("코드명에 입력불가능한 문자가 포함되어 있습니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nByte > 30)
		{
			alert("코드명을 2자 이상, 15자 이내로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 날짜 */
	else if(sType=="DATE")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;
		if(obj.value == "")
		{
			alert("날짜를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sValue.search(/\d{4}-(0[1-9]|1[0-2])-([0-3][0-9])/)==0) 
		{ 
			var arrDay = sValue.split("-");
			var year = parseInt(arrDay[0]);
			var month = parseInt(arrDay[1].replace(/^0(\d)/g,"$1"));
			var day = parseInt(arrDay[2].replace(/^0(\d)/g,"$1"));
			var d = new Date(year,month-1,day);
			if(d.getMonth() == month-1 && d.getDate() == day ) 
			{
				return true;
			}
			else
			{
				alert("유효한 날짜가 아닙니다."); 
				obj.value = sValue;
				obj.focus();
				return false;
			}
		}
		alert("날짜를 'YYYY-MM-DD' 형태로 입력해주세요."); 
		obj.value = sValue;
		obj.focus();
		return false; 
	}
	/* 세대주명 */
	else if(sType=="NAME")
	{
		sPattern = /[^ㄱ-ㅎ가-힝A-Za-z0-9()]/;
		sRe = /[^ㄱ-ㅎ가-힝A-Za-z0-9()]/gi;
		if(obj.value == "")
		{
			alert("이름을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("이름은 한글/영문만 입력가능합니다.");  
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength < 2 || nLength > 25)
		{
			alert("이름은 2자 이상 25자 이하로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 카드번호 */
	else if(sType=="HANDPHONE")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;		
		if(obj.value == "")
		{
			alert("휴대폰번호를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("휴대폰번호는 숫자만 입력가능합니다."); 
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}
		else if(nLength != 11 || nByte != 11)
		{
			alert("휴대폰번호를 숫자 11자리로 입력해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else
		{
			return true;
		}
	}
	/* 카드번호 */
	else if(sType=="COUNT")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;		
		if(obj.value == "")
		{
			alert("수량을 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sPattern.test(obj.value))
		{ 
			alert("수량은 숫자만 입력가능합니다."); 
			obj.value = (obj.value).replace(sRe, "");
			obj.focus();
			return false; 
		}		
		else
		{
			return true;
		}
	}
	/* TYPE */
	else if(sType=="TYPE")
	{
		if(obj.value == "")
		{
			alert("구분을 선택해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}		
		else
		{
			return true;
		}
	}
	/* FILE */
	else if(sType=="FILE")
	{

		if(obj.value == "")
		{
			alert("업로드 할 파일을 선택해주세요."); 
			obj.value = sValue;
			obj.focus();
			return false; 
		}		
		else
		{
			return true;
		}
	}
	/* 날짜 */
	else if(sType=="ARRIVALDATE")
	{
		sPattern = /[^0-9]/;
		sRe = /[^0-9]/gi;
		if(obj.value == "")
		{
			alert("날짜를 입력해주세요");
			obj.value = sValue;
			obj.focus();
			return false; 
		}
		else if(sValue.search(/\d{4}-(0[1-9]|1[0-2])-([0-3][0-9])/)==0) 
		{ 
			var arrDay = sValue.split("-");
			var year = parseInt(arrDay[0]);
			var month = parseInt(arrDay[1].replace(/^0(\d)/g,"$1"));
			var day = parseInt(arrDay[2].replace(/^0(\d)/g,"$1"));
			var d = new Date(year,month-1,day);
			if(d.getMonth() == month-1 && d.getDate() == day ) 
			{
				return true;
			}
			else
			{
				alert("유효한 날짜가 아닙니다."); 
				obj.value = sValue;
				obj.focus();
				return false;
			}
		}
		alert("날짜를 'YYYY-MM-DD' 형태로 입력해주세요."); 
		obj.value = sValue;
		obj.focus();
		return false; 
	}

	alert("체크 할 데이터가 없습니다.");
	return false;
}

/* 날짜입력 */
function fnDateChk(obj, sName)
{
	if(!obj)
	{
		alert('존재하지 않는 개체입니다.');
		return false;
	}
	var sPattern, sRe; 
	var sValue = fnTrim(obj.value);
	var nByte = fnGetByte(sValue);
	var nLength = (sValue).length;

	if(!isNaN(sValue) && sValue.length==8)
	{
		sValue = sValue.substring(0,4).concat('-').concat(sValue.substring(4,6)).concat('-').concat(sValue.substring(6,8));
		obj.value = sValue;
	}

	if(obj.value == "")
	{
		alert(sName + "를 입력해주세요");
		obj.value = sValue;
		obj.focus();
		return false; 
	}
	else if(sValue.search(/\d{4}-(0[1-9]|1[0-2])-([0-3][0-9])/)==0) 
	{ 
		var arrDay = sValue.split("-");
		var year = parseInt(arrDay[0]);
		var month = parseInt(arrDay[1].replace(/^0(\d)/g,"$1"));
		var day = parseInt(arrDay[2].replace(/^0(\d)/g,"$1"));
		var d = new Date(year,month-1,day);
		if(d.getMonth() == month-1 && d.getDate() == day ) 
		{
			return true;
		}
		else
		{
			alert("유효한 날짜가 아닙니다."); 
			obj.value = sValue;
			obj.focus();
			return false;
		}
	}
	alert(sName + "를 'YYYYMMDD 또는 YYYY-MM-DD' 형태로 입력해주세요."); 
	obj.value = sValue;
	obj.focus();
	return false; 
}

/* 금액입력 */
function fnCurrencyChk(obj, sName)
{
	var sPattern, sRe; 
	var sValue, nByte, nLength;
	var sFlag = '+';
	if(!obj)
	{
		alert('존재하지 않는 개체입니다.');
		return false;
	}

	sPattern = /[^0-9]/;
	sRe = /[^0-9]/gi;

	sValue = obj.value;	
	if(sValue.substr(0,1)=='-'){sFlag = '-';sValue = sValue.substr(1, sValue.length); }

	nValue = FormatUnNumber(sValue);
	nByte = fnGetByte(nValue);
	nLength = (nValue).length;
	sValue = FormatNumber(nValue);

	if(obj.value == "")
	{
		alert(sName + "을 입력해주세요");
		obj.value = sValue;
		obj.focus();
		return false;
	}
	else if(sPattern.test(nValue))
	{ 
		alert(sName + "은 숫자만 입력가능합니다.");  
		obj.value = FormatNumber((nValue).replace(sRe, ""));
		if(sFlag == '-'){obj.value = '-' + obj.value;}
		obj.focus();
		return false; 
	}
	else if(nLength < 1 || nLength > 15 || nValue > 922337203685477)
	{
		alert(sName + "을 922,337,203,685,477 이내로 입력해주세요."); 
		obj.value = sValue;
		obj.focus();
		return false; 
	}
	else
	{
		return true;
	}
}

function fnCommDataChk(obj, sText, nByte)
{
	if(!obj)
	{
		alert('존재하지 않는 개체입니다.');
		return false;
	}
	var sPattern, sRe; 
	var sValue = fnTrim(obj.value);
	var nByte = fnGetByte(sValue);
	var nLength = (sValue).length;

	sPattern = /[^0-9a-zA-Z가-힝ㄱ-ㅎ@!*&^%~/:(),._ -]/;
	sRe = /[^0-9a-zA-Z가-힝ㄱ-ㅎ@!*&^%~/:(),._ -]/gi;

	if(obj.value == "")
	{
		return true;
	}
	else if(sPattern.test(obj.value))
	{ 
		alert(sText + "에 입력불가능한 문자가 포함되어 있습니다.");  
		obj.value = (obj.value).replace(sRe, "");
		obj.focus();
		return false; 
	}
	else if(nByte > nByte)
	{
		alert(sText +"(을)를 " + nByte + "자 이내로 입력해주세요."); 
		obj.value = sValue;
		obj.focus();
		return false; 
	}
	else
	{
		return true;
	}
}



function fnDataChk0(obj, sType)
{
	if(obj.value != "")
	{
		return fnDataChk(obj, sType);
	}
}