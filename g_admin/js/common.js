function fnMenuOver(obj, nMenuIdx)
{
	/*
	var objs = document.getElementsByName("btnMenu");
	for(var i=1; i<=(objs.length==null?1:objs.length); i++)
	{
		if(i==nMenuIdx)
		{
			objs[i-1].src="/admin/images/menu/menu0" + i + "_over.jpg";
		}
		else
		{
			objs[i-1].src="/admin/images/menu/menu0" + i + ".jpg";
		}
	}
	*/
	sMenuIdx = nMenuIdx.toString();
	if(sMenuIdx.length == 1){sMenuIdx="0"+sMenuIdx;}
	obj.src = "/admin/images/menu/menu" + sMenuIdx + "_over.jpg";
}

function fnMenuOut(obj, nMenuIdx, sPageCode)
{
	/*
	var objs = document.getElementsByName("btnMenu");
	for(var i=1; i<=(objs.length==null?1:objs.length); i++)
	{
		if(i==nMenuIdx)
		{
			objs[i-1].src="/admin/images/menu/menu0" + i + "_over.jpg";
		}
		else
		{
			objs[i-1].src="/admin/images/menu/menu0" + i + ".jpg";
		}
	}
	*/
	sMenuIdx = nMenuIdx.toString();
	if(sMenuIdx.length == 1){sMenuIdx="0"+sMenuIdx;}
	if(obj.id==("btnMenu_"+sPageCode))
	{
		obj.src="/admin/images/menu/menu" + sMenuIdx + "_over.jpg";
	}
	else
	{
		obj.src="/admin/images/menu/menu" + sMenuIdx + ".jpg";
	}
}

function fnPopImg(sSrc)
{
	if(sSrc != "")
	{
		var popImg = window.open('/admin/inc/inc_pop_img.asp?src='+escape(sSrc), 'popImg', 'left=300px,top=150px,width=600px,height=500px,resizable=yes,status=no,scrollbars=no,scrollbars=yes');
	}
}

function fnRateFormat(nValue)
{
	var nRate = nValue;
	if(isNaN(nRate) || nRate == "0" || nRate == 0)
	{
		nRate = "0.00";
	}
	else 
	{	
		nRate = nRate.toString();
		if(nRate.lastIndexOf(".")<0)	{nRate = nRate + ".00";}
		else if(nRate.substring(nRate.length-1,1)==".")	{nRate = nRate + "0";}
	}
	return nRate;
}

//SELECT BOX 초기화(오브젝트, 디폴트 옵션, 디폴트 옵션 텍스트)
function fnSelectBoxInit(obj, sValue, sText)
{	
	if(typeof(obj)=="object" && obj)
	{
		if(sText && sText != "" )
		{
			obj.options.length = null;
			var newOption = document.createElement("option");
			newOption.setAttribute("value", sValue);
			newOption.appendChild(document.createTextNode(sText));
			obj.appendChild(newOption);
		}
	}
}

//숫자체크
function onlyNumber(objVal) {
	var replaceNum = unNumberFormat(objVal);
	var preNum = replaceNum;
	if(/[^0123456789]/g.test(replaceNum)) {
		preNum = "";
		//for (i = 0; i < (replaceNum.length - 1); i++) {
		//	preNum = preNum + replaceNum.charAt(i);
		//}
		alert("숫자가 아닙니다.\n\n0-9의 정수만 입력이 가능합니다.");
	}
	if(replaceNum == 0) {
		if(preNum != "") {
			//alert("첫자리 0은 허용하지 않습니다.");
		}
		//preNum = "";
	}
	return preNum;
}
 
//금액입력
function fnInputCurrency(obj)
{
	if(obj.value == "0" || obj.value == "" || obj.value == "-")
	{
	}
	else if(obj.value == "00")
	{
		obj.value = "0";
	}
	else
	{
		//obj.value = FormatNumber(obj.value);
		var sPattern = /^[+-]?[0-9]+$/;
		var sRe = /[^+-]?[^0-9]/gi;
		var val = FormatUnNumber(obj.value);
		if(obj.value == "undefined")
		{
			obj.value = "";
		}
		else if(sPattern.test(val))
		{
			obj.value = FormatNumber(Math.round(val));
		}
		else
		{
			//alert("숫자만 입력가능합니다."); 
			val = (val).replace(sRe, "");
			obj.value = FormatNumber(Math.round(val));
		}
	}
}

//숫자체크
function fnIntInit(objVal) 
{
	var val = FormatUnNumber(objVal);
	var nResult = 0;
	if(/[^0123456789]/g.test(val)) 
	{
		nResult = 0;
	}
	else
	{
		nResult = Number(val);
	}
	return nResult;
}