<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

Response.Buffer = True
Response.ContentType = "appllication/vnd.ms-excel" 
Response.CacheControl = "public"

Response.AddHeader "Content-Disposition","attachment; filename=출금확인.xls"

Dim sKColumn, sKeyword

Dim fromDate,toDate,difDate

Dim sKSDate : sKSDate = fnNullInit(request("txtSDate"))
Dim sKEDate : sKEDate = fnNullInit(request("txtEDate"))

MENU_IDX_ = 8
sMenuCode = "output"
PAGE_CODE_ = "output"
PAGE_TITLE_ = "출금확인"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&txtSDate="&sKSDate&"&txtEDate="&sKEDate

if isDate(sKSDate) and isDate(sKEDate) then
	if DateDiff("d", sKSDate, sKEDate) < 0 then
		sKEDate = sKSDate
	end if
else
	sKSDate = ""
	sKEDate = ""
end if

sFilter = ""

fnDBConn()

if sKeyword <> "" then
	if sKColumn = "wallet_no" then
		sFilter = sFilter & " and receive_wallet like ? "
		cmd.Parameters.Append cmd.CreateParameter("@receive_wallet", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	end if
end if

if isDate(sKSDate) and isDate(sKEDate) then
	sFilter = sFilter & " and convert(date, regist_date) between ? and ? "
	cmd.Parameters.Append cmd.CreateParameter("@reg_date1", adDate, adParamInput, , sKSDate)
	cmd.Parameters.Append cmd.CreateParameter("@reg_date2", adDate, adParamInput, , sKEDate)
end if

'sqlTable = "select a.*, app_name from store a left join app_info b on(a.app_id=b.app_id) where 1=1 " & sFilter
sqlTable = "select * from gcoin_transfer where 1=1 and send_wallet ='"& Session("SS_WALLET") &"' " & sFilter



cmd.CommandText = sqlTable
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing

fnDBClose()


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>

</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">


<div class="lyMainTbl">
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="10%">지갑번호</th>
		<th width="20%">G코인</th>
		<th width="20%">입금일자</th>
		<th width="20%">진행상태</th>
	</tr>
	<%
	if nData >= 0 then
		for i=0 to nData
	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td><%=aData("receive_wallet")(i)%></td>
		<td><%=fnFormatNumber(aData("gcoin")(i),0,0)%></td>
		<td><%=aData("regist_date")(i)%></td>
		<td><% If aData("result")(i) = "0" Then %><font color="green"><a href="charge_ok.asp?idx=<%=aData("idx")(i)%>">입금대기중</a></font><%ElseIf aData("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aData("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aData("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
	</tr>
	<%
		next
	%>

	<%else%>
	<tr class="msg145">
		<td colspan="6">등록된 출금내역이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>

</body>
</html>