<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

Dim sKColumn, sKeyword

Dim fromDate,toDate,difDate

Dim sKSDate : sKSDate = fnNullInit(request("txtSDate"))
Dim sKEDate : sKEDate = fnNullInit(request("txtEDate"))

MENU_IDX_ = 8
sMenuCode = "output"
PAGE_CODE_ = "output"
PAGE_TITLE_ = "출금확인"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&txtSDate="&sKSDate&"&txtEDate="&sKEDate

if isDate(sKSDate) and isDate(sKEDate) then
	if DateDiff("d", sKSDate, sKEDate) < 0 then
		sKEDate = sKSDate
	end if
else
	sKSDate = ""
	sKEDate = ""
end if

sFilter = ""

fnDBConn()

if sKeyword <> "" then
	if sKColumn = "wallet_no" then
		sFilter = sFilter & " and receive_wallet like ? "
		cmd.Parameters.Append cmd.CreateParameter("@receive_wallet", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	end if
end if

if isDate(sKSDate) and isDate(sKEDate) then
	sFilter = sFilter & " and convert(date, regist_date) between ? and ? "
	cmd.Parameters.Append cmd.CreateParameter("@reg_date1", adDate, adParamInput, , sKSDate)
	cmd.Parameters.Append cmd.CreateParameter("@reg_date2", adDate, adParamInput, , sKEDate)
end if

'sqlTable = "select a.*, app_name from store a left join app_info b on(a.app_id=b.app_id) where 1=1 " & sFilter
sqlTable = "select * from gcoin_transfer where 1=1 and send_wallet ='"& Session("SS_WALLET") &"' " & sFilter

'게시물 개수
sql = " select count(*) cnt from (" & sqlTable & ")a "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nRecordCount = trim(rs("cnt"))
end if
rs.close

'게시물 리스트	
sqlTable = " SELECT ROW_NUMBER() OVER(ORDER BY idx desc) AS RowNum, * FROM (" & sqlTable & ")a "	
sql = " ;with tbl as(" &  sqlTable & ") "
sql = sql & " SELECT TOP " & nPageSize & " * FROM tbl "
sql = sql & " WHERE RowNum >= " & (nPageNO-1)*nPageSize+1 & " and RowNum >= (SELECT MAX(RowNum)RowNum FROM (SELECT TOP " & (nPageNO-1)*nPageSize+1 & " RowNum FROM tbl ORDER BY RowNum ASC) a) ORDER BY RowNum ASC "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing

fnDBClose()

if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script type="text/javascript">

function wallet_reg()
{
	var sMsg = "";
	var nSelect = 0;
	var nvalue="";

	var txtWallet = document.getElementById("txtWallet")
	var txtGcoin = document.getElementById("txtGcoin")


	if(txtWallet.value==""){
		window.alert("지갑번호를 입력하세요");
		return;
	}

	if(txtGcoin.value==""){
		window.alert("보낼 G코인을 입력하세요");
		return;
	}
	
	
	sMsg = txtWallet.value+"에게 "+ txtGcoin.value + "G를 보내시겠습니까?";
	

	if(confirm(sMsg))
	{
			
		var frm = document.getElementById("frmReg");
		
		frm.hdMode.value = "send";
		frm.wallet.value = txtWallet.value;
		frm.gcoin.value = txtGcoin.value;
		frm.action = "wallet_send_ok.asp";
		frm.submit();
	}
}
function fnSchChk(frm)
{
	return true;
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../g_main_include.asp"-->
<div class="lyTitle"><%=PAGE_TITLE_%></div>
<div class="lyCSch">
	<form name="frmSch" id="frmSch" action="<%=sListFileName%>" method="post" onsubmit="return fnSchChk(this);">
	<span class="txtLabel">출금일자</span>
	<input name="txtSDate" type="text" id="txtSDate" size="10" value="<%=sKSDate%>" onclick="Calendar_D(this);" class="txtDate" /> ~
	<input name="txtEDate" type="text" id="txtEDate" size="10" value="<%=sKEDate%>" onclick="Calendar_D(this);" class="txtDate"  />
	<select name="optColumn" id="optColumn">
		<option value="wallet_no" <%if sKColumn="wallet_no" then%>selected<%end if%>>지갑번호</option>
	</select>
	<input type="text" class="txtKey" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<input type="image" src="/admin/images/common/btn_sch.jpg" width="62" height="23" align="absmiddle" title="검색하기" style="width:62px;height:23px">
	</form>
</div>
<div class="lyCSch">
<form name="frmReg" id="frmReg"  method="post">
<input type="hidden" name="hdMode" id="hdMode" value="">
<input type="hidden" name="gcoin" id="gcoin" value="">
<input type="hidden" name="wallet" id="wallet" value="">
	보낼 지갑번호 : <input type="text" id="txtWallet" name="txtWallet" size="5" maxlength="4" onkeyup="onlyNumberChek(this)"/>&nbsp;&nbsp;
	보낼 G코인 : <input type="text" id="txtGcoin" name="txtGcoin" size="12" maxlength="11" 
              style="ime-mode:disabled;" onkeypress="onlyNumber();"/>G&nbsp;&nbsp;&nbsp;<span class="button base"><a href="javascript:wallet_reg();">코인보내기</a></span>
</form>
</div>
<div class="lyMainTbl">
<%=fnRecordCnt(nRecordCount, nPageNO, nPageCount)%>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="10%">지갑번호</th>
		<th width="20%">G코인</th>
		<th width="20%">입금일자</th>
		<th width="20%">진행상태</th>
	</tr>
	<%
	Dim gcoin_sum
	gcoin_sum = 0
	if nData >= 0 then
		for i=0 to nData
	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td><%=aData("receive_wallet")(i)%></td>
		<td><%=fnFormatNumber(aData("gcoin")(i),0,0)%>G</td>
		<td><%=aData("regist_date")(i)%></td>
		<td><% If aData("result")(i) = "0" Then %><font color="green"><a href="charge_ok.asp?idx=<%=aData("idx")(i)%>">입금대기중</a></font><%ElseIf aData("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aData("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aData("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
	</tr>
	<%
		gcoin_sum = gcoin_sum + aData("gcoin")(i)
		next
	%>
	<tr>
		<td><font color=red>합계</font></td><td></td><td><font color=red><%=fnFormatNumber(gcoin_sum,0,0)%>G</font></td><td colspan="2"></td>
	</tr>
	<%else%>
	<tr class="msg145">
		<td colspan="6">등록된 출금내역이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>
<div class="lyRMsg"><span class="button base"><a href="output_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<%=fnPaging(sListFileName, nPageNO, nBlockSize, nPageCount, sQueryString)%>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>