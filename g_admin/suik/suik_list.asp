<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")
Dim sKColumn, sKeyword,sKColumn2

Dim fromDate,toDate,difDate
Dim aOutcoin, nOutcoin: nOutcoin = -1
Dim aIncoin, nIncoin: nIncoin = -1
Dim aChargecoin, nChargecoin: nChargecoin = -1
Dim aSettlementcoin, nSettlementcoin: nSettlementcoin = -1

MENU_IDX_ = 8
sMenuCode = "suik"
PAGE_CODE_ = "suik"
PAGE_TITLE_ = "수수료확인"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKColumn2 = fnKeywordFilter(Request("optColumn2"))

If sKColumn = "" Then
	sKColumn = year(date)
End If
If sKColumn2 = "" Then
	sKColumn2 = month(date)
End if

sFilter = ""


Dim company,wallet_no,g_fee,c_fee


fnDBConn()

sql = "select company,wallet_no,convert(float,fee)*0.01 as g_fee,convert(float,charge_fee)*0.01 as c_fee from gcoin_franchise  where wallet_no='"&Session("SS_WALLET") &"'"
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	company = rs("company")
	wallet_no = rs("wallet_no")
	'gcoin = rs("coin_sum")
	g_fee = rs("g_fee")
	c_fee = rs("c_fee")
end if
rs.close
ParamInit()
fnDBClose()
if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
function m_submit()
{
	document.frmSch.submit();

}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../g_main_include.asp"-->
<div class="lyTitle"><%=PAGE_TITLE_%></div>
<div class="lyCSch">
	<form name="frmSch" id="frmSch" action="<%=sListFileName%>" method="post" onsubmit="return fnSchChk(this);">
	<span class="txtLabel">검색년도</span>
	<select name="optColumn" id="optColumn">
		<option value="2017" <%if sKColumn="2017" then%>selected<%end if%>>2017년</option>
		<option value="2016" <%if sKColumn="2016" then%>selected<%end if%>>2016년</option>
		<option value="2015" <%if sKColumn="2015" then%>selected<%end if%>>2015년</option>
		<option value="2014" <%if sKColumn="2014" then%>selected<%end if%>>2014년</option>
	</select>
	<select name="optColumn2" id="optColumn2" onchange="javascript:m_submit();">
		<option value="1" <%if sKColumn2="1" then%>selected<%end if%>>1월</option>
		<option value="2" <%if sKColumn2="2" then%>selected<%end if%>>2월</option>
		<option value="3" <%if sKColumn2="3" then%>selected<%end if%>>3월</option>
		<option value="4" <%if sKColumn2="4" then%>selected<%end if%>>4월</option>
		<option value="5" <%if sKColumn2="5" then%>selected<%end if%>>5월</option>
		<option value="6" <%if sKColumn2="6" then%>selected<%end if%>>6월</option>
		<option value="7" <%if sKColumn2="7" then%>selected<%end if%>>7월</option>
		<option value="8" <%if sKColumn2="8" then%>selected<%end if%>>8월</option>
		<option value="9" <%if sKColumn2="9" then%>selected<%end if%>>9월</option>
		<option value="10" <%if sKColumn2="10" then%>selected<%end if%>>10월</option>
		<option value="11" <%if sKColumn2="11" then%>selected<%end if%>>11월</option>
		<option value="12" <%if sKColumn2="12" then%>selected<%end if%>>12월</option>
	</select>


	</form>
</div>
<div class="lyMainTbl">

<table class="tblMain">
	<tr>
		<th width="5%">년도</th>
		<th width="5%">월</th>
		<th width="5%">일</th>
		<th width="12%">이체금액</th>
		<th width="12%">이체수수료</th>
		<th width="12%">충전금액</th>
		<th width="12%">충전수수료</th>
		<th width="12%">출금금액</th>
		<th width="12%">청산금액</th>
		<th width="12%">잔액</th>
	</tr>
	<%
	Dim t_suik,t_jichul,t_fee1,t_fee2,t_charge,t_settlement,t_chak,o_sum,o_sum2,c_sum,c_sum2,c_sum3,nSsum, c_recomm, recomm_buy, t_recomm_buy
	t_suik=0
	t_jichul=0
	t_settlement=0
	t_charge=0
	t_fee1=0
	t_fee2=0
	t_chak=0
	nSsum=0
	o_sum=0
	o_sum2=0
	c_sum=0
	c_sum2=0
	c_sum3=0
	c_recomm=0
	recomm_buy=0
	t_recomm_buy=0
	

	If sKColumn2 = "1" Or sKColumn2 = "3" Or sKColumn2 = "5" Or sKColumn2 = "7" Or sKColumn2 = "8" Or sKColumn2 = "10" Or sKColumn2 = "12" then
		j = 31
	ElseIf sKColumn2 = "2" then
		j = 29
	Else
		j = 30
	End If
	
		for i=1 to j

			fnDBConn()

			sql=""
			sql="select f.wallet_no,sum(convert(money,b.gcoin)) as o_sum,sum(convert(money,b.fee)) as c_sum from gcoin_transfer as b,gcoin_franchise as f where b.receive_wallet = '"&wallet_no&"' and  DATEPART(yyyy, b.regist_date) ='"&sKColumn&"'  and  DATEPART(mm, b.regist_date) ='"&sKColumn2&"' and  DATEPART(dd, b.regist_date) ='"&i&"' group by f.wallet_no,f.fee"
			'Response.write sql
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nOutcoin = fnRs2Json(aOutcoin, rs)
				o_sum2 =aOutcoin("o_sum")
				c_sum2 =aOutcoin("c_sum")
			Else
				o_sum2 =0
				c_sum2 =0
			
			end if
			rs.close
			ParamInit()

			sql=""
			sql="select f.wallet_no,sum(convert(money,b.gcoin)) as o_sum from gcoin_transfer as b,gcoin_franchise as f where b.send_wallet = '"&wallet_no&"' and  DATEPART(yyyy, b.regist_date) ='"&sKColumn&"'  and  DATEPART(mm, b.regist_date) ='"&sKColumn2&"' and  DATEPART(dd, b.regist_date) ='"&i&"' group by f.wallet_no,f.fee"
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nIncoin = fnRs2Json(aIncoin, rs)
				o_sum =aIncoin("o_sum")
			Else
				o_sum =0
			
			end if
			rs.close
			ParamInit()


			sql=""
			sql="select wallet_no,sum(convert(money,fee)) as o_sum, sum(convert(money,gcoin)) as o_sum2, sum(convert(money,recomm)) as c_recomm  from gcoin_buy where wallet_no = '"&wallet_no&"' and  DATEPART(yyyy, regist_date) ='"&sKColumn&"'  and  DATEPART(mm, regist_date) ='"&sKColumn2&"' and  DATEPART(dd, regist_date) ='"&i&"' group by wallet_no"
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nChargecoin = fnRs2Json(aChargecoin, rs)
				c_sum =aChargecoin("o_sum")
				c_sum3 =aChargecoin("o_sum2")
				c_recomm =aChargecoin("c_recomm")
			Else
				c_sum =0
				c_sum3 =0
				c_recomm =0
			
			end if
			rs.close
			ParamInit()



			sql=""
			sql = "select sum(convert(money,gcoin)) g_sum from gcoin_settlement where 1=1 and wallet_no = '"&wallet_no&"' and  DATEPART(yyyy, regist_date) ='"&sKColumn&"'  and  DATEPART(mm, regist_date) ='"&sKColumn2&"' and  DATEPART(dd, regist_date) ='"&i&"' group by wallet_no"

			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
				nSsum = trim(rs("g_sum"))
			Else
				nSsum = 0
			end if
			rs.close

			'이체 추천인 수수료
			sql=""
			sql = "select sum(convert(money,recomm)) as recomm_buy from gcoin_transfer where receive_wallet = '"& wallet_no &"' and  DATEPART(yyyy, regist_date) ='"&sKColumn&"'  and  DATEPART(mm, regist_date) ='"&sKColumn2&"' and  DATEPART(dd, regist_date) ='"&i&"' and result='1' group by receive_wallet"

			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nSettlementcoin = fnRs2Json(aSettlementcoin, rs)
				If aSettlementcoin("recomm_buy") = "" Or isnull(aSettlementcoin("recomm_buy")) Then
					recomm_buy = 0
				else
					recomm_buy = CCur(aSettlementcoin("recomm_buy"))
				End If
			Else
				recomm_buy = 0
			end if
			rs.close
			ParamInit()


			t_suik=t_suik+CCur(o_sum2)+CCur(c_sum2)
			t_fee1=t_fee1+CCur(c_sum2)
			t_fee2=t_fee2+CCur(c_sum)
			t_jichul=t_jichul+CCur(o_sum)
			t_settlement=t_settlement+CCur(nSsum)
			t_charge=t_charge+CCur(c_sum3)

			t_recomm_buy=t_recomm_buy+CCur(recomm_buy)

			t_chak=t_chak+(CCur(o_sum2)+CCur(c_sum3))-(CCur(c_sum)+CCur(o_sum)+CCur(nSsum))

	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><a href="transfer_view.asp?m_year=<%=sKColumn%>&m_month=<%=sKColumn2%>&m_day=<%=i%>&wallet_no=<%=Session("SS_WALLET")%>"><%=sKColumn%></a></td>
		<td><a href="transfer_view.asp?m_year=<%=sKColumn%>&m_month=<%=sKColumn2%>&m_day=<%=i%>&wallet_no=<%=Session("SS_WALLET")%>"><%=sKColumn2%></a></td>
		<td><a href="transfer_view.asp?m_year=<%=sKColumn%>&m_month=<%=sKColumn2%>&m_day=<%=i%>&wallet_no=<%=Session("SS_WALLET")%>"><%=i%></a></td>
		<td><%=fnFormatNumber(CCur(o_sum2),0,0)%>원</td>

		<td><font color="red"><%=fnFormatNumber(CCur(c_sum2)+CCur(recomm_buy),0,0)%>원</font></td>
		<td><%=fnFormatNumber(CCur(c_sum3),0,0)%>원</td>
		<td><font color="red"><%=fnFormatNumber(CCur(c_sum),0,0)%>원</font></td>
		<td><%=fnFormatNumber(CCur(o_sum),0,0)%>원</td>
		<td><%=fnFormatNumber(CCur(nSsum),0,0)%>원</td>
		<td><%=fnFormatNumber((CCur(o_sum2)+CCur(c_sum3))-(CCur(c_sum)+CCur(o_sum)+CCur(nSsum)),0,0)%>원</td>
	</tr>
	<%
		fnDBClose()
		next
	%>

	

	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><font color="blue">total</font></td>
		<td></td>
		<td></td>
		<td><font color="blue"><%=fnFormatNumber(t_suik,0,0)%>원</font></td>
		<td><font color="red"><%=fnFormatNumber(t_fee1,0,0)%>원</font></td>
		<td><font color="blue"><%=fnFormatNumber(t_charge,0,0)%>원</font></td>
		<td><font color="red"><%=fnFormatNumber(t_fee2,0,0)%>원</font></td>
		<td><font color="blue"><%=fnFormatNumber(t_jichul,0,0)%>원</font></td>
		<td><font color="blue"><%=fnFormatNumber(t_settlement,0,0)%>원</font></td>
		<td><font color="blue"><%=fnFormatNumber(t_chak,0,0)%>원</font></td>
	</tr>
</table>
</div>
<div class="lyRMsg"><span class="button base"><a href="suik_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>