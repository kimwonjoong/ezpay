<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->

<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 

	fnDBConn()

	'지금 핀이 사용가능한 핀인지
	strSQL =  " SELECT IDX, WALLET_NO, UUID, GCOIN, PRIVATE_KEY, BITCOIN_WALLET, BTC FROM MEMBER WHERE WALLET_NO='"& Session("SS_USERID") &"' "
	cmd.CommandText = strSQL
	Set rs = cmd.execute()
	If rs.eof Or rs.bof Then
	Else
		wallet_no = rs("wallet_no")
		UUID = rs("UUID")
		GCOIN = rs("GCOIN")
		PRIVATE_KEY = rs("PRIVATE_KEY")
		BITCOIN_WALLET = rs("BITCOIN_WALLET")
		BTC = rs("BTC")
	End If
	rs.close
	fnDBClose()
%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>EZ PAY</title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">
		<meta name="format-detection" content="telephone=no"> <!-- 아이폰에서 계좌번호를 전화번호로 인식되는걸 막음! -->

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">
		<script type="text/javascript" src="/inc/Jquery.Js"></script>
		<script>
		$(document).ready(function() {
			 jQuery.ajax({
				   ContentType:'application/x-www-form-urlencoded',
				   type:'POST',
				   url:'http://110.10.129.222/php-client/sample/address-api/GetAddress.php',
				   data:'address=<%=BITCOIN_WALLET%>',
				   dataType:'JSON', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨 
				   success : function(data) {
						 // 통신이 성공적으로 이루어졌을 때 이 함수를 타게 된다.
						balance = data["balance"];
						balance = balance / 100000000;
						
						$("#balance").html(balance);
					},
				   complete : function(data) {
						 // 통신이 실패했어도 완료가 되었을 때 이 함수를 타게 된다.
				  },
				   error : function(xhr, status, error) {
						 //alert("통신 오류가 발생 하였습니다. 잠시 후 다시 시도해 주세요!");
						$("#balance").html("0");
				   }
			 });
		});

		function wallet_name() {
			var receive_wallet = $("#receive_wallet").val();
			receive_wallet = receive_wallet.toUpperCase();

			var gcoin_won = $("#gcoin").val();
			if (receive_wallet.length == 0){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_73%>',
					text: '<%=EZ_LAN_130%>',
					callback: function(result) {
						//$("#receive_wallet").focus();
					}
				});
				
			}else{
				$.ajax({
					 url: "bitcoin_name.asp",
					 type: "POST",
					 data: { receive_wallet: receive_wallet },  

					 success: function(data){
						data_split = data.split(',');

						if (data == "") {
							modal({
								type: 'error',
								title: '<%=EZ_LAN_73%>',
								text: receive_wallet+'<%=EZ_LAN_128%>',
								callback: function(result) {
								}
							});	
						} else {
							modal({
								type: 'confirm',
								title: '<%=EZ_LAN_73%>',
								text: receive_wallet+'('+data_split[0]+')' + '<%=EZ_LAN_129%>',
								callback: function(result) {
									//alert(result);
									if(!result){
										$("#receive_wallet").val('');
										$("#receive_wallet").focus();
									}
									$("#receive_wallet").val(receive_wallet);
									$("#r_name").val(data_split[0]);
									$("#BITCOIN_WALLET_OUT").val(data_split[1]);
								}
							});	
						}
					
					 },
		 
					 complete: function(data){
					//	location.href="index.asp";
					 }
				});
			}
		}	

		function re_set(){
			$("#send_gcoin").val('');
		}
		function poloniex(){
			var buy_0 = '0';
			$.get("https://blockchain.info/ko/ticker", function(data) {
				//$('#btc_last').html(data['KRW']['last']);
				$('#btc_buy').html(data['KRW']['buy']);
				$('#btc_sell').html(data['KRW']['sell']);
				//$('#btc_symbol').html(data['KRW']['symbol']);

				var send_mode = $("#send_mode option:selected").val();	//전환방법
				var now_bitcoin = $("#balance").html();					//보유 bitcoin
				var now_gcoin = '<%=GCOIN%>';							//보유 ez
				var insert_gcoin = $("#send_gcoin").val();				//입력값

				if (send_mode == "EZ") {
					var buy_0 = $("#send_gcoin").val() / 100000000 ;	//입력값 변환값
					var buy_1 = data['KRW']['buy'] / 100000000 ;		//시세 변환값
					var buy_2 = eval(buy_0 / buy_1);					//전환값

					if (Math.round(now_gcoin) < insert_gcoin) {
						var fullStr = $("#send_gcoin").val();
						var lastChar = fullStr.slice(0,-1);
						$("#send_gcoin").val(lastChar);
						alert("보유한 EZ가 부족합니다!");
					} else {
						$("#change_icon").html("Ｂ");
						$("#change_gcoin").val(buy_2);
					}
				} else if(send_mode == "BTC") {
					var buy_0 = $("#send_gcoin").val();					//입력값 변환값
					var buy_1 = data['KRW']['sell'] / 100000000 ;		//시세 변환값
					var buy_2 = eval(buy_0 * buy_1 / 0.00000001);		//전환값

					if (now_bitcoin < insert_gcoin) {
						var fullStr = $("#send_gcoin").val();
						var lastChar = fullStr.slice(0,-1);
						$("#send_gcoin").val(lastChar);
						alert("보유한 BTC가 부족합니다!");
					} else {
						$("#change_icon").html("￦");
						$("#change_gcoin").val(buy_2);
					}
				}

			});
			setTimeout("poloniex()", 1000); //1초후 데이터 읽음
		}

		function wallet_add(){
			$('#read_bar').hide();
			$('#logdin_bar').show();
			 jQuery.ajax({
				   ContentType:'application/x-www-form-urlencoded',
				   type:'POST',
				   url:'http://110.10.129.222/php-client/sample/address-api/GenerateAddressEndpoint.php',
				   data:'wallet_no=<%=Session("SS_USERID")%>',
				   dataType:'JSON', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨 
				   success : function(data) {
						// 통신이 성공적으로 이루어졌을 때 이 함수를 타게 된다.
						//private_key = data["private"];
						//address = data["address"];
						$("#SS_USERID").val(<%=Session("SS_USERID")%>);
						$("#private_key").val(data["private"]);
						$("#address").val(data["address"]);
						BITCOIN_CREATE();
						document.bitcoin_add.action="";
						document.bitcoin_add.submit();
					$('#read_bar').show();
					$('#logdin_bar').hide();
					},
				   complete : function(data) {
						 // 통신이 실패했어도 완료가 되었을 때 이 함수를 타게 된다.

				  },
				   error : function(xhr, status, error) {
						 //alert("통신 오류가 발생 하였습니다. 잠시 후 다시 시도해 주세요!");
				   }
			 });
		}
		function BITCOIN_CREATE(){
			$("#BADD").submit(function(e) { 
				var formURL = "BITCOIN_CREATE.ASP"; 
				var formData = $(this).serialize(); 
				var currentLocation = window.location;
					$.ajax({ 
						url : formURL, 
						type: "POST", 
						data : formData, 
						success:function(data, textStatus, jqXHR) {
						alert(data);
						window.location.reload();
					},
					error: function(jqXHR, textStatus, errorThrown) {
						window.location.reload();
					}
				});
				e.preventDefault();
			});
			$("#BADD").submit();
		}
		function ezbtc_chk(flag){
			var send_mode = $("#send_mode option:selected").val();
			var now_bitcoin = $("#balance").html();
			var now_gcoin = Math.floor('<%=GCOIN%>');

			if (send_mode == "EZ") {
				if (flag > now_gcoin) {
					alert("현재 보유한 EZ가 부족합니다!");
					$("#send_gcoin").val('');
				} else {
				//전환값 처리
				}
			}
		}
		</script>
		<script type="text/javascript">
		<!--
		//[] <--문자 범위 [^] <--부정 [0-9] <-- 숫자  
		//[0-9] => \d , [^0-9] => \D
		var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
		var rgx2 = /(\d+)(\d{3})/; 

		function getNumber(obj){
			 var num01;
			 var num02;
			 num01 = obj.value;
			 num02 = num01.replace(rgx1,"");
			 num01 = setComma(num02);
			 obj.value =  num01;
		}

		function setComma(inNum){
			 var outNum;
			 outNum = inNum; 
			 while (rgx2.test(outNum)) {
				  outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
			  }
			 return outNum;
		}
		//-->
		</script>
<script>
function send_ok()
{
	var mode = $("#send_mode").val();
	var gcoin = $("#send_gcoin").val();
	var change_gcoin = $("#change_gcoin").val();
	var receive_wallet = $("#receive_wallet").val();
	var private_key = $("#private_key").val();
	var BITCOIN_WALLET = $("#BITCOIN_WALLET").val();
	var BITCOIN_WALLET_OUT = $("#BITCOIN_WALLET_OUT").val();
	var SS_USERID = $("#SS_USERID").val();

	if (receive_wallet.length == 0){
		modal({
			type: 'info',
			title: '<%=EZ_LAN_73%>',
			text: '<%=EZ_LAN_130%>',
			callback: function(result) {
				$("#receive_wallet").focus();
			}
		});
		
	}else if (mode.length == 0){
		modal({
			type: 'info',
			title: '<%=EZ_LAN_73%>',
			text: 'Select Transfer Unit',
			callback: function(result) {
				$("#mode").focus();
			}
		});
	}else if (gcoin.length == 0){
		modal({
			type: 'info',
			title: '<%=EZ_LAN_73%>',
			text: '<%=EZ_LAN_131%>',
			callback: function(result) {
				$("#gcoin").focus();
			}
		});
		
	}else if (mode == "EZ" && gcoin < 10000){
		modal({
			type: 'info',
			title: '<%=EZ_LAN_73%>',
			text: '<%=EZ_LAN_132%>',
			callback: function(result) {
				$("#gcoin").focus();
			}
		});
	}else{
		var r_name = $("#r_name").val()
		modal({
			type: 'confirm',
			title: '<%=EZ_LAN_73%>',
			text: receive_wallet+'('+r_name+')<%=EZ_LAN_133%> ' + gcoin + mode + ' <%=EZ_LAN_133_1%>',
			callback: function(result) {
				//alert(result);
				if(result){			
					$.ajax({
						 url: "bitcoin_SendTransfer_pro.asp",
						 type: "POST",
						 data: { mode:mode, gcoin:gcoin, SS_USERID:SS_USERID, change_gcoin:change_gcoin, private_key:private_key, change_gcoin:change_gcoin, BITCOIN_WALLET:BITCOIN_WALLET, BITCOIN_WALLET_OUT:BITCOIN_WALLET_OUT},  

						 success: function(data){
							 if(data != ""){						 
								modal({
									type: 'success',
									title: '<%=EZ_LAN_73%>',
									text: '<%=EZ_LAN_134%>',
									callback: function(result) {
										location.href="bitcoin_SendTransfer.asp";
									}
								});		
							 }else{								 
								modal({
									type: 'error',
									title: '<%=EZ_LAN_73%>',
									text: data,
									callback: function(result) {
										location.href="bitcoin_SendTransfer.asp";
									}
								});				
							 }
						 },
			 
						 complete: function(data){
						//	location.href="index.asp";
						 }
					});
				}
			}
		});	
	
	}	
}
</script>

	</head>
	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">
		<script> poloniex(); </script>

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active">비트코인 송금하기</li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- banner start -->
				<!-- section start -->
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title">비트코인 송금하기</h1>
					<div class="separator-2"></div>
					<form id="BADD" name="BADD" method="post">
					
						<input type="hidden" id="r_name" name="r_name">	<!-- 받는 사람 bitcoin wallet 주소 -->
						<input type="hidden" id="private_key" name="private_key" value="<%=private_key%>">	<!-- 받는 사람 bitcoin wallet 주소 -->
						<input type="hidden" id="BITCOIN_WALLET_OUT" name="BITCOIN_WALLET_OUT">	<!-- 받는 사람 bitcoin wallet 주소 -->
					</form>
					<form role="form">
						<div class="form-group">
							<label for="exampleInputPassword1">보유 비트코인</label>
							<span style="text-align:center; color:#ff9900" id="balance" name="balance"></span> BTC
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">보유 EZ</label>
							<span style="text-align:center; color:#ff9900"><%=fnFormatNumber(GCOIN,0,0)%></span> EZ
						</div>
					<%If BITCOIN_WALLET = "" Or isnull(BITCOIN_WALLET) then%>
						<div class="form-group">
							<label for="exampleInputPassword1">비트코인 지갑생성</label>
							<span style="padding-left:20px;" id="read_bar"><button type="button" class="btn btn-default" onclick="javascript:wallet_add();" required>지갑주소생성</button></span>
							<span style="float:left; width:100%; margin-top:-25px; padding-left:140px; display:none;" id="logdin_bar"><img src="/inc/ajax-loader.gif" style="border:0px; "></span>
						</div>
					<%else%>
						<div class="form-group">
							<label for="exampleInputPassword1">보내는 지갑주소</label>
							<input type="text" class="form-control" id="BITCOIN_WALLET" name="BITCOIN_WALLET" value="<%=BITCOIN_WALLET%>" required readonly >
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">송금전환선택</label>
							<select class="form-control" name="send_mode" id="send_mode" required style="background-color:#575757;" onchange="re_set();">
								<option value="EZ">EZ </option>
								<option value="BTC">BTC</option>
							</select>
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">받는 지갑번호</label>
							<input type="text" class="form-control" id="receive_wallet" name="receive_wallet" STYLE="WIDTH:100%;" onblur="wallet_name();">
						</div>

						
						<div class="form-group">
							<label for="exampleInputPassword1">송금금액</label>
							<input type="tel" class="form-control" id="send_gcoin" name="send_gcoin" required  onblur="ezbtc_chk(this.value);">
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">전환값</label>
							<span style="font-size:11px;">[
								BUY : <span style="color:#ff9900;" id="btc_buy" name="btc_buy"></span>&nbsp;&nbsp;&nbsp;
								SELL : <span style="color:#ff9900;" id="btc_sell" name="btc_sell"></span>
							]</span>
							<span id="change_icon" style="float:left; width:30px;"> </span><input type="tel" class="form-control" id="change_gcoin" name="change_gcoin" required onchange="getNumber(this);" onkeyup="getNumber(this);" readonly>
						</div>

						<button type="button" class="btn btn-default"  onclick="send_ok();">송금하기</button>
					<%End If%>
					</form>
				</div>
			</div>
			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
