USE [master]
GO
/****** Object:  Database [ezpay]    Script Date: 07/19/2017 11:38:59 ******/
CREATE DATABASE [ezpay] ON  PRIMARY 
( NAME = N'ezpay', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\ezpay.mdf' , SIZE = 26624KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'ezpay_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.MSSQLSERVER\MSSQL\DATA\ezpay_log.ldf' , SIZE = 20096KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [ezpay] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [ezpay].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [ezpay] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [ezpay] SET ANSI_NULLS OFF
GO
ALTER DATABASE [ezpay] SET ANSI_PADDING OFF
GO
ALTER DATABASE [ezpay] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [ezpay] SET ARITHABORT OFF
GO
ALTER DATABASE [ezpay] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [ezpay] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [ezpay] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [ezpay] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [ezpay] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [ezpay] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [ezpay] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [ezpay] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [ezpay] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [ezpay] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [ezpay] SET  DISABLE_BROKER
GO
ALTER DATABASE [ezpay] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [ezpay] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [ezpay] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [ezpay] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [ezpay] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [ezpay] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [ezpay] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [ezpay] SET  READ_WRITE
GO
ALTER DATABASE [ezpay] SET RECOVERY FULL
GO
ALTER DATABASE [ezpay] SET  MULTI_USER
GO
ALTER DATABASE [ezpay] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [ezpay] SET DB_CHAINING OFF
GO
USE [ezpay]
GO
/****** Object:  Table [dbo].[gcoin_qna]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_qna](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[question] [varchar](max) NOT NULL,
	[answer] [varchar](max) NOT NULL,
	[reg_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_log]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_log](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[send_wallet_no] [varchar](50) NULL,
	[gcoin] [varchar](50) NOT NULL,
	[gcoin_price] [varchar](50) NULL,
	[gcoin_case] [char](1) NOT NULL,
	[reg_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_franchise]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_franchise](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[password] [varchar](50) NULL,
	[old_wallet] [varchar](50) NULL,
	[fee] [varchar](50) NULL,
	[charge_fee] [varchar](50) NULL,
	[gcoin] [varchar](50) NOT NULL,
	[company] [varchar](50) NOT NULL,
	[tel] [varchar](50) NOT NULL,
	[email] [varchar](256) NULL,
	[result] [varchar](50) NOT NULL,
	[regist_date] [datetime] NOT NULL,
 CONSTRAINT [PK_gcoin_franchise] PRIMARY KEY CLUSTERED 
(
	[wallet_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_exchange]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_exchange](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[exchange_name] [varchar](50) NULL,
	[rate] [float] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_buy_tmp]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_buy_tmp](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[gcoin] [varchar](50) NOT NULL,
	[fee] [varchar](50) NULL,
	[input_name] [varchar](50) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[mode] [varchar](50) NULL,
	[result] [char](10) NOT NULL,
	[regist_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_buy]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_buy](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[gcoin] [varchar](50) NOT NULL,
	[fee] [varchar](50) NULL,
	[input_name] [varchar](50) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[mode] [varchar](50) NULL,
	[result] [char](10) NOT NULL,
	[regist_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_bank]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_bank](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[bank_name] [nvarchar](50) NOT NULL,
	[account_no] [varchar](50) NOT NULL,
	[input_name] [nvarchar](50) NOT NULL,
	[mode] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[franchise_settlement]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[franchise_settlement](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[gcoin] [varchar](50) NOT NULL,
	[company] [varchar](50) NULL,
	[tel] [varchar](50) NULL,
	[bank_name] [varchar](50) NOT NULL,
	[account_no] [varchar](50) NOT NULL,
	[input_name] [varchar](50) NOT NULL,
	[result] [varchar](50) NOT NULL,
	[regist_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[errorlog]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[errorlog](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[aspcode] [varchar](50) NULL,
	[errornumber] [varchar](50) NULL,
	[source] [varchar](200) NULL,
	[category] [varchar](200) NULL,
	[errorfile] [varchar](200) NULL,
	[line] [int] NULL,
	[description] [varchar](500) NULL,
	[aspdescription] [varchar](500) NULL,
	[referer] [varchar](8000) NULL,
	[form] [varchar](8000) NULL,
	[querystring] [varchar](8000) NULL,
	[cookie] [varchar](8000) NULL,
	[ip] [varchar](50) NULL,
	[regdate] [datetime] NOT NULL,
	[fixdate] [datetime] NULL,
	[site] [varchar](100) NULL,
	[agent] [varchar](8000) NULL,
	[codepage] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[bitcoin_transfer]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bitcoin_transfer](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[send_bitcoin] [varchar](50) NULL,
	[receive_bitcoin] [varchar](50) NULL,
	[coin] [varchar](50) NULL,
	[status] [char](1) NULL,
	[reg_date] [datetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[bank_test]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bank_test](
	[idx] [int] NOT NULL,
	[Amount] [varchar](50) NOT NULL,
	[CustomerName] [varchar](50) NOT NULL,
	[bank_code] [varchar](50) NOT NULL,
	[regdate] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[bank_list]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[bank_list](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[bank_name] [nvarchar](50) NOT NULL,
	[mode] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[admin_member]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[admin_member](
	[id] [varchar](50) NOT NULL,
	[password] [varchar](255) NOT NULL,
	[name] [varchar](50) NOT NULL,
	[m_group] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[wallet_no] [varchar](50) NULL,
	[reg_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_settlement3]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_settlement3](
	[idx] [int] NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[gcoin] [varchar](50) NOT NULL,
	[bank_code] [varchar](50) NULL,
	[bank_name] [varchar](50) NOT NULL,
	[account_no] [varchar](50) NOT NULL,
	[input_name] [varchar](50) NOT NULL,
	[mode] [varchar](50) NULL,
	[result] [char](10) NOT NULL,
	[TR_DATE] [char](8) NULL,
	[SEQ] [numeric](6, 0) NULL,
	[regist_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gpoint_transfer_log]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gpoint_transfer_log](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[gpoint] [varchar](50) NOT NULL,
	[reg_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_transfer_log]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_transfer_log](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[gcoin] [varchar](50) NOT NULL,
	[change_coin] [varchar](50) NOT NULL,
	[mode] [varchar](50) NOT NULL,
	[s_mode] [varchar](50) NOT NULL,
	[regist_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[point_transfer]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[point_transfer](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[send_wallet] [varchar](50) NOT NULL,
	[receive_wallet] [varchar](50) NOT NULL,
	[point] [money] NOT NULL,
	[reg_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[pbcatvld]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pbcatvld](
	[pbv_name] [varchar](30) NOT NULL,
	[pbv_vald] [varchar](254) NULL,
	[pbv_type] [smallint] NULL,
	[pbv_cntr] [int] NULL,
	[pbv_msg] [varchar](254) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [pbcatv_x] ON [dbo].[pbcatvld] 
(
	[pbv_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pbcattbl]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pbcattbl](
	[pbt_tnam] [char](129) NOT NULL,
	[pbt_tid] [int] NULL,
	[pbt_ownr] [char](129) NOT NULL,
	[pbd_fhgt] [smallint] NULL,
	[pbd_fwgt] [smallint] NULL,
	[pbd_fitl] [char](1) NULL,
	[pbd_funl] [char](1) NULL,
	[pbd_fchr] [smallint] NULL,
	[pbd_fptc] [smallint] NULL,
	[pbd_ffce] [char](18) NULL,
	[pbh_fhgt] [smallint] NULL,
	[pbh_fwgt] [smallint] NULL,
	[pbh_fitl] [char](1) NULL,
	[pbh_funl] [char](1) NULL,
	[pbh_fchr] [smallint] NULL,
	[pbh_fptc] [smallint] NULL,
	[pbh_ffce] [char](18) NULL,
	[pbl_fhgt] [smallint] NULL,
	[pbl_fwgt] [smallint] NULL,
	[pbl_fitl] [char](1) NULL,
	[pbl_funl] [char](1) NULL,
	[pbl_fchr] [smallint] NULL,
	[pbl_fptc] [smallint] NULL,
	[pbl_ffce] [char](18) NULL,
	[pbt_cmnt] [varchar](254) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [pbcatt_x] ON [dbo].[pbcattbl] 
(
	[pbt_tnam] ASC,
	[pbt_ownr] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pbcatfmt]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pbcatfmt](
	[pbf_name] [varchar](30) NOT NULL,
	[pbf_frmt] [varchar](254) NULL,
	[pbf_type] [smallint] NULL,
	[pbf_cntr] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [pbcatf_x] ON [dbo].[pbcatfmt] 
(
	[pbf_name] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pbcatedt]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pbcatedt](
	[pbe_name] [varchar](30) NOT NULL,
	[pbe_edit] [varchar](254) NULL,
	[pbe_type] [smallint] NULL,
	[pbe_cntr] [int] NULL,
	[pbe_seqn] [smallint] NOT NULL,
	[pbe_flag] [int] NULL,
	[pbe_work] [char](32) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [pbcate_x] ON [dbo].[pbcatedt] 
(
	[pbe_name] ASC,
	[pbe_seqn] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pbcatcol]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[pbcatcol](
	[pbc_tnam] [char](129) NOT NULL,
	[pbc_tid] [int] NULL,
	[pbc_ownr] [char](129) NOT NULL,
	[pbc_cnam] [char](129) NOT NULL,
	[pbc_cid] [smallint] NULL,
	[pbc_labl] [varchar](254) NULL,
	[pbc_lpos] [smallint] NULL,
	[pbc_hdr] [varchar](254) NULL,
	[pbc_hpos] [smallint] NULL,
	[pbc_jtfy] [smallint] NULL,
	[pbc_mask] [varchar](31) NULL,
	[pbc_case] [smallint] NULL,
	[pbc_hght] [smallint] NULL,
	[pbc_wdth] [smallint] NULL,
	[pbc_ptrn] [varchar](31) NULL,
	[pbc_bmap] [char](1) NULL,
	[pbc_init] [varchar](254) NULL,
	[pbc_cmnt] [varchar](254) NULL,
	[pbc_edit] [varchar](31) NULL,
	[pbc_tag] [varchar](254) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
CREATE UNIQUE NONCLUSTERED INDEX [pbcatc_x] ON [dbo].[pbcatcol] 
(
	[pbc_tnam] ASC,
	[pbc_ownr] ASC,
	[pbc_cnam] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[memo]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[memo](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[memo] [text] NOT NULL,
	[reg_date] [datetime] NOT NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[member_msg_log]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[member_msg_log](
	[idx] [int] NOT NULL,
	[comm_id] [varchar](50) NOT NULL,
	[msg_idx] [int] NOT NULL,
	[div_idx] [int] NOT NULL,
	[try_idx] [int] NOT NULL,
	[send_cnt] [int] NOT NULL,
	[log_data] [varchar](8000) NOT NULL,
	[phone_list] [varchar](8000) NULL,
	[regid_list] [varchar](8000) NULL,
	[send_date] [datetime] NOT NULL,
	[msg_content] [varchar](500) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[member_msg_list_tmp]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[member_msg_list_tmp](
	[idx] [int] NOT NULL,
	[comm_id] [varchar](50) NOT NULL,
	[session_id] [varchar](50) NOT NULL,
	[m_userid] [varchar](50) NOT NULL,
	[store_code] [varchar](50) NULL,
	[handphone] [varchar](50) NOT NULL,
	[insert_date] [datetime] NOT NULL,
	[push_key] [varchar](30) NOT NULL,
	[msg_type] [char](1) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[member_msg_list]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[member_msg_list](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[comm_id] [varchar](50) NOT NULL,
	[msg_idx] [int] NOT NULL,
	[m_userid] [varchar](50) NOT NULL,
	[store_code] [varchar](50) NULL,
	[msg_type] [char](1) NOT NULL,
	[handphone] [varchar](50) NOT NULL,
	[status] [char](1) NOT NULL,
	[read_date] [datetime] NULL,
	[send_date] [datetime] NULL,
	[insert_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[member_msg_data]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[member_msg_data](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[comm_id] [varchar](50) NOT NULL,
	[m_userid] [varchar](50) NOT NULL,
	[store_code] [varchar](50) NULL,
	[push_key] [varchar](30) NOT NULL,
	[msg_cnt] [int] NOT NULL,
	[msg_send_cnt] [int] NOT NULL,
	[msg_type] [char](1) NOT NULL,
	[send_chk] [char](1) NOT NULL,
	[contents] [varchar](max) NOT NULL,
	[insert_date] [datetime] NOT NULL,
	[send_ip] [varchar](15) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_CTBX_SEQ]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_CTBX_SEQ](
	[TR_DATE] [char](8) NOT NULL,
	[SEND_ID] [varchar](20) NOT NULL,
	[TR_SEQ] [numeric](9, 0) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_CTBX_ERRCD]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_CTBX_ERRCD](
	[BANK_CD] [char](3) NOT NULL,
	[ERR_CD] [varchar](4) NOT NULL,
	[ERR_MSG] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_CTBX_CODE]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_CTBX_CODE](
	[CATEGORY1] [varchar](50) NOT NULL,
	[CATEGORY2] [varchar](50) NOT NULL,
	[CODE] [varchar](20) NOT NULL,
	[VALUE] [varchar](20) NULL,
	[BIGO] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_CTBX_BNK_ORG]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_CTBX_BNK_ORG](
	[BANK_GUBN] [char](2) NOT NULL,
	[BANK_CD] [varchar](3) NOT NULL,
	[PDT_ID] [char](4) NOT NULL,
	[ORG_CD] [varchar](10) NOT NULL,
	[ORG_CD_SUB] [varchar](10) NULL,
	[ORG_NM] [varchar](30) NULL,
	[BIZ_NO] [varchar](10) NULL,
	[SEND_ID] [varchar](10) NULL,
	[RECV_ID] [varchar](10) NULL,
	[IP] [varchar](20) NULL,
	[PORT] [numeric](6, 0) NULL,
	[ID_CD] [varchar](10) NULL,
	[CHNG_DT] [char](8) NULL,
	[CHNG_TM] [char](6) NULL,
	[OPEN_DT] [char](8) NULL,
	[OPEN_TM] [char](6) NULL,
	[IS_OPEN] [char](1) NULL,
	[NOTICE_DT] [char](8) NULL,
	[NOTICE_TM] [char](6) NULL,
	[IS_NOTICE] [char](1) NULL,
	[ACCT_DT] [char](8) NULL,
	[TRAN_DT] [char](8) NULL,
	[IS_REG] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_BANK_TRAN_LIST]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_BANK_TRAN_LIST](
	[TR_DATE] [char](8) NOT NULL,
	[SEQ] [numeric](6, 0) NOT NULL,
	[PAY_DATE] [char](8) NOT NULL,
	[BANK_CD] [char](3) NOT NULL,
	[ORG_CD] [varchar](10) NOT NULL,
	[ACCT_NO] [varchar](16) NOT NULL,
	[TRN_TIME] [char](6) NULL,
	[TRN_BANK_CD] [char](3) NULL,
	[TRN_ACCT_NO] [varchar](16) NULL,
	[TR_AMT] [numeric](13, 0) NULL,
	[TR_BALANCE] [numeric](13, 0) NULL,
	[IN_NAME] [varchar](20) NULL,
	[IN_OUT_GBN] [char](2) NULL,
	[ENTRY_DATE] [char](14) NULL,
	[ENTRY_IDNO] [varchar](20) NULL,
	[ERP_PROC_YN] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_BANK_TRAN]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_BANK_TRAN](
	[TR_DATE] [char](8) NOT NULL,
	[SEQ] [numeric](6, 0) NOT NULL,
	[BANK_CD] [char](3) NOT NULL,
	[ORG_CD] [varchar](10) NOT NULL,
	[OUT_BANK_CD] [char](3) NOT NULL,
	[OUT_ACCT_NO] [varchar](16) NOT NULL,
	[IN_BANK_CD] [char](3) NOT NULL,
	[IN_ACCT_NO] [varchar](16) NOT NULL,
	[TR_AMT] [numeric](13, 0) NOT NULL,
	[BAL_SIGN] [char](1) NULL,
	[BAL_AMT] [numeric](13, 0) NULL,
	[OUT_REMARK] [varchar](16) NULL,
	[IN_REMARK] [varchar](16) NULL,
	[CMS_CD] [varchar](13) NULL,
	[PROC_FLAG] [char](1) NULL,
	[ERROR_CD] [varchar](4) NULL,
	[UNABLE] [char](1) NULL,
	[SEND_DATETIME] [char](14) NULL,
	[RECV_DATETIME] [char](14) NULL,
	[ENTRY_DATE] [char](14) NULL,
	[ENTRY_IDNO] [varchar](20) NULL,
	[ERP_PROC_YN] [char](1) NULL,
	[settlement_idx] [bigint] NULL,
 CONSTRAINT [PK_TB_BANK_TRAN] PRIMARY KEY CLUSTERED 
(
	[TR_DATE] ASC,
	[SEQ] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_BANK_INQUIRY]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_BANK_INQUIRY](
	[TR_DATE] [char](8) NOT NULL,
	[SEQ] [numeric](6, 0) NOT NULL,
	[BANK_CD] [char](3) NOT NULL,
	[ORG_CD] [varchar](10) NOT NULL,
	[TRN_BANK_CD] [char](3) NULL,
	[TRN_ACCT_NO] [varchar](16) NULL,
	[TR_AMT] [numeric](13, 0) NULL,
	[CORP_NO] [varchar](10) NULL,
	[ACCT_NM] [varchar](20) NULL,
	[PROC_FLAG] [char](1) NULL,
	[ERROR_CD] [varchar](4) NULL,
	[SEND_DATETIME] [char](14) NULL,
	[RECV_DATETIME] [char](14) NULL,
	[ENTRY_DATE] [char](14) NULL,
	[ENTRY_IDNO] [varchar](20) NULL,
	[ERP_PROC_YN] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TB_BANK_BALANCE]    Script Date: 07/19/2017 11:39:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TB_BANK_BALANCE](
	[TR_DATE] [char](8) NOT NULL,
	[SEQ] [numeric](6, 0) NOT NULL,
	[BANK_CD] [char](3) NOT NULL,
	[ORG_CD] [varchar](10) NOT NULL,
	[ACCT_NO] [varchar](16) NULL,
	[PROC_FLAG] [char](1) NULL,
	[ERROR_CD] [varchar](4) NULL,
	[SEND_DATETIME] [varchar](14) NULL,
	[RECV_DATETIME] [varchar](14) NULL,
	[LAST_BALANCE] [numeric](15, 0) NULL,
	[CAN_BALANCE] [numeric](15, 0) NULL,
	[ENTRY_DATE] [char](14) NULL,
	[ENTRY_IDNO] [varchar](20) NULL,
	[ERP_PROC_YN] [char](1) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[rtn_seq]    Script Date: 07/19/2017 11:39:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create function [dbo].[rtn_seq] ()
returns INT
AS
BEGIN

Return(
 Select CAST(TR_SEQ AS int)
 From dbo.TB_CTBX_SEQ where datediff(day,TR_DATE,getdate())=0
)
END
GO
/****** Object:  Table [dbo].[member]    Script Date: 07/19/2017 11:39:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[member](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[password] [varchar](50) NULL,
	[s_name] [varchar](50) NULL,
	[handphone] [varchar](50) NOT NULL,
	[lang] [varchar](50) NULL,
	[uuid] [varchar](128) NULL,
	[gcoin] [varchar](50) NOT NULL,
	[private_key] [varchar](100) NULL,
	[bitcoin_wallet] [varchar](100) NULL,
	[BTC] [varchar](50) NULL,
	[USD] [varchar](50) NULL,
	[JPY] [varchar](50) NULL,
	[PHP] [varchar](50) NULL,
	[CNY] [varchar](50) NULL,
	[HKD] [varchar](50) NULL,
	[SGD] [varchar](50) NULL,
	[MYR] [varchar](50) NULL,
	[send_fee] [varchar](50) NULL,
	[settlement_fee] [varchar](50) NULL,
	[buy_fee] [varchar](50) NULL,
	[point] [varchar](50) NULL,
	[shopping_point] [varchar](50) NULL,
	[reg_id] [varchar](max) NOT NULL,
	[reg_id2] [varchar](max) NULL,
	[franchise_check] [varchar](50) NOT NULL,
	[h_check] [varchar](50) NULL,
	[status] [varchar](50) NULL,
	[memo] [varchar](512) NULL,
	[reg_date] [datetime] NOT NULL,
 CONSTRAINT [PK_member] PRIMARY KEY CLUSTERED 
(
	[wallet_no] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_transfer]    Script Date: 07/19/2017 11:39:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_transfer](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[gcoin] [varchar](50) NOT NULL,
	[send_wallet] [varchar](50) NOT NULL,
	[receive_wallet] [varchar](50) NOT NULL,
	[fee] [varchar](50) NULL,
	[mode] [varchar](50) NULL,
	[result] [varchar](50) NOT NULL,
	[regist_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_settlement2]    Script Date: 07/19/2017 11:39:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_settlement2](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[gcoin] [varchar](50) NOT NULL,
	[bank_code] [varchar](50) NULL,
	[bank_name] [nvarchar](50) NOT NULL,
	[bank_detail] [nvarchar](128) NULL,
	[account_no] [varchar](50) NOT NULL,
	[input_name] [nvarchar](50) NOT NULL,
	[mode] [varchar](50) NULL,
	[result] [char](10) NOT NULL,
	[TR_DATE] [char](8) NULL,
	[SEQ] [numeric](6, 0) NULL,
	[regist_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[gcoin_settlement]    Script Date: 07/19/2017 11:39:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[gcoin_settlement](
	[idx] [int] IDENTITY(1,1) NOT NULL,
	[wallet_no] [varchar](50) NOT NULL,
	[gcoin] [varchar](50) NOT NULL,
	[bank_name] [nvarchar](50) NOT NULL,
	[bank_detail] [nvarchar](128) NULL,
	[bank_code] [varchar](3) NULL,
	[account_no] [varchar](50) NOT NULL,
	[input_name] [nvarchar](50) NOT NULL,
	[mode] [varchar](50) NULL,
	[result] [char](10) NOT NULL,
	[TR_DATE] [char](8) NULL,
	[SEQ] [numeric](6, 0) NULL,
	[regist_date] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Default [DF_gcoin_qna_reg_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_qna] ADD  CONSTRAINT [DF_gcoin_qna_reg_date]  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF_gcoin_franchise_fee]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_franchise] ADD  CONSTRAINT [DF_gcoin_franchise_fee]  DEFAULT ((3)) FOR [fee]
GO
/****** Object:  Default [DF_gcoin_franchise_charge_fee]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_franchise] ADD  CONSTRAINT [DF_gcoin_franchise_charge_fee]  DEFAULT ((3)) FOR [charge_fee]
GO
/****** Object:  Default [DF_gcoin_franchise_gcoin]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_franchise] ADD  CONSTRAINT [DF_gcoin_franchise_gcoin]  DEFAULT ('0') FOR [gcoin]
GO
/****** Object:  Default [DF_gcoin_franchise_result]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_franchise] ADD  CONSTRAINT [DF_gcoin_franchise_result]  DEFAULT ('0') FOR [result]
GO
/****** Object:  Default [DF_gcoin_franchise_regist_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_franchise] ADD  CONSTRAINT [DF_gcoin_franchise_regist_date]  DEFAULT (getdate()) FOR [regist_date]
GO
/****** Object:  Default [DF_gcoin_buy_fee]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_buy] ADD  CONSTRAINT [DF_gcoin_buy_fee]  DEFAULT ('0') FOR [fee]
GO
/****** Object:  Default [DF_gcoin_buy_mode]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_buy] ADD  CONSTRAINT [DF_gcoin_buy_mode]  DEFAULT ('EZ') FOR [mode]
GO
/****** Object:  Default [DF_gcoin_buy_result]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_buy] ADD  CONSTRAINT [DF_gcoin_buy_result]  DEFAULT ((0)) FOR [result]
GO
/****** Object:  Default [DF_gcoin_buy_regist_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_buy] ADD  CONSTRAINT [DF_gcoin_buy_regist_date]  DEFAULT (getdate()) FOR [regist_date]
GO
/****** Object:  Default [DF_franchise_settlement_result]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[franchise_settlement] ADD  CONSTRAINT [DF_franchise_settlement_result]  DEFAULT ('0') FOR [result]
GO
/****** Object:  Default [DF_franchise_settlement_regist_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[franchise_settlement] ADD  CONSTRAINT [DF_franchise_settlement_regist_date]  DEFAULT (getdate()) FOR [regist_date]
GO
/****** Object:  Default [DF_bitcoin_transfer_status]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[bitcoin_transfer] ADD  CONSTRAINT [DF_bitcoin_transfer_status]  DEFAULT ('0') FOR [status]
GO
/****** Object:  Default [DF_bitcoin_transfer_reg_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[bitcoin_transfer] ADD  CONSTRAINT [DF_bitcoin_transfer_reg_date]  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF_admin_member_reg_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[admin_member] ADD  CONSTRAINT [DF_admin_member_reg_date]  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF_gpoint_transfer_log_reg_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gpoint_transfer_log] ADD  CONSTRAINT [DF_gpoint_transfer_log_reg_date]  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF_gcoin_transfer_log_s_mode]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_transfer_log] ADD  CONSTRAINT [DF_gcoin_transfer_log_s_mode]  DEFAULT ('EZ') FOR [s_mode]
GO
/****** Object:  Default [DF_gcoin_transfer_log_regist_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[gcoin_transfer_log] ADD  CONSTRAINT [DF_gcoin_transfer_log_regist_date]  DEFAULT (getdate()) FOR [regist_date]
GO
/****** Object:  Default [DF_point_transfer_reg_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[point_transfer] ADD  CONSTRAINT [DF_point_transfer_reg_date]  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF_memo_reg_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[memo] ADD  CONSTRAINT [DF_memo_reg_date]  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF_member_msg_list_insert_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[member_msg_list] ADD  CONSTRAINT [DF_member_msg_list_insert_date]  DEFAULT (getdate()) FOR [insert_date]
GO
/****** Object:  Default [DF_member_msg_data_insert_date]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[member_msg_data] ADD  CONSTRAINT [DF_member_msg_data_insert_date]  DEFAULT (getdate()) FOR [insert_date]
GO
/****** Object:  Default [DF_TB_BANK_TRAN_PROC_FLAG]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[TB_BANK_TRAN] ADD  CONSTRAINT [DF_TB_BANK_TRAN_PROC_FLAG]  DEFAULT ('N') FOR [PROC_FLAG]
GO
/****** Object:  Default [DF_TB_BANK_TRAN_ERP_PROC_YN]    Script Date: 07/19/2017 11:39:01 ******/
ALTER TABLE [dbo].[TB_BANK_TRAN] ADD  CONSTRAINT [DF_TB_BANK_TRAN_ERP_PROC_YN]  DEFAULT ('N') FOR [ERP_PROC_YN]
GO
/****** Object:  Default [DF_member_lang]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_lang]  DEFAULT ('kr') FOR [lang]
GO
/****** Object:  Default [DF_member_gcoin]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_gcoin]  DEFAULT ('0') FOR [gcoin]
GO
/****** Object:  Default [DF_member_BTC]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_BTC]  DEFAULT ((0)) FOR [BTC]
GO
/****** Object:  Default [DF_member_USD]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_USD]  DEFAULT ('0') FOR [USD]
GO
/****** Object:  Default [DF_member_JPY]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_JPY]  DEFAULT ('0') FOR [JPY]
GO
/****** Object:  Default [DF_member_PHP]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_PHP]  DEFAULT ('0') FOR [PHP]
GO
/****** Object:  Default [DF_member_CNY]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_CNY]  DEFAULT ('0') FOR [CNY]
GO
/****** Object:  Default [DF_member_HKD]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_HKD]  DEFAULT ('0') FOR [HKD]
GO
/****** Object:  Default [DF_member_SGD]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_SGD]  DEFAULT ('0') FOR [SGD]
GO
/****** Object:  Default [DF_member_MYR]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_MYR]  DEFAULT ('0') FOR [MYR]
GO
/****** Object:  Default [DF_member_send_fee]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_send_fee]  DEFAULT ((0)) FOR [send_fee]
GO
/****** Object:  Default [DF_member_settlement_fee]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_settlement_fee]  DEFAULT ('0') FOR [settlement_fee]
GO
/****** Object:  Default [DF_member_buy_fee]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_buy_fee]  DEFAULT ('0') FOR [buy_fee]
GO
/****** Object:  Default [DF_member_point]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_point]  DEFAULT ('0') FOR [point]
GO
/****** Object:  Default [DF_member_shopping_point]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_shopping_point]  DEFAULT ('0') FOR [shopping_point]
GO
/****** Object:  Default [DF_member_franchise_check]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_franchise_check]  DEFAULT ('N') FOR [franchise_check]
GO
/****** Object:  Default [DF_member_h_check]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_h_check]  DEFAULT ('정상') FOR [h_check]
GO
/****** Object:  Default [DF_member_status]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_status]  DEFAULT ('정상') FOR [status]
GO
/****** Object:  Default [DF_member_reg_date]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[member] ADD  CONSTRAINT [DF_member_reg_date]  DEFAULT (getdate()) FOR [reg_date]
GO
/****** Object:  Default [DF_gcoin_transfer_result]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[gcoin_transfer] ADD  CONSTRAINT [DF_gcoin_transfer_result]  DEFAULT ('1') FOR [result]
GO
/****** Object:  Default [DF_gcoin_transfer_regist_date]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[gcoin_transfer] ADD  CONSTRAINT [DF_gcoin_transfer_regist_date]  DEFAULT (getdate()) FOR [regist_date]
GO
/****** Object:  Default [DF_gcoin_settlement2_result]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[gcoin_settlement2] ADD  CONSTRAINT [DF_gcoin_settlement2_result]  DEFAULT ((0)) FOR [result]
GO
/****** Object:  Default [DF_gcoin_settlement2_regist_date]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[gcoin_settlement2] ADD  CONSTRAINT [DF_gcoin_settlement2_regist_date]  DEFAULT (getdate()) FOR [regist_date]
GO
/****** Object:  Default [DF_gcoin_settlement_result]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[gcoin_settlement] ADD  CONSTRAINT [DF_gcoin_settlement_result]  DEFAULT ((0)) FOR [result]
GO
/****** Object:  Default [DF_gcoin_settlement_regist_date]    Script Date: 07/19/2017 11:39:02 ******/
ALTER TABLE [dbo].[gcoin_settlement] ADD  CONSTRAINT [DF_gcoin_settlement_regist_date]  DEFAULT (getdate()) FOR [regist_date]
GO
