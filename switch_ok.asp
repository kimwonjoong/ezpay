<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>

<!--#include file="./inc/inc_db_lib.asp"-->

<%

on error resume next

Dim sResult : sResult = "N"
Dim nResultCnt : nResultCnt = 0

Dim wallet_no : wallet_no = Trim(Request("wallet_no"))
Dim gcoin : gcoin = Trim(Request("gcoin"))
Dim change_coin : change_coin = Trim(Request("change_gcoin"))
Dim s_mode : s_mode = Trim(Request("s_mode"))
Dim r_mode : r_mode = Trim(Request("r_mode"))

Dim now_gcoin,franchise_check,change_gcoin,fee,fee_coin
Dim s_count,ssCheck
s_count=0
fee_coin=0
now_gcoin=0

fnDBConn()

sCheck = "NO"

		strSQL =  "  select wallet_no,gcoin,USD,JPY,CNY,PHP,HKD from member where wallet_no='" & wallet_no & "'"

		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) Then

			If s_mode="EZ" then
				now_gcoin = rs("gcoin")
			ElseIf s_mode="USD" Then
				now_gcoin = rs("USD")
			ElseIf s_mode="JPY" Then
				now_gcoin = rs("JPY")
			ElseIf s_mode="CNY" Then
				now_gcoin = rs("CNY")
			ElseIf s_mode="PHP" Then
				now_gcoin = rs("PHP")
			ElseIf s_mode="HKD" Then
				now_gcoin = rs("HKD")
			ElseIf s_mode="SGD" Then
				now_gcoin = rs("SGD")
			ElseIf s_mode="MYR" Then
				now_gcoin = rs("MYR")
			Else
				now_gcoin = rs("gcoin")
			End If
			
		
			If CCur(now_gcoin) >= CCur(gcoin) Then
			    'Response.write "True"
				sCheck = "YES"
			Else
			    'Response.write "False"
				sCheck = "NO"
				sResult = "0"
			End if
			
		
		end If
		
		rs.close
		
		If(sCheck="YES") Then

			strSQL=""
		
			dbConn.BeginTrans

			strSQL = strSQL & "	insert into gcoin_transfer_log(wallet_no,gcoin,change_coin,mode,s_mode) values"
			strSQL = strSQL & "	( '" & wallet_no &"','" & gcoin &"','" & change_coin &"','" & r_mode &"','" & s_mode &"')"


			If s_mode = "EZ" Then
			
				If r_mode = "EZ" then
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
				ElseIf r_mode = "USD" Then
					strSQL = strSQL & "	update  member set USD=convert(money,USD)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "JPY" Then
					strSQL = strSQL & "	update  member set JPY=convert(money,JPY)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "PHP" Then
					strSQL = strSQL & "	update  member set PHP=convert(money,PHP)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "CNY" Then
					strSQL = strSQL & "	update  member set CNY=convert(money,CNY)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "HKD" Then
					strSQL = strSQL & "	update  member set HKD=convert(money,HKD)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "SGD" Then
					strSQL = strSQL & "	update  member set SGD=convert(money,SGD)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				ElseIf r_mode = "MYR" Then
					strSQL = strSQL & "	update  member set MYR=convert(money,MYR)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"
				End If

			ElseIf s_mode = "USD" Then

				If r_mode = "EZ" Then
					strSQL = strSQL & "	update  member set USD=convert(money,USD)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
				
			ElseIf s_mode = "JPY" Then

				If r_mode = "EZ" Then
					strSQL = strSQL & "	update  member set JPY=convert(money,JPY)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
			
			ElseIf s_mode = "PHP" Then

				If r_mode = "EZ" Then
					strSQL = strSQL & "	update  member set PHP=convert(money,PHP)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
				
			ElseIf s_mode = "CNY" Then

				If r_mode = "EZ" Then
					strSQL = strSQL & "	update  member set CNY=convert(money,CNY)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
				
			ElseIf s_mode = "HKD" Then

				If r_mode = "EZ" Then
					strSQL = strSQL & "	update  member set HKD=convert(money,HKD)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
			
			ElseIf s_mode = "SGD" Then

				If r_mode = "EZ" Then
					strSQL = strSQL & "	update  member set SGD=convert(money,SGD)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If

			ElseIf s_mode = "MYR" Then

				If r_mode = "EZ" Then
					strSQL = strSQL & "	update  member set MYR=convert(money,MYR)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
					strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)+convert(money,'"&change_coin&"')  where wallet_no='" & wallet_no & "'"						
				else
					sResult = "0"
				End If
				
			End if
			
			'Response.write strSQL
			If sResult <> "0" Then

				cmd.CommandText = strSQL
				cmd.Execute nResultCnt, , adExecuteNoRecords

				if nResultCnt <> 1 or Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
					dbConn.RollBackTrans
					fnErrSave()
				else
					dbConn.CommitTrans
					sResult = "Y"
				'	fnLogSave()
				end If

			End if

		End If

fnDBClose()


If sResult <> "Y" Then
	
	sResult = msg

End if
%>

<% 
If sResult = "Y" Then
	Response.write "YES"
Else
	Response.write msg
End If
%>