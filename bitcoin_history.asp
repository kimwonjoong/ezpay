<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->

<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 

	fnDBConn()

	Dim bank_name,account_no,input_name

	'지금 핀이 사용가능한 핀인지
	strSQL =  " SELECT IDX, WALLET_NO, UUID, GCOIN, PRIVATE_KEY, BITCOIN_WALLET, BTC FROM MEMBER WHERE WALLET_NO='"& Session("SS_USERID") &"' "
	cmd.CommandText = strSQL
	Set rs = cmd.execute()

	if not(rs.eof or rs.bof) then
		WALLET_NO = rs("WALLET_NO")
		UUID = rs("UUID")
		GCOIN = rs("GCOIN")
		PRIVATE_KEY = rs("PRIVATE_KEY")
		BITCOIN_WALLET = rs("BITCOIN_WALLET")
		'BITCOIN_WALLET = "12c8XmdKqzNqNthjjHG2Z6qgFQVwawaPxo"

		BTC = rs("BTC")
	end If
	rs.close
	fnDBClose()
%>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY</title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">
		<meta name="format-detection" content="telephone=no"> <!-- 아이폰에서 계좌번호를 전화번호로 인식되는걸 막음! -->

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">
		<script type="text/javascript" src="/inc/Jquery.Js"></script>
		<script>
		function comma(num){
		   var len, point, str;

		   num = num + "";
		   point = num.length % 3 ;
		   len = num.length;

		   str = num.substring(0, point);
		   while (point < len) {
			  if (str != "") str += ",";
			  str += num.substring(point, point + 3);
			  point += 3;
		   }

		   return str;
		}

		$(document).ready(function() {
			 jQuery.ajax({
				   ContentType:'application/x-www-form-urlencoded',
				   type:'POST',
				   url:'http://110.10.129.222/php-client/sample/address-api/GetAddress.php',
				   data:'address=<%=BITCOIN_WALLET%>',
				   dataType:'JSON', // 옵션이므로 JSON으로 받을게 아니면 안써도 됨 
				   success : function(data) { 
						 // 통신이 성공적으로 이루어졌을 때 이 함수를 타게 된다.
						balance = data["balance"];
						balance = balance / 100000000;
						$("#balance").html(balance);

						var history_html = "";
						history_html += "<table class='table table-striped table-colored'>";
						history_html += "<thead>";
						history_html += "<tr>";
						history_html += "<th>전환종류</th>";
						history_html += "<th>매수/매도 BTC</th>";
						history_html += "<th>보유 BTC</th>";
						history_html += "<th>처리일자</th>";
						history_html += "</tr>";
						history_html += "</thead>";
						for (b=0; b < data["txrefs"].length; b++) {

							if (data["txrefs"][b]["tx_input_n"] > "-1") {
								font_color = "style='color:#ff6633;'";
							} else if (data["txrefs"][b]["tx_output_n"] > "-1"){
								font_color = "";
							}


							history_html += "<tbody>";
							history_html += "<td>";
							if (data["txrefs"][b]["tx_input_n"] > "-1") {
								history_html += "<span "+font_color+">매도</span>";
							} else if (data["txrefs"][b]["tx_output_n"] > "-1"){
								history_html += "<span "+font_color+">매수</span>";
							}
							history_html += "</td>";

							history_html += "<td><span "+font_color+">"+ comma(data["txrefs"][b]["value"]) +"</span></td>";
							history_html += "<td><span "+font_color+">"+ data["txrefs"][b]["ref_balance"]/ 100000000 +"</span></td>";
							history_html += "<td><span "+font_color+">"+ data["txrefs"][b]["confirmed"] +"</span></td>";

							history_html += "</tr>";
							history_html += "<tbody>";
						}
						history_html += "</tbody>";
						history_html += "</table>";
						$("#history_txt").html(history_html);

					},
				   complete : function(data) {
						 // 통신이 실패했어도 완료가 되었을 때 이 함수를 타게 된다.

				  },
				   error : function(xhr, status, error) {
						 //alert("통신 오류가 발생 하였습니다. 잠시 후 다시 시도해 주세요!");
						$("#balance").html("0");
				   }
			 });
		});
		</script>


		<script type="text/javascript">
		<!--
		//[] <--문자 범위 [^] <--부정 [0-9] <-- 숫자  
		//[0-9] => \d , [^0-9] => \D
		var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
		var rgx2 = /(\d+)(\d{3})/; 

		function getNumber(obj){
			 var num01;
			 var num02;
			 num01 = obj.value;
			 num02 = num01.replace(rgx1,"");
			 num01 = setComma(num02);
			 obj.value =  num01;
		}

		function setComma(inNum){
			 var outNum;
			 outNum = inNum; 
			 while (rgx2.test(outNum)) {
				  outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
			  }
			 return outNum;
		}
		//-->
		</script>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active">비트코인 거래내역</li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- banner start -->
				<!-- section start -->
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title"><%=EZ_LAN_75%></h1>
					<div class="separator-2"></div>
					<div class="form-group">
						<label for="exampleInputPassword1">비트코인</label>
						<span style="text-align:center; color:#ff9900" id="balance" name="balance"></span> BTC
					</div>

						<span id="history_txt" name="history_txt" ></span>

				</div>
			</div>

			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
