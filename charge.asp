<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->

<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 
%>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY</title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">
		<meta name="format-detection" content="telephone=no"> <!-- 아이폰에서 계좌번호를 전화번호로 인식되는걸 막음! -->

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">
		<script type="text/javascript">
		<!--
		//[] <--문자 범위 [^] <--부정 [0-9] <-- 숫자  
		//[0-9] => \d , [^0-9] => \D
		var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
		var rgx2 = /(\d+)(\d{3})/; 

		function getNumber(obj){
			 var num01;
			 var num02;
			 num01 = obj.value;
			 num02 = num01.replace(rgx1,"");
			 num01 = setComma(num02);
			 obj.value =  num01;
		}

		function setComma(inNum){
			 var outNum;
			 outNum = inNum; 
			 while (rgx2.test(outNum)) {
				  outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
			  }
			 return outNum;
		}
		//-->
		</script>
		<script type="text/javascript">
			function charge_ok() {
				var SS_premium = '<%=Session("SS_premium")%>';
				var mode = $("#mode").val();
				var gcoin = $("#gcoin").val();
				gcoin = gcoin.replace(/[^\d]+/g, '');
				var input_name = $("#input_name").val();

				if (mode.length == 0){
					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_2%>',
						callback: function(result) {
							$("#mode").focus();
						}

					});
					
				}else if (gcoin.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_3%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
				
				}else if (gcoin < 10000 && mode == "EZ"){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_196%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
				
				}else if (gcoin < 1 && mode == "USD"){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_197%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				}else if (gcoin < 100 && mode == "JPY"){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_198%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				}else if (gcoin < 50 && mode == "CNY"){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_199%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				}else if (gcoin < 50 && mode == "PHP"){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_201%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				}else if (gcoin < 10 && mode == "HKD"){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_202%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				}else if (gcoin < 1 && mode == "SGD"){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_203%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				}else if (gcoin < 5 && mode == "MYR"){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_204%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				}else if (input_name.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_1%>',
						text: '<%=EZ_LAN_4%>',
						callback: function(result) {
							$("#input_name").focus();
						}
					});
								
				}else{
					$.ajax({
						 url: "charge_ok.asp",
						 type: "POST",
						 data: { mode: mode, gcoin: gcoin, input_name: input_name },  

						 success: function(data){
							 if(data == "YES"){						 
								modal({
									type: 'success',
									title: '<%=EZ_LAN_1%>',
									text: '<%=EZ_LAN_5%>',
									callback: function(result) {
										location.href="charge.asp";
									}
								});

							 }else if(data == "2"){
								modal({
									type: 'error',
									title: '<%=EZ_LAN_1%>',
									text: '<%=EZ_LAN_6%>',
									callback: function(result) {
										location.href="charge.asp";
									}
								});				
							 
							 }else if(data == "3"){
								modal({
									type: 'error',
									title: '<%=EZ_LAN_1%>',
									text: '<%=EZ_LAN_7%>',
									callback: function(result) {
										location.href="charge.asp";
									}
								});				
							 
							 }else if(data == "5"){
								modal({
									type: 'error',
									title: '<%=EZ_LAN_1%>',
									text: '<%=EZ_LAN_8%>',
									callback: function(result) {
										location.href="charge.asp";
									}
								});				

							 }else if(data == "6"){
								modal({
									type: 'error',
									title: '<%=EZ_LAN_1%>',
									text: '<%=EZ_LAN_225%>',
									callback: function(result) {
										location.href="charge.asp";
									}
								});				

								
							 }else if(data == "ERROR"){							 
								modal({
									type: 'error',
									title: '<%=EZ_LAN_1%>',
									text: '<%=EZ_LAN_9%>',
									callback: function(result) {
										location.href="charge.asp";
									}
								});				
							 }else{								 
								modal({
									type: 'error',
									title: '<%=EZ_LAN_1%>',
									text: '<%=EZ_LAN_10%>',
									callback: function(result) {
										location.href="charge.asp";
									}
								});				
							 }
						 },
			 
						 complete: function(data){
						//	location.href="index.asp";
						 }
					});
				}	
			}

			function insert_money(money)
			{
				$("#gcoin").val(money);
			}

			function list_close(){
				$('.object-visible, .object-non-visible.object-visible').css('display', 'none');
			}
		</script>


	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active"><%=EZ_LAN_1%></li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- banner start -->
				<!-- section start -->
			<%

				fnDBConn()

				Dim bank_name,account_no,input_name

				'지금 핀이 사용가능한 핀인지
				strSQL =  "  select bank_name,account_no,input_name from gcoin_bank where mode='kr'"

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					bank_name = rs("bank_name")
					account_no = rs("account_no")
					input_name = rs("input_name")
					'primeum = rs("primeum")
				end If

				rs.close
					
				fnDBClose()

			%>
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title"><%=EZ_LAN_1%></h1>
					<div class="separator-2"></div>


<%
	fnDBConn()

	sql_c = "select gcoin, input_name, result, regist_date from gcoin_buy where wallet_no='"& Session("ss_wallet") &"' and result='0' "
	cmd.CommandText = sql_c
	Set rs_c = cmd.execute()

	If rs_c.eof Or rs_c.bof Then
	Else
	%>
				<!-- main start -->
				<!-- ================ -->
				<div class="main object-non-visible" data-animation-effect="fadeInUpSmall" data-effect-delay="100">
					<div class="form-block center-block p-30 light-gray-bg border-clear">
						<h2 class="title"><%=EZ_LAN_153%></h2>
						<form class="form-horizontal" role="form" name="joinform" id="joinform" method="POST">
						<input type="hidden" name="now_wallet" id="now_wallet" value="<%=Session("SS_USERID")%>">
							<div class="form-group has-feedback">
								<div class="col-sm-offset-1">

									<style>
									/*테이블*/
									table {float:left; position:relative; width:100%; margin:0px auto; font-size:12px;}
									.t_list { border-top:1px solid #777; }
									.t_list th {font-weight:700; color:#393939; white-space:nowrap; background-color:#999999; border-bottom:1px solid #cdcdcd;}
									.t_list td {border-bottom:1px solid #cdcdcd;}
									.t_list th, .t_list td {padding:8px 10px; text-align:center; /*border-left:1px solid #cdcdcd;*/}
									.t_list .tit {text-align:left; max-width:150px;}
									.t_list tbody th {border-top:1px solid #cdcdcd; font-weight:700; color:#393939; background:#f6f3ec;}
									</style>
									<table class="t_list">
										<tr>
											<th width="15%"><%=EZ_LAN_11%></th>
											<th width="15%">EZ PAY</th>
											<th width="30%"><%=EZ_LAN_12%></th>
											<th width="20%"><%=EZ_LAN_13%></th>
										</tr>
										
										<%
										cnt = 0
										Do Until rs_c.eof
										If rs_c("gcoin") >= "1000" Then
											gcoin_com = FormatNumber(rs_c("gcoin"),0)
										Else
											gcoin_com = rs_c("gcoin")
										End If
										%>
										<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
											<td><%=rs_c("input_name")%></td>
											<td><%=gcoin_com%> EZ</td>
											<td><%=rs_c("regist_date")%></td>
											<td><%=EZ_LAN_222%></td>
										</tr>
										<%
										cnt = cnt + 1
										rs_c.movenext
										loop
										%>
									</table>

								</div>
								<div style="float:left; width:85%; margin-top:20px; margin-left:4%; font-size:12px; color:red;"><%=EZ_LAN_14%></div>
							</div>
							
							<div class="form-group">
								<div class="col-sm-offset-4 col-sm-8">
									<button type="button" class="btn btn-group btn-default btn-animated" onclick="list_close();"><%=EZ_LAN_223%><i class="fa fa-check"></i></button>
								</div>
							</div>
						</form>
					</div>
				</div>


				<!-- ================ -->
				<!-- main end -->
	<%
	End If

	rs_c.close
	fnDBClose()
%>
					
					<form role="form">
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_15%></label>
							<select class="form-control" name="mode" id="mode" required style="background-color:#575757;">
								<option value="EZ">EZ</option>
								<option value="USD">USD</option>
								<option value="JPY">JPY</option>
								<option value="CNY">CNY</option>
								<option value="PHP">PHP</option>
								<option value="HKD">HKD</option>
								<option value="SGD">SGD</option>
								<option value="MYR">MYR</option>
							</select>
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_16%></label>
							<input type="tel" class="form-control" id="gcoin" name="gcoin" required onchange="getNumber(this);" onkeyup="getNumber(this);">
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_17%></label>
							<input type="text" class="form-control" name="input_name" id="input_name" required>
						</div>
						<button type="button" class="btn btn-default"  onclick="charge_ok();"><%=EZ_LAN_224%></button>

						<div class="alert alert-info alert-dismissible" role="alert">							
							<font color="red"><b><%=EZ_LAN_18%> : <%=bank_name%><br><%=EZ_LAN_19%> : <%=account_no%><br><%=EZ_LAN_20%> : <%=input_name%></b></font>
						</div>
						<div class="alert alert-info alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<b style="color:red;"><%If Session("SS_premium") = "Y" Then%><%=EZ_LAN_21%><%Else%><%=EZ_LAN_21_1%><%End If%><%=EZ_LAN_22%><%=EZ_LAN_23%><%=EZ_LAN_23_1%></b>
						</div>
					</form>
				</div>
			</div>
			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
