<%@Language="VBScript" CODEPAGE="65001"%>
<%Response.ContentType="text/html;charset=UTF-8"%>
<%
address = "1HsP77zECCibaK73rKVxvcR3ujWZdjciqm"
%>
<script type="text/javascript" src="/ez_admin/JS/jquery.js"></script>
<script>
$(document).ready(function() {
/*
 $.ajax({
    url: "http://110.10.129.222/php-client/sample/address-api/AddressBalanceEndpoint.php?address=1HsP77zECCibaK73rKVxvcR3ujWZdjciqm",
    dataType: 'jsonp',
    jsonpCallback: "http://www.e-zpay.co.kr",
    success: function(data) {
      console.log('성공 - ', data);
    },
    error: function(xhr) {
      console.log('실패 - ', xhr);
    }
  });

*/

	 jQuery.ajax({
		   ContentType:"application/x-www-form-urlencoded",
		   type:"POST",
		   url:"http://110.10.129.222/php-client/sample/address-api/GetAddress.php",
		   data:"address=<%=address%>",
           dataType:"JSON", // 옵션이므로 JSON으로 받을게 아니면 안써도 됨 
           success : function(data) {
                 // 통신이 성공적으로 이루어졌을 때 이 함수를 타게 된다.
				t_form = "<table class='t_list'>";
				t_form += "<caption>비트코인 거래내역 관리</caption>";
				t_form += "<colgroup>";
				t_form += "<col style='3%' />";
				t_form += "<col />";
				t_form += "<col style='width:15%' />";
				t_form += "</colgroup>";
				t_form += "<thead>";

				t_form += "<tr>";
				t_form += "<th scope='col'>address</th>";
				t_form += "<td class='tit'>"+ data["address"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>total_received</th>";
				t_form += "<td class='tit'>"+ data["total_received"] +"</td>";
				t_form += "<tr>";
				t_form += "</tr>";
				t_form += "<th scope='col'>total_sent</th>";
				t_form += "<td class='tit'>"+ data["total_sent"] +" </td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>balance</th>";
				t_form += "<td class='tit'>"+ data["balance"] +"</td>";
				t_form += "</tr>";

				t_form += "<tr>";
				t_form += "<th scope='col'>unconfirmed_balance</th>";
				t_form += "<td class='tit'>"+ data["unconfirmed_balance"] +"</td>";
				t_form += "</tr>";

				t_form += "<tr>";
				t_form += "<th scope='col'>final_balance</th>";
				t_form += "<td class='tit'>"+ data["final_balance"] +"</td>";
				t_form += "</tr>";

				t_form += "<tr>";
				t_form += "<th scope='col'>n_tx</th>";
				t_form += "<td class='tit'>"+ data["n_tx"] +"</td>";
				t_form += "</tr>";


				t_form += "<tr>";
				t_form += "<th scope='col'>tx_hash</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["tx_hash"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>block_height</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["block_height"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>tx_input_n</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["tx_input_n"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>tx_output_n</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["tx_output_n"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>value</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["value"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>ref_balance</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["ref_balance"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>spent</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["spent"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>confirmations</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["confirmations"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>confirmed</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["confirmed"] +"</td>";
				t_form += "</tr>";
				t_form += "<tr>";
				t_form += "<th scope='col'>double_spend</th>";
				t_form += "<td class='tit'>"+ data["txrefs"][0]["double_spend"] +"</td>";
				t_form += "</tr>";

				t_form += "</thead>";
				t_form += "</table>";
				$("#t_forms").html(t_form);
			},
           complete : function(data) {
                 // 통신이 실패했어도 완료가 되었을 때 이 함수를 타게 된다.

          },
           error : function(xhr, status, error) {
                 alert("통신 오류가 발생 하였습니다. 잠시 후 다시 시도해 주세요!");
           }
     });
});
</script>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>EZPAY</title>
<body>
		<div id="t_forms"></div>
</body>
</html>
