
<%@Language="VBScript" CODEPAGE="65001"%>
<%Response.ContentType="text/html;charset=UTF-8"%>
<%

address = "2046"
%>
<script type="text/javascript" src="/ez_admin/JS/jquery-1.7.1.min.js"></script>
<script>

$(document).ready(function() {
     jQuery.ajax({
		   ContentType:"application/x-www-form-urlencoded",
		   type:"POST",
		   url:"http://110.10.129.222/php-client/sample/address-api/GenerateAddressEndpoint.php",
		   data: "wallet_no=2046",
           dataType:"JSON", // 옵션이므로 JSON으로 받을게 아니면 안써도 됨 
           success : function(data) {
                 // 통신이 성공적으로 이루어졌을 때 이 함수를 타게 된다.
 					t_form = "<table class='t_list'>";
					t_form += "<caption>비트코인 address생성 관리</caption>";
					t_form += "<colgroup>";
					t_form += "<col style='3%' />";
					t_form += "<col />";
					t_form += "<col style='width:15%' />";
					t_form += "</colgroup>";
					t_form += "<thead>";


					t_form += "<tr>";
					t_form += "<th scope='col'>private</th>";
					t_form += "<td class='tit'>"+ data["private"] +"</td>";
					t_form += "</tr>";


					t_form += "<tr>";
					t_form += "<th scope='col'>public</th>";
					t_form += "<td class='tit'>"+ data["public"] +"</td>";
					t_form += "</tr>";


					t_form += "<tr>";
					t_form += "<th scope='col'>address</th>";
					t_form += "<td class='tit'>"+ data["address"] +"</td>";
					t_form += "</tr>";


					t_form += "<tr>";
					t_form += "<th scope='col'>wif</th>";
					t_form += "<td class='tit'>"+ data["wif"] +"</td>";
					t_form += "</tr>";


					t_form += "</thead>";
					t_form += "</table>";
					$("#t_forms").html(t_form);
			},
           complete : function(data) {
                 // 통신이 실패했어도 완료가 되었을 때 이 함수를 타게 된다.

          },
           error : function(xhr, status, error) {
                 alert("통신 오류가 발생 하였습니다. 잠시 후 다시 시도해 주세요!");
           }
     });
});
</script>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>EZPAY</title>
<body>
		<div id="t_forms"></div>
</body>
</html>