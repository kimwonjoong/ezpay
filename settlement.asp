<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->
<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 
%>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY | <%=EZ_LAN_74%></title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<script type="text/javascript">
		<!--
		//[] <--문자 범위 [^] <--부정 [0-9] <-- 숫자  
		//[0-9] => \d , [^0-9] => \D
		var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
		var rgx2 = /(\d+)(\d{3})/; 

		function getNumber(obj){
			
			 var num01;
			 var num02;
			 num01 = obj.value;
			 num02 = num01.replace(rgx1,"");
			 num01 = setComma(num02);
			 obj.value =  num01;

		}

		function setComma(inNum){
			 
			 var outNum;
			 outNum = inNum; 
			 while (rgx2.test(outNum)) {
				  outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
			  }
			 return outNum;

		}
		//-->
		</script>

		
	<script type="text/javascript">
		function settlement_select(val)
		{
			//alert(val.value);
			var mode = val.split("|");
			if (mode[0].length > 0){
				$("#bank_name").val(mode[0]);
				$("#account_no").val(mode[1]);
				$("#input_name").val(mode[2]);
			}
		}
		function bank_select(val) {
			//alert(val.value);
			var mode = val;
			if (mode.length > 0){
				$.ajax({
					 url: "bank_list.asp",
					 type: "POST",
					 data: { mode: mode },  

					 success: function(data){						 
						 $("#bank_name").empty().append(data);
					 },
		 
					 complete: function(data){

					 }
				});
			}
		}
		function settlement_ok() {
			var mode = $("#mode").val();
			var gcoin = $("#gcoin").val();
			gcoin = gcoin.replace(/[^\d]+/g, '');
			var wallet_no = $("#wallet_no").val();
			var bank_name = $("#bank_name").val();
			var account_no = $("#account_no").val();
			var input_name = $("#input_name").val();
			//alert(mode);

			if (wallet_no.length == 0){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_64%>',
					callback: function(result) {
						$("#wallet_no").focus();
					}

				});
				
			}else if (mode.length == 0){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_181%>',
					callback: function(result) {
						$("#mode").focus();
					}

				});
				
			}else if (gcoin.length == 0){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_182%>',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (bank_name.length == 0){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_183%>',
					callback: function(result) {
						$("#bank_name").focus();
					}
				});
				
			}else if (account_no.length == 0){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_184%>',
					callback: function(result) {
						$("#account_no").focus();
					}
				});
				
			}else if (input_name.length == 0){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_185%>',
					callback: function(result) {
						$("#input_name").focus();
					}
				});
				
			}else if (gcoin < 10000 && mode == "EZ"){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_186%>',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 1 && mode == "USD"){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_214%>',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 100 && mode == "JPY"){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_215%>',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 50 && mode == "CNY"){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_216%>',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 50 && mode == "PHP"){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_217%>',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 10 && mode == "HKD"){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_218%>',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 1 && mode == "SGD"){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_219%>',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 5 && mode == "MYR"){

				modal({
					type: 'info',
					title: '<%=EZ_LAN_74%>',
					text: '<%=EZ_LAN_220%>',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else{

//				sMsg = "청산처리하시겠습니까?";
	
//				if(confirm(sMsg))
//				{	
				modal({
					type: 'confirm',
					title: '<%=EZ_LAN_74%>',
					text: bank_name+'('+account_no+')' + input_name + '<%=EZ_LAN_187%> ' + gcoin + '<%=EZ_LAN_187_1%>',
					callback: function(result) {
						//alert(result);
						if(result){							
							$.ajax({
								 url: "settlement_ok.asp",
								 type: "POST",
								 data: { mode: mode, gcoin: gcoin,wallet_no: wallet_no,bank_name: bank_name,account_no: account_no,input_name: input_name },  

								 success: function(data){
									 if(data == "YES"){						 
										modal({
											type: 'success',
											title: '<%=EZ_LAN_74%>',
											text: '<%=EZ_LAN_188%>',
											callback: function(result) {
												location.href="settlement.asp";
											}
										});		

									 }else if(data == "0"){
										modal({
											type: 'error',
											title: '<%=EZ_LAN_74%>',
											text: '<%=EZ_LAN_148%>',
											callback: function(result) {
												location.href="charge.asp";
											}
										});				
									 
									 }else if(data == "1"){
										modal({
											type: 'error',
											title: '<%=EZ_LAN_74%>',
											text: '<%=EZ_LAN_194%> 5000000-CCur(g_sum) <%=EZ_LAN_194_1%>',
											callback: function(result) {
												location.href="charge.asp";
											}
										});				
									 }else if(data == "2"){
										modal({
											type: 'error',
											title: '<%=EZ_LAN_74%>',
											text: '<%=EZ_LAN_195%> 2000000-CCur(g_sum) <%=EZ_LAN_195_1%>',
											callback: function(result) {
												location.href="charge.asp";
											}
										});				
									 
									 }else if(data == "3"){
										modal({
											type: 'error',
											title: '<%=EZ_LAN_74%>',
											text: '<%=EZ_LAN_154%>',
											callback: function(result) {
												location.href="charge.asp";
											}
										});				
									 }else if(data == "4"){
										modal({
											type: 'error',
											title: '<%=EZ_LAN_74%>',
											text: '<%=EZ_LAN_155%>',
											callback: function(result) {
												location.href="charge.asp";
											}
										});				
									 
									 }else if(data == "5"){
										modal({
											type: 'error',
											title: '<%=EZ_LAN_74%>',
											text: '<%=EZ_LAN_9%>',
											callback: function(result) {
												location.href="charge.asp";
											}
										});				

									 }else if(data == "9"){
										modal({
											type: 'error',
											title: '<%=EZ_LAN_74%>',
											text: '사용하실 수 없습니다!',
											callback: function(result) {
												location.href="charge.asp";
											}
										});				


									 }else{								 
										modal({
											type: 'error',
											title: '<%=EZ_LAN_74%>',
											text: '<%=EZ_LAN_9%>',
											callback: function(result) {
												location.href="settlement.asp";
											}
										});				
									 }
								 },
					 
								 complete: function(data){
								//	location.href="index.asp";
								 }
							});

						}
					}
				});	
			
					

	//			}
				
			}	
		}

		function insert_money(money)
		{
			$("#gcoin").val(money);
		}
		
	</script>
	
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active"><%=EZ_LAN_74%></li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- banner start -->
				<!-- section start -->
			<%

				fnDBConn()

				Dim bank_name,account_no,input_name

				'지금 핀이 사용가능한 핀인지
				strSQL =  "  select bank_name,account_no,input_name from gcoin_bank where mode='kr'"

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					bank_name = rs("bank_name")
					account_no = rs("account_no")
					input_name = rs("input_name")
				end If

				rs.close
					
				fnDBClose()

			%>
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title"><%=EZ_LAN_74%></h1>
					<div class="separator-2"></div>
					<form role="form">
					<input type="hidden" name="wallet_no" id="wallet_no" value="<%=Session("SS_USERID")%>">
						
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_189%></label>
							<select class="form-control" name="mode" id="mode" required onchange="bank_select(this.value);" style="background-color:#575757;">
								<option value="">===<%=EZ_LAN_136%>===</option>
								<option value="EZ">EZ</option>
								<option value="USD">USD</option>
								<option value="JPY">JPY</option>
								<option value="CNY">CNY</option>
								<option value="PHP">PHP</option>
								<option value="HKD">HKD</option>
								<option value="SGD">SGD</option>
								<option value="MYR">MYR</option>
							</select>
						</div>
						<div class="form-group">
						<label for="exampleInputPassword1"><%=EZ_LAN_221%></label>
							<select class="form-control" name="settlement_list" id="settlement_list" required onchange="settlement_select(this.value);" style="background-color:#575757;">
								<option value="">===<%=EZ_LAN_136%>===</option>
								<%

									fnDBConn()

									Dim aData,nData
									nData = -1
									'지금 핀이 사용가능한 핀인지
									strSQL =  "select * from gcoin_settlement where wallet_no='"&Session("SS_USERID")&"'"

									
									cmd.CommandText = strSQL
									Set rs = cmd.execute()

									if not(rs.eof or rs.bof) then
										nData = fnRs2JsonArr(aData, rs)
									end if

									rs.close
										
									fnDBClose()

									if nData >= 0 then
										for i=0 to nData

								%>
									<option value="<%=aData("bank_name")(i)%>|<%=aData("account_no")(i)%>|<%=aData("input_name")(i)%>"><%=aData("bank_name")(i)%>(<%=aData("account_no")(i)%>:<%=aData("input_name")(i)%>)</option>
								<%
								
									Next
								else
								%>
									<option value=""><%=EZ_LAN_137%></option>
								<%
								End if
								%>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_190%></label>
							<input type="tel" class="form-control" id="gcoin" name="gcoin" required onchange="getNumber(this);" onkeyup="getNumber(this);">
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_191%></label>
							<select class="form-control" name="bank_name" id="bank_name" required style="background-color:#575757;">
								<option value="">===<%=EZ_LAN_136%>===</option>
							</select>
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_19%></label>
							<input type="tel" class="form-control" name="account_no" id="account_no" required>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_17%></label>
							<input type="text" class="form-control" name="input_name" id="input_name" required>
						</div>

						<div class="alert alert-info alert-dismissible" role="alert">							
							<font color="red"><b><%If Session("SS_premium") = "Y" Then%><%=EZ_LAN_144%> <%=EZ_LAN_145%><%Else%><%=EZ_LAN_146%><%End If%></b></font>
						</div>
						
					
						<button type="button" class="btn btn-default"  onclick="settlement_ok();"><%=EZ_LAN_193%></button>
					</form>
				</div>
			</div>
			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
