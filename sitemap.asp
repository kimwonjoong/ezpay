<?xml version="1.0" encoding="UTF-8"?>

<!DOCTYPE html>
<html lang="ko">
<head>
<meta charset="utf-8">
<title>EZPAY</title>
<!--#include virtual="/EZ_ADMIN/inc/common.asp" -->
<style>
</style>
</head>
<body>
<div id="wrap">	
	<div id="contents">
	<div style="text-align:center; padding:5px 0 25px 10px; font-size: 24px; font-weight: bold;">SITEMAP</div>
		<div style="width:25%; float:left;">
		<table class="t_list" style="border-right: 1px solid #d2d2d2;">
			<caption>SITEMAP</caption>
			<colgroup>
				<col />
			</colgroup>
			<thead>   
				<tr>
					<th scope="col" style="line-height:30px; border-right: 1px solid #a4a4a4;">EZ PAY</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="/charge.asp" alt="충전하기" title="충전하기">충전하기</a></td>
				</tr>
				<tr>
					<td><a href="/send.asp" alt="송금하기" title="송금하기">송금하기</a></td>
				</tr>
				<tr>
					<td><a href="/settlement.asp" alt="청산하기" title="청산하기">청산하기</a></td>
				</tr>
				<tr>
					<td><a href="/information.asp" alt="마이페이지" title="마이페이지">마이페이지</a></td>
				</tr>
				<tr>
					<td><a href="/shopping_send.asp" alt="MY쇼핑포인트사용하기" title="MY쇼핑포인트사용하기">MY쇼핑포인트사용하기</a></td>
				</tr>
				<tr>
					<td><a href="/franchise_join.asp" alt="Premium MemberShip" title="Premium MemberShip">Premium MemberShip</a></td>
				</tr>
				<tr>
					<td><a href="/mypage.asp">회원정보수정</a></td>
				</tr>
			</tbody>
		</table>
		</div>
		<div style="width:25%; float:left;">
		<table class="t_list" >
			<caption>SITEMAP</caption>
			<colgroup>
				<col />
			</colgroup>
			<thead>   
				<tr>
					<th scope="col" style="line-height:30px;">국제환율정보</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="/exchange.asp" alt="국제환율정보" title="국제환율정보">국제환율정보</a></td>
				</tr>
				<tr>
					<td><a href="/switch.asp" alt="외국환전환" title="외국환전환">외국환전환</a></td>
				</tr>
			</tbody>
		</table>	
		</div>
		<div style="width:25%; float:left;">
		<table class="t_list" style="border-left: 1px solid #d2d2d2; border-right: 1px solid #d2d2d2;">
			<caption>SITEMAP</caption>
			<colgroup>
				<col />
			</colgroup>
			<thead>   
				<tr>
					<th scope="col" style="line-height:30px;border-left: 1px solid #a4a4a4; border-right: 1px solid #a4a4a4;">비트코인</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="javascript:alert('서비스 준비중!');" alt="송금하기" title="송금하기">송금하기</a></td>
				</tr>
				<tr>
					<td><a href="javascript:alert('서비스 준비중!');" alt="외국환전환" title="외국환전환">외국환전환</a></td>
				</tr>
				<tr>
					<td><a href="javascript:alert('서비스 준비중!');" alt="거래내역" title="거래내역">거래내역</a></td>
				</tr>
			</tbody>
		</table>	
		</div>
		<div style="width:25%; float:left;">
		<table class="t_list" style="border-right: 1px solid #d2d2d2;">
			<caption>SITEMAP</caption>
			<colgroup>
				<col />
			</colgroup>
			<thead>   
				<tr>
					<th scope="col" style="line-height:30px;">쇼핑몰</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><a href="javascript:alert('서비스 준비중!');" alt="쇼핑몰" title="쇼핑몰">쇼핑몰</a></td>
				</tr>
			</tbody>
		</table>	
		</div>
	
		</div>

	<!--#include virtual="/ez_admin/inc/footer.asp" -->
</div>
<!-- wrap -->
</body>
</html>