<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->
<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 
%>
<%
	fnDBConn()

	'지금 핀이 사용가능한 핀인지
	strSQL =  "  select s_name, handphone, talk_cacao, talk_wechat, talk_line from member where wallet_no='"& Session("SS_USERID") &"'"

	cmd.CommandText = strSQL
	Set rs = cmd.execute()

	if not(rs.eof or rs.bof) Then
		s_name = rs("s_name")
		handphone = rs("handphone")
		talk_cacao = rs("talk_cacao")
		talk_wechat = rs("talk_wechat")
		talk_line = rs("talk_line")
	end If
	
	rs.close

	fnDBClose()
%>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY</title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<script type="text/javascript">
		<!--
		//[] <--문자 범위 [^] <--부정 [0-9] <-- 숫자  
		//[0-9] => \d , [^0-9] => \D
		var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
		var rgx2 = /(\d+)(\d{3})/; 

		function getNumber(obj){
			
			 var num01;
			 var num02;
			 num01 = obj.value;
			 num02 = num01.replace(rgx1,"");
			 num01 = setComma(num02);
			 obj.value =  num01;

		}

		function setComma(inNum){
			 
			 var outNum;
			 outNum = inNum; 
			 while (rgx2.test(outNum)) {
				  outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
			  }
			 return outNum;

		}
		//-->
		</script>

		<script type="text/javascript">
		
			function send_ok()
			{			
				var wallet_no = $("#wallet_no").val();
				var password1 = $("#password1").val();
				var password2 = $("#password2").val();			
				var s_name = $("#s_name").val();
				var handphone = $("#handphone").val();
				var talk_cacao = $("#talk_cacao").val();
				var talk_wechat = $("#talk_wechat").val();
				var talk_line = $("#talk_line").val();

				if (password1.length > 0 && password1 != password2){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_77%>',
						text: '<%=EZ_LAN_53%>',
						callback: function(result) {
							$("#password1").focus();
						}
					});
					
				}else if (s_name.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_77%>',
						text: '<%=EZ_LAN_104%>',
						callback: function(result) {
							$("#s_name").focus();
						}
					});
					
				}else if (handphone.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_77%>',
						text: '<%=EZ_LAN_105%>',
						callback: function(result) {
							$("#handphone").focus();
						}
					});

				}else if (talk_cacao.length == 0 && talk_wechat.length == 0 && talk_line.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_77%>',
						text: '<%=EZ_LAN_106%>',
						callback: function(result) {
							$("#talk_cacao").focus();
						}
					});
					
				}else{

					$.ajax({
						 url: "mypage_ok.asp",
						 type: "POST",
						 data: { wallet_no: wallet_no, password: password1, handphone: handphone, s_name: s_name, talk_cacao: talk_cacao, talk_wechat: talk_wechat, talk_line: talk_line },  

						 success: function(data){

							 if(data == "YES"){						 
								modal({
									type: 'success',
									title: '<%=EZ_LAN_77%>',
									text: '<%=EZ_LAN_120%>',
									callback: function(result) {
										location.href="mypage.asp";
									}
								});		
							 }else{								 
								modal({
									type: 'error',
									title: '<%=EZ_LAN_77%>',
									text: '<%=EZ_LAN_121%>',
									callback: function(result) {
										location.href="mypage.asp";
									}
								});				
							 }
						 },
			 
						 complete: function(data){
						//	location.href="index.asp";
						 }
					});
				}	
			}
			
		</script>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active">회원정보수정</li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- banner start -->
				<!-- section start -->
			<%

				fnDBConn()

				Dim bank_name,account_no,input_name

				'지금 핀이 사용가능한 핀인지
				strSQL =  "  select bank_name,account_no,input_name from gcoin_bank where mode='kr'"

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					bank_name = rs("bank_name")
					account_no = rs("account_no")
					input_name = rs("input_name")
				end If

				rs.close
					
				fnDBClose()

			%>
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title"><%=EZ_LAN_77%></h1>
					<div class="separator-2"></div>
					<form role="form">
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_70%></label>
							<input type="text" class="form-control" name="wallet_no" id="wallet_no" readonly value="<%=Session("SS_USERID")%>">
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_115%></label>
							<input type="password" name="password1" id="password1" class="form-control margin-bottom-20" required>			
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_116%></label>
							<input type="password" name="password2" id="password2" class="form-control margin-bottom-20" required>	
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_117%></label>
							<input type="text" class="form-control" name="s_name" id="s_name" required value="<%=s_name%>">	
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_118%></label>
							<input type="text" class="form-control" name="handphone" id="handphone" required value="<%=handphone%>">			
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">KAKAO TALK ID</label>
							<input type="text" class="form-control" name="talk_cacao" id="talk_cacao" required value="<%=talk_cacao%>">			
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">WE CHAT ID</label>
							<input type="text" class="form-control" name="talk_wechat" id="talk_wechat" required value="<%=talk_wechat%>">			
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1">LINE ID</label>
							<input type="text" class="form-control" name="talk_line" id="talk_line" required value="<%=talk_line%>">			
						</div>

						<button type="button" class="btn btn-default"  onclick="send_ok();"><%=EZ_LAN_77%></button>
					</form>
				</div>
			</div>
			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
