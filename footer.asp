<!--#include virtual="/inc/Language.asp"-->
<!-- footer start (Add "dark" class to #footer in order to enable dark footer) -->
			<!-- ================ -->
			<footer id="footer" class="clearfix dark">
				<!-- .footer start -->
				<!-- ================ -->
				<div class="footer">
					<div class="container">
						<div class="footer-inner">
							<div class="row">
								<div class="col-md-33">
									<ul class="list-icons">
										<li><i class="fa fa-phone pr-10 text-default"></i> <%=EZ_LAN_49%> : <tel>02-568-0185</tel></li>
										<li><i class="fa fa-phone pr-10 text-default"></i> <%=EZ_LAN_50%> : <tel>070-4196-2711</tel></li>
										<li><i class="fa fa-envelope-o pr-10 text-default"></i> SNS : <sns>(KakaoTalk : ezpay | Line : e-zpay | WeChat : e-zpay) </sns></li>
										<li><a href="mailto:ezpay@gmail.com"><i class="fa fa-envelope-o pr-10"></i>ezpay@e-zpay.co.kr</a></li>
									</ul>
									<!--p class="text-center"><i class="fa fa-phone pr-5 pl-10"></i>고객센터 대표전화 : <tel>1577-5784</tel> <i class="fa fa-phone pr-5 pl-10"></i>해외지점 대표전화 : <tel>070-4110-9988</tel> <br><i class="fa fa-envelope-o pr-5 pl-10"></i><email>ezpay@gmail.com</email>
									</p-->
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .footer end -->

				<!-- .subfooter start -->
				<!-- ================ -->
				<div class="subfooter">
					<div class="container">
						<div class="subfooter-inner">
							<div class="row">
								<div class="col-md-12">
									<p class="text-center">Copyright © 2017 EZ Pay. All Rights Reserved</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- .subfooter end -->

			</footer>
			<!-- footer end -->