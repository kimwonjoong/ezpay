<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

MENU_IDX_ = 8
sMenuCode = "gamang"
PAGE_CODE_ = "gamang"
PAGE_TITLE_ = "가맹점출금신청"
sListFileName = PAGE_CODE_ & "_output.asp"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function fnUpdateChk(frm)
{
	if(confirm("출금신청을 하시겠습니까?"))
	{
		return true;
	}
	return false;
}
</script>
</head> 
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<div class="lyTitle"><%=PAGE_TITLE_%></div>

<div class="csCaption1" style="float:left;width:200px;">가맹점출금신청</div>
<form name="frmEdit" id="frmEdit" method="post" action="gamang_output_ok.asp" onsubmit="return fnUpdateChk(this);">
<input type="hidden" name="hdMode" id="hdMode" value="write">
<div class="lyMainTbl">
<table class="tblContent">
	<tr class="top">
		<th>가맹점지갑번호</th>
		<td><input type="text" readonly name="wallet_no" id="wallet_no" size="4" value="<%=Session("SS_WALLET")%>"></td>
	</tr>
	<tr>
		<th>G코인</th>
		<td><input type="text" name="gcoin" id="gcoin" size="15">G</td>
	</tr>
	<tr>
		<th>은행명</th>
		<td><select name="bank_name"><option value="우리은행">우리은행</option></select></td>
	</tr>
	<tr>
		<th>계좌번호</th>
		<td><input type="text" name="account_no" id="account_no" size="15"></td>
	</tr>
	<tr>
		<th>입금자명</th>
		<td><input type="text" name="input_name" id="input_name" size="15"></td>
	</tr>
</table>

<div class="lyCBtn">
	<span class="button base"><input type="submit" value="등록"></span>
	<span class="button base"><input type="button" value="취소" onclick="history.back();"></span>
</div>
</form>

<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>