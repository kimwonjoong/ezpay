<!--#include file="../inc/inc_top_common.asp"-->
<script runat="server" Language="Javascript" src="../inc/JSON_JS.asp"></script>
<%
on Error resume Next

nData = fnForm2Json(aData)

Dim handphone
Dim PushServerURL,ApplicationAPIKey,tickerText,contentTitle,message,sSuccRegIDList,sSuccPhoneList,sPushResult


Dim sreg_id: sreg_id=""

Dim nResultCnt : nResultCnt = 0
Dim cnt : cnt = 0

Dim sResultMsg : sResultMsg = ""
Dim arrTemp: arrTemp = ""

Dim sResult : sResult = "N"

Dim strSQL: strSQL=""

Dim nIdx : nIdx = fnNullInit(aData("idx"))


Dim now_gcoin,now_wallet,sCheck


fnDBConn()

sCheck = "NO"


	'지금 핀이 사용가능한 핀인지
	strSQL =  "  select wallet_no,gcoin from franchise_settlement where idx="& nIdx 

	

	cmd.CommandText = strSQL
	Set rs = cmd.execute()

	if not(rs.eof or rs.bof) then
		now_gcoin = rs("gcoin")
		now_wallet = rs("wallet_no")
		sCheck = "YES"
	end If
	
	rs.close
	
	If(sCheck="YES") Then

		strSQL=""
	
		dbConn.BeginTrans

		strSQL = strSQL & "	update  franchise_settlement set result='1' where idx=" & nIdx & " "
	
		cmd.CommandText = strSQL
		cmd.Execute nResultCnt, , adExecuteNoRecords



		if nResultCnt <> 1 or Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
			dbConn.RollBackTrans
			fnErrSave()
		else
			dbConn.CommitTrans
			sResult = "Y"
		'	fnLogSave()
		end If

	End If
	

	If sResult = "Y" then

		strSQL =  ""
		sreg_id = ""

		strSQL = " select reg_id from member where wallet_no='"& now_wallet &"'"

		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) Then		
			sreg_id = trim(rs("reg_id"))
		end if
		rs.close
		Set rs = Nothing

		If(sreg_id <> "") then
			' 안드로이드 ASP 푸시 요청
			PushServerURL = "https://android.googleapis.com/gcm/send"
			'  브라우저APIkey
			ApplicationAPIKey = "AIzaSyC-eisgmovdUQQgTwQ7f9hOFdrCvKqu0Lo"
			 ' regId(받을사람이 지급받은 registration_ids - 여러명일 경우 배열로 받아처리하면 될 듯 최대 1000)
			tickerText = "G코인"
			' 알림 제목
			contentTitle = "G코인 전자지갑"
			' 알림 내용
			message = "청산요청하신 " & fnFormatNumber(now_gcoin,0,0) & "원이 청산되었습니다."
			 
			sSuccRegIDList = ""
			sSuccPhoneList = ""
			sPushResult = fnMultiPushSend(message, sreg_id, handphone, "", sSuccRegIDList, sSuccPhoneList)
		End If

	End if

	fnDBClose()

	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('청산신청처리가 완료되었습니다.');"
	Response.Write "location.href='franchise_settlement.asp';"
	Response.Write "</script>"




%>

<%

'MUTI PUSH 발송(메세지, 전송 할 REG_ID(','구분), 전송 할 폰번호(','구분), 전송 실패 사유(','구분), 성공한 REG_ID(','구분), 성공한 폰번호(','구분))
Function fnMultiPushSend(sMsg, sRegIDList, sPhoneList, sFailReason, sSuccRegIDList, sSuccPhoneList)
	Dim APIKEY : APIKEY = "AIzaSyC-eisgmovdUQQgTwQ7f9hOFdrCvKqu0Lo"
	Dim sValue, aValue, nValue, i
	Dim sResultString : sResultString = ""

	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)
	
	Dim aRegID, aPhone, aSuccRegID, aSuccPhone
	Dim nRegID, nPhone, nSuccRegID, nSuccPhone
	Dim aFailReason, nFailReason

	aRegID = split(sRegIDList, ",")
	aPhone = split(sPhoneList, ",")
	
	nRegID = -1
	nPhone = -1

	if isArray(aRegID) then
		nRegID = ubound(aRegID, 1)
	end if
	
	if isArray(aPhone) then
		nPhone = ubound(aPhone, 1)
	end if

	if nRegID >=0 then
		Dim SEND_URL
		Dim seneHTTP
		SEND_URL = "https://android.googleapis.com/gcm/send"
		Set seneHTTP = Server.CreateObject("MSXML2.ServerXMLHTTP")
		seneHTTP.Open "POST",""& SEND_URL &"",false
		seneHTTP.SetRequestHeader "POST", ""& SEND_URL &" HTTP/1.0"
		seneHTTP.SetRequestHeader "Authorization", "key=" & APIKEY & ""
		seneHTTP.SetRequestHeader "Content-Type", "application/json"									'멀티 전송시
		'seneHTTP.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded"		'단일 전송시

		If seneHTTP.readyState = 1 Then		
			Dim collapse_key : collapse_key = ""

			for i=1 to 4 '4자리만 생성
				collapse_key = collapse_key & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
			next
			
			Dim json1 : Set json1 = jsObject()
			set json1("data") = jsObject()
			json1("data")("msg") = sMsg			
			json1("delay_while_idle") = true	'디바이스가 idle상태일때는 전달하지 않음(기본 false)
			json1("collapse_key") = collapse_key	 '메세지 그룹설정(오프라인에서 온라인 상태가 되면 많은 메세지가 전다되는 것을 막고 그룹내에서 가장 최신의 메시지만 전달)
			json1("registration_ids") = aRegID
			'json1("dry_run") = true		'전송TEST(기본 false)

			seneHTTP.send toJSON(json1)	 '멀티 전송시
			'str_tb = "registration_id=" & reg_id & "&collapse_key=" & collapse_key & "&delay_while_idle=1&data.msg=" & r_msg&result	'단일 전송시

			On Error Resume Next
			seneHTTP.waitForResponse 5
			If (seneHTTP.readyState = 4) And (seneHTTP.Status = 200) Then
				sResultString = Trim(seneHTTP.ResponseText)
			else
				sResultString="HTTP_ERR4" & "(" & seneHTTP.readyState & "/"  & seneHTTP.Status & ")"
			End If

			
		else
			sResultString="HTTP_ERR1"
		End If

		Set seneHTTP = Nothing
		fnMultiPushSend = sResultString
		
		'전송결과
		Dim nSuccess : nSuccess = 0		'성공건수
		Dim nFail : nFail = 0				'실패건수
		Dim jsonResult						'전송결과(JSON)
		Dim nResult							'전송결과 수
		Dim nTryCnt	: nTryCnt = 0													'전송 실패 시 재시도 횟수
		Dim nMaxTryCnt	: nMaxTryCnt = 1										'전송 실패 시 최대 재시도 횟수
		Dim nMaxSendCnt	: nMaxSendCnt = 1000									'1회 최대 전송 개수
		Dim sPushType, sKComm
		Dim sTmpMsgUserIdx, sSessionID, sPushKey, sUserID, nMsgCnt
		Dim nMsgIdx, sMsgChk, nDivIdx
		
		if sResultString = "HTTP_ERR1" then
			nFail = nRegID + 1
		elseif left(sResultString,9) = "HTTP_ERR4" then
			nFail = nRegID + 1 
		elseif len(sResultString) > 1 then		

			Dim jsonData : set jsonData = JSON.parse(sResultString)
			
			nSuccess = jsonData.success			'성공건수
			nFail = jsonData.failure					'실패건수
			nResult = jsonData.results.length		'결과건수

			sRegIDList = ""							'재시도 할 REG_ID 리스트 초기화
			'sPhoneList = ""							'재시도 할 PHONE 리스트 초기화
			sFailReason = ""							'재시도 사유 초기화
			for i=0 to nResult-1			
				'전송실패
				if instr(JSON.stringify(jsonData.results.get(i)), "error") then
					if sRegIDList <> "" then
						sRegIDList = sRegIDList & ","
						sPhoneList = sPhoneList & ","
						sFailReason = sFailReason & ","
					end if
					sRegIDList = sRegIDList & aRegID(i)
					sPhoneList = sPhoneList & aPhone(i)
					sFailReason = sFailReason & JSON.stringify(jsonData.results.get(i))
				'전송성공			
				else
					if sSuccRegIDList <> "" then
						sSuccRegIDList = sSuccRegIDList & ","
						sSuccPhoneList = sSuccPhoneList & ","
					end if
					sSuccRegIDList = sSuccRegIDList & aRegID(i)
					sSuccPhoneList = sSuccPhoneList & aPhone(i)
				end if
			next
			set jsonData = nothing
		end if
		
		nTryCnt = nTryCnt + 1	
		'실패건수가 있고 최대 재시도 횟수 내 인경우 실패건 재시도
		if nFail > 0 and nTryCnt < nMaxTryCnt and sRegIDList <> "" then
			'재시도
			sPushResult = fnMultiPushSend(sMsg, sRegIDList, sPhoneList, sFailReason, sSuccRegIDList, sSuccPhoneList)
		'전송완료
		else
			
			fnMultiPushSend = sResultString
			'결과출력
				'시도횟수
				'response.write "* PUSH 전송시도횟수 : " & nTryCnt & "회 시도(최대 " & nMaxTryCnt & "회까지 가능)<BR/>"
				'response.write "* 전송 메세지[" & sMsg & "]"
				
				'성공건
				aSuccRegID = split(sSuccRegIDList, ",")
				aSuccPhone = split(sSuccPhoneList, ",")
				nSuccRegID = -1
				nSuccPhone = -1
				if isArray(aSuccRegID) then
					nSuccRegID = ubound(aSuccRegID, 1)
				end if				
				if isArray(aSuccPhone) then
					nSuccPhone = ubound(aSuccPhone, 1)
				end if
				nSuccess = nSuccRegID+1
				'Response.Write "<br/><br/>===================================<BR/>"
				'Response.Write "* 성공 총 " & nSuccess& "건 <br/>"
				'Response.Write "===========================================<br/>"
				'for i=0 to nSuccRegID
				'	response.write (i+1) & ". " & aSuccPhone(i) & " [" & aSuccRegID(i) & "]<br/>"
				'next
				'Response.Write "<br/>========================================<BR/><br/>"

				'실패건
				aRegID = split(sRegIDList, ",")
				aPhone = split(sPhoneList, ",")
				aFailReason = split(sFailReason, ",")
				
				nRegID = -1
				nPhone = -1
				nFailReason = -1
				if isArray(aRegID) then
					nRegID = ubound(aRegID, 1)
				end if				
				if isArray(aPhone) then
					nPhone = ubound(aPhone, 1)
				end if
				if isArray(aFailReason) then
					nFailReason = ubound(aFailReason, 1)
				end if
				nFail = nRegID+1
				'Response.Write "<br/><br/>===================================<BR/>"
				'Response.Write "* 실패 총 " & nFail & "건 <br/>"
				'Response.Write "===========================================<br/>"
				'for i=0 to nRegID
				'	response.write (i+1) & ". " & aPhone(i) & " [" & aRegID(i) & "]<br/>"
				'	response.write "&nbsp;&nbsp;&nbsp;&nbsp;실패사유 (" & aFailReason(i) & ")<br/>"
				'	Response.Write "<br/>-------------------------------------------------------------------<br/>"					
				'next
				'Response.Write "<br/>=======================================<BR/><br/>"

			If Err and Err.Number <> 0 Then
				'Response.Write "<br/><br/>===================================<BR/>"
				'response.write "ERROR[" & Err.Number & "] Description :" & Err.Description & "<br/>"
				'Response.Write "Source : " & Err.Source & "<BR/>"
				'Response.Write "HelpContext : " & Err.HelpContext & "<BR/>"
				'Response.Write "<br/>======================================<BR/><br/>"
				'response.end
			End if
		end if 
	else		
		fnMultiPushSend = "PUSH발송 할 휴대폰번호가 없습니다"
		
	end if
end Function

Function Get_AbString( strSource, chrSource )

	Dim strDest, strTemp

	strDest = Split( strSource, chrSource )
	strTemp = ""

	For j = 0 To Ubound(strDest) 
	strTemp = strTemp & strDest(j)
	Next

	Get_AbString = strTemp

End Function
%>