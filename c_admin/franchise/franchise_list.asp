<!--#include file="../inc/inc_top_common.asp"-->
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<%
sbMemberAuthChk("P")

Dim sKColumn, sKeyword

Dim fromDate,toDate,difDate,coin_sum

Dim sKSDate : sKSDate = fnNullInit(request("txtSDate"))
Dim sKEDate : sKEDate = fnNullInit(request("txtEDate"))
Dim aOutcoin, nOutcoin: nOutcoin = -1

MENU_IDX_ = 8
sMenuCode = "franchise"
PAGE_CODE_ = "franchise"
'PAGE_TITLE_ = "가맹점관리"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&txtSDate="&sKSDate&"&txtEDate="&sKEDate

if isDate(sKSDate) and isDate(sKEDate) then
	if DateDiff("d", sKSDate, sKEDate) < 0 then
		sKEDate = sKSDate
	end if
else
	sKSDate = ""
	sKEDate = ""
end if

sFilter = ""

fnDBConn()

if sKeyword <> "" then
	if sKColumn = "wallet_no" then
		sFilter = sFilter & " and f.wallet_no like ? "
		cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "company" then
		sFilter = sFilter & " and f.company like ? "
		cmd.Parameters.Append cmd.CreateParameter("@company", adVarChar, adParamInput, 250, "%" & sKeyword & "%")	
	end if
end if

if isDate(sKSDate) and isDate(sKEDate) then
	sFilter = sFilter & " and convert(date, f.regist_date) between ? and ? "
	cmd.Parameters.Append cmd.CreateParameter("@reg_date1", adDate, adParamInput, , sKSDate)
	cmd.Parameters.Append cmd.CreateParameter("@reg_date2", adDate, adParamInput, , sKEDate)
end if

'sqlTable = "select a.*, app_name from store a left join app_info b on(a.app_id=b.app_id) where 1=1 " & sFilter
sqlTable = "select * from gcoin_franchise where 1=1 " & sFilter

'게시물 개수
sql = " select count(*) cnt from (" & sqlTable & ")a "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nRecordCount = trim(rs("cnt"))
end if
rs.close

'게시물 리스트	
sqlTable = " SELECT ROW_NUMBER() OVER(ORDER BY idx desc) AS RowNum, * FROM (" & sqlTable & ")a "	
sql = " ;with tbl as(" &  sqlTable & ") "
sql = sql & " SELECT TOP " & nPageSize & " * FROM tbl "
sql = sql & " WHERE RowNum >= " & (nPageNO-1)*nPageSize+1 & " and RowNum >= (SELECT MAX(RowNum)RowNum FROM (SELECT TOP " & (nPageNO-1)*nPageSize+1 & " RowNum FROM tbl ORDER BY RowNum ASC) a) ORDER BY RowNum ASC "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing
fnDBClose()

if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script type="text/javascript">
function input_ok(idx)
{

	sMsg = "승인처리하시겠습니까?";
	
	if(confirm(sMsg))
	{		
		var frm = document.getElementById("frmReg");
		
		frm.idx.value = idx;
		frm.action = "input_send_ok.asp";
		frm.submit();
	}
}
function input_ok2(idx)
{

	sMsg = "승인보류처리하시겠습니까?";
	
	if(confirm(sMsg))
	{		
		var frm = document.getElementById("frmReg");
		
		frm.idx.value = idx;
		frm.action = "input_send_ok2.asp";
		frm.submit();
	}
}
function input_ok3(idx)
{

	sMsg = "승인거절처리하시겠습니까?";
	
	if(confirm(sMsg))
	{		
		var frm = document.getElementById("frmReg");
		
		frm.idx.value = idx;
		frm.action = "input_send_ok3.asp";
		frm.submit();
	}
}
function fnSchChk(frm)
{
	return true;
}
function clickEdit(wallet_no){
	if(confirm("수정 하시겠습니까?")){
		document.myform.wallet_no.value = wallet_no;
		document.myform.action = "franchise_update.asp" ;
		document.myform.submit();		
	}
}

function clickDel(wallet_no){
	if(confirm("정말로 삭제 하시겠습니까? \n삭제하면 모든 잔여코인이\n모두 사라지며 복구가\n불가능합니다.")){
		document.myform.wallet_no.value = wallet_no;
		document.myform.action = "franchise_delete.asp" ;
		document.myform.submit();		
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<div class="lyMainTitle"><img src="/g_admin/images/common/icon_05.png" alt="<%=PAGE_TITLE_%>"></div>
<br>
<div class="lyCSch">
	<form name="frmSch" id="frmSch" action="<%=sListFileName%>" method="post" onsubmit="return fnSchChk(this);">
	<span class="txtLabel">가맹점신청일자</span>
	<input name="txtSDate" type="text" id="txtSDate" size="10" value="<%=sKSDate%>" onclick="Calendar_D(this);" class="txtDate" /> ~
	<input name="txtEDate" type="text" id="txtEDate" size="10" value="<%=sKEDate%>" onclick="Calendar_D(this);" class="txtDate"  />
	<select name="optColumn" id="optColumn">
		<option value="wallet_no" <%if sKColumn="wallet_no" then%>selected<%end if%>>지갑번호</option>
		<option value="company" <%if sKColumn="company" then%>selected<%end if%>>업체명</option>
	</select>
	<input type="text" class="txtKey" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<input type="image" src="/g_admin/images/common/btn_sch.jpg" width="62" height="23" align="absmiddle" title="검색하기" style="width:62px;height:23px">
	</form>
</div>
<form name="frmReg" id="frmReg"  method="post">
<input type="hidden" name="idx" id="idx" value="">
</form>
<div class="lyMainTbl">
<%=fnRecordCnt(nRecordCount, nPageNO, nPageCount)%>
<table class="tblMain">
	<tr>
		<th width="5%">번호</th>
		<th width="6%">지갑번호</th>
		<th width="8%">EZ페이</th>
		<th width="7%">수수료</th>
		<th width="7%">충전수수료</th>
		<th width="12%">가맹점명</th>
		<th width="8%">비상연락처</th>
		<th width="18%">가입일자</th>
		<th width="18%">승인여부</th>
		<th width="9%">수정</th>
		<th width="9%">삭제</th>
	</tr>
	<%

	fnDBConn()
	Dim gcoin_sum
	gcoin_sum = 0
	if nData >= 0 then
		for i=0 to nData


			sql=""
			sql="select sum(convert(money,m.gcoin)) as o_sum from member as m,gcoin_franchise as f where f.wallet_no = '"&aData("wallet_no")(i)&"' and f.wallet_no = m.wallet_no "
			'Response.write sql
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nOutcoin = fnRs2Json(aOutcoin, rs)
				If len(aOutcoin("o_sum"))=0 Then
					coin_sum =0
				Else
					coin_sum =aOutcoin("o_sum")
				End if
			Else
				coin_sum =0
			
			end if
			rs.close
			ParamInit()
			'Response.write "coin_sum:"&coin_sum
	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td><% If aData("result")(i) = "1" Then %><a href="franchise_view.asp?wallet_no=<%=aData("wallet_no")(i)%>"><% End If %><%=aData("wallet_no")(i)%></a></td>		
		<td><%=fnFormatNumber(coin_sum,0,0)%>EZ</td>
		<td><%=aData("fee")(i)%>%</td>
		<td><%=aData("charge_fee")(i)%>%</td>
		<td><%=aData("company")(i)%></td>
		<td><%=aData("tel")(i)%></td>
		<td><%=aData("regist_date")(i)%></td>
		<td><% If aData("result")(i) = "0" Then %><span class="button base"><a href="javascript:input_ok(<%=aData("idx")(i)%>);">승인</a></span>&nbsp;<span class="button base"><a href="javascript:input_ok2(<%=aData("idx")(i)%>);">보류</a></span>&nbsp;<span class="button base"><a href="javascript:input_ok3(<%=aData("idx")(i)%>);">거절</a></span><%ElseIf aData("result")(i) = "1" Then%><font color="blue">승인완료</font><%ElseIf aData("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aData("result")(i) = "9" Then%><font color="red">승인보류</font><%ElseIf aData("result")(i) = "8" Then%><font color="red">승인거절</font><%End If%></td>
		<td><% If aData("result")(i) = "1" Then %><span class="button base"><a href="javascript:clickEdit(<%=aData("wallet_no")(i)%>);">수정</a></span><%End if%></td>
		<td><span class="button base"><a href="javascript:clickDel(<%=aData("wallet_no")(i)%>);">삭제</a></span></td>
	</tr>
	<%
		gcoin_sum = gcoin_sum + coin_sum
		next
	%>
	<tr>
		<td><font color=red>합계</font></td><td></td><td><font color=red><%=fnFormatNumber(gcoin_sum,0,0)%>EZ</font></td><td colspan="8"></td>
	</tr>
	<%else%>
	<tr class="msg145">
		<td colspan="11">등록된 가맹점내역이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>
<form name="myform" method="post">
<input type="hidden" name="wallet_no">
</form>
<div class="lyRMsg"><span class="button base"><a href="franchise_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<%=fnPaging(sListFileName, nPageNO, nBlockSize, nPageCount, sQueryString)%>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>

<% fnDBClose()%>