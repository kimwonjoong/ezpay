<!--#include file="../inc/inc_top_common.asp"-->
<%
Dim aFranchise, nFranchise : nFranchise = -1
Dim idx,wallet_no,gcoin,fee,company,tel,regist_date,result,id,password,email

wallet_no = request("wallet_no")

MENU_IDX_ = 8
sMenuCode = "franchise"
PAGE_CODE_ = "franchise"
PAGE_TITLE_ = "가맹점관리"
sListFileName = PAGE_CODE_ & "_list.asp"


fnDBConn()


sql = "select f.*,m.id,m.password from gcoin_franchise as f,admin_member as m where m.wallet_no =f.wallet_no and f.wallet_no ='"&wallet_no&"'"
cmd.CommandText = sql
Set rs = cmd.Execute()


if not(rs.eof or rs.bof) Then

idx = rs("idx")
wallet_no = rs("wallet_no")
'gcoin = rs("coin_sum")
id = rs("id")
password = rs("password")
fee = rs("fee")
company = rs("company")
tel = rs("tel")
email = rs("email")
regist_date = rs("regist_date")
result = rs("result")

End If




%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript" src="/g_admin/editor/SE2.3.1.O9858/js/HuskyEZCreator.js" charset="utf-8"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script src="../js/script_ajax_cond.js" type="text/javascript"></script>



</script>
</head> 
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->

<div class="lyTitle"><%=PAGE_TITLE_%></div>
<form name="frmEdit" id="frmEdit" method="post" >
<input type="hidden" name="idx" value="<%=idx%>">
<input type="hidden" name="wallet_no" value="<%=wallet_no%>">
<div class="lyMainTbl">
<table class="tblContent">

	<tr class="top">
		<th width="20%">가맹점명</th>
		<td><%=company%></td>
	</tr>
	<tr>
		<th width="20%">지갑번호</th>
		<td><%=wallet_no%></td>
	</tr>
	<tr>
		<th width="20%">아이디</th>
		<td><%=id%></td>
	</tr>
	<tr>
		<th width="20%">패스워드</th>
		<td><%=password%></td>
	</tr>
	<tr>
		<th width="20%">EZ페이</th>
		<td>EZ</td>
	</tr>
	<tr>
		<th width="20%">수수료</th>
		<td>
		<%=fee*0.01%>%
		</td>
	</tr>
	<tr>
		<th width="20%">연락처</th>
		<td><%=tel%></td>
	</tr>
	<tr>
		<th width="20%">email</th>
		<td><%=email%></td>
	</tr>
	<tr>
		<th width="20%">승인여부</th>
		<td><% If result = "0" then%>미승인<% ElseIf result = "1" Then%>승인<%End if%></td>
	</tr>
	<tr>
		<th width="20%">가입일</th>
		<td><%=regist_date%></td>
	</tr>	
	

</table>
</div>
<div class="lyCBtn">

		<span class="button base"><input type="button" value="리스트" onclick="history.back();"></span>

</div>
</form>

<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>