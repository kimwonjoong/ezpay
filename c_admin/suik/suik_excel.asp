<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

Response.Buffer = True
Response.ContentType = "appllication/vnd.ms-excel" 
Response.CacheControl = "public"

Response.AddHeader "Content-Disposition","attachment; filename=수수료확인.xls"

Dim sKColumn, sKeyword,sKColumn2

Dim fromDate,toDate,difDate
Dim aOutcoin, nOutcoin: nOutcoin = -1
Dim aIncoin, nIncoin: nIncoin = -1
Dim aChargecoin, nChargecoin: nChargecoin = -1

Dim company,wallet_no,g_fee,c_fee


nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKColumn2 = fnKeywordFilter(Request("optColumn2"))

If sKColumn = "" Then
	sKColumn = year(date)
End If
If sKColumn2 = "" Then
	sKColumn2 = month(date)
End if

sFilter = ""

fnDBConn()

sql = "select company,wallet_no,convert(float,fee)*0.01 as g_fee,convert(float,charge_fee)*0.01 as c_fee from gcoin_franchise  where wallet_no='"&Session("SS_WALLET") &"'"
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	company = rs("company")
	wallet_no = rs("wallet_no")
	'gcoin = rs("coin_sum")
	g_fee = rs("g_fee")
	c_fee = rs("c_fee")
end if
rs.close
ParamInit()
fnDBClose()


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">

<div class="lyTitle"><%=PAGE_TITLE_%></div>

<div class="lyMainTbl">

<table class="tblMain">
	<tr>
		<th width="5%">년도</th>
		<th width="5%">월</th>
		<th width="5%">일</th>
		<th width="12%">이체금액</th>
		<th width="12%">이체수수료</th>
		<th width="12%">충전금액</th>
		<th width="12%">충전수수료</th>
		<th width="12%">출금금액</th>
		<th width="12%">청산금액</th>
		<th width="12%">잔액</th>
	</tr>
	<%
	Dim t_suik,t_jichul,t_fee1,t_fee2,t_charge,t_settlement,t_chak,o_sum,o_sum2,c_sum,c_sum2,c_sum3,nSsum
	t_suik=0
	t_jichul=0
	t_settlement=0
	t_charge=0
	t_fee1=0
	t_fee2=0
	t_chak=0
	nSsum=0
	o_sum=0
	o_sum2=0
	c_sum=0
	c_sum2=0
	c_sum3=0
	

	If sKColumn2 = "1" Or sKColumn2 = "3" Or sKColumn2 = "5" Or sKColumn2 = "7" Or sKColumn2 = "8" Or sKColumn2 = "10" Or sKColumn2 = "12" then
		j = 31
	ElseIf sKColumn2 = "2" then
		j = 29
	Else
		j = 30
	End If
	
		for i=1 to j

			fnDBConn()

			sql=""
			sql="select f.wallet_no,sum(convert(money,b.gcoin)) as o_sum,sum(convert(money,b.fee)) as c_sum from gcoin_transfer as b,gcoin_franchise as f where b.receive_wallet = '"&wallet_no&"' and  DATEPART(yyyy, b.regist_date) ='"&sKColumn&"'  and  DATEPART(mm, b.regist_date) ='"&sKColumn2&"' and  DATEPART(dd, b.regist_date) ='"&i&"' group by f.wallet_no,f.fee"
			'Response.write sql
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nOutcoin = fnRs2Json(aOutcoin, rs)
				o_sum2 =aOutcoin("o_sum")
				c_sum2 =aOutcoin("c_sum")
			Else
				o_sum2 =0
				c_sum2 =0
			
			end if
			rs.close
			ParamInit()

			sql=""
			sql="select f.wallet_no,sum(convert(money,b.gcoin)) as o_sum from gcoin_transfer as b,gcoin_franchise as f where b.send_wallet = '"&wallet_no&"' and  DATEPART(yyyy, b.regist_date) ='"&sKColumn&"'  and  DATEPART(mm, b.regist_date) ='"&sKColumn2&"' and  DATEPART(dd, b.regist_date) ='"&i&"' group by f.wallet_no,f.fee"
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nIncoin = fnRs2Json(aIncoin, rs)
				o_sum =aIncoin("o_sum")
			Else
				o_sum =0
			
			end if
			rs.close
			ParamInit()


			sql=""
			sql="select wallet_no,sum(convert(money,fee)) as o_sum,sum(convert(money,gcoin)) as o_sum2 from gcoin_buy where wallet_no = '"&wallet_no&"' and  DATEPART(yyyy, regist_date) ='"&sKColumn&"'  and  DATEPART(mm, regist_date) ='"&sKColumn2&"' and  DATEPART(dd, regist_date) ='"&i&"' group by wallet_no"
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nChargecoin = fnRs2Json(aChargecoin, rs)
				c_sum =aChargecoin("o_sum")
				c_sum3 =aChargecoin("o_sum2")
			Else
				c_sum =0
				c_sum3 =0
			
			end if
			rs.close
			ParamInit()

			sql=""
			sql = "select sum(convert(money,gcoin)) g_sum from gcoin_settlement where 1=1 and wallet_no = '"&wallet_no&"' and  DATEPART(yyyy, regist_date) ='"&sKColumn&"'  and  DATEPART(mm, regist_date) ='"&sKColumn2&"' and  DATEPART(dd, regist_date) ='"&i&"' group by wallet_no"

			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
				nSsum = trim(rs("g_sum"))
			Else
				nSsum = 0
			end if
			rs.close

			t_suik=t_suik+CCur(o_sum2)+CCur(c_sum2)
			t_fee1=t_fee1+CCur(c_sum2)
			t_fee2=t_fee2+CCur(c_sum)
			t_jichul=t_jichul+CCur(o_sum)
			t_settlement=t_settlement+CCur(nSsum)
			t_charge=t_charge+CCur(c_sum3)
			t_chak=t_chak+(CCur(o_sum2)+CCur(c_sum3))-(CCur(c_sum)+CCur(o_sum)+CCur(nSsum))


	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=sKColumn%></td>
		<td><%=sKColumn2%></td>
		<td><%=i%></td>
		<td><%=fnFormatNumber(CCur(o_sum2)+CCur(c_sum2),0,0)%></td>
		<td><font color="red"><%=fnFormatNumber(CCur(c_sum2),0,0)%></font></td>
		<td><%=fnFormatNumber(CCur(c_sum3),0,0)%></td>
		<td><font color="red"><%=fnFormatNumber(CCur(c_sum),0,0)%></font></td>
		<td><%=fnFormatNumber(CCur(o_sum),0,0)%></td>
		<td><%=fnFormatNumber(CCur(nSsum),0,0)%></td>
		<td><%=fnFormatNumber((CCur(o_sum2)+CCur(c_sum3))-(CCur(c_sum)+CCur(o_sum)+CCur(nSsum)),0,0)%></td>
	</tr>
	<%
		fnDBClose()
		next
	%>

	

	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><font color="blue">total</font></td>
		<td></td>
		<td></td>
		<td><font color="blue"><%=fnFormatNumber(t_suik,0,0)%></font></td>
		<td><font color="red"><%=fnFormatNumber(t_fee1,0,0)%></font></td>
		<td><font color="blue"><%=fnFormatNumber(t_charge,0,0)%></font></td>
		<td><font color="red"><%=fnFormatNumber(t_fee2,0,0)%></font></td>
		<td><font color="blue"><%=fnFormatNumber(t_jichul,0,0)%></font></td>
		<td><font color="blue"><%=fnFormatNumber(t_settlement,0,0)%></font></td>
		<td><font color="blue"><%=fnFormatNumber(t_chak,0,0)%></font></td>
	</tr>
</table>
</div>

</body>
</html>