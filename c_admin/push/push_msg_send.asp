<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")
on error resume next
%>
<html>
<link rel="stylesheet" type="text/css" href="./css/default.css">
<head>
<link href="../css/default.css" rel="stylesheet" type="text/css">
</head>
<body>
<%
Dim nTryCnt	: nTryCnt = 0													'전송 실패 시 재시도 횟수
Dim nMaxTryCnt	: nMaxTryCnt = 10											'전송 실패 시 최대 재시도 횟수
Dim nMaxSendCnt	: nMaxSendCnt = 1000									'1회 최대 전송 개수
Dim sPushType, sKComm
Dim sTmpMsgUserIdx, sSessionID, sPushKey, sUserID, nMsgCnt
Dim nMsgIdx, sDupChk, sIP

MENU_IDX_ = 7
sMenuCode = "admin"
PAGE_CODE_ = "push"
sTmpMsgUserIdx = fnKeyworeFilter(request.form("hdTmpMsgUserIdx"))
sSessionID = fnKeyworeFilter(request.form("hdSessionID"))
sPushKey = fnKeyworeFilter(request.form("hdPushKey"))
sMsg = fnKeyworeFilter(request.form("txtMsg"))
sPushType = fnKeyworeFilter(request.form("rdSendType"))
sUserID = fnKeyworeFilter(request.form("hdUserID"))
nMsgCnt = fnIntInit(request.form("txtMsgCnt"))
sKComm = fnKeywordFilter(Request.form("hdKComm"))

sIP = trim(Request.ServerVariables("REMOTE_ADDR"))

if SS_GROUP = "A" then
else
	sKComm = SS_COMM_ID
end if

'세션정보체크
if sUserID <> SS_USER_ID or sSessionID <> SESSION_ID_ then
	response.write "<script type='text/javascript'>alert('세션정보가 일치하지 않습니다.');parent.document.location.href='push_list.asp';</script>"
	response.end
end if

'메세지
if len(sMsg) < 1 then
	response.write "<script type='text/javascript'>alert('PUSH 메세지가 없습니다.');parent.document.location.href='push_list.asp';</script>"
	response.end
end if

'메세지구분
if sPushType="p" then
elseif sPushType="m" then
else
	response.write "<script type='text/javascript'>alert('메세지구분을 선택해주세요.');parent.document.location.href='push_list.asp';</script>"
	response.end
end if

'전송대상
if nMsgCnt <= 0 then
	response.write "<script type='text/javascript'>alert('전송 할 대상이 선택되지 않았습니다..');parent.document.location.href='push_list.asp';</script>"
	response.end
end if

fnDBConn()

sDupChk = "fail"

'중복체크
sql = " select * from member_msg_data where push_key=? " 
cmd.Parameters.Append cmd.CreateParameter("@push_key", adVarChar, adParamInput, 30, sPushKey)
cmd.CommandText = sql
Set rs = cmd.Execute(sql)		
if rs.bof or rs.eof then
	sDupChk = ""
end if
rs.close
Set rs = nothing
ParamInit()

'중복인경우
if sDupChk = "fail" then
	response.write "<script type='text/javascript'>alert('이미 처리된 메세지 입니다.');parent.document.location.href='push_list.asp';</script>"
else
	dbConn.BeginTrans
	
	'푸쉬인경우 DB에 저장 후 후발송
	if sPushType = "p" then
		'메세지 저장(상태:0 -> 처리요청)
		sql = "insert into member_msg_data (comm_id, m_userid, store_code, push_key, msg_cnt, msg_send_cnt, msg_type, send_chk, contents, insert_date, send_ip) values (?, ?, ?, ?, ?, 0, ?, '0', ?, getdate(), ?) ; select ? = @@identity "
		cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, sKComm)
		cmd.Parameters.Append cmd.CreateParameter("@m_userid", adVarChar, adParamInput, 50, SS_USER_ID)
		cmd.Parameters.Append cmd.CreateParameter("@store_code", adVarChar, adParamInput, 50, SS_STORE_CODE)
		cmd.Parameters.Append cmd.CreateParameter("@push_key", adVarChar, adParamInput, 30, sPushKey)
		cmd.Parameters.Append cmd.CreateParameter("@msg_cnt", adInteger, adParamInput, 4, nMsgCnt)
		cmd.Parameters.Append cmd.CreateParameter("@msg_type", adChar, adParamInput, 1, sPushType)
		cmd.Parameters.Append cmd.CreateParameter("@contents", adLongVarChar, adParamInput, LenB(sMsg), sMsg)
		cmd.Parameters.Append cmd.CreateParameter("@send_ip", adVarChar, adParaminput, 15, sIP)
		cmd.Parameters.Append cmd.CreateParameter("@new_idx", adInteger, adParamOutput, 0)
		cmd.CommandText = sql
		cmd.Execute , , adExecuteNoRecords
		nMsgIdx = cmd.Parameters("@new_idx").value   
		ParamInit()

		'전송대상 저장(상태:0 -> 발송전)
		sql = " insert into member_msg_list (comm_id, msg_idx, m_userid, store_code, msg_type, handphone, status, insert_date) select ? comm_id, ? msg_idx, m_userid, store_code, ? msg_type, handphone, '0', getdate() from member_msg_list_tmp where session_id=? and m_userid=? and push_key=? "
		cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, sKComm)
		cmd.Parameters.Append cmd.CreateParameter("@msg_idx", adInteger, adParamInput, 4, nMsgIdx)
		cmd.Parameters.Append cmd.CreateParameter("@msg_type", adChar, adParamInput, 1, sPushType)
		cmd.Parameters.Append cmd.CreateParameter("@session_id", adVarChar, adParamInput, 50, sSessionID)
		cmd.Parameters.Append cmd.CreateParameter("@m_userid", adVarChar, adParamInput, 50, SS_USER_ID)
		cmd.Parameters.Append cmd.CreateParameter("@push_key", adVarChar, adParamInput, 30, sPushKey)
		cmd.CommandText = sql
		cmd.Execute , , adExecuteNoRecords
		ParamInit()
	'알림인경우 DB에 저장만함
	else
		'메세지 저장(상태:2 -> 발송완료)
		sql = "insert into member_msg_data (comm_id, m_userid, store_code, push_key, msg_cnt, msg_send_cnt, msg_type, send_chk, contents, insert_date, send_ip) values (?, ?, ?, ?, ?, ?, ?, '2', ?, getdate(), ?) ; select ? = @@identity "
		cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, sKComm)
		cmd.Parameters.Append cmd.CreateParameter("@m_userid", adVarChar, adParamInput, 50, SS_USER_ID)
		cmd.Parameters.Append cmd.CreateParameter("@store_code", adVarChar, adParamInput, 50, SS_STORE_CODE)
		cmd.Parameters.Append cmd.CreateParameter("@push_key", adVarChar, adParamInput, 30, sPushKey)
		cmd.Parameters.Append cmd.CreateParameter("@msg_cnt", adInteger, adParamInput, 4, nMsgCnt)
		cmd.Parameters.Append cmd.CreateParameter("@msg_send_cnt", adInteger, adParamInput, 4, nMsgCnt)
		cmd.Parameters.Append cmd.CreateParameter("@msg_type", adChar, adParamInput, 1, sPushType)
		cmd.Parameters.Append cmd.CreateParameter("@contents", adLongVarChar, adParamInput, LenB(sMsg), sMsg)
		cmd.Parameters.Append cmd.CreateParameter("@send_ip", adVarChar, adParaminput, 15, sIP)
		cmd.Parameters.Append cmd.CreateParameter("@new_idx", adInteger, adParamOutput, 0)
		cmd.CommandText = sql
		cmd.Execute , , adExecuteNoRecords
		nMsgIdx = cmd.Parameters("@new_idx").value   
		ParamInit()

		'전송대상 저장(상태:1 -> 발송완료)
		sql = " insert into member_msg_list (comm_id, msg_idx, m_userid, store_code, msg_type, handphone, status, send_date, insert_date) select ? comm_id, ? msg_idx, m_userid, store_code, ? msg_type, handphone, '1', getdate(), getdate() from member_msg_list_tmp where session_id=? and m_userid=? and push_key=? "
		cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 20, sKComm)
		cmd.Parameters.Append cmd.CreateParameter("@msg_idx", adInteger, adParamInput, 4, nMsgIdx)
		cmd.Parameters.Append cmd.CreateParameter("@msg_type", adChar, adParamInput, 1, sPushType)
		cmd.Parameters.Append cmd.CreateParameter("@session_id", adVarChar, adParamInput, 50, sSessionID)
		cmd.Parameters.Append cmd.CreateParameter("@m_userid", adVarChar, adParamInput, 50, SS_USER_ID)
		cmd.Parameters.Append cmd.CreateParameter("@push_key", adVarChar, adParamInput, 30, sPushKey)
		cmd.CommandText = sql
		cmd.Execute , , adExecuteNoRecords
		ParamInit()

	end if

	'임시데이터 삭제
	sql = " delete from member_msg_list_tmp where session_id=?"
	cmd.Parameters.Append cmd.CreateParameter("@session_id", adVarChar, adParamInput, 50, sSessionID)
	cmd.CommandText = sql
	cmd.Execute , , adExecuteNoRecords
	ParamInit()

	if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
		dbConn.RollBackTrans

		'fnErrChk()
		'response.end

		Response.Write "<script type='text/javascript'>"
		Response.Write "alert('처리중 오류가 발생하여 메세지 전송에 실패하였습니다.');"
		Response.Write "location.href='ifr_push_msg.asp?pushKey="&sPushKey&"&hdKComm=" & sKComm & "';"
		Response.Write "</script>"
	else		
		dbConn.CommitTrans
		
		if sPushType="p" then
%>		
	<div>메세지구분 : <%=sPushType%></div>
	<div>메세지건수 : <input type="text" readOnly name="txtMsgCnt" id="txtMsgCnt" value="<%=nMsgCnt%>">건</div>
	<div>메세지내용 : <%=sMsg%></div>
	<div>메세지전송건수 : <input type="text" readOnly name="txtSendCnt" id="txtSendCnt" value="0"></div>
	<div>메세지전송결과</div>
	<div id="lyLoading" style="text-align:center;">처리중입니다. 잠시만 기다려주세요.</div>
	<div id="lyResult" style="width:100%;height:200px;overflow-x;hidden;overflow-y:scroll">

	</div>
	<form name="frmPushSend" id="frmPushSend" method="post" action="push_msg_send_ok.asp" target="ifrHidden">
	<input type="hidden" name="hdMsgIdx" id="hdMsgIdx" value="<%=nMsgIdx%>">
	<input type="hidden" name="hdPushKey" id="hdPushKey" value="<%=sPushKey%>">
	<input type="hidden" name="hdDivIdx" id="hdDivIdx" value="1">
	</form>
	<iframe src="" name="ifrHidden" id="ifrHidden" frameborder="0" width="95%" height="500"></iframe>
<%
			Response.Write "<script type='text/javascript'>"
			Response.Write "var frm = document.getElementById('frmPushSend');"
			Response.Write "frm.submit();"
			Response.Write "</script>"
		else			
			Response.Write "<script type='text/javascript'>"
			response.write "alert('알림메세지 전송이 완료되었습니다.');"
			response.write "parent.document.location.href='push_msg_list.asp';"
			Response.Write "</script>"
		end if
	end if
end if
fnDBClose()		
%>
</body>
</html>