<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")
on error resume next
%>
<html>
<link rel="stylesheet" type="text/css" href="/g_admin/css/default.css">
<head>
<script runat="server" Language="Javascript" src="/g_admin/inc/JSON_JS.asp"></script>
</head>
<body>
<%
Dim nTryCnt	: nTryCnt = 0													'전송 실패 시 재시도 횟수
Dim nMaxTryCnt	: nMaxTryCnt = 1										'전송 실패 시 최대 재시도 횟수
Dim nMaxSendCnt	: nMaxSendCnt = 1000									'1회 최대 전송 개수
Dim sPushType, sKComm
Dim sTmpMsgUserIdx, sSessionID, sPushKey, sUserID, nMsgCnt
Dim nMsgIdx, sMsgChk, nDivIdx

MENU_IDX_ = 7
sMenuCode = "admin"
PAGE_CODE_ = "push"
nMsgIdx = fnIdxInit(request.form("hdMsgIdx"))
nDivIdx = fnIdxInit(request.form("hdDivIdx"))
sSessionID = fnKeyworeFilter(request.form("hdSessionID"))
sPushKey = fnKeyworeFilter(request.form("hdPushKey"))
sUserID = fnKeyworeFilter(request.form("hdUserID"))
nMsgCnt = fnIntInit(request.form("txtMsgCnt"))
fnDBConn()

sMsgChk = "no"

'메세지
if len(nMsgIdx) < 1 then
	response.write "<script type='text/javascript'>alert('PUSH 메세지가 없습니다.');parent.parent.document.location.href='push_list.asp';</script>"
	response.end
end if

if len(sPushKey) < 1 then
	response.write "<script type='text/javascript'>alert('PUSH KEY가 없습니다.');parent.parent.document.location.href='push_list.asp';</script>"
	response.end
end if

'메세지체크
sql = " select * from member_msg_data where idx=? and m_userid='" & SS_USER_ID & "' " 
cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParamInput, 4, nMsgIdx)
if SS_GROUP = "A" then
else
	sql = sql & " and comm_id=? "
	cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, SS_COMM_ID)
end if
cmd.CommandText = sql
Set rs = cmd.Execute()		
if rs.bof or rs.eof then
	sMsgChk = "no"
else
	sMsgChk = fnNullInit(rs("send_chk"))
	sMsg = fnNullInit(rs("contents"))
	sPushType = fnNullInit(rs("msg_type"))
	if sMsgChk = "2" then
		sMsgChk = "complete"
	end if
end if
rs.close
ParamInit()

nData = -1


'중복인경우
if sMsgChk = "no" then
	response.write "<script type='text/javascript'>alert('메세지 정보가 없습니다.');parent.parent.document.location.href='push_list.asp';</script>"
elseif sMsgChk = "complete" then
	response.write "<script type='text/javascript'>alert('발송이 완료된 메세지입니다.');parent.parent.document.location.href='push_list.asp';</script>"
else
	dbConn.BeginTrans
	'전송대상 가져오기
	sql = "select top " & nMaxSendCnt & " b.reg_id, a.handphone from member_msg_list a WITH(NOLOCK) join member b WITH(NOLOCK) on(a.handphone=b.handphone) where a.msg_idx=? and a.status='0' "
	cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParamInput, 4, nMsgIdx)
	if SS_GROUP = "A" then
	else
		sql = sql & " and a.comm_id=? "
		cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, SS_COMM_ID)
	end if
	sql = sql & " order by a.idx "
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.bof or rs.eof) then
		aData = rs.getRows()
	end if
	rs.close
	ParamInit()

	if isArray(aData) then
		nData = ubound(aData, 2)
	end if
	'메세지 상태 및 발송개수 변경(발송중으로)
	sql = "update member_msg_data set send_chk=(case when msg_cnt <= isnull(msg_send_cnt,0)+"&(nData+1)&" then '2' else '1' end), msg_send_cnt=isnull(msg_send_cnt,0)+"&(nData+1)&" where idx=? and send_chk in ('0','1') "
	cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParamInput, 4, nMsgIdx)
	if SS_GROUP = "A" then
	else
		sql = sql & " and comm_id=? "
		cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, SS_COMM_ID)
	end if
	cmd.CommandText = sql
	cmd.Execute , , adExecuteNoRecords
	ParamInit()

	'전송대상 상태변경
	sql = "update member_msg_list set status='1', send_date=getdate() where msg_idx=? and idx in(select top " & nMaxSendCnt & " idx from member_msg_list where msg_idx=? and status='0' "
	cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParamInput, 4, nMsgIdx)
	cmd.Parameters.Append cmd.CreateParameter("@msg_idx", adInteger, adParamInput, 4, nMsgIdx)
	if SS_GROUP = "A" then
	else
		sql = sql & " and comm_id=? "
		cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, SS_COMM_ID)
	end if
	sql = sql & "  order by idx) "
	cmd.CommandText = sql
	cmd.Execute , , adExecuteNoRecords

	if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
		dbConn.RollBackTrans

		nData = -1
		fnErrSave()

		Response.Write "<script type='text/javascript'>"
		Response.Write "alert('처리중 오류가 발생하여 메세지 전송에 실패하였습니다.');"
		Response.Write "parent.parent.document.location.href='push_list.asp';"
		Response.Write "</script>"
	else		
		dbConn.CommitTrans
	end if

end if
Set rs = nothing
fnDBClose()	

Dim sRegIDList, sPhoneList, sSuccRegIDList, sSuccPhoneList
Dim sPushResult
if nData >= 0 then
	sRegIDList = ""
	sPhoneList = ""
	sSuccRegIDList = ""
	sSuccPhoneList = ""
	for i=0 to nData
		if i > 0 then
			sRegIDList = sRegIDList & ","
			sPhoneList = sPhoneList & ","
		end if
		sRegIDList = sRegIDList & trim(aData(0,i))
		sPhoneList = sPhoneList & trim(aData(1,i))
	next

	If nData >=0 then
		sPushResult = fnMultiPushSend(sMsg, sRegIDList, sPhoneList, "", sSuccRegIDList, sSuccPhoneList, SS_COMM_ID)
		
%>
			<form name="frmPushSend" id="frmPushSend" method="post" action="push_msg_send_ok.asp">
			<input type="hidden" name="hdMsgIdx" id="hdMsgIdx" value="<%=nMsgIdx%>">
			<input type="hidden" name="hdPushKey" id="hdPushKey" value="<%=sPushKey%>">
			<input type="hidden" name="hdDivIdx" id="hdDivIdx" value="<%=nDivIdx+1%>">
			</form>
<%
			response.write "<script type='text/javascript'>"
			response.write "function sleep(msecs)"
		    response.write "{"
			response.write " var start = new Date().getTime();"
			response.write " var cur = start;"
			response.write " while (cur - start < msecs) "
		    response.write " {"
            response.write "   cur = new Date().getTime();"
			response.write " }"
			response.write "}"

			response.write "var obj = parent.document.getElementById('lyResult');"
			response.write "obj.innerHTML += '<br/>" & sPushResult & "';"
			response.write "parent.document.getElementById('txtSendCnt').value = eval(eval(parent.document.getElementById('txtSendCnt').value)+eval(" & (nData+1)& "));"			
			'response.write "alert('* PUSH발송 완료("&(nSuccess+nFail)&"건 시도 / " & nTryCnt &"회 시도)\n- 성공 : " & nSuccess & "건 \n- 실패 : " & nFail & "건');"
			response.write "if(eval(parent.document.getElementById('txtSendCnt').value) < eval(parent.document.getElementById('txtMsgCnt').value)){"
			Response.Write "var frm = document.getElementById('frmPushSend');"
			'Response.Write "sleep(10000);"
			Response.Write "frm.submit();}else{"
			response.write "parent.document.getElementById('lyLoading').innerHTML = '메세지 전송이 완료되었습니다.';"			
			response.write "alert('메세지 전송이 완료되었습니다.');"
			response.write "parent.parent.document.location.href='push_msg_list.asp';"
			response.write "}"
			response.write "</script>"
	Else
		response.write "<script type='text/javascript'>"
		response.write "alert('PUSH발송 할 휴대폰번호가 없습니다.');"
		response.write "</script>"
	End If
end if	

'MUTI PUSH 발송(메세지, 전송 할 REG_ID(','구분), 전송 할 폰번호(','구분), 전송 실패 사유(','구분), 성공한 REG_ID(','구분), 성공한 폰번호(','구분))
Function fnMultiPushSend(sMsg, sRegIDList, sPhoneList, sFailReason, sSuccRegIDList, sSuccPhoneList, comm_id)
	Dim APIKEY : APIKEY = "AIzaSyC-eisgmovdUQQgTwQ7f9hOFdrCvKqu0Lo"
	Dim sValue, aValue, nValue, i
	Dim sResultString : sResultString = ""

	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)
	
	Dim aRegID, aPhone, aSuccRegID, aSuccPhone
	Dim nRegID, nPhone, nSuccRegID, nSuccPhone
	Dim aFailReason, nFailReason

	aRegID = split(sRegIDList, ",")
	aPhone = split(sPhoneList, ",")
	
	nRegID = -1
	nPhone = -1

	if isArray(aRegID) then
		nRegID = ubound(aRegID, 1)
	end if
	
	if isArray(aPhone) then
		nPhone = ubound(aPhone, 1)
	end if

	if nRegID >=0 then
		Dim SEND_URL
		Dim seneHTTP
		SEND_URL = "https://android.googleapis.com/gcm/send"
		Set seneHTTP = Server.CreateObject("MSXML2.ServerXMLHTTP")
		seneHTTP.Open "POST",""& SEND_URL &"",false
		seneHTTP.SetRequestHeader "POST", ""& SEND_URL &" HTTP/1.0"
		seneHTTP.SetRequestHeader "Authorization", "key=" & APIKEY & ""
		seneHTTP.SetRequestHeader "Content-Type", "application/json"									'멀티 전송시
		'seneHTTP.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded"		'단일 전송시

		If seneHTTP.readyState = 1 Then		
			Dim collapse_key : collapse_key = ""

			for i=1 to 4 '4자리만 생성
				collapse_key = collapse_key & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
			next
			
			Dim json1 : Set json1 = jsObject()
			set json1("data") = jsObject()
			json1("data")("msg") = sMsg			
			json1("delay_while_idle") = true	'디바이스가 idle상태일때는 전달하지 않음(기본 false)
			json1("collapse_key") = collapse_key	 '메세지 그룹설정(오프라인에서 온라인 상태가 되면 많은 메세지가 전다되는 것을 막고 그룹내에서 가장 최신의 메시지만 전달)
			json1("registration_ids") = aRegID
			'json1("dry_run") = true		'전송TEST(기본 false)

			seneHTTP.send toJSON(json1)	 '멀티 전송시
			'str_tb = "registration_id=" & reg_id & "&collapse_key=" & collapse_key & "&delay_while_idle=1&data.msg=" & r_msg&result	'단일 전송시

			On Error Resume Next
			seneHTTP.waitForResponse 5
			If (seneHTTP.readyState = 4) And (seneHTTP.Status = 200) Then
				sResultString = Trim(seneHTTP.ResponseText)
			else
				sResultString="HTTP_ERR4" & "(" & seneHTTP.readyState & "/"  & seneHTTP.Status & ")"
			End If

			fnDBConn()
				'푸쉬발송 로그저장
				sql = "insert into member_msg_log (comm_id, msg_idx, div_idx, try_idx, send_cnt, log_data, phone_list, regid_list, send_date)values(?, ?, ?, ?, ?, ?, ?, ?, getdate()) "
				cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 20, comm_id)
				cmd.Parameters.Append cmd.CreateParameter("@msg_idx", adInteger, adParamInput, 4, nMsgIdx)
				cmd.Parameters.Append cmd.CreateParameter("@div_idx", adInteger, adParamInput, 4, nDivIdx)
				cmd.Parameters.Append cmd.CreateParameter("@try_idx", adInteger, adParamInput, 4, nTryCnt+1)
				cmd.Parameters.Append cmd.CreateParameter("@send_cnt", adInteger, adParamInput, 4, nRegID+1)
				cmd.Parameters.Append cmd.CreateParameter("@log_data", adVarChar, adParamInput, 8000, fnStrCut(fnNullInit(sResultString),8000,""))
				cmd.Parameters.Append cmd.CreateParameter("@phone_list", adVarChar, adParamInput, 8000, fnStrCut(fnNullInit(sPhoneList),8000,""))
				cmd.Parameters.Append cmd.CreateParameter("@regid_list", adVarChar, adParamInput, 8000, fnStrCut(fnNullInit(sRegIDList),8000,""))
				cmd.CommandText = sql
				cmd.Execute , , adExecuteNoRecords
				ParamInit()
				if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
					fnErrSave()
					'response.end
				end if
			fnDBClose()
		else
			sResultString="HTTP_ERR1"
		End If

		Set seneHTTP = Nothing
		fnMultiPushSend = sResultString
		
		'전송결과
		Dim nSuccess : nSuccess = 0		'성공건수
		Dim nFail : nFail = 0				'실패건수
		Dim jsonResult						'전송결과(JSON)
		Dim nResult							'전송결과 수
		
		if sResultString = "HTTP_ERR1" then
			nFail = nRegID + 1
		elseif left(sResultString,9) = "HTTP_ERR4" then
			nFail = nRegID + 1 
		elseif len(sResultString) > 1 then		

			Dim jsonData : set jsonData = JSON.parse(sResultString)
			
			nSuccess = jsonData.success			'성공건수
			nFail = jsonData.failure					'실패건수
			nResult = jsonData.results.length		'결과건수

			sRegIDList = ""							'재시도 할 REG_ID 리스트 초기화
			sPhoneList = ""							'재시도 할 PHONE 리스트 초기화
			sFailReason = ""							'재시도 사유 초기화
			for i=0 to nResult-1			
				'전송실패
				if instr(JSON.stringify(jsonData.results.get(i)), "error") then
					if sRegIDList <> "" then
						sRegIDList = sRegIDList & ","
						sPhoneList = sPhoneList & ","
						sFailReason = sFailReason & ","
					end if
					sRegIDList = sRegIDList & aRegID(i)
					sPhoneList = sPhoneList & aPhone(i)
					sFailReason = sFailReason & JSON.stringify(jsonData.results.get(i))
				'전송성공			
				else
					if sSuccRegIDList <> "" then
						sSuccRegIDList = sSuccRegIDList & ","
						sSuccPhoneList = sSuccPhoneList & ","
					end if
					sSuccRegIDList = sSuccRegIDList & aRegID(i)
					sSuccPhoneList = sSuccPhoneList & aPhone(i)
				end if
			next
			set jsonData = nothing
		end if
		
		nTryCnt = nTryCnt + 1	
		'실패건수가 있고 최대 재시도 횟수 내 인경우 실패건 재시도
		if nFail > 0 and nTryCnt < nMaxTryCnt and sRegIDList <> "" then
			'재시도
			sPushResult = fnMultiPushSend(sMsg, sRegIDList, sPhoneList, sFailReason, sSuccRegIDList, sSuccPhoneList, comm_id)
		'전송완료
		else
			
			fnMultiPushSend = sResultString
			'결과출력
				'시도횟수
				'response.write "* PUSH 전송시도횟수 : " & nTryCnt & "회 시도(최대 " & nMaxTryCnt & "회까지 가능)<BR/>"
				'response.write "* 전송 메세지[" & sMsg & "]"
				
				'성공건
				aSuccRegID = split(sSuccRegIDList, ",")
				aSuccPhone = split(sSuccPhoneList, ",")
				nSuccRegID = -1
				nSuccPhone = -1
				if isArray(aSuccRegID) then
					nSuccRegID = ubound(aSuccRegID, 1)
				end if				
				if isArray(aSuccPhone) then
					nSuccPhone = ubound(aSuccPhone, 1)
				end if
				nSuccess = nSuccRegID+1
				'Response.Write "<br/><br/>===================================<BR/>"
				'Response.Write "* 성공 총 " & nSuccess& "건 <br/>"
				'Response.Write "===========================================<br/>"
				'for i=0 to nSuccRegID
				'	response.write (i+1) & ". " & aSuccPhone(i) & " [" & aSuccRegID(i) & "]<br/>"
				'next
				'Response.Write "<br/>========================================<BR/><br/>"

				'실패건
				aRegID = split(sRegIDList, ",")
				aPhone = split(sPhoneList, ",")
				aFailReason = split(sFailReason, ",")
				
				nRegID = -1
				nPhone = -1
				nFailReason = -1
				if isArray(aRegID) then
					nRegID = ubound(aRegID, 1)
				end if				
				if isArray(aPhone) then
					nPhone = ubound(aPhone, 1)
				end if
				if isArray(aFailReason) then
					nFailReason = ubound(aFailReason, 1)
				end if
				nFail = nRegID+1
				'Response.Write "<br/><br/>===================================<BR/>"
				'Response.Write "* 실패 총 " & nFail & "건 <br/>"
				'Response.Write "===========================================<br/>"
				'for i=0 to nRegID
				'	response.write (i+1) & ". " & aPhone(i) & " [" & aRegID(i) & "]<br/>"
				'	response.write "&nbsp;&nbsp;&nbsp;&nbsp;실패사유 (" & aFailReason(i) & ")<br/>"
				'	Response.Write "<br/>-------------------------------------------------------------------<br/>"					
				'next
				'Response.Write "<br/>=======================================<BR/><br/>"

			If Err and Err.Number <> 0 Then
				'Response.Write "<br/><br/>===================================<BR/>"
				'response.write "ERROR[" & Err.Number & "] Description :" & Err.Description & "<br/>"
				'Response.Write "Source : " & Err.Source & "<BR/>"
				'Response.Write "HelpContext : " & Err.HelpContext & "<BR/>"
				'Response.Write "<br/>======================================<BR/><br/>"
				'response.end
			End if
		end if 
	else		
		fnMultiPushSend = "PUSH발송 할 휴대폰번호가 없습니다"
		'response.write "<script type='text/javascript'>"
		'response.write "alert('PUSH발송 할 휴대폰번호가 없습니다.');"
		'response.write "</script>"
	end if
end Function
%>