<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Dim sKColumn, sKeyword
Dim aData1, nData1
Dim nIdx
Dim sRegDate, sUserID, sMsgType, nMsgCnt, nSendCnt, sSendChk

MENU_IDX_ = 7
sMenuCode = "admin"
PAGE_CODE_ = "push"
sListFileName = PAGE_CODE_ & "_list.asp"
nIdx = fnIdxInit(request("idx"))
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeyworeFilter(Request("optColumn"))
sKeyword = fnKeyworeFilter(Request("txtKeyword"))
sQueryString = "optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&idx="&nIdx
sFilter = ""

fnDBConn()

sql="select * from member_msg_data where idx=?"
cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParamInput, 4, nIdx)
if SS_GROUP = "S" then
	sql = sql & " and m_userid=? "
	cmd.Parameters.Append cmd.CreateParameter("@m_userid", adVarChar, adParamInput, 50, SS_USER_ID)
end if
cmd.CommandText = sql
Set rs = cmd.execute()
if not(rs.eof or rs.bof) then
	sRegDate = fnNullInit(rs("insert_date"))
	sUserID = fnNullInit(rs("m_userid"))
	sMsg = fnNullInit(rs("contents"))
	sSendChk = fnNullInit(rs("send_chk"))
	sMsgType = fnNullInit(rs("msg_type"))
	nMsgCnt = fnNullInit(rs("msg_cnt"))
	nSendCnt = fnNullInit(rs("msg_send_cnt"))
	sMsg = fnHTMLConvert(sMsg, "N")
end if
rs.close
ParamInit()

sqlTable = "select * from member_msg_list where msg_idx=? " & sFilter
cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParamInput, 4, nIdx)

if sKeyword <> "" then
	if sKColumn = "handphone" then
		sFilter = sFilter & " and handphone like ? "
		cmd.Parameters.Append cmd.CreateParameter("@handphone", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "location" then
		sFilter = sFilter & " and location like ? "
		cmd.Parameters.Append cmd.CreateParameter("@location", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	end if
end if

'게시물 개수
sql = " select count(*) cnt from (" & sqlTable & ")a "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nRecordCount = trim(rs("cnt"))
end if
rs.close

'게시물 리스트	
sqlTable = " SELECT ROW_NUMBER() OVER(ORDER BY idx desc) AS RowNum, * FROM (" & sqlTable & ")a "	
sql = " ;with tbl as(" &  sqlTable & ") "
sql = sql & " SELECT TOP " & nPageSize & " handphone, status, send_date, read_date, insert_date FROM tbl"
sql = sql & " WHERE RowNum >= (SELECT MAX(RowNum)RowNum FROM (SELECT TOP " & (nPageNO-1)*nPageSize+1 & " RowNum FROM tbl ORDER BY RowNum ASC) a) ORDER BY RowNum ASC "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	aData1 = rs.getRows()
end if
rs.close
Set rs = nothing

fnDBClose()

if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 

nData1 = -1

if isArray(aData1) then
	nData1 = ubound(aData1, 2)
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
// 전체선택/해제
function fnSelectAll(obj)
{
	var bFlag = obj.checked;
	var objs = document.getElementsByName("chkSelect");
	for(var i=0; i < (objs.length==null?1:objs.length); i++)
	{
		objs[i].checked = bFlag;
	}
}
function fnDetailClose()
{
	var obj = document.getElementById("lyDetail");
	obj.style.display='none';
}
function fnLogView()
{
	var obj = document.getElementById("lyDetail");
	var frm = document.getElementById("frmDetail");
	var objTitle = document.getElementById("lyReportTitle");
	frm.submit();
	obj.style.display='block';
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<div class="lyMainTitle"><img src="/g_admin/images/common/subtitle02.png" height="39" alt="알림(푸쉬)"></div>
<div class="lyTab">
	<!--span class="button tab"><a href="push_list.asp">알림메세지 전송</a></span-->
	<%if SS_GROUP="A" or SS_GROUP="S" then%>
	<span class="button tab"><a href="push_list.asp?pty=p">PUSH메세지 전송</a></span>
	<%end if%>
	<span class="button tab_over"><a href="push_msg_list.asp">메세지 전송현황 보기</a></span>
</div>
<div class="csCaption1">메세지정보</div>
<div class="lyMainTbl">
<table class="tblContent">
	<tr class="top">
		<th width="20%">요청건수</th>
		<td><%=nMsgCnt%>건</td>
	</tr>
	<tr>
		<th>전송건수</th>
		<td><%=nSendCnt%>건</td>
	</tr>
	<tr>
		<th>상태</th>
		<td><%=fnSendStatus(sSendChk)%></td>
	</tr>
	<tr class="contents2">
		<th>메세지</th>
		<td><%=sMsg%></td>
	</tr>
	<tr>
		<th>발송자</th>
		<td><%=sUserID%></td>
	</tr> 
	<%if sMsgType="p" and (SS_GROUP="A" or SS_GROUP="S") then%>
	<tr>
		<th>발송로그</th>
		<td><span class="button mustard"><input type="button" value="발송로그보기" onclick="fnLogView();"></span></td>
	</tr> 
	<%end if%>
	<tr class="btm">
		<th>등록일</th>
		<td><%=sRegDate%></td>
	</tr>
</table>
<div class="lyRBtn">
	<span class="button base"><a href="push_msg_list.asp">목록</a></span>
</div>
<div class="csCaption1">전송목록</div>
<%=fnRecordCnt(nRecordCount, nPageNO, nPageCount)%>
<div class="lyMainTbl">
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="10%">휴대폰번호</th>
		<th width="10%">전송현황</th>
		<th width="22%">전송일자</th>
		<th width="22%">확인일자</th>
		<th width="21%">등록일</th>
	</tr>
	<%
	if nData1 >= 0 then
		for i=0 to nData1
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td>
		<%if SS_GROUP="A" then%>
				<%=trim(aData1(0,i))%>
			<%else%>
				<%=fnRDataSec(trim(aData1(0,i)),4)%>
			<%end if%>
		</td>
		<td><%=fnMsgStatus(trim(aData1(1,i)))%></td>
		<td><%=trim(aData1(2,i))%></td>
		<td><%=trim(aData1(3,i))%></td>
		<td><%=trim(aData1(4,i))%></td>
	</tr>
	<%
		next
	%>

	<%else%>
	<tr class="msg145">
		<td colspan="6">알림메세지 전송내역이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>
<div class="lyRBtn">
	<span class="button base"><a href="push_msg_list.asp">목록</a></span>
</div>
<%=fnPaging("push_msg_list.asp", nPageNO, nBlockSize, nPageCount, sQueryString)%>
<div id="lyDetail" style="display:none;position:absolute;top:60px;width:900px;height:600px;overflow:hidden;background:#ffffff;border:5px solid #bbbbbb;padding:20px 10px 10px 10px; ">
	<div class="csCaption1" id="lyReportTitle">발송로그</div>
	<iframe width="98%" height="530" name="ifrDetail" id="ifrDetail" frameborder="0" style="clear:both;border:1px solid #bbbbbb;" align="center" scrolling="yes"></iframe>
	<p style="text-align:right;padding:5px 10px"><span class="button base"><input type="button" value="닫 기" onclick="fnDetailClose();" style="cursor:pointer" alt="닫 기" title="닫 기"></span></p>
</div>
<form name="frmDetail" id="frmDetail" action="push_log_view.asp" target="ifrDetail" method="post">
<input type="hidden" name="hdIdx" id="hdIdx" value="<%=nIdx%>">
</form>
<br/>
<br/>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>