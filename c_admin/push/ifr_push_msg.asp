<!--#include file="../inc/inc_top_common.asp"-->
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<%
sbMemberAuthChk("A")

Dim sPushKey : sPushKey = fnKeyworeFilter(request("pushKey"))	'fnPushKey()
Dim sKComm : sKComm = fnKeywordFilter(Request("hdKComm"))
Dim sType : sType = "p"
Dim sTypeText : sTypeText = "PUSH 메세지 전송하기"

sMenuCode = "admin"
PAGE_CODE_ = "push"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">

function fnTypeChange(obj)
{
	var frm= document.getElementById("frmPushSend");
	if(obj.value == "")
	{
		frm.txtMsg.value = "";
	}
	else
	{
		frm.txtMsg.value = "안녕하세요.";
	}
}
function fnPushSendChk(frm)
{
	/*
	if(frm.hdTmpMsgUserIdx.value=="")
	{
		alert("[1. 전송대상선택]창에서 메세지전송 할 대상을 선택해주세요.");
		return false;
	}
	*/

	if(frm.hdPushKey.value == "")
	{
		alert("[1. 전송대상선택]에서 메세지를 전송 할 대상을 선택해주세요.");
		return false;
	}
	if(frm.currPushKey.value != frm.hdPushKey.value)
	{
		alert("PUSH키가 일치하지 않습니다.");
		return false;
	}

	if(frm.txtMsg.value=="")
	{
		alert("전송 할 메세지를 입력해주세요.");
		frm.txtMsg.focus();
		return false;
	}

	if(frm.txtMsgCnt.value=="")
	{
		alert("메세지를 전송 할 대상이 없습니다.");
		return false;
	}
	if(fnDataChk(frm.txtMsgCnt, 'PUSH_MSG') == false){return false;}

	if(confirm(frm.txtMsgCnt.value + "건의 메세지를 전송하시겠습니까?"))
	{
		return true;
	}
	return false;
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%" height="100%">
<form name="frmPushSend" id="frmPushSend" onsubmit="return fnPushSendChk(this);" action="push_msg_send.asp" method="post">
<input type="hidden" name="hdSessionID" id="hdSessionID" value="<%=SESSION_ID_%>">
<input type="hidden" name="hdUserID" id="hdUserID" value="<%=SS_USER_ID%>">		
<input type="hidden" name="currPushKey" id="currPushKey" value="<%=sPushKey%>">			
<input type="hidden" name="hdPushKey" id="hdPushKey" value="">			
<input type="hidden" name="hdTmpMsgUserIdx" id="hdTmpMsgUserIdx" value="">
<input type="hidden" name="rdSendType" id="rdSendType" value="<%=sType%>">
<input type="hidden" name="hdKComm" id="hdKComm" value="<%=sKComm%>">
<div style="padding:5px">
<table class="tblContent">
	<tr class="contents2_top">
		<th width="30%">전송대상</th>
		<td width="70%">
			<textarea name="txtUser" id="txtUser" cols="30" rows="12" readOnly style="border:1px solid #eeeeee"></textarea><br/>
			<input type="text" name="txtMsgCnt" id="txtMsgCnt" value="" style="margin-top:5px;width:80px;border:1px solid #eeeeee" readOnly> 건<br/>
		</td>
	</tr>
	<tr class="contents2">
		<th>메세지</th>
		<td>
			<textarea name="txtMsg" id="txtMsg" cols="30" rows="10"></textarea>
		</td>
	</tr>
</table>
</div>
<div class="lyLMsg">* <span class="spBlue">[1. 전송대상선택]</span>창에서 메세지전송대상을 선택해주세요.</div>
<div class="lyCBtn">
	<span class="button xLarge base"><input type="submit" value="<%=sTypeText%>"></span>
</div>
</form>
</body>
</html>