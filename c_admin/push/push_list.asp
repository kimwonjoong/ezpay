<!--#include file="../inc/inc_top_common.asp"-->
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<%
sbMemberAuthChk("A")

Dim sKColumn, sKeyword, sType : sType = "p"
Dim sPushKey : sPushKey = fnPushKey()

MENU_IDX_ = 7
sMenuCode = "admin"
PAGE_CODE_ = "push"
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf8" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%" onload="intervalCall();">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<div class="lyMainTitle"><img src="/g_admin/images/common/subtitle02.png" height="39" alt="알림(푸쉬)"></div>
<div class="lyTab">
	<!--span class="button tab<%if sType="m" then%>_over<%end if%>"><a href="push_list.asp">알림메세지 전송</a></span-->
	<span class="button tab_over"><a href="push_list.asp?pty=p">PUSH메세지 전송</a></span>
	<span class="button tab"><a href="push_msg_list.asp">메세지 전송현황 보기</a></span>
</div>
<br/>
<%if SS_EXPIRE_CHK = "ON" then%>
<div style="clear:both;width:100%;height:680px;">
	<div style="float:left;width:48%;height:680px;margin-left:10px">
		<div style="float:left;width:100%;height:680px;;overflow:hidden;border:1px solid #dddddd;padding:5px">
			<div class="csCaption1" style="text-align:left">1. 전송대상선택</div>
			<iframe src="ifr_push_user.asp?pushKey=<%=sPushKey%>&pty=<%=sType%>" width="100%" height="660" frameborder="0" scrolling="no" name="ifrPushUser" id="ifrPushUser"></iframe>
		</div>
	</div>
	<div style="float:left;width:42px;height:680px;background:url('/g_admin/images/common/bt_next01.gif') no-repeat 22px 250px;"></div>
	<div style="float:right;width:43%;;height:680px;margin-right:10px">
		<div style="height:100%;overflow:hidden;height:680px;border:1px solid #dddddd;padding:5px">
			<div class="csCaption1" style="text-align:left">2. 메세지전송</div>
			<iframe src="ifr_push_msg.asp?pushKey=<%=sPushKey%>&pty=<%=sType%>" width="100%" height="660" frameborder="0" scrolling="no" name="ifrPushMsg" id="ifrPushMsg"></iframe>
		</div>
	</div>
</div>
<%else%>
<div class="lyCSch">사용기간이 만료되어 알림메세지 전송이 불가능합니다.</div>
<%end if%>
<br/>
<br/>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>