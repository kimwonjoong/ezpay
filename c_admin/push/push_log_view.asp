<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Dim nIdx
Dim nTotalCnt : nTotalCnt = 0
Dim nSuccCnt : nSuccCnt = 0
Dim nFailCnt : nFailCnt = 0

nIdx = trim(request("hdIdx"))

if nIdx <> "" then
	fnDBConn()

	'발송현황
	sql = "select sum(case when try_idx=1 then send_cnt else 0 end) t_sum, sum(case when try_idx=2 then send_cnt else 0 end) f_sum from member_msg_log where msg_idx=" & nIdx & " "
	Set rs = dbConn.execute(sql)
	if not(rs.eof or rs.bof) then
		nTotalCnt = fnDblInit(rs("t_sum"))
		nFailCnt = fnDblInit(rs("f_sum"))
		nSuccCnt = nTotalCnt - nFailCnt
	end if
	rs.close

	'발송내역
	sql = "select * from member_msg_log where msg_idx=" & nIdx & " order by idx "
	Set rs = dbConn.execute(sql)
	if not(rs.eof or rs.bof) then
		nData = fnRs2JsonArr(aData, rs)
	end if
	rs.close
	Set rs = nothing

	fnDBClose()
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
</script>
</HEAD>
<BODY leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" style="overflow-x:hidden;overflow-y:scroll;padding:5px">
<div class="lyMainTbl">
<table class="tblContent" style="table-layout:fixed">
	<tr class="top contents2">		
		<td>전체회차 1번째시도 기준 전송현황 [총 <%=nTotalCnt%>건  전송시도 : <span class="spBlue"><%=nSuccCnt%>건 전송완료</span> (<%=nFailCnt%>건 실패) ]</td>
	</tr>	
<%if nData >= 0 then%>
	<%for i=0 to nData%>
		<tr class="<%if i=0 then%>top<%end if%> contents2">			
			<td height="auto" style="word-break:break-all">
				<%if fnIntInit(aData("try_idx")(i)) = 1 then%>
				<div style="font-weight:bold;background:#D1DEE9;padding:4px 2px 2px 2px;margin-bottom:3px">
					▒ [<%=aData("div_idx")(i)%>회차] (<%=aData("send_cnt")(i)%>건 발송요청) / 최초발송요청일자 : <%=aData("send_date")(i)%>
				</div>
				<%end if%>
				<div style="font-weight:bold;background:#DCD9C5;padding:4px 2px 2px 10px">
					☞ <%=aData("try_idx")(i)%>번째 시도(<%=aData("send_cnt")(i)%>건 발송요청) / 발송일자 : <%=aData("send_date")(i)%>
				</div>
				<div style="padding:3px 3px 3px 5px"><span class="spBlue" >* 발송결과</span><p style="border:1px solid #dddddd;padding:5px;"><%=aData("log_data")(i)%></p></div>
				<div style="padding:3px 3px 3px 5px"><span class="spBlue">* 휴대폰리스트</span><p style="border:1px solid #dddddd;padding:5px;"><%=aData("phone_list")(i)%></p></div>
				<!--div style="padding:3px"><span class="spBlue">* RegID리스트</span><p style="border:1px solid #dddddd;padding:5px;"><%=aData("regid_list")(i)%></p></div-->
			</td>
		</tr>
	<%next%>
<%else%>
	<tr class="top msg145">
		<td class="ac">발송 로그가 없습니다.</td>
	</tr>
<%end if%>
</table>
</div>
</body>
</html>