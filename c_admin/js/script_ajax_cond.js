var xhrSchOption;
var xhrSchType = "";
var xhrSchForm;

function fnXHRSchOption(obj, sArgMode)
{
	xhrSchType = sArgMode;
	xhrSchForm = obj.form;
	if(xhrSchType == "ctg" || xhrSchType == "kCtg" )
	{
		var sOptCoupon = obj.options[obj.selectedIndex].value;
		var sURL = "../inc/inc_ajax_cond.asp?txtKeyword=" + sOptCoupon + "&mode=" + sArgMode;
	
		//XHR 객체 얻기
		if (window.ActiveXObject) 
		{ 
			xhrSchOption = new ActiveXObject('Microsoft.XMLHTTP');
		} 	
		else if (window.XMLHttpRequest) 
		{ 
			xhrSchOption = new XMLHttpRequest();      
		} 
		
		//콜백함수지정(readyState값이 변할때마다 호출)
		xhrSchOption.onreadystatechange = cbXHRSchOption;
		
		//요청초기화
		xhrSchOption.open("GET", sURL, true);

		//서버에 요청
		xhrSchOption.send(null);
	}
}
function parse_xml(req) {
	try { 
		var responseXML = new ActiveXObject("Microsoft.XMLDOM"); 
		responseXML.async = "false";    
		responseXML.loadXML(req.responseText);  
		responseXML = responseXML.documentElement;

		alert(responseXML);
		alert(responseXML.childNodes[0].nodeName);
		return responseXML;
	}
	catch(e) {
		return req.responseXML;
	}
}
//xhrSendMail 콜백함수 
function cbXHRSchOption() 
{
	//응답완료
	if(xhrSchOption.readyState == 4) 
	{
		//상태:OK(200)
		if(xhrSchOption.status == 200)
		{
			//서버응답
			var oNewDoc = xhrSchOption.responseXML;
			var newValue = "";
			var newText = "";
			var newText2 = "";

			var oCurObj = null;
			var oNewObj = null;

			if(xhrSchType == "ctg")
			{
				$("#lyDList1").empty();
				
				var oCurObj = document.getElementById("lyDList1");		
				var oCurObj2 = document.getElementById("lyDList2");			
				var oNewObj = oNewDoc.getElementsByTagName("list");

				if(typeof(oNewObj)=="object")
				{
					if(oNewObj.length == 0)
					{
						var newP = document.createElement("div");
						$(newP).css("text-align", "center").css("height", "100px").css("padding-top","105px");
						newP.innerText = "* 등록된 상품이 없습니다.";
						oCurObj.appendChild(newP);
						return false;
					}

					for(var i=0; i < oNewObj.length; i++)
					{
						var newInput = document.createElement("input");
						var newSpan = document.createElement("span");
						var newP = document.createElement("p");
						var newSpanText = document.createElement("span");
						
						newValue = oNewObj[i].attributes[0].value;
						newText = " " + oNewObj[i].attributes[1].value;		

						var sCouponChk = "Y";
						var oSelectItem = $(oCurObj2).find("input[name='hdCouponIdx']");
						var nCurObj2 = fnIntInit(oSelectItem.length);
						//선택여부 체크
						for(var j=0; j<nCurObj2; j++)
						{
							if(oSelectItem.eq(j).val() == newValue)
							{
								sCouponChk = "N";
								break;
							}
						}

						newP.setAttribute("id", "pAdd"+newValue);
						$(newP).attr("id", "pAdd"+newValue);

						newSpanText.setAttribute("name", "spanText");
						$(newSpanText).attr("name", "spanText");
						newSpanText.innerText = newText;
						$(newSpanText).text(newText);

						newSpan.setAttribute("name", "spanAdd");
						newSpan.setAttribute("id", "spanAdd"+newValue);
						$(newSpan).attr("name", "spanAdd");
						$(newSpan).attr("id", "spanAdd"+newValue);

						newInput.setAttribute("name", "btnAdd");
						newInput.setAttribute("id", "btnAdd"+newValue);
						newInput.setAttribute("value", "선택");
						newInput.setAttribute("type", "button");

						$(newInput).attr("value", "선택");
						$(newInput).attr("type", "button");
						$(newInput).attr("name", "btnAdd");
						$(newInput).attr("id", "btnAdd"+newValue);
												
						$(newP).bind("mouseover", function(){$(this).css("backgroundColor","#FDFBDB"); });		
						$(newP).bind("mouseout", function(){$(this).css("backgroundColor","#FFFFFF"); });		

						if(sCouponChk == "Y")
						{
							newSpan.setAttribute("class", "button white small");
							$(newSpan).attr("class", "button white small");
							$(newInput).bind("click", function(){ fnCouponAdd(this);});		
						}
						else
						{
							newSpan.setAttribute("class", "button white2 small");
							$(newSpan).attr("class", "button white2 small");
						}			
						newSpan.appendChild(newInput);
						newP.appendChild(newSpan);
						newP.appendChild(newSpanText);
						oCurObj.appendChild(newP);
					}
				}
			}
			else
			{
				if(xhrSchType == "kBCode")
				{
					oCurObj = xhrSchForm.optKCCode;
					oNewObj = oNewDoc.getElementsByTagName("list");
				}
				if(typeof(oNewObj)=="object")
				{
					oCurObj.options.length = null;
					for(var i=0; i < oNewObj.length; i++)
					{
						var newOption = document.createElement("option");
						
						newValue = oNewObj[i].attributes[0].value;
						newText = oNewObj[i].attributes[1].value;		
						
						newOption.setAttribute("value", newValue);
						newOption.appendChild(document.createTextNode(newText));
						
						oCurObj.appendChild(newOption);
					}
				}
			}
		}
		else
		{
			alert('응답오류가 발생하였습니다. 관리자에 문의해주세요.' + xhrSchOption.responseText);
		}
	}
}
function transXml(xmlDoc){
  var nodes = xmlDoc.childNodes;
  var removeNodesArr = new Array();
  for(var i=0;i<nodes.length;i++){
   if(nodes[i].nodeType != 1){
    removeNodesArr.push(i);
   }else{
    if(nodes[i].childNodes.length > 1 || nodes[i].firstChild.nodeType == 1){
     transXml(nodes[i]);
    }
   }
  }
  for(var j=removeNodesArr.size() ; j>0 ; j--){
   xmlDoc.removeChild(nodes[removeNodesArr[j-1]]);
  }
 }