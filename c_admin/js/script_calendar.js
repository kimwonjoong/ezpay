var oTarget;
var pop_top;
var pop_left;
var cal_Day;
var oPopup = null;

function fnHide()
{
	if(oPopup)
	{
		oPopup.parentNode.removeChild(oPopup);
		oPopup = null;
	}
}

function Calendar_Click(e) {
	cal_Day = e.title;
	if (cal_Day.length > 6) {
		oTarget.value = cal_Day;
	}
	fnHide();	
}

function Calendar_D(obj) {
	var now = obj.value.split("-");
	oTarget = obj;
	//pop_top = eval(document.body.clientTop + GetObjectTop(obj) - document.body.scrollTop);
	//pop_left = eval(document.body.clientLeft + GetObjectLeft(obj) -  document.body.scrollLeft);
/*	
	var nScrollTop = document.body.scrollTop;
	var nScrollLeft = document.body.scrollLeft;

	if(nScrollTop == 0){nScrollTop = document.documentElement.scrollTop;}
	if(nScrollLeft == 0){nScrollLeft = document.documentElement.scrollLeft;}

	pop_top = eval(document.body.clientTop + GetObjectTop(obj) - nScrollTop);
	pop_left = eval(document.body.clientLeft + GetObjectLeft(obj) -  nScrollLeft);

	if(nScrollTop > 0){pop_top = eval(pop_top + nScrollTop);}
*/
	pop_top = eval(document.body.clientTop + GetObjectTop(obj));
	pop_left = eval(document.body.clientLeft + GetObjectLeft(obj));

	if (now.length == 3) {
		Show_cal(now[0],now[1],now[2]);					
	} else {
		now = new Date();
		Show_cal(now.getFullYear(), now.getMonth()+1, now.getDate());
	}
}

function Calendar_M(obj) {
	var now = obj.value.split("-");
	oTarget = obj;
	//pop_top = document.body.clientTop + GetObjectTop(obj) - document.body.scrollTop;
	//pop_left = document.body.clientLeft + GetObjectLeft(obj) -  document.body.scrollLeft;

/*	
	var nScrollTop = document.body.scrollTop;
	var nScrollLeft = document.body.scrollLeft;

	if(nScrollTop == 0){nScrollTop = document.documentElement.scrollTop;}
	if(nScrollLeft == 0){nScrollLeft = document.documentElement.scrollLeft;}

	pop_top = eval(document.body.clientTop + GetObjectTop(obj) - nScrollTop);
	pop_left = eval(document.body.clientLeft + GetObjectLeft(obj) -  nScrollLeft);

	if(nScrollTop > 0){pop_top = eval(pop_top + nScrollTop);}
*/
	pop_top = eval(document.body.clientTop + GetObjectTop(obj));
	pop_left = eval(document.body.clientLeft + GetObjectLeft(obj));

	if (now.length == 2) {
		Show_cal_M(now[0],now[1]);					
	} else {
		now = new Date();
		Show_cal_M(now.getFullYear(), now.getMonth()+1);
	}
}

function doOver(el) {
	cal_Day = el.title;

	if (cal_Day.length > 7) {
		el.style.borderColor = "#d98c89";
		var sBgColor = el.style.backgroundColor;
		var sColor = el.style.color;
		el.style.cssText = "font-family:tahoma;font-size:11px;cursor:pointer;border:solid #d98c89 1px;background-color:"+sBgColor+";color:"+sColor+";";
	}
}

function doOut(el) {
	cal_Day = el.title;

	if (cal_Day.length > 7) {
		el.style.borderColor = "#FFFFFF";
		var sBgColor = el.style.backgroundColor;
		var sColor = el.style.color;
		el.style.cssText = "font-family:tahoma;font-size:11px;cursor:pointer;border:solid #FFFFFF 1px;background-color:"+sBgColor+";color:"+sColor+";";
	}
}

function day2(d) {	// 2자리 숫자료 변경
	var str = new String();
	
	if (parseInt(d) < 10) {
		str = "0" + parseInt(d);
	} else {
		str = "" + parseInt(d);
	}
	return str;
}

function Show_cal(sYear, sMonth, sDay) {
	fnHide();
	var Months_day = new Array(0,31,28,31,30,31,30,31,31,30,31,30,31)
	var Month_Val = new Array("01","02","03","04","05","06","07","08","09","10","11","12");
	var intThisYear = new Number(), intThisMonth = new Number(), intThisDay = new Number();

	datToday = new Date();													// 현재 날자 설정
	
	intThisYear = parseInt(sYear,10);
	intThisMonth = parseInt(sMonth,10);
	intThisDay = parseInt(sDay,10);
	
	if (intThisYear == 0) intThisYear = datToday.getFullYear();				// 값이 없을 경우
	if (intThisMonth == 0) intThisMonth = parseInt(datToday.getMonth(),10)+1;	// 월 값은 실제값 보다 -1 한 값이 돼돌려 진다.
	if (intThisDay == 0) intThisDay = datToday.getDate();
	
	switch(intThisMonth) {
		case 1:
				intPrevYear = intThisYear -1;
				intPrevMonth = 12;
				intNextYear = intThisYear;
				intNextMonth = 2;
				break;
		case 12:
				intPrevYear = intThisYear;
				intPrevMonth = 11;
				intNextYear = intThisYear + 1;
				intNextMonth = 1;
				break;
		default:
				intPrevYear = intThisYear;
				intPrevMonth = parseInt(intThisMonth,10) - 1;
				intNextYear = intThisYear;
				intNextMonth = parseInt(intThisMonth,10) + 1;
				break;
	}
	intPPyear = intThisYear-1
	intNNyear = intThisYear+1

	NowThisYear = datToday.getFullYear();									// 현재 년
	NowThisMonth = datToday.getMonth()+1;									// 현재 월
	NowThisDay = datToday.getDate();											// 현재 일
	
	datFirstDay = new Date(intThisYear, intThisMonth-1, 1);			// 현재 달의 1일로 날자 객체 생성(월은 0부터 11까지의 정수(1월부터 12월))
	intFirstWeekday = datFirstDay.getDay();									// 현재 달 1일의 요일을 구함 (0:일요일, 1:월요일)
	//intSecondWeekday = intFirstWeekday;
	intThirdWeekday = intFirstWeekday;
	
	datThisDay = new Date(intThisYear, intThisMonth, intThisDay);	// 넘어온 값의 날자 생성
	//intThisWeekday = datThisDay.getDay();										// 넘어온 날자의 주 요일
	
	intPrintDay = 1;																// 달의 시작 일자
	secondPrintDay = 1;
	thirdPrintDay = 1;

	Stop_Flag = 0
	
	if ((intThisYear % 4)==0) {												// 4년마다 1번이면 (사로나누어 떨어지면)
		if ((intThisYear % 100) == 0) {
			if ((intThisYear % 400) == 0) {
				Months_day[2] = 29;
			}
		} else {
			Months_day[2] = 29;
		}
	}
	intLastDay = Months_day[intThisMonth];						// 마지막 일자 구함

	Cal_HTML = "<form name='frmCalendar'>";
	Cal_HTML += "<table id='Cal_Table' border='0' bgcolor='#90afd0' cellpadding='1' cellspacing='1' width='100%' style='font-size:12px;font-family:굴림;'>";
	Cal_HTML += "<tr height='28' align='center' bgcolor='#ffffff'>";
	Cal_HTML += "<td colspan='7' align='center'>";
	Cal_HTML += "	<select name='selYear' STYLE='font-family:tahoma;font-size:11px;' OnChange='fnChangeYearD(document.frmCalendar.selYear.value, document.frmCalendar.selMonth.value, "+intThisDay+")';>";
	for (var optYear=(intThisYear-3); optYear<(intThisYear+3); optYear++) {
		Cal_HTML += "		<option value='"+optYear+"' ";
		if (optYear == intThisYear) Cal_HTML += " selected>\n";
		else Cal_HTML += ">\n";
		Cal_HTML += optYear+"</option>\n";
	}
	Cal_HTML += "	</select>년";
	Cal_HTML += "&nbsp;&nbsp;<img src='/admin/images/common/btn_prev_s2_g.gif' border='0' style='cursor:pointer;' OnClick='Show_cal("+intPrevYear+","+intPrevMonth+","+intThisDay+");'> ";
	Cal_HTML += "<select name='selMonth' style='font-family:tahoma;font-size:11px;' OnChange='fnChangeYearD(document.frmCalendar.selYear.value, document.frmCalendar.selMonth.value, "+intThisDay+")';>";
	for (var i=1; i<13; i++) {	
		Cal_HTML += "		<option value='"+Month_Val[i-1]+"' ";
		if (intThisMonth == parseInt(Month_Val[i-1],10)) Cal_HTML += " selected>\n";
		else Cal_HTML += ">\n";
		Cal_HTML += Month_Val[i-1]+"</option>\n";
	}
	Cal_HTML += "	</select>월&nbsp;";
	Cal_HTML += "<img src='/admin/images/common/btn_next_s2_g.gif' border='0' style='cursor:pointer;' OnClick='Show_cal("+intNextYear+","+intNextMonth+","+intThisDay+");'>";
	Cal_HTML += "</td></tr>";
	Cal_HTML += "<tr align='center' bgcolor='#90afd0' style='color:#90afd0;' height='25'>";
	Cal_HTML += "	<td style='padding-top:2px;' width='28'><font color='#EEEEEE'><b>일</b></font></td>";
	Cal_HTML += "	<td style='padding-top:2px;' width='28'><font color='#EEEEEE'><b>월</b></font></td>";
	Cal_HTML += "	<td style='padding-top:2px;' width='28'><font color='#EEEEEE'><b>화</b></font></td>";
	Cal_HTML += "	<td style='padding-top:2px;' width='28'><font color='#EEEEEE'><b>수</b></font></td>";
	Cal_HTML += "	<td style='padding-top:2px;' width='28'><font color='#EEEEEE'><b>목</b></font></td>";
	Cal_HTML += "	<td style='padding-top:2px;' width='28'><font color='#EEEEEE'><b>금</b></font></td>";
	Cal_HTML += "	<td style='padding-top:2px;' width='28'><font color='#EEEEEE'><b>토</b></font></td>";
	Cal_HTML += "</tr>";
		
	for (intLoopWeek=1; intLoopWeek < 7; intLoopWeek++) {	// 주단위 루프 시작, 최대 6주
		Cal_HTML += "<tr height='25' align='right' bgcolor='white'>"
		for (intLoopDay=1; intLoopDay <= 7; intLoopDay++) {	// 요일단위 루프 시작, 일요일 부터
			if (intThirdWeekday > 0) {											// 첫주 시작일이 1보다 크면
				Cal_HTML += "<td>";
				intThirdWeekday--;
			} else {
				if (thirdPrintDay > intLastDay) {								// 입력 날짝 월말보다 크다면
					Cal_HTML += "<td>";
				} else {																// 입력날짜가 현재월에 해당 되면
					Cal_HTML += "<td onmouseover='doOver(this);' onmouseout='doOut(this);' onClick='Calendar_Click(this);' title='"+intThisYear+"-"+day2(intThisMonth).toString()+"-"+day2(thirdPrintDay).toString()+"' style='font-family:tahoma;font-size:11px;cursor:pointer;border:solid #ffffff 1px;";
					if (intThisYear == NowThisYear && intThisMonth==NowThisMonth && thirdPrintDay==intThisDay) {
						Cal_HTML += "background-color:#d7e5f3;";
					}
					
					switch(intLoopDay) {
						case 1:															// 일요일이면 빨간 색으로
							Cal_HTML += "color:#e25e5e;"
							break;
						case 7:
							Cal_HTML += "color:#4e70c7;"
							break;
						default:
							Cal_HTML += "color:#626262;"
							break;
					}
					Cal_HTML += "'>"+thirdPrintDay;
				}
				thirdPrintDay++;
				
				if (thirdPrintDay > intLastDay) {								// 만약 날짜 값이 월말 값보다 크면 루프문 탈출
					Stop_Flag = 1;
				}
			}
			Cal_HTML += "&nbsp;</td>";
		}
		Cal_HTML += "</tr>";
		if (Stop_Flag==1) break;
	}
	Cal_HTML += "</table></form>";

	if(!oPopup)
	{
		oPopup = document.createElement('div');
		oPopup.innerHTML = Cal_HTML;
		oPopup.style.position = 'absolute';
		oPopup.style.width = '220px';
		document.body.appendChild(oPopup);

		oPopup.setAttribute("position", "absolute");
		oPopup.setAttribute("width", "220");
	}
	else
	{
		oPopup.setAttribute("display", "block");
		oPopup.style.display = 'block';
	}

	var calHeight = document.getElementById("Cal_Table").offsetHeight;

	//행이 6개 행인지, 5개인지 구분
	if (intLoopWeek == 6)	calHeight = 58+(26*6);
	else	calHeight = 58+(26*5);

	oPopup.style.cssText = "z-index:220;width:220px;height:"+calHeight+"px;display:block;position:absolute;top:"+eval(pop_top + 20)+"px;left:"+pop_left+"px;";
	oPopup.setAttribute("top", eval(pop_top + 20));
	oPopup.setAttribute("left", pop_left);
	oPopup.setAttribute("height", calHeight);
	oPopup.setAttribute("z-index", "220");

	oPopup.style.height = calHeight + "px";
	oPopup.style.left = pop_left + "px";
	oPopup.style.top = eval(pop_top + 20) + "px";
}


function Show_cal_M(sYear, sMonth) {
	var intThisYear = new Number(), intThisMonth = new Number()
	datToday = new Date();													// 현재 날자 설정
	
	intThisYear = parseInt(sYear,10);
	intThisMonth = parseInt(sMonth,10);
	
	if (intThisYear == 0) intThisYear = datToday.getFullYear();				// 값이 없을 경우
	if (intThisMonth == 0) intThisMonth = parseInt(datToday.getMonth(),10)+1;	// 월 값은 실제값 보다 -1 한 값이 돼돌려 진다.
			
	switch(intThisMonth) {
		case 1:
				intPrevYear = intThisYear -1;
				intNextYear = intThisYear;
				break;
		case 12:
				intPrevYear = intThisYear;
				intNextYear = intThisYear + 1;
				break;
		default:
				intPrevYear = intThisYear;
				intNextYear = intThisYear;
				break;
	}
	intPPyear = intThisYear-1
	intNNyear = intThisYear+1

	Cal_HTML = "<table id='Cal_Table' border='0' bgcolor='#90afd0' cellpadding='1' cellspacing='1' width='100%' style='font-size : 12;font-family:굴림;'>\n";
	Cal_HTML += "<tr height='30' align='center' bgcolor='#f4f4f4'>\n";
	Cal_HTML += "<td colspan='4' align='center'>\n";
	Cal_HTML += "<a style='cursor:pointer;' OnClick='Show_cal_M("+intPPyear+","+intThisMonth+");'><img src='/admin/images/common/btn_prev_s2_g.gif' border='0'></a>&nbsp;";
	Cal_HTML += "<select name='selYear' STYLE='font-size:11px;' OnChange='fnChangeYearM(this.value, "+intThisMonth+")';>";
	for (var optYear=(intThisYear-2); optYear<(intThisYear+2); optYear++) {
			Cal_HTML += "		<option value='"+optYear+"' ";
			if (optYear == intThisYear) Cal_HTML += " selected>\n";
			else Cal_HTML += ">\n";
			Cal_HTML += optYear+"</option>\n";
	}
	Cal_HTML += "	</select>\n";
	Cal_HTML += "<img src='/admin/images/common/btn_next_s2_g.gif' border='0' style='cursor:pointer;' OnClick='Show_cal_M("+intNNyear+","+intThisMonth+");'>";
	Cal_HTML += "</td></tr>\n";
	Cal_HTML += "<tr><td colspan=4 height='1' bgcolor='#000000'></td></tr>";
	Cal_HTML += "<tr height='20' align='center' bgcolor='white'>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-01"+" style='cursor:pointer;'>1월</td>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-02"+" style='cursor:pointer;'>2월</td>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-03"+" style='cursor:pointer;'>3월</td>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-04"+" style='cursor:pointer;'>4월</td>";
	Cal_HTML += "</tr>\n";
	Cal_HTML += "<tr height='20' align='center' bgcolor='white'>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-05"+" style='cursor:pointer;'>5월</td>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-06"+" style='cursor:pointer;'>6월</td>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-07"+" style='cursor:pointer;'>7월</td>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-08"+" style='cursor:pointer;'>8월</td>";
	Cal_HTML += "</tr>\n";
	Cal_HTML += "<tr height='20' align='center' bgcolor='white'>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-09"+" style='cursor:pointer;'>9월</td>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-10"+" style='cursor:pointer;'>10월</td>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-11"+" style='cursor:pointer;'>11월</td>";
	Cal_HTML += "<td onClick='Calendar_Click(this);' onmouseover='doOver(this)' onmouseout='doOut(this)' title="+intThisYear+"-12"+" style='cursor:pointer;'>12월</td>";
	Cal_HTML += "</tr>\n";
	Cal_HTML += "</table>";

	var oPopBody = oPopup;
	oPopBody.style.backgroundColor = "#626262";
	oPopBody.style.border = "solid #626262 1px";
	oPopBody.innerHTML = Cal_HTML;

	oPopup.style.position = 'absolute';
	oPopup.style.left = pop_left + "px";
	oPopup.style.top = (pop_top + 20) + "px";
	oPopup.style.width = 160 + "px";
	oPopup.style.height = 99 + "px";
}


//----------------------------------
//	일달력 년도리스트에서 년도 선택
//----------------------------------
function fnChangeYearD(sYear,sMonth,sDay){
	Show_cal(sYear, sMonth, sDay);
}


//----------------------------------
//	월달력 년도리스트에서 년도 선택
//----------------------------------
function fnChangeYearM(sYear,sMonth){
	Show_cal_M(sYear, sMonth);
}


/**
	HTML 개체용 유틸리티 함수
**/
function GetObjectTop(obj)
{
	var totaloffset=obj.offsetTop;
    var parentEl=obj.offsetParent;
		
	if(parentEl)
	{
		while (parentEl!=null){
		    totaloffset=totaloffset+parentEl.offsetTop;
		    parentEl=parentEl.offsetParent;
		}
	}
	else if(obj.y)
	{
		totaloffset += obj.y;
	}
    return totaloffset;
}

function GetObjectLeft(obj)
{
	var totaloffset=obj.offsetLeft;
    var parentEl=obj.offsetParent;

    if(parentEl)
	{
		while (parentEl!=null)
		{
			totaloffset=totaloffset+parentEl.offsetLeft;
			parentEl=parentEl.offsetParent;
		}
	}
	else if(obj.x)
	{
		totaloffset += obj.x;
	}
    return totaloffset;
}
/**/
document.onclick = function(e){
	if(oPopup)
	{
		var nLeft = oPopup.style.left;
		var nTop = oPopup.style.top;

		var nWidth = oPopup.style.width;
		var nHeight = oPopup.style.height;
		
		nLeft = eval(nLeft.replace('px', ''));
		nTop = eval(nTop.replace('px', ''));

		nWidth = eval(nWidth.replace('px', ''));
		nHeight = eval(nHeight.replace('px', ''));

		var nRight = nLeft + nWidth;
		var nBottom = nTop + nHeight;

		if(e)
		{
			var nELeft = e.clientX;
			var nETop = e.clientY;
		}
		else
		{
			var nELeft = event.clientX;
			var nETop = event.clientY;
		}
		
		var nScrollTop = document.body.scrollTop;
		if(nScrollTop == 0){nScrollTop = document.documentElement.scrollTop;}

		if(!(nLeft <= nELeft && eval(nTop - 20 - nScrollTop) <= nETop && nRight >= nELeft && eval(nBottom - nScrollTop) >= nETop))
		{
			fnHide();
		}
	}
};