<!--#include file="inc/inc_top_common.asp"-->
<%
on error resume next

Dim sUserID, sPassword, sLoginChk
Dim sIDSaveChk
Dim sGroup, sUserName, sStoreCode, nStoreIdx, sWallet

sUserID = Trim(Request.Form("txtUserID"))
sPassword = Trim(Request.Form("txtUserPass"))
sIDSaveChk = Trim(Request.Form("chkIDSave"))

If sUserID = "" Then
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('아이디를 입력하세요.');"
	Response.Write "history.back();"
	Response.Write "</script>"
ElseIf sPassword = "" Then
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('비밀번호를 입력하세요.');"
	Response.Write "history.back();"
	Response.Write "</script>"
Else
	fnDBConn()

	sLoginChk = "N"

	'아이디 및 비밀번호 체크
'	sql = " Select * from admin_member where id=? and password=? "			
'	cmd.Parameters.Append cmd.CreateParameter("@id", adVarChar, adParaminput, 50, sUserID)
'	cmd.Parameters.Append cmd.CreateParameter("@password", adVarChar, adParaminput, 50, sPassword)
'	cmd.CommandText = sql
'	Set rs = cmd.Execute()
'	If not(rs.eof or rs.bof) Then
'		sLoginChk = "Y"
'		sUserName = fnNullInit(rs("name"))	
'		sGroup = fnNullInit(rs("m_group"))
'		If sGroup ="P" Then
'			sWallet = fnNullInit(rs("wallet_no"))
'		End if
'	End If	
'	rs.close	
'	Set rs = nothing	
'	ParamInit()
	
	
	If sUserID = "admin1234" And sPassword = "qaz1234" Then
		sLoginChk = "Y"
		if sLoginChk = "Y" then
			'로그인 세션 설정
			sUserName = "관리자"
			sGroup = "A"

			Session("SS_USERID") = sUserID
			Session("SS_USERNAME") = sUserName
			Session("SS_GROUP") = sGroup
			Session("SS_WALLET") = sWallet
			
			'ID저장 쿠키 체크
			if sIDSaveChk = "on" then
				Response.Cookies("saveID") = sUserID
				Response.Cookies("saveIDChk") = "on"
			else
				Response.Cookies("saveID") = ""
				Response.Cookies("saveIDChk") = "off"
			end if
			
			Response.Cookies("saveID").Expires = DateAdd("m", 1, Now())
			Response.Cookies("saveIDChk").Expires = DateAdd("m", 1, Now())
			If Session("SS_GROUP") ="P" Then
				Response.Redirect "g_main.asp"
			Else	
				Response.Redirect "main.asp"
			End if
		else
			
			Response.Write "<script type='text/javascript'>"
			Response.Write "alert('아이디 또는 비밀번호가 일치하지 않습니다.');"
			Response.Write "history.back();"
			Response.Write "</script>"
		end if
	Else
	'fnErrChk()
		'response.end
		
		Response.Write "<script type='text/javascript'>"
		Response.Write "alert('처리중 오류가 발생하여 로그인에 실패하였습니다.');"
		Response.Write "location.href='login.asp';"
		Response.Write "</script>"
		
	end if

	fnDBClose()
End If
%>