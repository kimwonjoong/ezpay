<!--#include file="inc/inc_top_common.asp"-->
<%
If NOT(isNull(SS_USER_ID) or SS_USER_ID = "") Then
	response.write "<script type='text/javascript'>location.href='member_logout.asp';</script>"
	Response.End
End If

Dim sSaveIDChk, sSaveID
sSaveIDChk = Request.Cookies("saveIDChk")
if sSaveIDChk = "on" then
	sSaveID = Request.Cookies("saveID")
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="css/default.css" rel="stylesheet" type="text/css">
<script src="js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="js/script_value_check.js" type="text/javascript"></script>
<script type="text/javascript">
function fnFocus(sArgID)
{
	document.getElementById(sArgID).focus();
}

function fnLoginChk(frm)
{
	if(fnDataChk(frm.txtUserID, 'LOGINUSERID') == false){return false;}
	if(fnDataChk(frm.txtUserPass, 'LOGINPASSWORD') == false){return false;}
	else
	{
		return true;
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%" onload="fnFocus('txtUserID');">
<div style="text-align:center;width:100%;">
	<div style="width:1024px;text-align:center;margin:auto; ">
<!-- Save for Web Slices (login.psd) -->
<form name="frmLogin" id="frmLogin" method="post" action="member_login.asp" onsubmit="return fnLoginChk(this);">
<table width="1024" height="768" border="0" cellpadding="0" cellspacing="0" align="center" bgcolor="#F1CF37">
	<tr>
		<td colspan="6" width="1024" height="285">
			<img src="images/index_01.jpg" width="1024" height="285" alt=""></td>
	</tr>
	<tr>
		<td rowspan="4" width="671" height="80">
			<img src="images/index_02.jpg" width="671" height="80" alt=""></td>
		<td background="images/lindex_03.jpg" width="149" height="29" bgcolor="#F1CF37">
			<!--img src="images/index_03.jpg" width="149" height="29" alt=""-->
			<input type="text" title="아이디" name="txtUserID" id="txtUserID" size="10" tabindex="1" class="base" style="font-size:14px;border:0px solid #ffffff;border-bottom:1px solid #ffffff;background:#F1CF37;width:100%;color:#ffffff" value="<%=sSaveID%>">
			</td>
		<td rowspan="4" width="20" height="80">
			<img src="images/index_04.jpg" width="20" height="80" alt=""></td>
		<td colspan="2" rowspan="2" width="132" height="58">
			<!--img src="images/index_05.jpg" width="132" height="58" alt=""-->
			<input type="image" src="images/index_05.jpg" width="132" height="58" alt="로그인" tabindex="3" style="border:0px">
			</td>
		<td rowspan="4" width="52" height="80">
			<img src="images/index_06.jpg" width="52" height="80" alt=""></td>
	</tr>
	<tr>
		<td background="images/index_07.jpg" width="149" height="29" bgcolor="#F1CF37">
			<!--img src="images/index_07.jpg" width="149" height="29" alt=""-->
			<input type="password" title="비밀번호" name="txtUserPass" id="txtUserPass" tabindex="2" size="10" class="base" style="font-size:13px;border:0px solid #ffffff;background:#F1CF37;width:100%;color:#ffffff">
			</td>
	</tr>
	<tr>
		<td rowspan="2" width="149" height="22">
			<img src="images/index_08.jpg" width="149" height="22" alt=""></td>
		<td colspan="2" width="132" height="9">
			<img src="images/index_09.jpg" width="132" height="9" alt=""></td>
	</tr>
	<tr>
		<td bgcolor="#F1CF37" width="13" height="13">
			<!--img src="images/index_10.jpg" width="13" height="13" alt=""-->
			<input type="checkbox" name="chkIDSave" id="chkIDSave" value="on" <%if sSaveIDChk="on" then%>checked<%end if%> style="width:13px;height:13px;">
		</td>
		<td align="left" style="color:#ffffff;font-size:10px;padding-left:2px;font-family:굴림" width="119" height="13" bgcolor="#F1CF37">
		<!--img src="images/index_11.jpg" width="119" height="13" alt=""--><label for="chkIDSave">ID저장</label></td>
	</tr>
	<tr>
		<td colspan="6" width="1024" height="403">
			<img src="images/index_12.jpg" width="1024" height="403" alt=""></td>
	</tr>
</table>
</form>
<!-- End Save for Web Slices -->
</div>
</div>
</body>
</html>