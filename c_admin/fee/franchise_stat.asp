<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("P")

Dim sKColumn, sKeyword

Dim fromDate,toDate,difDate


MENU_IDX_ = 8
sMenuCode = "fee"
PAGE_CODE_ = "month"
PAGE_TITLE_ = "수수료관리"
sListFileName = PAGE_CODE_ & "_stat.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))

If sKColumn = "" Then
	sKColumn = year(date)
End if

sFilter = ""

fnDBConn()


sql ="select DATEPART(yyyy, regist_date) as m_year,DATEPART(mm, regist_date) as m_month, sum(convert(money,gcoin)) as g_sum,sum(convert(money,gcoin))*0.03 as fee from gcoin_buy where result='1' and DATEPART(yyyy, regist_date) ='"& sKColumn &"' group by DATEPART(yyyy, regist_date),DATEPART(mm, regist_date)  order by DATEPART(yyyy, regist_date) desc,DATEPART(mm, regist_date) desc"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing

fnDBClose()

if isNumeric(nRecordCount) then
	nRecordCount = CCur(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
function m_submit()
{
	document.frmSch.submit();

}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<div class="lyMainTitle"><img src="/g_admin/images/common/icon_06.png" alt="<%=PAGE_TITLE_%>"></div>
<div class="lyCSch">
	<form name="frmSch" id="frmSch" action="<%=sListFileName%>" method="post" onsubmit="return fnSchChk(this);">
	<span class="txtLabel">검색년도</span>
	<select name="optColumn" id="optColumn" onchange="javascript:m_submit();">
		<option value="2015" <%if sKColumn="2015" then%>selected<%end if%>>2015년</option>
		<option value="2014" <%if sKColumn="2014" then%>selected<%end if%>>2014년</option>
	</select>


	</form>
</div>
<div class="lyMainTbl">

<table class="tblMain">
	<tr>
		<th width="10%">년도</th>
		<th width="10%">월</th>
		<th width="10%">수익</th>
		<th width="14%">지출</th>
		<th width="10%">차액</th>
	</tr>
	<%
	Dim t_suik,t_jichul,t_chak
	t_suik=0
	t_jichul=0
	t_chak=0

	if nData >= 0 then
		for i=0 to nData

		t_suik=t_suik+CCur(aData("g_sum")(i))
		t_jichul=t_jichul+CCur(aData("fee")(i))
		t_chak=t_chak+(CCur(aData("g_sum")(i))-CCur(aData("fee")(i)))
	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=aData("m_year")(i)%></td>
		<td><%=aData("m_month")(i)%></td>
		<td><%=fnFormatNumber(aData("g_sum")(i),0,0)%>원</td>
		<td><font color="red"><%=fnFormatNumber(aData("fee")(i),0,0)%>원</font></td>
		<td><%=fnFormatNumber(CCur(aData("g_sum")(i))-CCur(aData("fee")(i)),0,0)%>원</td>
	</tr>
	<%
		next
	%>

	<%else%>
	<tr class="msg145">
		<td colspan="6">등록된 수수료내역이 없습니다.</td>
	</tr>
	<%end if%>

	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><font color="blue">total</font></td>
		<td></td>
		<td><font color="blue"><%=fnFormatNumber(t_suik,0,0)%>원</font></td>
		<td><font color="red"><%=fnFormatNumber(t_jichul,0,0)%>원</font></td>
		<td><font color="blue"><%=fnFormatNumber(t_chak,0,0)%>원</font></td>
	</tr>
</table>
</div>

<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>