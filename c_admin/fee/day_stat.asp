<!--#include file="../inc/inc_top_common.asp"-->
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<%
sbMemberAuthChk("P")

Dim sKColumn, sKeyword,sKColumn2,sKColumn3

Dim fromDate,toDate,difDate
Dim aOutcoin, nOutcoin: nOutcoin = -1
Dim aIncoin, nIncoin: nIncoin = -1
Dim aChargecoin, nChargecoin: nChargecoin = -1
Dim aSettlementcoin, nSettlementcoin: nSettlementcoin = -1



MENU_IDX_ = 8
sMenuCode = "fee"
PAGE_CODE_ = "day"
'PAGE_TITLE_ = "수수료관리"
sListFileName = PAGE_CODE_ & "_stat.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKColumn2 = fnKeywordFilter(Request("optColumn2"))
sKColumn3 = fnKeywordFilter(Request("optColumn3"))


If sKColumn = "" Then
	sKColumn = year(date)
End If
If sKColumn2 = "" Then
	sKColumn2 = month(date)
End If
If sKColumn3 = "" Then
	sKColumn3 = day(date)
End If


sQueryString = "optColumn="&sKColumn&"&optColumn2="&sKColumn2&"&optColumn3="&sKColumn3


sFilter = ""

Dim r_check
r_check="0"

fnDBConn()

sql ="select company,wallet_no,convert(float,fee)*0.01 as g_fee,convert(float,charge_fee)*0.01 as c_fee from gcoin_franchise where result ='1' order by company asc"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing



if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
function m_submit()
{
	document.frmSch.submit();

}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->

<div class="lyMainTitle"><img src="/g_admin/images/common/icon_06.png" alt="<%=PAGE_TITLE_%>"></div>
<br>
<div class="lyCSch">
	<form name="frmSch" id="frmSch" action="<%=sListFileName%>" method="post" onsubmit="return fnSchChk(this);">
	<span class="txtLabel">검색일자</span>
	<select name="optColumn" id="optColumn">
		<option value="2016" <%if sKColumn="2016" then%>selected<%end if%>>2016년</option>
		<option value="2015" <%if sKColumn="2015" then%>selected<%end if%>>2015년</option>
		<option value="2014" <%if sKColumn="2014" then%>selected<%end if%>>2014년</option>
	</select>
	<select name="optColumn2" id="optColumn2">
		<option value="1" <%if sKColumn2="1" then%>selected<%end if%>>1월</option>
		<option value="2" <%if sKColumn2="2" then%>selected<%end if%>>2월</option>
		<option value="3" <%if sKColumn2="3" then%>selected<%end if%>>3월</option>
		<option value="4" <%if sKColumn2="4" then%>selected<%end if%>>4월</option>
		<option value="5" <%if sKColumn2="5" then%>selected<%end if%>>5월</option>
		<option value="6" <%if sKColumn2="6" then%>selected<%end if%>>6월</option>
		<option value="7" <%if sKColumn2="7" then%>selected<%end if%>>7월</option>
		<option value="8" <%if sKColumn2="8" then%>selected<%end if%>>8월</option>
		<option value="9" <%if sKColumn2="9" then%>selected<%end if%>>9월</option>
		<option value="10" <%if sKColumn2="10" then%>selected<%end if%>>10월</option>
		<option value="11" <%if sKColumn2="11" then%>selected<%end if%>>11월</option>
		<option value="12" <%if sKColumn2="12" then%>selected<%end if%>>12월</option>
	</select>
	<select name="optColumn3" id="optColumn3" onchange="javascript:m_submit();">
		<option value="1" <%if sKColumn3="1" then%>selected<%end if%>>1일</option>
		<option value="2" <%if sKColumn3="2" then%>selected<%end if%>>2일</option>
		<option value="3" <%if sKColumn3="3" then%>selected<%end if%>>3일</option>
		<option value="4" <%if sKColumn3="4" then%>selected<%end if%>>4일</option>
		<option value="5" <%if sKColumn3="5" then%>selected<%end if%>>5일</option>
		<option value="6" <%if sKColumn3="6" then%>selected<%end if%>>6일</option>
		<option value="7" <%if sKColumn3="7" then%>selected<%end if%>>7일</option>
		<option value="8" <%if sKColumn3="8" then%>selected<%end if%>>8일</option>
		<option value="9" <%if sKColumn3="9" then%>selected<%end if%>>9일</option>
		<option value="10" <%if sKColumn3="10" then%>selected<%end if%>>10일</option>
		<option value="11" <%if sKColumn3="11" then%>selected<%end if%>>11일</option>
		<option value="12" <%if sKColumn3="12" then%>selected<%end if%>>12일</option>
		<option value="13" <%if sKColumn3="13" then%>selected<%end if%>>13일</option>
		<option value="14" <%if sKColumn3="14" then%>selected<%end if%>>14일</option>
		<option value="15" <%if sKColumn3="15" then%>selected<%end if%>>15일</option>
		<option value="16" <%if sKColumn3="16" then%>selected<%end if%>>16일</option>
		<option value="17" <%if sKColumn3="17" then%>selected<%end if%>>17일</option>
		<option value="18" <%if sKColumn3="18" then%>selected<%end if%>>18일</option>
		<option value="19" <%if sKColumn3="19" then%>selected<%end if%>>19일</option>
		<option value="20" <%if sKColumn3="20" then%>selected<%end if%>>20일</option>
		<option value="21" <%if sKColumn3="21" then%>selected<%end if%>>21일</option>
		<option value="22" <%if sKColumn3="22" then%>selected<%end if%>>22일</option>
		<option value="23" <%if sKColumn3="23" then%>selected<%end if%>>23일</option>
		<option value="24" <%if sKColumn3="24" then%>selected<%end if%>>24일</option>
		<option value="25" <%if sKColumn3="25" then%>selected<%end if%>>25일</option>
		<option value="26" <%if sKColumn3="26" then%>selected<%end if%>>26일</option>
		<option value="27" <%if sKColumn3="27" then%>selected<%end if%>>27일</option>
		<option value="28" <%if sKColumn3="28" then%>selected<%end if%>>28일</option>
		<option value="29" <%if sKColumn3="29" then%>selected<%end if%>>29일</option>
		<option value="30" <%if sKColumn3="30" then%>selected<%end if%>>30일</option>
		<option value="31" <%if sKColumn3="31" then%>selected<%end if%>>31일</option>
	</select>


	</form>
</div>
<div class="lyMainTbl">

<table class="tblMain">
	<tr>
		<th width="10%">가맹점명</th>
		<th width="6%">지갑주소</th>
		<th width="6%">이체<br>수수료율</th>
		<th width="6%">충전<br>수수료율</th>
		<th width="10%">이체금액</th>
		<th width="10%">이체수수료</th>
		<th width="10%">충전금액</th>
		<th width="10%">충전수수료</th>
		<th width="10%">출금금액</th>
		<th width="10%">청산금액</th>
		<th width="10%">잔액</th>
	</tr>
	<%
	Dim t_charge,t_suik,t_fee1,t_fee2,t_jichul,t_chak,t_settlement,o_sum,c_sum,o_sum2,c_sum2,c_sum3,s_sum
	t_suik=0
	t_jichul=0
	t_chak=0
	t_charge=0
	t_fee1=0
	t_fee2=0
	t_settlement=0
	o_sum=0
	o_sum2=0
	c_sum=0
	c_sum2=0
	c_sum3=0
	s_sum=0
	if nData >= 0 then
		for i=0 to nData

			sql=""
			sql="select sum(convert(money,b.gcoin)) as o_sum,sum(convert(money,b.fee)) as c_sum from gcoin_transfer as b,gcoin_franchise as f where b.receive_wallet = '"&aData("wallet_no")(i)&"' and  DATEPART(yyyy, b.regist_date) ='"&sKColumn&"'  and  DATEPART(mm, b.regist_date) ='"&sKColumn2&"' and  DATEPART(dd, b.regist_date) ='"&sKColumn3&"'		group by f.wallet_no"
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nOutcoin = fnRs2Json(aOutcoin, rs)
				o_sum =aOutcoin("o_sum")
				c_sum =aOutcoin("c_sum")
			Else
				o_sum =0
				c_sum =0
			
			end if
			rs.close
			ParamInit()


			sql=""
			sql="select sum(convert(money,b.gcoin)) as o_sum2 from gcoin_transfer as b,gcoin_franchise as f where b.send_wallet = '"&aData("wallet_no")(i)&"' and  DATEPART(yyyy, b.regist_date) ='"&sKColumn&"'  and  DATEPART(mm, b.regist_date) ='"&sKColumn2&"' and  DATEPART(dd, b.regist_date) ='"&sKColumn3&"'		group by f.wallet_no"
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nIncoin = fnRs2Json(aIncoin, rs)
				o_sum2 =aIncoin("o_sum2")
			Else
				o_sum2 =0
		
			end if
			rs.close
			ParamInit()

			sql=""
			sql="select sum(convert(money,gcoin)) as c_sum2 from gcoin_buy where wallet_no = '"&aData("wallet_no")(i)&"' and  DATEPART(yyyy, regist_date) ='"&sKColumn&"'  and  DATEPART(mm, regist_date) ='"&sKColumn2&"' and  DATEPART(dd, regist_date) ='"&sKColumn3&"' and result='1'		group by wallet_no"
			
			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nChargecoin = fnRs2Json(aChargecoin, rs)
				c_sum3 = CCur(aChargecoin("c_sum2"))
				c_sum2 = CCur(aChargecoin("c_sum2"))*CCur(aData("c_fee")(i))
			Else
				c_sum2 = 0
				c_sum3 = 0
			end if
			rs.close
			ParamInit()

			sql=""
			sql = "select sum(convert(money,gcoin)) as s_sum from gcoin_settlement where wallet_no = '"&aData("wallet_no")(i)&"' and  DATEPART(yyyy, regist_date) ='"&sKColumn&"'  and  DATEPART(mm, regist_date) ='"&sKColumn2&"' and  DATEPART(dd, regist_date) ='"&sKColumn3&"' and result='1' group by wallet_no"

			cmd.CommandText = sql
			Set rs = cmd.Execute()
			if not(rs.eof or rs.bof) then
				nSettlementcoin = fnRs2Json(aSettlementcoin, rs)
				s_sum = CCur(aSettlementcoin("s_sum"))
			Else
				s_sum = 0
			end if
			rs.close
			ParamInit()

			t_suik=t_suik+CCur(o_sum)+CCur(c_sum)
			t_jichul=t_jichul+CCur(o_sum2)
			t_fee1=t_fee1+CCur(c_sum)
			t_fee2=t_fee2+CCur(c_sum2)
			t_charge=t_charge+CCur(c_sum3)
			t_settlement=t_settlement+CCur(s_sum)
			t_chak=t_chak+(CCur(o_sum)+CCur(c_sum3))-(CCur(c_sum2)+CCur(o_sum2)+CCur(s_sum))
		
		

	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><a href="transfer_view.asp?wallet_no=<%=aData("wallet_no")(i)%>&m_year=<%=sKColumn%>&m_month=<%=sKColumn2%>&m_day=<%=sKColumn3%>"><%=aData("company")(i)%></td>
		<td><a href="transfer_view.asp?wallet_no=<%=aData("wallet_no")(i)%>&m_year=<%=sKColumn%>&m_month=<%=sKColumn2%>&m_day=<%=sKColumn3%>"><%=aData("wallet_no")(i)%></a></td>
		<td><%=aData("g_fee")(i)%>%</td>
		<td><%=aData("c_fee")(i)%>%</td>
		<td><%=fnFormatNumber(CCur(o_sum)+CCur(c_sum),0,0)%>원</td>
		<td><%=fnFormatNumber(CCur(c_sum),0,0)%>원</td>
		<td><%=fnFormatNumber(CCur(c_sum3),0,0)%>원</td>
		<td><%=fnFormatNumber(CCur(c_sum2),0,0)%>원</td>
		<td><%=fnFormatNumber(CCur(o_sum2),0,0)%>원</td>
		<td><%=fnFormatNumber(CCur(s_sum),0,0)%>원</td>
		<td><%=fnFormatNumber((CCur(o_sum)+CCur(c_sum3))-(CCur(c_sum2)+CCur(o_sum2)+CCur(s_sum)),0,0)%>원</td>
	
	</tr>
	<%
		next
	%>

	<%else%>
	<tr class="msg145">
		<td colspan="11">등록된 수수료내역이 없습니다.</td>
	</tr>
	<%end if%>

	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><font color="blue">total</font></td>
		<td></td>
		<td></td>
		<td></td>
		<td><font color="blue"><%=fnFormatNumber(t_suik,0,0)%>원</font></td>
		<td><font color="red"><%=fnFormatNumber(t_fee1,0,0)%>원</font></td>
		<td><font color="blue"><%=fnFormatNumber(t_charge,0,0)%>원</font></td>
		<td><font color="red"><%=fnFormatNumber(t_fee2,0,0)%>원</font></td>
		<td><font color="blue"><%=fnFormatNumber(t_jichul,0,0)%>원</font></td>
		<td><font color="blue"><%=fnFormatNumber(t_settlement,0,0)%>원</font></td>
		<td><font color="blue"><%=fnFormatNumber(t_chak,0,0)%>원</font></td>
	</tr>
</table>
</div>
<div class="lyRMsg"><span class="button base"><a href="fee_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<% fnDBClose()%>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>