<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Response.Buffer = True
Response.ContentType = "appllication/vnd.ms-excel" 
Response.CacheControl = "public"

Response.AddHeader "Content-Disposition","attachment; filename=일별이체내역.xls"

Dim sKColumn, sKeyword, nIdx
Dim fromDate,toDate,difDate
Dim aCharge, nCharge: nCharge = -1
Dim aCharge2, nCharge2: nCharge2 = -1
Dim aSend, nSend: nSend = -1
Dim aSettlement, nSettlement: nSettlement = -1

Dim wallet_no,m_year,m_month,m_day


wallet_no = fnKeywordFilter(request("wallet_no"))
m_year = fnKeywordFilter(request("m_year"))
m_month = fnKeywordFilter(request("m_month"))
m_day = fnKeywordFilter(request("m_day"))

sQueryString = "wallet_no="&wallet_no&"&m_year="&m_year&"&m_month="&m_month&"&m_day="&m_day



fnDBConn()



if wallet_no <> "" then
	

	
	sql = " select * from gcoin_transfer where receive_wallet=? and DATEPART(yyyy, regist_date) ='"&m_year&"'  and  DATEPART(mm, regist_date) ='"&m_month&"' and  DATEPART(dd, regist_date) ='"&m_day&"' "
	cmd.Parameters.Append cmd.CreateParameter("@receive_wallet", adVarChar, adParaminput, 50, wallet_no)
	cmd.CommandText = sql
	Set rs = cmd.Execute()
	if not(rs.eof or rs.bof) then
		nSend = fnRs2JsonArr(aSend, rs)
	end if
	rs.close
	ParamInit()


	
end if
Set rs = nothing

fnDBClose()


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">

<div class="csCaption1">이체내역</div>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="15%">받은지갑주소</th>
		<th width="15%">신청금액</th>
		<th width="10%">이체수수료</th>
		<th width="15%">지급코인</th>
		<th width="15%">진행상태</th>
		<th width="25%">출금일자</th>
	</tr>
	<%
	if nSend >= 0 then
		for i=0 to nSend
	%>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=i+1%></td>
		<td><%=aSend("send_wallet")(i)%></td>
		<td><%=fnFormatNumber(CCur(aSend("gcoin")(i))+CCur(aSend("fee")(i)),0,0)%></td>
		<td><%=fnFormatNumber(aSend("fee")(i),0,0)%></td>
		<td><%=fnFormatNumber(aSend("gcoin")(i),0,0)%></td>
		<td><% If aSend("result")(i) = "0" Then %><font color="green">대기중</font><%ElseIf aSend("result")(i) = "1" Then%><font color="blue">처리완료</font><%ElseIf aSend("result")(i) = "2" Then%><font color="red">취소</font><%ElseIf aSend("result")(i) = "9" Then%><font color="red">기타오류</font><%End If%></td>
		<td><%=aSend("regist_date")(i)%></td>
	</tr>
	<%
		next
	%>
	<%else%>
	<tr class="msg145">
		<td colspan="7">등록된 이체정보가 없습니다.</td>
	</tr>
	<%end if%>
</table>

</body>
</html>