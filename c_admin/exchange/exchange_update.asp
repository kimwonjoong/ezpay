<!--#include file="../inc/inc_top_common.asp"-->
<%
sbMemberAuthChk("A")

Dim sKColumn, sKeyword, nIdx
Dim fromDate,toDate,difDate
Dim aCharge, nCharge: nCharge = -1
Dim aSend, nSend: nSend = -1
Dim aReceive, nReceive: nReceive = -1
Dim aSettlement, nSettlement: nSettlement = -1

MENU_IDX_ = 8
PAGE_CODE_ = "exchange"
sListFileName = PAGE_CODE_ & "_list.asp"
nIdx = fnIdxInit(request("idx"))
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "pageNO="&nPageNO&"&optColumn="&sKColumn&"&txtKeyword="&sKeyword

sFilter = ""

if nIdx = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('환율 정보가 없습니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if

fnDBConn()

sql = " select * from gcoin_exchange where idx=? "
cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParamInput, 4, nIdx)
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2Json(aData, rs)
else
	nIdx = ""
end if
rs.close
ParamInit()


fnDBClose()

if nIdx = "" then	
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('환율정보가 없습니다.');"
	Response.Write "location.href='" & sListFileName & "';"
	Response.Write "</script>"
	response.end
end if
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}

function fnUpdateChk(sMode)
{
	var sMsg = "";
	var sAction = "";
	if(sMode == "modify")
	{
		sMsg = "수정";
		sAction = "exchange_update_ok.asp";

		if(confirm("외국환정보를 " + sMsg + " 하시겠습니까?"))
		{
			var frm = document.getElementById("frmEdit");
			frm.hdMode.value = sMode;
			frm.action = sAction;
			frm.submit();
		}
	}
	else
	{
		alert("잘못된 접근입니다.");
		return false;
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<form name="frmEdit" id="frmEdit" method="post" action="exchange_update.asp">
<div class="lyMainTitle"><img src="/g_admin/images/common/icon_05.png" alt="<%=PAGE_TITLE_%>"></div>
<div class="lyMainTbl">
<table class="tblContent">
	<tr class="top">
		<th width="20%">환율명</th>
		<td colspan="3"><%=aData("exchange_name")%></td>
	</tr>
	<tr>
	
		<th>EZ페이</th>
		<td colspan="3"><input type="text" name="rate" value="<%=aData("rate")%>">%</td>
	</tr>
	
</table>
</div>
<div class="lyRBtn">
	
	<input type="hidden" name="hdIdx" id="hdIdx" value="<%=nIdx%>">
	<input type="hidden" name="hdMode" id="hdMode" value="">
	<input type="hidden" name="pageNO" id="pageNO" value="<%=nPageNO%>">
	<input type="hidden" name="optColumn" id="optColumn" value="<%=sKColumn%>">
	<input type="hidden" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<%if SS_GROUP="A" then%>
		<span class="button base"><input type="button" value="수정" onclick="fnUpdateChk('modify');"></span>
		<!--span class="button base"><input type="button" value="삭제" onclick="fnUpdateChk('delete');"></span-->
	<%end if%>
	<span class="button base"><a href="<%=sListFileName%>?<%=sQueryString%>">목록</a></span>
	
</div>
</form>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>