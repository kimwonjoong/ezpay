<!--#include file="../inc/inc_top_common.asp"-->

<%
sbMemberAuthChk("P")

Dim sKColumn, sKeyword

Dim fromDate,toDate,difDate

Dim sKSDate : sKSDate = fnNullInit(request("txtSDate"))
Dim sKEDate : sKEDate = fnNullInit(request("txtEDate"))

MENU_IDX_ = 8
sMenuCode = "admin"
PAGE_CODE_ = "member"
PAGE_TITLE_ = "회원관리"
sListFileName = PAGE_CODE_ & "_list.asp"
nPageNO = fnPagingInit(Request("pageNO"))
sKColumn = fnKeywordFilter(Request("optColumn"))
sKeyword = fnKeywordFilter(Request("txtKeyword"))
sQueryString = "optColumn="&sKColumn&"&txtKeyword="&sKeyword&"&txtSDate="&sKSDate&"&txtEDate="&sKEDate

if isDate(sKSDate) and isDate(sKEDate) then
	if DateDiff("d", sKSDate, sKEDate) < 0 then
		sKEDate = sKSDate
	end if
else
	sKSDate = ""
	sKEDate = ""
end if

sFilter = ""

fnDBConn()

if sKeyword <> "" then
	if sKColumn = "wallet_no" then
		sFilter = sFilter & " and wallet_no like ? "
		cmd.Parameters.Append cmd.CreateParameter("@wallet_no", adVarChar, adParamInput, 250, "%" & sKeyword & "%")
	elseif sKColumn = "handphone" then
		sFilter = sFilter & " and handphone like ? "
		cmd.Parameters.Append cmd.CreateParameter("@handphone", adVarChar, adParamInput, 250, "%" & sKeyword & "%")	
	end if
end if

if isDate(sKSDate) and isDate(sKEDate) then
	sFilter = sFilter & " and convert(date, reg_date) between ? and ? "
	cmd.Parameters.Append cmd.CreateParameter("@reg_date1", adDate, adParamInput, , sKSDate)
	cmd.Parameters.Append cmd.CreateParameter("@reg_date2", adDate, adParamInput, , sKEDate)
end if

'sqlTable = "select a.*, app_name from store a left join app_info b on(a.app_id=b.app_id) where 1=1 " & sFilter
sqlTable = "select * from member where 1=1 " & sFilter

'게시물 개수
sql = " select count(*) cnt from (" & sqlTable & ")a "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nRecordCount = trim(rs("cnt"))
end if
rs.close

'게시물 리스트	
sqlTable = " SELECT ROW_NUMBER() OVER(ORDER BY reg_date desc) AS RowNum, * FROM (" & sqlTable & ")a "	
sql = " ;with tbl as(" &  sqlTable & ") "
sql = sql & " SELECT TOP " & nPageSize & " * FROM tbl "
sql = sql & " WHERE RowNum >= " & (nPageNO-1)*nPageSize+1 & " and RowNum >= (SELECT MAX(RowNum)RowNum FROM (SELECT TOP " & (nPageNO-1)*nPageSize+1 & " RowNum FROM tbl ORDER BY RowNum ASC) a) ORDER BY RowNum ASC "
cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof) then
	nData = fnRs2JsonArr(aData, rs)
end if
rs.close
Set rs = nothing

fnDBClose()

if isNumeric(nRecordCount) then
	nRecordCount = CLng(nRecordCount)
else
	nRecordCount = 0
end if
nPageCount = fnCeiling(nRecordCount/nPageSize)

If nPageNO > nPageCount Then
	nPageNO = 1
End If 
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="../css/default.css" rel="stylesheet" type="text/css">
<script src="../js/jquery-1.4.2.min.js" type="text/javascript"></script>
<script src="../js/script_value_check.js" type="text/javascript"></script>
<script src="../js/common.js" type="text/javascript"></script>
<script src="../js/script_calendar.js" type="text/javascript"></script>
<script type="text/javascript">
function fnSchChk(frm)
{
	return true;
}
function clickEdit(idx){
	//alert('서비스 준비중입니다.');

	if(confirm("정말로 수정 하시겠습니까?")){
		document.myform.idx.value = idx;
		document.myform.action = "member_update.asp" ;
		document.myform.submit();		
	}
}

function clickDel(wallet_no){
	if(confirm("정말로 삭제 하시겠습니까?")){
		document.myform.wallet_no.value = wallet_no;
		document.myform.action = "member_delete.asp" ;
		document.myform.submit();		
	}
}
</script>
</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%">
<!--#include file="../inc/inc_head.asp"-->
<!--#include file="../main_include.asp"-->
<div class="lyMainTitle"><img src="/g_admin/images/common/title_member.jpg" alt="<%=PAGE_TITLE_%>"></div>
<div class="lyCSch">
	<form name="frmSch" id="frmSch" action="<%=sListFileName%>" method="post" onsubmit="return fnSchChk(this);">
	<span class="txtLabel">가입일자</span>
	<input name="txtSDate" type="text" id="txtSDate" size="10" value="<%=sKSDate%>" onclick="Calendar_D(this);" class="txtDate" /> ~
	<input name="txtEDate" type="text" id="txtEDate" size="10" value="<%=sKEDate%>" onclick="Calendar_D(this);" class="txtDate"  />
	<select name="optColumn" id="optColumn">
		<option value="wallet_no" <%if sKColumn="wallet_no" then%>selected<%end if%>>지갑번호</option>
		<option value="handphone" <%if sKColumn="handphone" then%>selected<%end if%>>휴대폰</option>
	</select>
	<input type="text" class="txtKey" name="txtKeyword" id="txtKeyword" value="<%=sKeyword%>">
	<input type="image" src="/g_admin/images/common/btn_sch.jpg" width="62" height="23" align="absmiddle" title="검색하기" style="width:62px;height:23px">
	</form>
</div>
<div class="lyMainTbl">
<%=fnRecordCnt(nRecordCount, nPageNO, nPageCount)%>
<table class="tblMain">
	<tr>
		<th width="10%">번호</th>
		<th width="10%">지갑번호</th>
		<th width="10%">이름</th>
		<th width="15%">휴대폰</th>
		<th width="15%">EZ페이</th>
		<th width="15%">가입일자</th>
		<th width="10%">휴대폰상태</th>
		<th width="10%">회원상태</th>
		<th width="8%">수정</th>
		<th width="8%">삭제</th>
	</tr>
	<%
	if nData >= 0 then
		for i=0 to nData
	%>
	
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td><%=nRecordCount - i - (nPageNO-1)*nPageSize%></td>
		<td align="center"><a href="member_view.asp?wallet_no=<%=aData("wallet_no")(i)%>&pageNO=<%=nPageNO%>&sQueryString"><%=aData("wallet_no")(i)%></a></td>
		<td class="al"><%=aData("s_name")(i)%></td>
		<td><%=aData("handphone")(i)%></td>
		<td><%=fnFormatNumber(aData("gcoin")(i),0,0)%>EZ</td>
		<td><%=aData("reg_date")(i)%></td>
		<td><% If aData("h_check")(i) = "체크" Then %><font color="red"><%=aData("h_check")(i)%></font><%else%><%=aData("h_check")(i)%><%End if%></td>
		<td><% If aData("status")(i) = "차단" Then %><font color="red"><%=aData("status")(i)%></font><%else%><%=aData("status")(i)%><%End if%></td>
		<td><span class="button base"><a href="javascript:clickEdit('<%=aData("idx")(i)%>');">수정</a></span></td>
		<td><span class="button base"><a href="javascript:clickDel('<%=aData("wallet_no")(i)%>');">삭제</a></span></td>
	</tr>
	<%
		next
	%>

	<%else%>
	<tr class="msg145">
		<td colspan="8">등록된 회원이 없습니다.</td>
	</tr>
	<%end if%>
</table>
</div>
<form name="myform" method="post">
<input type="hidden" name="wallet_no">
<input type="hidden" name="idx">
</form>
<div class="lyRMsg"><span class="button base"><a href="member_excel.asp?<%=sQueryString%>">엑셀저장하기</a></span></div>
<%=fnPaging(sListFileName, nPageNO, nBlockSize, nPageCount, sQueryString)%>
<!--#include file="../inc/inc_footer.asp"-->
</body>
</html>