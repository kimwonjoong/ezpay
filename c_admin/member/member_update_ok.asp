<!--#include file="../inc/inc_top_common.asp"-->
<!--#include file="../inc/inc_data_chk.asp"-->
<%
sbMemberAuthChk("S")
on error resume next

Dim nIdx
Dim sKColumn, sKeyword
Dim nResultCnt : nResultCnt = 0
Dim sUserID
Dim gcoin : gcoin = ""
Dim USD : USD = ""
Dim JPY : JPY = ""
Dim CNY : CNY = ""
Dim PHP : PHP = ""
Dim HKD : HKD = ""
Dim buy_fee : buy_fee = ""
Dim send_fee : send_fee = ""
Dim settlement_fee : settlement_fee = ""
Dim memo : memo = ""
Dim status : status = ""
Dim h_check : h_check = ""
Dim handphone : handphone = ""

MENU_IDX_ = 8
PAGE_CODE_ = "member"
sListFileName = PAGE_CODE_ & "_list.asp"

nData = fnForm2Json(aData)

sMode = fnKeyworeFilter(aData("hdMode"))
nIdx = fnIdxInit(aData("hdIdx"))
sUserID = fnKeywordFilter(aData("hdUserID"))
nPageNO = fnPagingInit(aData("pageNO"))
sKColumn = fnKeyworeFilter(aData("optColumn"))
sKeyword = fnKeyworeFilter(aData("txtKeyword"))
sQueryString = "pageNO="&nPageNO&"&optColumn="&sKColumn&"&txtKeyword="&sKeyword
handphone= fnKeyworeFilter(aData("handphone"))
gcoin= fnKeyworeFilter(aData("gcoin"))
USD= fnKeyworeFilter(aData("USD"))
JPY= fnKeyworeFilter(aData("JPY"))
CNY= fnKeyworeFilter(aData("CNY"))
PHP= fnKeyworeFilter(aData("PHP"))
HKD= fnKeyworeFilter(aData("HKD"))

buy_fee= fnKeyworeFilter(aData("buy_fee"))
send_fee= fnKeyworeFilter(aData("send_fee"))
settlement_fee= fnKeyworeFilter(aData("settlement_fee"))
h_check= fnKeyworeFilter(aData("h_check"))
status= fnKeyworeFilter(aData("status"))

memo= fnKeyworeFilter(aData("memo"))


	if nIdx = "" then	
		Response.Write "<script type='text/javascript'>"
		Response.Write "alert('회원 정보가 없습니다.');"
		Response.Write "location.href='" & sListFileName & "';"
		Response.Write "</script>"
		response.end
	end if

	'Response.write "USD:" & USD & "<br>"
	'Response.write "gcoin:" & gcoin & "<br>"
	'Response.write "memo:" & memo & "<br>"
	'Response.End
	

		fnDBConn()

		dbConn.BeginTrans

		sql = " update member set handphone=?, gcoin=?, USD=?,JPY=?,CNY=?,PHP=?,HKD=?,buy_fee=?,send_fee=?,settlement_fee=?, memo=?, status=?,h_check=?  "
		cmd.Parameters.Append cmd.CreateParameter("@handphone", adVarChar, adParaminput, 50, handphone)
		cmd.Parameters.Append cmd.CreateParameter("@gcoin", adInteger, adParaminput, 4, gcoin)
		cmd.Parameters.Append cmd.CreateParameter("@USD",  adVarChar , adParaminput, 20, USD)
		cmd.Parameters.Append cmd.CreateParameter("@JPY",  adVarChar , adParaminput, 20, JPY)
		cmd.Parameters.Append cmd.CreateParameter("@CNY",  adVarChar, adParaminput, 20, CNY)
		cmd.Parameters.Append cmd.CreateParameter("@PHP",  adVarChar, adParaminput, 20, PHP)
		cmd.Parameters.Append cmd.CreateParameter("@HKD",  adVarChar , adParaminput, 20, HKD)
		cmd.Parameters.Append cmd.CreateParameter("@buy_fee",  adVarChar , adParaminput, 20, buy_fee)
		cmd.Parameters.Append cmd.CreateParameter("@send_fee",  adVarChar , adParaminput, 20, send_fee)
		cmd.Parameters.Append cmd.CreateParameter("@settlement_fee",  adVarChar , adParaminput, 20, settlement_fee)
		cmd.Parameters.Append cmd.CreateParameter("@memo", adVarChar, adParaminput, 512, memo)
		cmd.Parameters.Append cmd.CreateParameter("@status", adVarChar, adParaminput, 50, status)
		cmd.Parameters.Append cmd.CreateParameter("@h_check", adVarChar, adParaminput, 50, h_check)
		
		sql = sql & " WHERE idx=? "
		cmd.Parameters.Append cmd.CreateParameter("@idx", adInteger, adParaminput, 4, nIdx)

		cmd.CommandText = sql
		cmd.Execute  nResultCnt, , adExecuteNoRecords

		if nResultCnt <> 1 or Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
			dbConn.RollBackTrans

			'fnErrChk()
			
			Response.Write "<script type='text/javascript'>"
			Response.Write "alert('오류가 발생하여 수정에 실패하였습니다.');"
			Response.Write "history.back();"
			Response.Write "</script>"
		else			
			dbConn.CommitTrans

			Response.Write "<script type='text/javascript'>"
			Response.Write "alert('수정이 완료되었습니다.');"
			Response.Write "location.href='member_list.asp';"
			Response.Write "</script>"
		end if

		fnDBClose()



%>