<!--#include file="inc/inc_top_common.asp"-->
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<%
sbMemberAuthChk("P")

Dim nMemberCount1,nMemberCount2,nGsum1,nGsum2,nSsum1,nSsum2,nGsum3,nGsum4,nSsum3,nSsum4,nSsum,nGsum5,nSsum5,nFsum1,nFsum2,nFsum3,nFsum4,nCsum3,nCsum4



MENU_IDX_ = 8
sMenuCode = "member"
PAGE_CODE_ = "member"
PAGE_TITLE_ = "회원관리"
sListFileName = PAGE_CODE_ & "_list.asp"


fnDBConn()

sql = "select count(*) cnt from member where 1=1 and DATEdiff(d,reg_date, GETDATE()) = 1 "


cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("cnt"))) then
	nMemberCount1 = trim(rs("cnt"))
Else
	nMemberCount1 = 0
end if
rs.close

sql = "select count(*) cnt2 from member where 1=1 and DATEdiff(d,reg_date, GETDATE()) = 0 "


cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("cnt2"))) then
	nMemberCount2 = trim(rs("cnt2"))
Else
	nMemberCount2 = 0
end if
rs.close


sql = "select sum(convert(money,gcoin)) g_sum from gcoin_transfer where 1=1 and left(receive_wallet,1)='9' and DATEdiff(d,regist_date, GETDATE()) = 1 and mode='C' and result='1' "

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nGsum1 = trim(rs("g_sum"))
Else
	nGsum1 = 0
end if
rs.close


sql = "select sum(convert(money,gcoin)) g_sum from gcoin_transfer where 1=1 and left(receive_wallet,1)='9' and DATEdiff(d,regist_date, GETDATE()) = 0 and mode='C' and result='1'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nGsum2 = trim(rs("g_sum"))
Else
	nGsum2 = 0
end if
rs.close



sql = "select sum(convert(money,fee)) g_sum from gcoin_transfer where 1=1 and left(receive_wallet,1)='9' and  DATEdiff(d,regist_date, GETDATE()) = 1 and mode='C' and result='1' "

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nFsum1 = trim(rs("g_sum"))
Else
	nFsum1 = 0
end if
rs.close


sql = "select sum(convert(money,fee)) g_sum from gcoin_transfer where 1=1 and left(receive_wallet,1)='9' and DATEdiff(d,regist_date, GETDATE()) = 0 and mode='C' and result='1'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nFsum2 = trim(rs("g_sum"))
Else
	nFsum2 = 0
end if
rs.close

sql = "select sum(convert(money,gcoin)) g_sum from gcoin_buy where 1=1 and left(wallet_no,1)='9' and DATEdiff(d,regist_date, GETDATE()) = 1 and mode='C' and result='1' "

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nCsum3 = trim(rs("g_sum"))
Else
	nCsum3 = 0
end if
rs.close


sql = "select sum(convert(money,gcoin)) g_sum from gcoin_buy where 1=1 and left(wallet_no,1)='9' and DATEdiff(d,regist_date, GETDATE()) = 0 and mode='C' and result='1'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nCsum4 = trim(rs("g_sum"))
Else
	nCsum4 = 0
end if
rs.close


sql = "select sum(convert(money,fee)) g_sum from gcoin_buy where 1=1 and left(wallet_no,1)='9' and DATEdiff(d,regist_date, GETDATE()) = 1 and mode='C' and result='1' "

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nFsum3 = trim(rs("g_sum"))
Else
	nFsum3 = 0
end if
rs.close


sql = "select sum(convert(money,fee)) g_sum from gcoin_buy where 1=1 and left(wallet_no,1)='9' and DATEdiff(d,regist_date, GETDATE()) = 0 and mode='C' and result='1'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nFsum4 = trim(rs("g_sum"))
Else
	nFsum4 = 0
end if
rs.close


sql = "select sum(convert(money,gcoin)) g_sum from gcoin_settlement where 1=1 and DATEdiff(d,regist_date, GETDATE()) = 1 and mode='C' and result='1' "

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nSsum1 = trim(rs("g_sum"))
Else
	nSsum1 = 0
end if
rs.close


sql = "select sum(convert(money,gcoin)) g_sum from gcoin_settlement where 1=1 and DATEdiff(d,regist_date, GETDATE()) = 0 and mode='C' and result='1'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nSsum2 = trim(rs("g_sum"))
Else
	nSsum2 = 0
end if
rs.close

sql = "select sum(convert(money,CNY)) g_sum from member "

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nSsum = trim(rs("g_sum"))
Else
	nSsum = 0
end if
rs.close



sql = "select count(idx) g_sum from gcoin_buy where 1=1 and DATEdiff(d,regist_date, GETDATE()) = 1 and result='0' and mode='C' "

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nGsum3 = trim(rs("g_sum"))
Else
	nGsum3 = 0
end if
rs.close


sql = "select count(idx) g_sum from gcoin_buy where 1=1 and DATEdiff(d,regist_date, GETDATE()) = 0 and result='0' and mode='C'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nGsum4 = trim(rs("g_sum"))
Else
	nGsum4 = 0
end if
rs.close


sql = "select count(idx) g_sum from gcoin_settlement where 1=1 and DATEdiff(d,regist_date, GETDATE()) = 1 and result='0' and mode='C'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nSsum3 = trim(rs("g_sum"))
Else
	nSsum3 = 0
end if
rs.close


sql = "select count(idx) g_sum from gcoin_settlement where 1=1 and DATEdiff(d,regist_date, GETDATE()) = 0 and result='0' and mode='C'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nSsum4 = trim(rs("g_sum"))
Else
	nSsum4 = 0
end if
rs.close

sql = "select count(idx) g_sum from franchise_settlement where 1=1 and DATEdiff(d,regist_date, GETDATE()) = 1 and result='0'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nGsum5 = trim(rs("g_sum"))
Else
	nGsum5 = 0
end if
rs.close


sql = "select count(idx) g_sum from franchise_settlement where 1=1 and DATEdiff(d,regist_date, GETDATE()) = 0 and result='0'"

cmd.CommandText = sql
Set rs = cmd.Execute()
if not(rs.eof or rs.bof Or Isnull(rs("g_sum"))) then
	nSsum5 = trim(rs("g_sum"))
Else
	nSsum5 = 0
end if
rs.close


Set rs = nothing

fnDBClose()


%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="ko" xml:lang="ko">
<head>

<meta http-equiv="Content-Type" content="text/html; charset=euc-kr" />
<title><%=TITLE_%></title>
<link href="css/default.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap-theme.min.css">
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="js/script_value_check.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
<script src="js/script_calendar.js" type="text/javascript"></script>
<script language="javascript"> 
<!-- 

function pageLoad() { 
 
 
 $.ajax({
		   type: 'post'
		   , async: true
		   , url: 'realtime_memo_connect.asp'
		   , beforeSend: function() {
		   }
		   , success: function(data) {			    

			   var agent = navigator.userAgent.toLowerCase(); 
			   if (data =="1")
			   {
					if (agent.indexOf("chrome") != -1) {
						document.getElementById('player2').volume=1.0;
						document.getElementById('player2').play();
					}else{
						document.getElementById('player').play();
					}			 
	   				//alert("충전대기자가 있습니다.");	
					location.reload(true);
			   }else if(data =="2"){
				   if (agent.indexOf("chrome") != -1) {
						document.getElementById('player2').volume=1.0;
						document.getElementById('player2').play();
					}else{
						document.getElementById('player').play();
					}			 
	   				//alert("청산대기자가 있습니다.");	
					location.reload(true);
			   }else if(data =="3"){
				   if (agent.indexOf("chrome") != -1) {
						document.getElementById('player2').volume=1.0;
						document.getElementById('player2').play();
					}else{
						document.getElementById('player').play();
					}			 
	   				//alert("가맹점청산대기자가 있습니다.");	
					location.reload(true);
			   
			   }else if(data =="4"){
				   if (agent.indexOf("chrome") != -1) {
						document.getElementById('player2').volume=1.0;
						document.getElementById('player2').play();
					}else{
						document.getElementById('player').play();
					}			 
	   				//alert("가맹점신청대기자가 있습니다.");	
					location.reload(true);
				}

			}
		  , error: function(data, status, err) {
			 console.log("error forward : "+data);
			 
		  }
		  , complete: function() {
		   }
	});

}
 
 
function intervalCall() { 
 
 setInterval("pageLoad()", 30000); // 3초간격으로 pageLoad()함수 호출 
 
} 


//--> 
</script> 

</head>
<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" align="center" width="100%" onload="intervalCall();">
<!--#include file="inc/inc_head.asp"-->

<div class="lyMainTitle"><img src="/g_admin/images/common/icon_01.png" alt="<%=PAGE_TITLE_%>"></div>
<br><br><br>
<!--embed src="bell2.wav" autostart="true" hidden="true" width="0" height="0" id="my_sound"-->
<audio id="player2" src="bell2.wav" preload="auto"></audio>

<object classid="CLSID:22D6F312-B0F6-11D0-94AB-0080C74C7E95" type="application/x-oleobject" id="player" width="300" height="45" style="width:0;height:0;top:0;left:0">
<param name="filename" value="./bell2.wav"><param name="AutoStart" value="0">
<param name="EnableContextMenu" value="false">
<param name="PlayCount" value="1">
</object>

<div align="left">&nbsp;<font size="3" color="blue"><b>현재 고객보유금액 : <%=fnFormatNumber(nSsum,0,0)%>위안화</b></font></div>
<div class="lyMainTbl">

<table class="tblMain">
	<tr>
		<th width="9%"></th>
		<th width="10%">신규회원가입</th>
		<th width="10%">이체금액</th>
		<th width="10%">이체수수료</th>
		<th width="10%">충전금액</th>
		<th width="10%">충전수수료</th>
		<th width="10%">청산금액</th>
		<th width="10%">충전대기자수</th>
		<th width="10%">청산대기자수</th>
		<th width="10%">가맹점청산대기자수</th>
	</tr>

	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td>어제</td>
		<td><%=fnFormatNumber(nMemberCount1,0,0)%>명</td>
		<td><%=fnFormatNumber(nGsum1,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nFsum1,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nCsum3,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nFsum3,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nSsum1,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nGsum3,0,0)%>명</td>
		<td><%=fnFormatNumber(nSsum3,0,0)%>명</td>
		<td><%=fnFormatNumber(nGsum5,0,0)%>명</td>
	</tr>
	<tr onmouseover="this.style.background='#FDFBDB';" onmouseout="this.style.background='#ffffff';">
		<td>오늘</td>
		<td><%=fnFormatNumber(nMemberCount2,0,0)%>명</td>
		<td><%=fnFormatNumber(nGsum2,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nFsum2,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nCsum4,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nFsum4,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nSsum2,0,0)%>위안화</td>
		<td><%=fnFormatNumber(nGsum4,0,0)%>명</td>
		<td><%=fnFormatNumber(nSsum4,0,0)%>명</td>
		<td><%=fnFormatNumber(nSsum5,0,0)%>명</td>
	</tr>
</table>
</div>



<!--#include file="inc/inc_footer.asp"-->
</body>
</html>