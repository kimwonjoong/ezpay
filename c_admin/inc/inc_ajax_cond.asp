<!--#include file="../inc/inc_top_common.asp"--><?xml version="1.0" encoding="euc-kr"?>
<%
sbMemberAuthChk("")

Response.ContentType = "text/xml"
Response.Charset = "euc-kr"

Dim aData1, nData1
Dim sKeyword : sKeyword = fnKeywordFilter(Request("txtKeyword"))
sMode = Trim(Request("mode"))	

response.write "<xml>" & vbcrlf
response.write "<body>" & vbcrlf

if sKeyword <> "" then

	if sMode = "ctg" or sMode = "kCtg" then
%>
		<card>
			<%
				fnDBConn()		

				sql = " select idx, '['+ convert(varchar, coupon_charge) +'%]'+coupon_name from coupon where coupon_category=? and app_status='1' and del_chk='0' order by coupon_charge desc "
				cmd.Parameters.Append cmd.CreateParameter("@coupon_category", adVarChar, adParamInput, 50, sKeyword)
				cmd.CommandText = sql
				set rs = cmd.execute()
				If not(rs.eof or rs.bof) then
					aData1 = rs.getRows()
				end if
				rs.close
				Set rs = nothing
				fnDBClose()
				
				if isArray(aData1) then
					nData1 = ubound(aData1, 2)
				else
					nData1 = -1
				end if
				
				for i=0 to nData1
			%>
					<list value1="<%=Trim(aData1(0, i))%>" text1="<%=Trim(aData1(1, i))%>" />
			<%
				next
			%>
		</card>
<%
	end if
end if

response.write "</body>" & vbcrlf
response.write "</xml>" & vbcrlf
%>