<%

response.charset = "utf-8"
%>
<%
Dim dbConn, cmd, cmd2
Dim DefaultConnString

Set dbConn = Nothing
Set cmd = Nothing
Set cmd2 = Nothing

'---------------------------------------------------
' DB연결 및 Command개체 초기화
'---------------------------------------------------
Sub fnDBConn()
	If dbConn is Nothing Then
		Dim sConnect, fnDBConn, sDBIP, sDB, sDBID, sDBPassword
		
		sDBIP="45.77.25.42"
		sDB="ezpay"
		sDBID="sa"
		sDBPassword="gksrmf$$7556"
		
		'sConnect = "Provider=SQLOLEDB.1;Password=" & sDBPassword & ";Persist Security Info=False;User ID=" & sDBID & ";Initial Catalog=" & sDB & ";Data Source=" & sDBIP	
		sConnect = "Provider=SQLOLEDB.1;Password=" & sDBPassword & ";User ID=" & sDBID & ";Initial Catalog=" & sDB & ";Data Source=" & sDBIP	
		DefaultConnString = sConnect
		Set dbConn = Server.CreateObject("ADODB.Connection")
		dbConn.Open sConnect

		'Server.ScriptTimeOut = 10000 'asp스크립트 타임아웃 시간을 늘려준다.
		dbConn.CommandTimeout=180
	End If

	If cmd is Nothing Then
		Set cmd = Server.CreateObject("ADODB.Command")
		cmd.ActiveConnection = dbConn
		cmd.CommandType = adCmdText
		cmd.CommandTimeout=180	'초단위
	End If

End Sub

Sub fnCmd2Init()
	If cmd2 is Nothing Then
		Set cmd2 = Server.CreateObject("ADODB.Command")
		cmd2.ActiveConnection = dbConn
		cmd2.CommandType = adCmdText
	End If
End Sub

'---------------------------------------------------
' DB연결 해제 Command개체 반환
'---------------------------------------------------
Sub fnDBClose()
	if (Not dbConn is Nothing) Then 
		if (dbConn.State = adStateOpen) Then dbConn.Close
		Set dbConn = Nothing
	End if

	if (Not cmd is Nothing) Then 
		Set cmd.ActiveConnection = Nothing
	  Set cmd = Nothing
	End If
	
	if (Not cmd2 is Nothing) Then 
		Set cmd2.ActiveConnection = Nothing
	  Set cmd2 = Nothing
	End if
End Sub

'---------------------------------------------------
' 트랜잭션을 시작하고, Connetion 개체를 반환한다.
'---------------------------------------------------
Function BeginTrans(dbConnectionString)
	If IsObject(dbConnectionString) Then
		If dbConnectionString is Nothing Then
			dbConnectionString = DefaultConnString
		End If
	End If

	Set dbConn = Server.CreateObject("ADODB.Connection")
	dbConn.Open dbConnectionString
	dbConn.BeginTrans
	Set BeginTrans = dbConn
End Function

'---------------------------------------------------
' 활성화된 트랜잭션을 커밋한다.
'---------------------------------------------------
Sub CommitTrans(dbConnectionObj)
	If Not dbConnectionObj Is Nothing Then
		dbConnectionObj.CommitTrans
		dbConnectionObj.Close
		Set ConnectionObj = Nothing
	End If
End Sub

'---------------------------------------------------
' 활성화된 트랜잭션을 롤백한다.
'---------------------------------------------------
Sub RollbackTrans(dbConnectionObj)
	If Not dbConnectionObj Is Nothing Then
		dbConnectionObj.RollbackTrans
		dbConnectionObj.Close
		Set ConnectionObj = Nothing
	End If
End Sub

Function fnInputFilter(str)
	str = trim(str)
	str = replace(str,"'","")
	str = replace(str,";","")
	str = replace(str,"--","")
	fnInputFilter = str
End Function

Function fnKeywordFilter(str)
	str = fnNullInit(str)
	str = replace(str,"'","")
	str = replace(str,";","")
	str = replace(str,"--","")
	fnKeywordFilter = str
End Function

Function fnKeyworeFilter(str)
	str = fnNullInit(str)
	str = replace(str,"'","")
	str = replace(str,";","")
	str = replace(str,"--","")
	fnKeyworeFilter = str
End Function

'금액 데이터 처리1
Function fnFormatNumber(sArgString, sArgReturnString, nArgIdx)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) then
		sNewStr = FormatNumber(sNewStr, nArgIdx)
	else
		sNewStr = sArgReturnString
	end if
	
	fnFormatNumber = sNewStr
	
End Function

'금액 데이터 처리2
Function fnFormatNumber2(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) then
		sNewStr = sNewStr
	else
		sNewStr = 0
	end if
	
	fnFormatNumber2 = sNewStr
	
End Function

'비율
Function fnDiscunt(sArgNum1, nArgRate)
	Dim nNum1, nNum2
	Dim nRate

	nNum1 = Trim(sArgNum1)
	nRate = Trim(nArgRate)
	nNum2 = ""
	if isNumeric(nNum1) And IsNumeric(nRate) Then
		nNum1 = CDbl(nNum1)
		nNum2 = (nNum1 * nRate) / 100
	end if
	
	fnDiscunt = nNum2
End Function 
'비율
Function fnRate(sArgNum1, sArgNum2)
	Dim nNum1, nNum2
	Dim nRate

	nNum1 = sArgNum1
	nNum2 = sArgNum2
	
	nNum1 = Trim(nNum1)
	nNum2 = Trim(nNum2)
	
	nRate = 0
	if isNumeric(nNum1) And IsNumeric(nNum2) Then
		nNum1 = CDbl(nNum1)
		nNum2 = CDbl(nNum2)
		If nNum1 > 0 and nNum2 > 0 then
			nRate = FormatNumber((nNum1/nNum2)*100,2)
		End If
	end if
	
	fnRate = nRate
End Function 

'널값 공백으로 초기화
Function fnNullInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	if isNull(sNewStr) or sNewStr = "" then
		sNewStr = ""
	end If	
	
	sNewStr = Trim(sNewStr)
	
	fnNullInit = sNewStr
End Function

'만 나이 구하는 함수
Function FuNnAge(dAgeDate) 
	DIM	nAge, nTemp

	If IsDate(dAgeDate) then

		nAge = DateDiff("YYYY",dAgeDate ,FormatDateTime(Now, 2)) 
				 
		If Right(dAgeDate, 6) = "-02-29" Then   '윤달
			nTemp = "-02-28"
		Else
			nTemp = dAgeDate
		End If
		
		nTemp = Left(DateAdd("YYYY", -1, FormatDateTime(Now, 2)), 4) & Right(nTemp, 6)   '현재년도의 전년도에 회원탄생을을 구한다
		nTemp = DateDiff("D", nTemp, FormatDateTime(Now, 2)) * 1

		If nTemp < 365 Then                                                              ' 일수로 일년이 지나지 않았으면....
			nAge = nAge - 1
		End If
	Else
		nAge = 0
	End If
	
	FuNnAge = nAge
End Function

Function fnBackScript(sArgMsg, sArgScript)
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('" & sArgMsg & "');"
	Response.Write sArgScript
	Response.Write "</script>"
End Function

'널데이터 처리
Function fnNullChk(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNull(sNewStr) or sNewStr = "" then
		sNewStr = "&nbsp;"
		sNewStr = "-"
	end if
	
	fnNullChk = sNewStr
	
End Function

Sub ParamInit()
	Dim nParameter, i
	if (Not cmd is Nothing) Then 
		nParameter = cmd.Parameters.Count
		for i=1 to nParameter
			cmd.Parameters.Delete(0)
		next
	End if
End Sub

Sub ParamInit2(commandCmd)
	Dim nParameter, i
	if (Not commandCmd is Nothing) Then 
		nParameter = commandCmd.Parameters.Count
		for i=1 to nParameter
			commandCmd.Parameters.Delete(0)
		next
	End if
End Sub

'올림함수
Function fnCeiling(nArgNum)
	If CLng(nArgNum) < nArgNum Then
		fnCeiling = CLng(nArgNum) + 1
	Else
		fnCeiling = CLng(nArgNum)
    End If
    
End Function

'리스트 새글 아이콘
Function fnNewIcon(sArgDate)
	
	Dim sNewIcon
							
	If datediff ("h", CDate(sArgDate), Now()) < 48 Then
		sNewIcon = "&nbsp;<img src='/admin/images/common/icon_new.gif' width='26' height='13' border='0' align='absmiddle'>"
	Else
		sNewIcon = ""
	End If
	
	fnNewIcon = sNewIcon
	
End Function

'조회수쿠키체크
Function fnVisitedChk(sArgBID, sArgTableName)

	Dim sIP, sValue, sCookie
	Dim bChk
	
	sIP = Request.ServerVariables("REMOTE_ADDR")	'IP주소
	sValue = "@" & sIP & "_" & sArgBID
	sCookie = Trim(request.cookies("ck_" & sArgTableName))
	
	bChk = False		
	if InStr(1, sCookie, sValue, 1) = 0 then
		response.cookies("ck_" & sArgTableName) = sCookie & sValue
		bChk = True				'조회수 증가
	end if

	fnVisitedChk = bChk

End Function

'문자열 바이트수로 자르기(문자열,최대바이트수,대체문자)
Function fnStrCut(sArgStr, nArgMaxByte, sArgTail)
	
	Dim sResult, cTemp, nByteSum, nIdx, nLen
	
	nByteSum=1
	nIdx=1
	nLen=Len(sArgStr)
	Do While nByteSum<=nArgMaxByte and nIdx<=nLen
		cTemp = Mid(sArgStr, nIdx, 1)
		If Asc(cTemp) > 0 and Asc(cTemp) < 255 Then
			nByteSum = nByteSum + 1
		Else
			nByteSum = nByteSum + 2
		End If
		nIdx = nIdx + 1
		sResult = sResult & cTemp
	Loop
	
	nIdx = nIdx - 1
	If nIdx = nLen Then
		fnStrCut = sArgStr
	Else
		fnStrCut = sResult & sArgTail
	End If
End Function

'HTML 변환 함수
Function fnHTMLConvert(sArgContent, cArgHtml)
	Dim sResult
	sResult = sArgContent
	
	if isNull(sResult) then
		sResult = ""
	end if

	if cArgHtml="Y" then 'html 허용
			
		sResult=Replace(sResult,chr(13)&chr(10),"<br>") '엔터는 한줄띄움
		sResult=RePlace(sResult,"  ","&nbsp;&nbsp;")
	else
		sResult=Replace(sResult, "&" , "&amp;")
		sResult=Replace(sResult, "%" , "&#37")	
		sResult=Replace(sResult, "<" , "&lt;")
		sResult=Replace(sResult, ">" , "&gt;")
		sResult=Replace(sResult,chr(13)&chr(10),"<br>") '엔터는 한줄띄움
	end if
		
	fnHtmlConvert = sResult
	
End Function

Sub fnErrChk()
	Dim i
	
	if isObject(dbConn) then
		for i=0 to dbConn.Errors.Count-1
			response.write "DB 에러 " & i & " : "& dbConn.Errors.item(i).description & "<= 1<br>"
		next
	end if

	if err then   '또는 err.Number<>0
		response.write "<br>err.Description : " & err.Description		'에러에 대한 자세한 설명
		response.write "<br>err.Number : " & err.Number				'에러번호
		response.write "<br>err.Source : " & err.Source					'어떤 에러인지 간략한 설명
		response.write "<br>err.HelpFile : " & err.HelpFile					'도움말파일
		response.write "<br>err.HelpContext : " & err.HelpContext		'출력내용
	end if
	'response.end
End Sub

Function fnGetErr()
	Dim i
	Dim sEMsg : sEMsg = ""

	if isObject(dbConn) then
		for i=0 to dbConn.Errors.Count-1
			'response.write "DB 에러 " & i & " : "& dbConn.Errors.item(i).description & "<= 1<br>"
			sEMsg = sEMsg & "DB 에러 " & i & " : "& dbConn.Errors.item(i).description & "<= 1<br>\n"
		next
	end if

	if err then   '또는 err.Number<>0
		response.write "<br>err.Description : " & err.Description		'에러에 대한 자세한 설명
		response.write "<br>err.Number : " & err.Number				'에러번호
		'response.write "<br>err.Source : " & err.Source					'어떤 에러인지 간략한 설명
		'response.write "<br>err.HelpFile : " & err.HelpFile					'도움말파일
		'response.write "<br>err.HelpContext : " & err.HelpContext		'출력내용
		'response.write "<br>err.NativeError : " & err.NativeError		'DB 에러번호
		sEMsg = sEMsg &  "\n<br>err.Description : " & err.Description		'에러에 대한 자세한 설명
		sEMsg = sEMsg &  "\n<br>err.Number : " & err.Number				'에러번호
		sEMsg = sEMsg &  "\n<br>err.Source : " & err.Source					'어떤 에러인지 간략한 설명
		sEMsg = sEMsg &  "\n<br>err.HelpFile : " & err.HelpFile					'도움말파일
		sEMsg = sEMsg &  "\n<br>err.HelpContext : " & err.HelpContext		'출력내용
	end if
	'response.end
	fnGetErr = sEMsg
End Function

'거래일시 데이터 처리
Function fnFormatDateTime(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) and len(sNewStr) = 14 then
		'20090601192430 => 2009.06.01.19:24:30
		sNewStr = left(sNewStr, 4) & "." & mid(sNewStr, 5,2)  & "." & mid(sNewStr, 7,2) & " " & mid(sNewStr, 9,2) & ":" & mid(sNewStr, 11,2) & ":" & mid(sNewStr, 13,2)
	elseIf Not(IsNull(sNewStr)) And IsDate(sNewStr) Then
		sNewStr = FormatDateTime(sNewStr,0)
	else
		sNewStr = "-"
	end if
	
	fnFormatDateTime = sNewStr
	
End Function


'날짜 데이터 처리
Function fnFormatDate(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) and len(sNewStr) = 8 then
		'20090930
		sNewStr = left(sNewStr, 4) & "-" & mid(sNewStr, 5,2)  & "-" & mid(sNewStr, 7,2)
	elseIf Not(IsNull(sNewStr)) And IsDate(sNewStr) Then
		sNewStr = FormatDateTime(sNewStr,2)
	Else
		sNewStr = "-"
	End If
	
	fnFormatDate = sNewStr
	
End Function


'거래시간 데이터 처리
Function fnFormatTime(sArgString)
	
	Dim sNewStr
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) and len(sNewStr) = 6 then
		'01192430 => 19:24:30
		sNewStr = left(sNewStr, 2) & ":" & mid(sNewStr, 3,2)  & ":" & mid(sNewStr, 5,2)
	elseIf Not(IsNull(sNewStr)) And IsDate(sNewStr) Then
		sNewStr = FormatDateTime(sNewStr,0)
	else
		sNewStr = "-"
	end if
	
	fnFormatTime = sNewStr
	
End Function

Function fnIntInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	End If

	sNewStr=Replace(sNewStr, "," , "")
	
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = CInt(sNewStr)
	else
		sNewStr = 0
	end If	
	
	fnIntInit = sNewStr
End Function

Function fnPagingInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	End If

	sNewStr=Replace(sNewStr, "," , "")
	
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = CDbl(sNewStr)
	else
		sNewStr = 0
	end If	

	if sNewStr < 1 then
		sNewStr = 1
	end if
	
	fnPagingInit = sNewStr
End Function

Function fnIdxInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	End If

	sNewStr=Replace(sNewStr, "," , "")
	
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = CDbl(sNewStr)
	else
		sNewStr = ""
	end If	
	
	fnIdxInit = sNewStr
End Function

Function fnDblInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNumeric(sNewStr) Then
		sNewStr = CDbl(sNewStr)
	else
		sNewStr = 0
	end If	
	
	fnDblInit = sNewStr
End Function

'게시물건수(페이지번호/총페이지수)
Function fnRecordCnt(nArgRecordCnt, nArgPageNO, nArgPageCnt)
	if isNumeric(nArgRecordCnt) then
%>
	<div class="lyRecordCnt">총 <%=fnFormatNumber(nArgRecordCnt,"0",0)%>건 (<%=nArgPageNO%>/<%=nArgPageCnt%>)</div>
<%
	end if
End Function 

'게시물건수(페이지번호/총페이지수)
Function fnRecordCnt2(nArgRecordCnt)
	if isNumeric(nArgRecordCnt) then
%>
	<div class="lyRecordCnt">총 <%=nArgRecordCnt%>건</div>
<%
	end if
End Function 

'페이징
Function fnPaging(sListFileName, nPageNO, nBlockSize, nPageCount, sQueryString)
	nCurrentBlockpage = fnCeiling(nPageNO/nBlockSize)					'현재 페이지의 블럭카운트
	nTempPage = CLng(nBlockSize*(nCurrentBlockpage-1)+1)			'현재 블럭의 첫번째 페이지카운트
	if sQueryString <> "" then
		sQueryString = "&" & sQueryString
	end if
%>
	<div class="lyPaging">
<%
		'<<(-10) : nCurrentBlockpage > 1
		if nTempPage > 1 then
			response.write "<a href='" & sListFileName & "?pageNO=" & (nTempPage-nBlockSize) & sQueryString & "'><img src='/admin/images/common/prev10.jpg' width='16' height='13' align='absmiddle'></a>&nbsp;"
		else
			response.write "<img src='/admin/images/common/prev10.jpg' width='16' height='13' align='absmiddle'>&nbsp;"
		end if

		' <(-1) : nCurrentBlockpage < blocksize
		if nPageNO > 1 then
			response.write "<a href='" & sListFileName & "?pageNO=" & (nPageNO-1) & sQueryString & "'><img src='/admin/images/common/prev.jpg' width='13' height='13' align='absmiddle'></a>&nbsp;&nbsp;"
		else
			response.write "<img src='/admin/images/common/prev.jpg' width='13' height='13' align='absmiddle'>&nbsp;&nbsp;"
		end if

		' [n1,n2,3,n4,...] : (blocksize*current_blockpage) ~ nBlockSize
		i = 1
		Do until i > nBlockSize or nTempPage > nPageCount
			if nTempPage = nPageNO then
				response.write "<a href='" & sListFileName & "?pageNO=" & nTempPage & sQueryString &  "' class='curr_no'>" & nTempPage & "</a>&nbsp;"
			else
				response.write "<a href='" & sListFileName & "?pageNO=" & nTempPage & sQueryString &  "'>" & nTempPage & "</a>&nbsp;"
			end if
			nTempPage = nTempPage + 1
			i = i + 1
		Loop

		' >(+1) : nCurrentBlockpage < blocksize 
		if nPageNO < nPageCount then
			response.write "&nbsp;<a href='" & sListFileName & "?pageNO=" & (nPageNO+1) & sQueryString & "'><img src='/admin/images/common/next.jpg' width='13' height='13' align='absmiddle'></a>&nbsp;"
		else
			response.write "&nbsp;<img src='/admin/images/common/next.jpg' width='13' height='13' align='absmiddle'>&nbsp;"
		end if

		' >>(+10) : nTempPage < last_blockpage 
		if nTempPage <= nPageCount then
			response.write "<a href='" & sListFileName & "?pageNO=" & nTempPage & sQueryString &  "'><img src='/admin/images/common/next10.jpg' width='16' height='13' align='absmiddle'></a> "
		else
			response.write "<img src='/admin/images/common/next10.jpg' width='16' height='13' align='absmiddle'>"
		end if
%>   	 
	</div>
<%
End Function

'페이징2(상세페이지 내에 리스트 페이징)
Function fnPaging2(sListFileName, nPageNO, nBlockSize, nPageCount, sQueryString)
	nCurrentBlockpage = fnCeiling(nPageNO/nBlockSize)					'현재 페이지의 블럭카운트
	nTempPage = CLng(nBlockSize*(nCurrentBlockpage-1)+1)			'현재 블럭의 첫번째 페이지카운트
	if sQueryString <> "" then
		sQueryString = "&" & sQueryString
	end if
%>
	<div class="lyPaging">
<%
		'<<(-10) : nCurrentBlockpage > 1
		if nTempPage > 1 then
			response.write "<a href='" & sListFileName & "?pageNO2=" & (nTempPage-nBlockSize) & sQueryString & "'><img src='/admin/images/common/prev10.jpg' width='16' height='13' align='absmiddle'></a>&nbsp;"
		else
			response.write "<img src='/admin/images/common/prev10.jpg' width='16' height='13' align='absmiddle'>&nbsp;"
		end if

		' <(-1) : nCurrentBlockpage < blocksize
		if nPageNO > 1 then
			response.write "<a href='" & sListFileName & "?pageNO2=" & (nPageNO-1) & sQueryString & "'><img src='/admin/images/common/prev.jpg' width='13' height='13' align='absmiddle'></a>&nbsp;&nbsp;"
		else
			response.write "<img src='/admin/images/common/prev.jpg' width='13' height='13' align='absmiddle'>&nbsp;&nbsp;"
		end if

		' [n1,n2,3,n4,...] : (blocksize*current_blockpage) ~ nBlockSize
		i = 1
		Do until i > nBlockSize or nTempPage > nPageCount
			if nTempPage = nPageNO then
				response.write "<a href='" & sListFileName & "?pageNO2=" & nTempPage & sQueryString &  "' class='curr_no'>" & nTempPage & "</a>&nbsp;"
			else
				response.write "<a href='" & sListFileName & "?pageNO2=" & nTempPage & sQueryString &  "'>" & nTempPage & "</a>&nbsp;"
			end if
			nTempPage = nTempPage + 1
			i = i + 1
		Loop

		' >(+1) : nCurrentBlockpage < blocksize 
		if nPageNO < nPageCount then
			response.write "&nbsp;<a href='" & sListFileName & "?pageNO2=" & (nPageNO+1) & sQueryString & "'><img src='/admin/images/common/next.jpg' width='13' height='13' align='absmiddle'></a>&nbsp;"
		else
			response.write "&nbsp;<img src='/admin/images/common/next.jpg' width='13' height='13' align='absmiddle'>&nbsp;"
		end if

		' >>(+10) : nTempPage < last_blockpage 
		if nTempPage <= nPageCount then
			response.write "<a href='" & sListFileName & "?pageNO2=" & nTempPage & sQueryString &  "'><img src='/admin/images/common/next10.jpg' width='16' height='13' align='absmiddle'></a> "
		else
			response.write "<img src='/admin/images/common/next10.jpg' width='16' height='13' align='absmiddle'>"
		end if
%>   	 
	</div>
<%
End Function

'파일명 랜덤 키 생성(중복체크용)
Function fnFileKey()
	Dim sPushKey
	Dim sValue, aValue, nValue
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	sPushKey = ""

	for i=1 to 6 '6자리만 생성
		sPushKey = sPushKey & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next
	fnFileKey = sPushKey
End Function

'PUSH 랜덤 키 생성(중복체크용)
Function fnPushKey()
	Dim sPushKey
	Dim sValue, aValue, nValue
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	sPushKey = ""

	for i=1 to 8 '8자리만 생성
		sPushKey = sPushKey & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next
	sPushKey = Replace(Date(), "-", "") & hour(time()) & minute(time()) & second(time()) & sPushKey
	fnPushKey = sPushKey
End Function

'쿠폰번호 생성
Function fnCouponNO()
	Dim coupon_number
	Dim sValue, aValue, nValue
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "A,B,C,D,E,F,G,H,I,J,K,L,N,M,O,P,Q,R,S,T,U,V,W,X,Y,Z,0,1,2,3,4,5,6,7,8,9"
	'sValue = "0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	coupon_number = ""

	for i=1 to 16 '8자리만 생성
		coupon_number = coupon_number & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next

	'12+4=16자리
	'coupon_number = second(time()) & coupon_number & mid(Replace(Date(), "-", ""),3) & hour(time())

	fnCouponNO = coupon_number
End Function
Function fnMenuCnt(ByRef nArgMenuCnt)
	nArgMenuCnt = nArgMenuCnt + 1
	fnMenuCnt = nArgMenuCnt
end function
'주문번호생성
Function fnOrderNO()
	Dim sOrderNO
	Dim sValue, aValue, nValue
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	sOrderNO = ""

	for i=1 to 8 '8자리만 생성
		sOrderNO = sOrderNO & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next
	sOrderNO = Replace(Date(), "-", "") & hour(time()) & minute(time()) & second(time()) & sOrderNO
	fnOrderNO = sOrderNO
End Function

'환급여부(0:미신청, 1:신청, 2:승인완료, 3:승인실패, 4:환급완료, 5:유료업체결제전, 6:무료가입회원)
Function fnRecommStatus(sArgType)
	Dim sType : sType = trim(sArgType)
	if sType = "0" then
'		fnRecommStatus = "미신청"
	elseif sType = "1" then
'		fnRecommStatus = "신청"
	elseif sType = "2" then
'		fnRecommStatus = "승인완료"
	elseif sType = "3" then
'		fnRecommStatus = "승인실패"
	elseif sType = "4" then
'		fnRecommStatus = "환급완료"
	elseif sType = "5" then
'		fnRecommStatus = "유료업체결제전"
	elseif sType = "6" then
'		fnRecommStatus = "무료가입회원"
	else
		fnRecommStatus = "-"
	end if
End Function

Function fnPushType(sArgType)
	Dim sType : sType = trim(sArgType)
	if sType = "p" then
		fnPushType = "PUSH"
	elseif sType = "m" then
'		fnPushType = "알림"
	else
		fnPushType = "-"
	end if
End Function

Function fnSendStatus(sArgType)
	Dim sType : sType = trim(sArgType)
	if sType = "0" then
'		fnSendStatus = "미발송"
	elseif sType = "1" then
'		fnSendStatus = "일부발송"
	elseif sType = "2" then
'		fnSendStatus = "발송완료"
	else
		fnSendStatus = "-"
	end if
End Function

Function fnMsgStatus(sArgType)
	Dim sType : sType = trim(sArgType)
	if sType = "0" then
'		fnMsgStatus = "미발송"
	elseif sType = "1" then
'		fnMsgStatus = "발송완료"
	elseif sType = "2" then
'		fnMsgStatus = "수신확인"
	else
		fnMsgStatus = "-"
	end if
End Function

Function fnRs2Arr(ByRef aData, rs)
	Dim nCnt, i
	nCnt = rs.Fields.count
	redim aData(nCnt, 2)
	for i=0 to nCnt-1
		aData(i, 0) = rs.Fields.item(i).Name
		aData(i, 1) = fnNullInit(rs.Fields.item(i).value)
	next
	fnRs2Arr = nCnt
End Function

Function fnRs2Arr2(ByRef aData, rs)
	Dim nCnt
	aData = rs.getRows()
	if isArray(aData) then
		nCnt = ubound(aData, 2)
	else
		nCnt = -1
	end if

	fnRs2Arr2 = nCnt
End Function

Function fnGetKey(sArgKey)
	fnGetKey = mid(fnNullInit(sArgKey), 2)
End Function

Function fnGetKeyType(sArgKey)
	fnGetKeyType = left(fnNullInit(sArgKey), 1)
End Function

Function fnRs2Json(ByRef aData, rs)
	Dim nCnt, i
	Set aData = jsObject()
	nCnt = rs.Fields.count

	for i=0 to nCnt-1
		aData(lcase(rs.Fields.item(i).Name)) = fnNullInit(rs.Fields.item(i).value)
	next

	fnRs2Json = nCnt
End Function

Function fnRs2JsonArr(ByRef aData, rs)
	Dim nCnt, i, nRsCnt : nRsCnt = -1
	Set aData = jsObject()

	if not(rs.eof or rs.bof) then
		nCnt = rs.Fields.count
		do until rs.eof or rs.bof
			nRsCnt = nRsCnt + 1
			for i=0 to nCnt-1
				if nRsCnt=0 then 
					Set aData(lcase(rs.Fields.item(i).Name)) = jsObject()
				end if
				aData(lcase(rs.Fields.item(i).Name))(nRsCnt) = fnNullInit(rs.Fields.item(i).value)
			next
			rs.movenext
		loop
	end if

	fnRs2JsonArr = nRsCnt
End Function

Function fnRs2JsonArrR(ByRef aData, rs)
	Dim nCnt, i, nRsCnt : nRsCnt = -1
	Set aData = jsObject()

	if not(rs.eof or rs.bof) then
		nCnt = rs.Fields.count
		do until rs.eof or rs.bof
			nRsCnt = nRsCnt + 1
			Set aData(nRsCnt) = jsObject()
			for i=0 to nCnt-1
				aData(nRsCnt)(lcase(rs.Fields.item(i).Name)) = fnNullInit(rs.Fields.item(i).value)
			next
			rs.movenext
		loop
	end if

	fnRs2JsonArrR = nRsCnt
End Function

Function fnUpload2Json(ByRef aData, upload)
	Dim nCnt, i, oItem
	Set aData = jsObject()
	nCnt = 0

	if isObject(upload) then
		For each oItem in upload.form
			aData(oItem.name) = fnKeywordFilter(oItem.value)
			nCnt = nCnt  + 1
		Next
	end if
	fnUpload2Json = nCnt
End Function

Function fnForm2Json(ByRef aData)
	Dim nCnt, i, oItem
	Set aData = jsObject()
	nCnt = 0

	For each oItem in request.form
		aData(oItem) = fnKeywordFilter(request.form(oItem))
		nCnt = nCnt  + 1
	Next
	fnForm2Json = nCnt
End Function

Function getJson(json, key)
	if isObject(json) then
		getJson = json(key)
	else
		getJson = ""
	end if
End function

'결제수단
Function fnPayType(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if sNewStr = "11" then
'		sNewStr = "신용카드"
	elseif sNewStr = "31" then
'		sNewStr = "휴대폰"
	elseif sNewStr = "21" then
'		sNewStr = "계좌이체"
	elseif sNewStr = "22" then
'		sNewStr = "무통장입금"
	elseif sNewStr = "32" then
'		sNewStr = "ARS"
	elseif sNewStr = "41" then
'		sNewStr = "포인트"
	elseif sNewStr = "81" then
'		sNewStr = "배치"
	elseif sNewStr = "00" then
'		sNewStr = "무료가입이벤트"
	else
		sNewStr = "-"
	end if

	fnPayType = sNewStr
	
End Function

'커뮤니티 카테고리
Function fnCommTag(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if sNewStr = "close" then
		sNewStr = "폐점신고"
	elseif sNewStr = "free" then
		sNewStr = "자유토론"
	elseif sNewStr = "suges" then
		sNewStr = "건의"
	else
		sNewStr = "-"
	end if

	fnCommTag = sNewStr
	
End Function

'상품 카테고리
Function fnProductTag(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if sNewStr = "service" then
'		sNewStr = "서비스"
	elseif sNewStr = "event" then
'		sNewStr = "이벤트"
	elseif sNewStr = "social" then
'		sNewStr = "소셜"
	else
		sNewStr = "-"
	end if

	fnProductTag = sNewStr
	
End Function

function fnDataSec(sArgStr, nArgLen)
	Dim nLen : nLen = len(sArgStr)
	Dim i
	if sArgStr <> "" then
		sArgStr = left(sArgStr, 4)
		for i=1 to nLen-nArgLen
			sArgStr = sArgStr & "*"
		next
	end if
	fnDataSec = sArgStr
End Function

function fnRDataSec(sArgStr, nArgLen)
	Dim nLen : nLen = len(sArgStr)
	Dim i
	Dim sTmpStr : sTmpStr = sArgStr
	if sTmpStr <> "" and nLen > nArgLen then
		sTmpStr = left(sTmpStr, nLen-nArgLen)
		for i=1 to nArgLen
			sTmpStr = sTmpStr & "*"
		next
	end if
	fnRDataSec = sTmpStr
End Function

Function fnImgView(sImgSrc, nMaxW, nMaxH)
	Dim sImgTag : sImgTag = ""
	Dim sImgPath : sImgPath = ""
	Dim FSO, sFilePath
	sImgSrc = "/upload/" & sImgSrc
	sImgPath = replace(sImgSrc, "/", "\")
	sFilePath = server.MapPath("/") & sImgPath

	Set FSO = CreateObject("Scripting.FileSystemObject")

	If FSO.FileExists(sFilePath) then

		Dim img_size, img_width, img_height 		

		'이미지 사이즈
		set img_size = LoadPicture(sFilePath)
		img_width = CLng(CDbl(img_Size.Width)*24/635) 
		img_height = CLng(CDbl(img_Size.Height)*24/635)
		set img_size = nothing
		
		'최대 크기
		if img_width > nMaxW then
			img_height = CLng(img_height * (nMaxW/img_width))
			'img_height = img_height + 30
			img_width = nMaxW
		end if
		
		if img_height > nMaxH then
			img_width= CLng(img_width * (nMaxH/img_height))
			'img_width = img_width + 30
			img_height = nMaxH
		end if 
			
		sImgTag = "<div class='lyImg'><img src='" & sImgSrc & "' height=" & img_height & " width=" & img_width & " ></div>"
	else
		sImgTag = "<div class='lyImgText' style='padding:" & (nMaxH/2-10) & "px 0px;width:"&nMaxW&"px'>No Img</div>"
	End If	
	Set FSO = nothing		

	fnImgView = sImgTag
end function

function fnFileSave(upload, obj)
	Dim filename, filepath, fileext, filenameonly, i
	
	filename = obj.FileName
	filepath = upload.DefaultPath & "\" & filename

	If upload.FileExists(filepath) Then
		If InStrRev(filename, ".") <> 0 Then 
			filenameonly = Left(filename, InStrRev(filename, ".") - 1) 
			fileext = Mid(filename, InStrRev(filename, ".")) 
		Else 
			filenameonly = filename 
			fileext = "" 
		End If

		i = 2
		Do While (1)
			filepath = upload.DefaultPath & "\" & filenameonly & "[" & i & "]" & fileext
			If Not upload.FileExists(filepath) Then Exit Do 
			i = i + 1
		Loop 
	End If

	obj.SaveAs filepath
	fnFileSave = obj.LastSavedFileName
end function

Function fnGetTel(sArgTel)
	Dim aTel(2), aTmpTel, i
	Dim nTel
	aTmpTel = split(sArgTel, "-")
	nTel = ubound(aTmpTel)

	if isNumeric(sArgTel) and len(sArgTel) = 9 then
		aTel(0) = left(sArgTel, 2)
		aTel(1) = mid(sArgTel, 4, 3)
		aTel(2) = right(sArgTel, 4)
	elseif isNumeric(sArgTel) and len(sArgTel) = 10 then
		aTel(0) = left(sArgTel, 3)
		aTel(1) = mid(sArgTel, 4, 3)
		aTel(2) = right(sArgTel, 4)
	elseif isNumeric(sArgTel) and len(sArgTel) = 11 then
		aTel(0) = left(sArgTel, 3)
		aTel(1) = mid(sArgTel, 4, 4)
		aTel(2) = right(sArgTel, 4)
	elseif isNumeric(sArgTel) and len(sArgTel) = 12 then
		aTel(0) = left(sArgTel, 4)
		aTel(1) = mid(sArgTel, 5, 4)
		aTel(2) = right(sArgTel, 4)
	else
		for i=0 to 2
			aTel(i) = ""
			if nTel >= i then
				aTel(i) = aTmpTel(i)
			end if
		next
	end if
	fnGetTel = aTel
End Function

Function fnGetRegNO(sArgRegNO)
	Dim aRegNO(2), aTmpRegNO, i
	Dim nRegNO

	if isNumeric(sArgRegNO) and len(sArgRegNO) = 10 then
		aRegNO(0) = left(sArgRegNO, 3)
		aRegNO(1) = mid(sArgRegNO, 4, 2)
		aRegNO(2) = right(sArgRegNO, 5)
	else
		aTmpRegNO = split(sArgRegNO, "-")
		nRegNO = ubound(aTmpRegNO)	
		for i=0 to 2
			aRegNO(i) = ""
			if nRegNO >= i then
				aRegNO(i) = aTmpRegNO(i)
			end if
		next
	end if
	fnGetRegNO = aRegNO
End Function

Function fnGetEmail(sArgEmail)
	Dim aEmail(1), aTmpEmail, i
	Dim nEmail
	aTmpEmail = split(sArgEmail, "@")
	nEmail = ubound(aTmpEmail)
	
	for i=0 to 1
		aEmail(i) = ""
		if nEmail >= i then
			aEmail(i) = aTmpEmail(i)
		end if
	next
	fnGetEmail = aEmail
End Function

Function fnGetPeriod(sArgPeriod)
	Dim aPeriod(1), aTmpPeriod, i
	Dim nPeriod
	aTmpPeriod = split(sArgPeriod, "~")
	nPeriod = ubound(aTmpPeriod)
	
	for i=0 to 1
		aPeriod(i) = ""
		if nPeriod >= i then
			aPeriod(i) = aTmpPeriod(i)
		end if
	next
	fnGetPeriod = aPeriod
End Function

function fnErrSave()
	
	Dim i, site : site = "APT_APP[" & sAppID & SS_APP_ID & "]"
	Dim sDBChk : sDBChk = "OFF"
	
	if isObject(dbConn) and not(dbConn is Nothing) then
		sDBChk = "ON"
	else
		fnDBConn()
	end if

	for i=0 to dbConn.Errors.Count-1
		ParamInit()
		'response.write "DBddd 에러 " & i & " : "& dbConn.Errors.item(i).description & "<= 1<br>"
		'cmd.CommandText="{call dbo.insert_errorlog(?,?,?,?,? ,?,?,?,?,? ,?,?,?,?,?,?)}"
		'cmd.CommandType=1
		cmd.CommandType = adCmdStoredProc
		cmd.CommandText = "dbo.insert_errorlog"
		cmd.Parameters.Append cmd.CreateParameter("@aspcode",adVarChar,adParamInput,50,SS_USER_ID)
		cmd.Parameters.Append cmd.CreateParameter("@errornumber",adVarChar,adParamInput,50,i)
		cmd.Parameters.Append cmd.CreateParameter("@source",adVarChar,adParamInput,200,"")
		cmd.Parameters.Append cmd.CreateParameter("@category",adVarChar,adParamInput,200,SS_GROUP)
		cmd.Parameters.Append cmd.CreateParameter("@errorfile",adVarChar,adParamInput,200,"")

		cmd.Parameters.Append cmd.CreateParameter("@line",adInteger,adParamInput,4,i)
		cmd.Parameters.Append cmd.CreateParameter("@description",adVarChar,adParamInput,500,dbConn.Errors.item(i).description)
		cmd.Parameters.Append cmd.CreateParameter("@aspdescription",adVarChar,adParamInput,500,"")
		cmd.Parameters.Append cmd.CreateParameter("@form",adVarChar,adParamInput,8000,fnForm2Str(4000))
		cmd.Parameters.Append cmd.CreateParameter("@querystring",adVarChar,adParamInput,8000,left(request.servervariables("script_name") & "|" & request.servervariables("query_string"),4000))

		cmd.Parameters.Append cmd.CreateParameter("@cookie",adVarChar,adParamInput,8000,left(request.cookies,4000))
		cmd.Parameters.Append cmd.CreateParameter("@referer",adVarChar,adParamInput,8000,left(request.servervariables("http_referer"),4000))
		cmd.Parameters.Append cmd.CreateParameter("@ip",adVarChar,adParamInput,50,request.servervariables("remote_addr"))
		cmd.Parameters.Append cmd.CreateParameter("@site",adVarChar,adParamInput,50,site)
		cmd.Parameters.Append cmd.CreateParameter("@agent",adVarChar,adParamInput,8000,left(request.servervariables("HTTP_USER_AGENT"),4000))
		cmd.Parameters.Append cmd.CreateParameter("@codepage",adVarChar,adParamInput,10,session.codepage)
		cmd.Execute , , adExecuteNoRecords
	next

	if Err.Number <> 0 or Server.GetLastError.Number then   '또는 err.Number<>0
		ParamInit()
		
		'cmd.CommandText="{call dbo.insert_errorlog(?,?,?,?,? ,?,?,?,?,? ,?,?,?,?,?,?)}"
		'cmd.CommandType=1
		cmd.CommandType = adCmdStoredProc
		cmd.CommandText = "dbo.insert_errorlog"

		cmd.Parameters.Append cmd.CreateParameter("@aspcode",adVarChar,adParamInput,50,SS_USER_ID)
		cmd.Parameters.Append cmd.CreateParameter("@errornumber",adVarChar,adParamInput,50,err.Number)
		cmd.Parameters.Append cmd.CreateParameter("@source",adVarChar,adParamInput,200,err.Source)
		cmd.Parameters.Append cmd.CreateParameter("@category",adVarChar,adParamInput,200,SS_GROUP)
		cmd.Parameters.Append cmd.CreateParameter("@errorfile",adVarChar,adParamInput,200,err.HelpFile)

		cmd.Parameters.Append cmd.CreateParameter("@line",adInteger,adParamInput,4,0)
		cmd.Parameters.Append cmd.CreateParameter("@description",adVarChar,adParamInput,500,err.Description)
		cmd.Parameters.Append cmd.CreateParameter("@aspdescription",adVarChar,adParamInput,500,err.HelpContext)

		cmd.Parameters.Append cmd.CreateParameter("@form",adVarChar,adParamInput,8000,fnForm2Str(4000))
		cmd.Parameters.Append cmd.CreateParameter("@querystring",adVarChar,adParamInput,8000,left(request.servervariables("script_name") & "|" & request.servervariables("query_string"),4000))

		cmd.Parameters.Append cmd.CreateParameter("@cookie",adVarChar,adParamInput,8000,left(request.cookies,4000))
		cmd.Parameters.Append cmd.CreateParameter("@referer",adVarChar,adParamInput,8000,left(request.servervariables("http_referer"),4000))
		cmd.Parameters.Append cmd.CreateParameter("@ip",adVarChar,adParamInput,50,request.servervariables("remote_addr"))
		cmd.Parameters.Append cmd.CreateParameter("@site",adVarChar,adParamInput,50,site)
		cmd.Parameters.Append cmd.CreateParameter("@agent",adVarChar,adParamInput,8000,left(request.servervariables("HTTP_USER_AGENT"),4000))
		cmd.Parameters.Append cmd.CreateParameter("@codepage",adVarChar,adParamInput,10,session.codepage)
		cmd.Execute , , adExecuteNoRecords

		'response.write "<br>err.Description : " & err.Description		'에러에 대한 자세한 설명
		'response.write "<br>err.Number : " & err.Number				'에러번호
		'response.write "<br>err.Source : " & err.Source					'어떤 에러인지 간략한 설명
		'response.write "<br>err.HelpFile : " & err.HelpFile					'도움말파일
		'response.write "<br>err.HelpContext : " & err.HelpContext		'출력내용

	end if

	if sDBChk = "OFF" then
		fnDBClose()
	end if

	fnErrSave = "Y"
end function

function fnLogSave()
	
	Dim i, site : site = "Toline"
	Dim sDBChk : sDBChk = "OFF"
	
	if isObject(dbConn) and not(dbConn is Nothing) then
		sDBChk = "ON"
	else
		fnDBConn()
	end if

	ParamInit()
	cmd.CommandType = adCmdStoredProc
	cmd.CommandText = "dbo.insert_errorlog"

	cmd.Parameters.Append cmd.CreateParameter("@aspcode",adVarChar,adParamInput,50,"APP")
	cmd.Parameters.Append cmd.CreateParameter("@errornumber",adVarChar,adParamInput,50,"0")
	cmd.Parameters.Append cmd.CreateParameter("@source",adVarChar,adParamInput,200,"LOG")
	cmd.Parameters.Append cmd.CreateParameter("@category",adVarChar,adParamInput,200,"APP")
	cmd.Parameters.Append cmd.CreateParameter("@errorfile",adVarChar,adParamInput,200,"")

	cmd.Parameters.Append cmd.CreateParameter("@line",adInteger,adParamInput,4,0)
	cmd.Parameters.Append cmd.CreateParameter("@description",adVarChar,adParamInput,500,"")
	cmd.Parameters.Append cmd.CreateParameter("@aspdescription",adVarChar,adParamInput,500,"")

	cmd.Parameters.Append cmd.CreateParameter("@form",adVarChar,adParamInput,8000,fnForm2Str(4000))
	cmd.Parameters.Append cmd.CreateParameter("@querystring",adVarChar,adParamInput,8000,left(request.servervariables("script_name") & "|" & request.servervariables("query_string"),4000))

	cmd.Parameters.Append cmd.CreateParameter("@cookie",adVarChar,adParamInput,8000,left(request.cookies,4000))
	cmd.Parameters.Append cmd.CreateParameter("@referer",adVarChar,adParamInput,8000,left(request.servervariables("http_referer"),4000))
	cmd.Parameters.Append cmd.CreateParameter("@ip",adVarChar,adParamInput,50,request.servervariables("remote_addr"))
	cmd.Parameters.Append cmd.CreateParameter("@site",adVarChar,adParamInput,50,site)
	cmd.Parameters.Append cmd.CreateParameter("@agent",adVarChar,adParamInput,8000,left(request.servervariables("HTTP_USER_AGENT"),4000))
	cmd.Parameters.Append cmd.CreateParameter("@codepage",adVarChar,adParamInput,10,session.codepage)
	cmd.Execute , , adExecuteNoRecords


	if sDBChk = "OFF" then
		fnDBClose()
	end if

	fnLogSave = "Y"
end function

Function fnForm2Str(nMaxLen)
	Dim sResult, oItem
	sResult = ""

	if isObject(uploadform) then
		For each oItem in uploadform.form
			sResult = sResult & "" & oItem.name & "=" & fnKeywordFilter(oItem.value) & ";"
		Next
	else
		For each oItem in request.form
			sResult = sResult & "" & oItem & "=" & fnKeywordFilter(request.form(oItem)) & ";"
		Next
	end if
	fnForm2Str = left(sResult, nMaxLen)
End Function

Function fnGet2Str(nMaxLen)
	Dim sResult, oItem
	sResult = request.servervariables("HTTP_USER_AGENT") & request.servervariables("http_referer") & request.servervariables("query_string")
	fnGet2Str = left(sResult, nMaxLen)
End Function

'환경설정
Function fnAppConfig(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if sNewStr = "kakao_msg" then
		sNewStr = "카카오톡 초대 메세지"
	else
		sNewStr = "-"
	end if

	fnAppConfig = sNewStr
	
End Function


'---------------------------------------------------------------------
' URLEncodeUTF8 (아스키--> UTF8  )
'---------------------------------------------------------------------
Public Function URLEncodeUTF8(byVal szSource)
 
Dim szChar, WideChar, nLength, i, result
nLength = Len(szSource)
 
'szSource = Replace(szSource," ","+")
 
For i = 1 To nLength
 szChar = Mid(szSource, i, 1)
 
 If Asc(szChar) < 0 Then             
  WideChar = CLng(AscB(MidB(szChar, 2, 1))) * 256 + AscB(MidB(szChar, 1, 1))
 
  If (WideChar And &HFF80) = 0 Then
   result = result & "%" & Hex(WideChar)
  ElseIf (WideChar And &HF000) = 0 Then
   result = result & _
   "%" & Hex(CInt((WideChar And &HFFC0) / 64) Or &HC0) & _
   "%" & Hex(WideChar And &H3F Or &H80)
  Else
   result = result & _
   "%" & Hex(CInt((WideChar And &HF000) / 4096) Or &HE0) & _
   "%" & Hex(CInt((WideChar And &HFFC0) / 64) And &H3F Or &H80) & _
   "%" & Hex(WideChar And &H3F Or &H80)
  End If
 Else
  result = result + szChar
 End If
Next
URLEncodeUTF8 = result
End Function
 

'---------------------------------------------------------------------
' URLDecodeUTF8 (UTF8 --> 아스키 )
'---------------------------------------------------------------------
 

Public Function URLDecodeUTF8(byVal pURL)
Dim i, s1, s2, s3, u1, u2, result
pURL = Replace(pURL,"+"," ")
 
For i = 1 to Len(pURL)
 
 if Mid(pURL, i, 1) = "%" then
 
  s1 = CLng("&H" & Mid(pURL, i + 1, 2))
 
  '2바이트일 경우
  if ((s1 AND &HC0) = &HC0) AND ((s1 AND &HE0) <> &HE0) then
   s2 = CLng("&H" & Mid(pURL, i + 4, 2))
 
   u1 = (s1 AND &H1C) / &H04
   u2 = ((s1 AND &H03) * &H04 + ((s2 AND &H30) / &H10)) * &H10
   u2 = u2 + (s2 AND &H0F)
   result = result & ChrW((u1 * &H100) + u2)
   i = i + 5
 
  '3바이트일 경우
  elseif (s1 AND &HE0 = &HE0) then
   s2 = CLng("&H" & Mid(pURL, i + 4, 2))
   s3 = CLng("&H" & Mid(pURL, i + 7, 2))
 
   u1 = ((s1 AND &H0F) * &H10)
   u1 = u1 + ((s2 AND &H3C) / &H04)
   u2 = ((s2 AND &H03) * &H04 +  (s3 AND &H30) / &H10) * &H10
   u2 = u2 + (s3 AND &H0F)
   result = result & ChrW((u1 * &H100) + u2)
   i = i + 8
  end if
 else
  result = result & Mid(pURL, i, 1)
 end if
Next
URLDecodeUTF8 = result
End Function


'MUTI PUSH 발송(메세지, 전송 할 REG_ID(','구분), 전송 할 폰번호(','구분), 전송 실패 사유(','구분), 성공한 REG_ID(','구분), 성공한 폰번호(','구분))
Function fnOnePushSend(sMsg, sRegID, sPhone, aAppID)
	Dim APIKEY : APIKEY = "AIzaSyC-eisgmovdUQQgTwQ7f9hOFdrCvKqu0Lo"
	Dim sValue, aValue, nValue, i
	Dim sResultString : sResultString = ""
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)
	
	sRegID = fnNullInit(sRegID)
	sPhone = fnNullInit(sPhone)
	sMsg = fnNullInit(sMsg)
	aAppID = fnNullInit(aAppID)
	if sRegID <> "" and sPhone <> "" and sMsg <> "" and aAppID <> "" then
		Dim SEND_URL
		Dim seneHTTP
		SEND_URL = "https://android.googleapis.com/gcm/send"
		Set seneHTTP = Server.CreateObject("MSXML2.ServerXMLHTTP")
		seneHTTP.Open "POST",""& SEND_URL &"",false
		seneHTTP.SetRequestHeader "POST", ""& SEND_URL &" HTTP/1.0"
		seneHTTP.SetRequestHeader "Authorization", "key=" & APIKEY & ""
		'seneHTTP.SetRequestHeader "Content-Type", "application/json"									'멀티 전송시
		seneHTTP.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded"		'단일 전송시

		If seneHTTP.readyState = 1 Then		
			Dim collapse_key : collapse_key = ""

			for i=1 to 4 '4자리만 생성
				collapse_key = collapse_key & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
			next
			
			'Dim json1 : Set json1 = jsObject()
			'set json1("data") = jsObject()
			'json1("data")("msg") = sMsg			
			'json1("delay_while_idle") = true	'디바이스가 idle상태일때는 전달하지 않음(기본 false)
			'json1("collapse_key") = collapse_key	 '메세지 그룹설정(오프라인에서 온라인 상태가 되면 많은 메세지가 전다되는 것을 막고 그룹내에서 가장 최신의 메시지만 전달)
			'json1("registration_ids") = aRegID
			'json1("dry_run") = true		'전송TEST(기본 false)
			'json1.Flush
			'seneHTTP.send toJSON(json1)	 '멀티 전송시

			seneHTTP.send  "registration_id=" & sRegID & "&collapse_key=" & collapse_key & "&delay_while_idle=1&data.msg=" & sMsg	'단일 전송시

			On Error Resume Next
			seneHTTP.waitForResponse 5
			If (seneHTTP.readyState = 4) And (seneHTTP.Status = 200) Then
				sResultString = Trim(seneHTTP.ResponseText)
			else
				sResultString="HTTP_ERR4" & "(" & seneHTTP.readyState & "/"  & seneHTTP.Status & ")"
			End If
			
			Dim sDBChk : sDBChk = "OFF"

			if isObject(dbConn) and not(dbConn is Nothing) then
				sDBChk = "ON"
			else
				fnDBConn()
			end if
				ParamInit()
				'푸쉬발송 로그저장
				sql = "insert into member_msg_log (app_id, msg_idx, div_idx, try_idx, send_cnt, log_data, phone_list, regid_list, send_date)values(?, ?, ?, ?, ?, ?, ?, ?, getdate()) "
				cmd.Parameters.Append cmd.CreateParameter("@app_id", adVarChar, adParamInput, 20, aAppID)
				cmd.Parameters.Append cmd.CreateParameter("@msg_idx", adInteger, adParamInput, 4, 0)
				cmd.Parameters.Append cmd.CreateParameter("@div_idx", adInteger, adParamInput, 4, 1)
				cmd.Parameters.Append cmd.CreateParameter("@try_idx", adInteger, adParamInput, 4, 1)
				cmd.Parameters.Append cmd.CreateParameter("@send_cnt", adInteger, adParamInput, 4, 1)
				cmd.Parameters.Append cmd.CreateParameter("@log_data", adVarChar, adParamInput, 8000, sResultString)
				cmd.Parameters.Append cmd.CreateParameter("@phone_list", adVarChar, adParamInput, 8000, sPhone)
				cmd.Parameters.Append cmd.CreateParameter("@regid_list", adVarChar, adParamInput, 8000, sRegID)
				cmd.CommandText = sql
				cmd.Execute , , adExecuteNoRecords
				ParamInit()
				if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
					fnErrSave()
					'response.end
				end if
			if sDBChk = "OFF" then
				fnDBClose()
			end if
		else
			sResultString="HTTP_ERR1"
		End If

		Set seneHTTP = Nothing
		fnOnePushSend = sResultString
	else		
		fnOnePushSend = "PUSH발송 할 휴대폰번호가 없습니다"
	end if
end Function


%>