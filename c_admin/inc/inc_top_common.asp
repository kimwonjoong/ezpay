<%@ codepage="65001" language="VBScript" %>
<% Option Explicit %>

<!--METADATA TYPE= "typelib"  NAME= "ADODB Type Library" FILE="C:\Program Files\Common Files\SYSTEM\ADO\msado15.dll"  -->
<!--#include virtual="/c_admin/inc/inc_db_lib.asp"-->
<!--#include virtual="/c_admin/inc/JSON_2.0.4.asp" -->
<script runat="server" Language="Javascript" src="/g_admin/inc/JSON_JS.asp"></script>
<%

Dim sMenuCode, PAGE_CODE_, PAGE_TITLE_, sMnStyle
Dim SS_USER_ID, SS_GROUP, SS_USER_NAME, sModiChk, SS_STORE_CODE, SS_APP_ID
Dim SS_COMM_ID : SS_COMM_ID = "CBCOIN"
Dim sSSAdminChk

Dim sql, rs, i, j, sTableName, sqlTable
Dim aData, nData : nData = -1
Dim UPLOADFORM
Dim aFeeType, nFeeType : nFeeType = 0
Dim COUPON_VIEW_CNT : COUPON_VIEW_CNT = 100	'단일앱 등록시 노출가능한 상품 최대 개수

'페이징
Dim sFDList : sFDList = ""	'SELECT 필드
Dim sListFileName, sMsg, sMode, sModeStr, sFilter, sTitleName
Dim nPageNO, nPageSize, nPageCount, nRecordCount, nPageNO2
Dim nCurrentBlockpage, nBlockSize, nTempPage, sQueryString, sListQueryString

'검색조건
Dim sTableChk
Dim sStartDate, sEndDate, nStartDate, nEndDate

'스타일설정
Dim sVMode, bLayout, sStyleCss, sHtml, sBtmCss
Dim sCaption1, sCaption2
Dim sBTableStyle, sNBTableStyle, sTitleStyle, sCaptionStyle, sColStyle, aDataStyle(4), aSumTxtStyle(3), aSumStyle(3), sListMSG

Dim HOST_, DOMAIN_, TITLE_, MENU_IDX_, HOME_URL_
Dim SESSION_ID_

Dim MAIN_URL, LOGIN_URL, INDEX_URL
Dim sEMsg : sEMsg = ""
Dim SS_EXPIRE_CHK : SS_EXPIRE_CHK =  "ON"
Dim SS_EXPIRE_DATE : SS_EXPIRE_DATE =  ""

Dim nMaxSendCnt_	 :	nMaxSendCnt_ = 1000									'1회 최대 전송 개수
Dim nTryCnt_	:	nTryCnt_ = 0													'전송 실패 시 재시도 횟수
Dim nMaxTryCnt_	:	nMaxTryCnt_ = 2										'전송 실패 시 최대 재시도 횟수
Dim sAppID : sAppID = ""
Dim oItem, sItemKey, sItemValue
Dim CURR_DATE : CURR_DATE = date()
Dim CSS_ : CSS_ = ""

HOST_ = "yy7910ae.cafe24.com"
DOMAIN_ = "yy7910ae.cafe24.com"
HOME_URL_ = "http://" & HOST_ & ""		'	""
TITLE_ = "EZ페이 APP"
MAIN_URL = HOME_URL_ & "/g_admin/main.asp"
INDEX_URL = HOME_URL_ & "/g_admin/index.asp"
LOGIN_URL = HOME_URL_ & "/g_admin/login.asp"

MENU_IDX_ = 0
PAGE_CODE_ = ""
PAGE_TITLE_ = ""
nPageNO = 1
nBlockSize = 10
nPageSize = 20
nCurrentBlockpage = 0
nPageCount = 1
nRecordCount = 0

'세션데이터
SESSION_ID_ = trim(session.sessionid)
SS_APP_ID = fnNullInit(Trim(Session("SS_APPID")))
SS_USER_ID = fnNullInit(Trim(Session("SS_USERID")))
SS_GROUP = fnNullInit(Trim(Session("SS_GROUP")))
SS_USER_NAME = fnNullInit(Trim(Session("SS_USERNAME")))
SS_STORE_CODE = fnNullInit(Trim(Session("SS_STORECODE")))

sVMode = ""
sListFileName = ""

sMnStyle = "font-weight:bold;"
sStyleCss = "<link href='../css/default.css' rel='stylesheet' type='text/css'>"
sHtml = "<html>"
sCaption1 = "<img src='../images/common/mb_icon01.gif' width='20' height='20'>"
sCaption2 = "<img src='../images/common/mb_icon02.gif' width='8' height='5'>"

sBTableStyle = " border='1' bordercolor='#555555' style='border-collapse:collapse' cellspacing='0' cellpadding='0' "	
sNBTableStyle = " border='0' bordercolor='#555555' style='border-collapse:collapse' cellspacing='0' cellpadding='0' "	
sTitleStyle = " style='font-size:12pt;font-weight:bold;color:#555555;padding:5px 3px 5px 3px;text-align:center;height:50px;height:27.5pt;border-right:0px;border-top:0px;border-left:0px;border-bottom:0px' "
sCaptionStyle = " style='font-size:10pt;font-weight:bold;color:#555555;padding:5px 3px 5px 3px;text-align:left;height:50px;height:17.5pt;border-right:0px;border-top:0px;border-left:0px;border-bottom:0px' "

sColStyle = " style='background:#e1ecfc;font-size:9pt;font-weight:bold;height:24px;height:18.0pt;text-align:center' "

aDataStyle(0) = " style=""text-align:center;background:#ffffff;font-size:9pt;height:20px;height:15.0pt;mso-number-format:'\@'"" "
aDataStyle(1) = " style=""text-align:left;background:#ffffff;font-size:9pt;height:20px;height:15.0pt;padding-left:4px;height:20px;mso-number-format:'\@'"" "
aDataStyle(2) = " style='text-align:right;background:#ffffff;font-size:9pt;height:20px;height:15.0pt;padding-right:4px;' "
aDataStyle(3) = " style='background:#d3e5ff;font-size:9pt;height:20px;height:15.0pt;text-align:center' "

aSumTxtStyle(0) = " style='background:#aeccf4;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:center' "
aSumTxtStyle(1) = " style='background:#d3e5ff;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:center' "
aSumTxtStyle(2) = " style='background:#d6e284;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:center' "
aSumStyle(0) = " style='background:#aeccf4;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:right;padding-right:4px;' "
aSumStyle(1) = " style='background:#d3e5ff;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:right;padding-right:4px;' "
aSumStyle(2) = " style='background:#d6e284;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:right;padding-right:4px;' "

sListMSG = " style='background:#ffffff;font-size:9pt;height:200px;text-align:center;' "

if sVMode = "print" then
	Response.write "<script defer> "
	Response.write "	function fnPrint(bArgLayout) "
	Response.write "	{ parent.document.getElementById('ifrSaveAs').contentWindow.focus();"
	Response.write "	/*document.getElementById('ly_prt_bottom').style.display = 'none';*/"
	Response.write "	factory.printing.header = ''; /* Header에 들어갈 문장 */ "
	Response.write "	factory.printing.footer = ''; /* Footer에 들어갈 문장 */ "
	Response.write "	factory.printing.portrait = bArgLayout; /* true 면 세로인쇄, false 면 가로 인쇄 */ "
	Response.write "	factory.printing.leftMargin = 10.0; /* 왼쪽 여백 사이즈 */ "
	Response.write "	factory.printing.topMargin = 10.0; /* 위 여백 사이즈 */ "
	Response.write "	factory.printing.rightMargin = 10.0; /* 오른쪽 여백 사이즈 */ "
	Response.write "	factory.printing.bottomMargin = 10.0; /* 아래 여백 사이즈 */ "
	Response.write "	/*factory.printing.paperSize = 'A4';*/ /* 용지 사이즈 */ "
	Response.write "	/*factory.printing.SetMarginMeasure(2);*/ /* 테두리 여백 사이즈 단위를 인치로 설정합니다. */ "
	Response.write "	/*factory.printing.SetPageRange(false, 1, 3);*/ /* True로 설정하고 1, 3이면 1페이지에서 3페이지까지 출력 */ "
	Response.write "	factory.printing.Preview(); /* 미리보기 */ "
	Response.write "	/*document.getElementById('ly_prt_bottom').style.display = 'block';*/"
	Response.write "	/* factory.printing.Print(true, window); /* 출력하기 */ "
	Response.write "	} "
	Response.write "</script>"
elseif sVMode = "xls" then	
	response.buffer = False
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.CacheControl="public"
	Response.AddHeader "Content-disposition","attachment;filename=" & sListFileName & "_" & replace(date(),"-","") & ".xls"
elseif sVMode = "doc" then
	response.buffer = false
	sVMode = "xls"

	sHtml = ""
	sStyleCss = "<style type='text/css'>"
	sStyleCss = sStyleCss & "html, body {font-size: 12px;line-height: 1.5em;font-family: Dotum, 돋움, sans-serif;height:100%;width:100%;}"
	sStyleCss = sStyleCss & "* { margin:0;padding:0; }"
	sStyleCss = sStyleCss & "p, div, th, td,input,select {color: #333333;font-size: 12px;font-family: Dotum, 돋움, sans-serif;}"
	sStyleCss = sStyleCss & "h1{color: #333333;font-size: 14px;font-weight:bold;font-family: Dotum, 돋움, sans-serif;}"	
	sStyleCss = sStyleCss & "table.hnbd2 {}"
	sStyleCss = sStyleCss & "table.hnbd2 th {height:30px; backgroundColor:#eeeeee;}"
	sStyleCss = sStyleCss & "table.hnbd2 td {color:#005f68; font-size:11px; font-weight:bold; text-align:center; line-height:1.2;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.nmgl {margin:0px;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.top {border-top:1px solid #c0dbde;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.lln {border-left:1px solid #c0dbde;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.btm {border-bottom:1px solid #c0dbde; border-right:1px solid #c0dbde;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.btm1 {border-bottom:0;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.bgln {backgroundColor:#dddddd}"
	sStyleCss = sStyleCss & "table.hnbd2 td.bgcl { background:#eaf4f5;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.n {text-align:left;padding-left:10px;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.c {text-align:center;font-size:12px; color:#363636; font-weight:normal;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.r12 {text-align:right;padding-right:5px; color:#363636; font-size:12px; font-weight:normal;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.ro12 {text-align:right;padding-right:5px; color:#d34600; font-size:12px; font-weight:normal;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.l12 {text-align:left;padding-left:20px; color:#363636; font-size:12px; font-weight:normal;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.12b {text-align:left;padding-left:10px; font-size:12px; font-weight:bold;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.c12b {text-align:center;font-size:12px; font-weight:bold;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.r12b {text-align:right;padding-right:5px; font-size:12px; font-weight:bold;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.l12b {text-align:left;padding-left:10px; font-size:12px; font-weight:bold;}"
	sStyleCss = sStyleCss & "</style>"
	Response.ContentType = "application/msword"
	Response.CacheControl="public"
	Response.AddHeader "Content-disposition","attachment;filename=clm_" & sListFileName & "_" & replace(date(),"-","") & ".doc"
end if

sModiChk = "N"
sSSAdminChk = "N"

if SS_GROUP <> "A" and SS_GROUP <> "C" and SS_GROUP <> "S" and SS_GROUP <> "P" and SS_GROUP <> "G" then
	SS_GROUP = ""
else	
	If SS_GROUP = "A" Then
		sSSAdminChk = "Y"
		sModiChk = "Y"
	End If
end if

'사용권한(로그인) 체크
SUB	sbMemberAuthChk(sArgPageLevel)
	Dim sUserID, sGroup
	Dim sPageURL
	
	sPageURL = HOME_URL_ & "/g_admin/member_logout.asp"

	sUserID = Trim(Session("SS_USERID"))
	sGroup = Trim(Session("SS_GROUP"))

	if isNull(sGroup) Or (sGroup <> "A" and sGroup <> "S" and SS_GROUP <> "C" and SS_GROUP <> "P") then
		sGroup = ""
	end if

	if sArgPageLevel = "" then

	elseif (sArgPageLevel <> "A" and sArgPageLevel <> "S" and SS_GROUP <> "C" and SS_GROUP <> "P") then
		sArgPageLevel = "A"
	end if
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "top.location.href='" & sPageURL & "';"
		response.write "</script>"
		Response.End
	end if	
	
	'전체가능 이거나 관리자인경우
	If sArgPageLevel = "" Or sGroup = "A" Then

	'A
	ElseIf sArgPageLevel = "A" And sGroup <> "A" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	'A + C
	ElseIf sArgPageLevel = "P" And sGroup <> "P" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	'A + C
	ElseIf sArgPageLevel = "S" And sGroup <> "C" And sGroup <> "S" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	ElseIf sGroup = "" then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	end if

END SUB

'팝업페이지 사용권한(로그인) 체크
SUB	sbPopMemberAuthChk(sArgPageLevel)
	Dim sUserID, sGroup
	Dim sPageURL
		
	sPageURL = HOME_URL_ & "/g_admin/member_logout.asp"

	sUserID = Trim(Session("SS_USERID"))
	sGroup = Trim(Session("SS_GROUP"))

	if isNull(sGroup) Or (sGroup <> "A" and sGroup <> "S" And sGroup <> "P") then
		sGroup = ""
	end if
	
	if sArgPageLevel = "" then

	elseif (sArgPageLevel <> "A" and sArgPageLevel <> "S" And sGroup <> "P") then
		sArgPageLevel = "A"
	end if
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "opener.location.href='" & sPageURL & "';"
		response.write "self.close();"
		response.write "</script>"
		Response.End
	end if
	
	'전체가능 이거나 관리자인경우
	If sArgPageLevel = "" Or sGroup = "A" Then

	'A
	ElseIf sArgPageLevel = "A" And sGroup <> "A" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "opener.location.href='/g_admin/main.asp';"
		response.write "self.close();"
		response.write "</script>"
		Response.End
	'A + C
	ElseIf sArgPageLevel = "S" And sGroup <> "S" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "opener.location.href='/g_admin/main.asp';"
		response.write "self.close();"
		response.write "</script>"
		Response.End
	ElseIf sArgPageLevel = "P" And (sGroup <> "P" Or sGroup <> "B") Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "opener.location.href='/g_admin/main.asp';"
		response.write "self.close();"
		response.write "</script>"
		Response.End
	ElseIf sGroup = "" then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "opener.location.href='/g_admin/main.asp';"
		response.write "self.close();"
		response.write "</script>"
		Response.End
	end if
END Sub


'사용권한(로그인) 체크 (특정아이디만 사용가능)
SUB	sbMemberIDAuthChk(sArgUserID)
	Dim sUserID, sGroup
	Dim sPageURL
	
	sPageURL = HOME_URL_ & "/g_admin/member_logout.asp"

	sUserID = Trim(Session("SS_USERID"))
	sGroup = Trim(Session("SS_GROUP"))	
	
	if isNull(sGroup) Or (sGroup <> "A" and sGroup <> "B" and sGroup <> "C" and sGroup <> "E") then
		sGroup = ""
	end If
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "top.location.href='" & sPageURL & "';"
		response.write "</script>"
		Response.End
	end if
	
	If sArgUserID <> sUserID Or sGroup = "" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	end if

END Sub

'사용권한(로그인) 체크 (특정아이디 권한없음)
SUB	sbMemberIDNOAuthChk(sArgUserID)
	Dim sUserID, sGroup
	Dim sPageURL
	
	sPageURL = HOME_URL_ & "/g_admin/member_logout.asp"

	sUserID = Trim(Session("SS_USERID"))
	sGroup = Trim(Session("SS_GROUP"))	
	
	if isNull(sGroup) Or (sGroup <> "A" and sGroup <> "B" and sGroup <> "C" and sGroup <> "E") then
		sGroup = ""
	end If
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "top.location.href='" & sPageURL & "';"
		response.write "</script>"
		Response.End
	end if
	
	If sArgUserID = sUserID Or sGroup = "" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	end if

END Sub

'사용권한(로그인) 체크 (특정아이디만 사용가능) - 아이디 리스트
SUB	sbMemberIDSAuthChk(sArgUserID)
	Dim sUserID, sGroup
	Dim sPageURL
	Dim aUserList, nUserList, sAuthChk, i
	
	sPageURL = HOME_URL_ & "/g_admin/member_logout.asp"

	sUserID = LCase(fnNullInit(Session("SS_USERID")))
	sGroup = fnNullInit(Session("SS_GROUP"))	
	
	if isNull(sGroup) Or (sGroup <> "A" and sGroup <> "B" and sGroup <> "C" and sGroup <> "E") then
		sGroup = ""
	end If
	
	aUserList = Split(sArgUserID, ",")
	nUserList = UBound(aUserList)
	sAuthChk = "False"
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "top.location.href='" & sPageURL & "';"
		response.write "</script>"
		Response.End
	end if

	For i=0 To nUserList
		If LCase(fnNullInit(aUserList(i))) = sUserID Then
			sAuthChk = "True"
		End if
	Next
	
	If sAuthChk = "False" Or sGroup = "" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	end if

END Sub

'MUTI PUSH 발송(메세지, 전송 할 REG_ID(','구분), 전송 할 폰번호(','구분), 전송 실패 사유(','구분), 성공한 REG_ID(','구분), 성공한 폰번호(','구분))
Function fnPushSend(sMsg, sRegIDList, sPhoneList, sFailReason, sSuccRegIDList, sSuccPhoneList, comm_id)
	Dim APIKEY : APIKEY = "AIzaSyC-eisgmovdUQQgTwQ7f9hOFdrCvKqu0Lo"
	Dim sValue, aValue, nValue, i
	Dim sResultString : sResultString = ""

	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)
	
	Dim aRegID, aPhone, aSuccRegID, aSuccPhone
	Dim nRegID, nPhone, nSuccRegID, nSuccPhone
	Dim aFailReason, nFailReason

	aRegID = split(sRegIDList, ",")
	aPhone = split(sPhoneList, ",")
	
	nRegID = -1
	nPhone = -1

	if isArray(aRegID) then
		nRegID = ubound(aRegID, 1)
	end if
	
	if isArray(aPhone) then
		nPhone = ubound(aPhone, 1)
	end if

	if nRegID >=0 then
		Dim SEND_URL
		Dim seneHTTP
		SEND_URL = "https://android.googleapis.com/gcm/send"
		Set seneHTTP = Server.CreateObject("MSXML2.ServerXMLHTTP")
		seneHTTP.Open "POST",""& SEND_URL &"",false
		seneHTTP.SetRequestHeader "POST", ""& SEND_URL &" HTTP/1.0"
		seneHTTP.SetRequestHeader "Authorization", "key=" & APIKEY & ""
		seneHTTP.SetRequestHeader "Content-Type", "application/json"									'멀티 전송시
		'seneHTTP.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded"		'단일 전송시

		If seneHTTP.readyState = 1 Then		
			Dim collapse_key : collapse_key = ""

			for i=1 to 4 '4자리만 생성
				collapse_key = collapse_key & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
			next

			'멀티 전송시
			Dim json1 : Set json1 = jsObject()
			set json1("data") = jsObject()
			json1("data")("msg") = sMsg			
			json1("delay_while_idle") = true	'디바이스가 idle상태일때는 전달하지 않음(기본 false)
			json1("collapse_key") = collapse_key	 '메세지 그룹설정(오프라인에서 온라인 상태가 되면 많은 메세지가 전다되는 것을 막고 그룹내에서 가장 최신의 메시지만 전달)
			json1("registration_ids") = aRegID

			'json1("dry_run") = true		'전송TEST(전송테스트 시 주석해제)(기본 false)
			'json1.Flush
			
			seneHTTP.send toJSON(json1)	 

			On Error Resume Next
			seneHTTP.waitForResponse 5
			If (seneHTTP.readyState = 4) And (seneHTTP.Status = 200) Then
				sResultString = Trim(seneHTTP.ResponseText)
			else
				sResultString="HTTP_ERR4" & "(" & seneHTTP.readyState & "/"  & seneHTTP.Status & ")"
			End If
			
			Dim sDBChk : sDBChk = "OFF"

			if isObject(dbConn) and not(dbConn is Nothing) then
				sDBChk = "ON"
			else
				fnDBConn()
			end if
			ParamInit()
			'푸쉬발송 로그저장
			sql = "insert into member_msg_log (comm_id, msg_idx, div_idx, try_idx, send_cnt, log_data, phone_list, regid_list, send_date, msg_content)values(?, ?, ?, ?, ?, ?, ?, ?, getdate(), ?) "
			cmd.Parameters.Append cmd.CreateParameter("@comm_id", adVarChar, adParamInput, 50, comm_id)
			cmd.Parameters.Append cmd.CreateParameter("@msg_idx", adInteger, adParamInput, 4, 0)
			cmd.Parameters.Append cmd.CreateParameter("@div_idx", adInteger, adParamInput, 4, 1)
			cmd.Parameters.Append cmd.CreateParameter("@try_idx", adInteger, adParamInput, 4, nTryCnt_+1)
			cmd.Parameters.Append cmd.CreateParameter("@send_cnt", adInteger, adParamInput, 4, nRegID+1)
			cmd.Parameters.Append cmd.CreateParameter("@log_data", adVarChar, adParamInput, 8000, fnStrCut(fnNullInit(sResultString),8000,""))
			cmd.Parameters.Append cmd.CreateParameter("@phone_list", adVarChar, adParamInput, 8000, fnStrCut(fnNullInit(sPhoneList),8000,""))
			cmd.Parameters.Append cmd.CreateParameter("@regid_list", adVarChar, adParamInput, 8000, fnStrCut(fnNullInit(sRegIDList),8000,""))
			cmd.Parameters.Append cmd.CreateParameter("@msg_content", adVarChar, adParamInput, 255, sMsg)
			cmd.CommandText = sql
			cmd.Execute , , adExecuteNoRecords
			ParamInit()
			if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
				fnErrSave()
				'response.end
			end if
			if sDBChk = "OFF" then
				fnDBClose()
			end if
		else
			sResultString="HTTP_ERR1"
		End If

		Set seneHTTP = Nothing
		fnPushSend = sResultString
		
		'전송결과
		Dim nSuccess : nSuccess = 0		'성공건수
		Dim nFail : nFail = 0				'실패건수
		Dim jsonResult						'전송결과(JSON)
		Dim nResult							'전송결과 수
		if sResultString = "HTTP_ERR1" then
			nFail = nRegID + 1
		elseif left(sResultString,9) = "HTTP_ERR4" then
			nFail = nRegID + 1 
		elseif len(sResultString) > 1 then		

			Dim jsonData : set jsonData = JSON.parse(sResultString)
			
			nSuccess = jsonData.success			'성공건수
			nFail = jsonData.failure					'실패건수
			nResult = jsonData.results.length		'결과건수

			sRegIDList = ""							'재시도 할 REG_ID 리스트 초기화
			sPhoneList = ""							'재시도 할 PHONE 리스트 초기화
			sFailReason = ""							'재시도 사유 초기화
			for i=0 to nResult-1			
				'전송실패
				if instr(JSON.stringify(jsonData.results.get(i)), "error") then
					if sRegIDList <> "" then
						sRegIDList = sRegIDList & ","
						sPhoneList = sPhoneList & ","
						sFailReason = sFailReason & ","
					end if
					sRegIDList = sRegIDList & aRegID(i)
					sPhoneList = sPhoneList & aPhone(i)
					sFailReason = sFailReason & JSON.stringify(jsonData.results.get(i))
				'전송성공			
				else
					if sSuccRegIDList <> "" then
						sSuccRegIDList = sSuccRegIDList & ","
						sSuccPhoneList = sSuccPhoneList & ","
					end if
					sSuccRegIDList = sSuccRegIDList & aRegID(i)
					sSuccPhoneList = sSuccPhoneList & aPhone(i)
				end if
			next
			set jsonData = nothing
		end if
		
		nTryCnt_ = nTryCnt_ + 1	
		'실패건수가 있고 최대 재시도 횟수 내 인경우 실패건 재시도
		if nFail > 0 and nTryCnt_ < nMaxTryCnt_ and sRegIDList <> "" then
			'재시도
			sPushResult = fnPushSend(sMsg, sRegIDList, sPhoneList, sFailReason, sSuccRegIDList, sSuccPhoneList, comm_id)
		'전송완료
		else
			
			fnPushSend = sResultString
			'결과출력
				'시도횟수
				response.write "* PUSH 전송시도횟수 : " & nTryCnt_ & "회 시도(최대 " & nMaxTryCnt_ & "회까지 가능)<BR/>"
				response.write "* 전송 메세지[" & sMsg & "]"
				
				'성공건
				aSuccRegID = split(sSuccRegIDList, ",")
				aSuccPhone = split(sSuccPhoneList, ",")
				nSuccRegID = -1
				nSuccPhone = -1
				if isArray(aSuccRegID) then
					nSuccRegID = ubound(aSuccRegID, 1)
				end if				
				if isArray(aSuccPhone) then
					nSuccPhone = ubound(aSuccPhone, 1)
				end if
				nSuccess = nSuccRegID+1
				Response.Write "<br/><br/>===================================<BR/>"
				Response.Write "* 성공 총 " & nSuccess& "건 <br/>"
				Response.Write "===========================================<br/>"
				for i=0 to nSuccRegID
					response.write (i+1) & ". " & aSuccPhone(i) & " [" & aSuccRegID(i) & "]<br/>"
				next
				Response.Write "<br/>========================================<BR/><br/>"

				'실패건
				aRegID = split(sRegIDList, ",")
				aPhone = split(sPhoneList, ",")
				aFailReason = split(sFailReason, ",")
				
				nRegID = -1
				nPhone = -1
				nFailReason = -1
				if isArray(aRegID) then
					nRegID = ubound(aRegID, 1)
				end if				
				if isArray(aPhone) then
					nPhone = ubound(aPhone, 1)
				end if
				if isArray(aFailReason) then
					nFailReason = ubound(aFailReason, 1)
				end if
				nFail = nRegID+1
				Response.Write "<br/><br/>===================================<BR/>"
				Response.Write "* 실패 총 " & nFail & "건 <br/>"
				Response.Write "===========================================<br/>"
				for i=0 to nRegID
					response.write (i+1) & ". " & aPhone(i) & " [" & aRegID(i) & "]<br/>"
					if nFailReason >= i then
						response.write "&nbsp;&nbsp;&nbsp;&nbsp;실패사유 (" & aFailReason(i) & ")<br/>"
					end if
					Response.Write "<br/>-------------------------------------------------------------------<br/>"					
				next
				Response.Write "<br/>=======================================<BR/><br/>"

			If Err and Err.Number <> 0 Then
				Response.Write "<br/><br/>===================================<BR/>"
				response.write "ERROR[" & Err.Number & "] Description :" & Err.Description & "<br/>"
				Response.Write "Source : " & Err.Source & "<BR/>"
				Response.Write "HelpContext : " & Err.HelpContext & "<BR/>"
				Response.Write "<br/>======================================<BR/><br/>"
				'response.end
			End if
		end if 
	else		
		fnPushSend = "PUSH발송 할 휴대폰번호가 없습니다"
		'response.write "<script type='text/javascript'>"
		'response.write "alert('PUSH발송 할 휴대폰번호가 없습니다.');"
		'response.write "</script>"
	end if
end Function
%>