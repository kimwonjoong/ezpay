<%
'정규식체크
function fnRegChk(sArgPattern, sArgValue)
	Dim regEx, match, matches
	SET regEx = New RegExp
	
	regEx.Pattern = sArgPattern     ' 패턴을 설정합니다.
	regEx.IgnoreCase = "True"      ' 대/소문자를 구분하지 않도록 합니다.
	regEx.Global = "True"          ' 전체 문자열을 검색하도록 설정합니다.
	regEx.Test(sArgValue)
	SET Matches = regEx.Execute(sArgValue)

	if Matches.count > 0 then
			fnRegChk = "False"
	Else
			fnRegChk = "True"
	end if
end function

'데이터 포맷 체크
function fnDataChk(sArgValue, sArgType) 
	Dim sValue, nByte, nLength
	Dim sPattern, sReturn
	Dim nValue

	sValue = trim(sArgValue)
	nByte = len(sValue)
	nLength = len(sValue)

	
	sReturn = "True"

	if sArgType = "REGNO" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('사업자번호를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then
			response.write "<script type='text/javascript'>alert('사업자번호는 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nByte <> 10 or nLength <> 10 then
			response.write "<script type='text/javascript'>alert('사업자번호 10자리를 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "REGNO1" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('사업자번호 앞 3자리를 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then
			response.write "<script type='text/javascript'>alert('사업자번호는 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength <> 3 or nByte <> 3 then
			response.write "<script type='text/javascript'>alert('사업자번호 앞 3자리를 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "REGNO2" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('사업자번호 가운데 2자리를 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then
			response.write "<script type='text/javascript'>alert('사업자번호는 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength <> 2 or nByte <> 2 then
			response.write "<script type='text/javascript'>alert('사업자번호 가운데 2자리를 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "REGNO3" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('사업자번호 끝자리 5자리를 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then
			response.write "<script type='text/javascript'>alert('사업자번호는 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength <> 5 or nByte <> 5 then
			response.write "<script type='text/javascript'>alert('사업자번호 끝자리 5자리를 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "USERID" then
		sPattern = "[^a-zA-Z0-9_]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('아이디를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('아이디는 영문 또는 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 16 then
			response.write "<script type='text/javascript'>alert('아이디를 영문/숫자조합 2~14자 이내로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "LOGINUSERID" then
		sPattern = "[^a-zA-Z0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('아이디를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('아이디는 영문 또는 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 16 then
			response.write "<script type='text/javascript'>alert('아이디를 영문/숫자조합 2~14자 이내로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "BOSSNAME" then
		sPattern = "[^가-힣A-Z0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('대표자명을 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('대표자명은 한글/영문만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 25 then
			response.write "<script type='text/javascript'>alert('대표자명은 2자 이상 25자 이하로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "APPNAME" then
		sPattern = "[^가-힣A-Z0-9_ ]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('앱명을 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		'elseif fnRegChk(sPattern, sArgValue) = "False" then				
		'	response.write "<script type='text/javascript'>alert('앱명은 한글/영문만 입력가능합니다.'); history.back();</script>" 
		'	sReturn = "False" 
		elseif nLength < 2 or nByte > 50 then
			response.write "<script type='text/javascript'>alert('앱명은 2자 이상 25자 이하로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "STORE_NAME" then
		sPattern = "[^가-힣 A-Z0-9ㄱ-ㅎa-z]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('업체명을 입력해주세요.');history.back();</script>"
			sReturn = "False"
		elseif nLength < 1 or nLength > 25 then
			response.write "<script type='text/javascript'>alert('업체명을 1자 이상 25자 이하로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "COUPON_NAME" then
		sPattern = "[^가-힣 A-Z0-9ㄱ-ㅎa-z()]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('쿠폰명을 입력해주세요.');history.back();</script>"
			sReturn = "False"
		elseif nLength < 1 or nByte > 256 then
			response.write "<script type='text/javascript'>alert('쿠폰명을 1자 이상 120자 이하로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "CONTACT_NAME" then
		sPattern = "[^가-힣 A-Z0-9ㄱ-ㅎa-z]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('담당자명을 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('담당자명은 한글/영문만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 1 or nLength > 25 then
			response.write "<script type='text/javascript'>alert('담당자명은 1자 이상 25자 이하로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "DEPTHNAME" then
		sPattern = "[^가-힣 A-Z]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('부서명을 입력해주세요.');history.back();</script>"
			sReturn = "False"
		elseif nLength < 2 or nLength > 25 then
			response.write "<script type='text/javascript'>alert('부서명을 2자 이상 25자 이하로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "DEPTLNAME" then
		sPattern = "[^가-힣 A-Z]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('팀명을 입력해주세요.');history.back();</script>"
			sReturn = "False"
		elseif nLength < 2 or nLength > 25 then
			response.write "<script type='text/javascript'>alert('팀명을 2자 이상 25자 이하로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "CARDNO" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('카드번호를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then			
			response.write "<script type='text/javascript'>alert('카드번호는 숫자만 입력가능 합니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength <> 10 or nByte <> 10 then
			response.write "<script type='text/javascript'>alert('카드번호를 숫자 10자로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "PASSWORD" then
		sPattern = "[^0-9a-zA-Z._@!#$%^&*()~]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('비밀번호를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('비밀번호는 영문 또는 숫자만 입력가능합니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 14 or nByte > 14 then
			response.write "<script type='text/javascript'>alert('비밀번호를 영문/숫자조합 2~14자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "LOGINPASSWORD" then
		sPattern = "[^가-힣0-9a-zA-Z._@!#$%^&*()~]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('비밀번호를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then			
			response.write "<script type='text/javascript'>alert('비밀번호에 입력불가능한 특수문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 3 or nLength > 14 or nByte > 28 then
			response.write "<script type='text/javascript'>alert('비밀번호를 영문/숫자조합 2~14자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "USERNAME" then
		sPattern = "[^가-힣A-Z0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('사용자명을 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('사용자명은 한글/영문만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 10 then
			response.write "<script type='text/javascript'>alert('사용자명은 2자 이상 10자 이하로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "DUTY" then
		sPattern = "[^가-힣A-Z0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('직책을 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('직책은 한글/영문만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 10 then
			response.write "<script type='text/javascript'>alert('직책은 2자 이상 10자 이하로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "ZIPCODE" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('우편번호를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" or nLength <> 3 or nByte <> 3 then
			response.write "<script type='text/javascript'>alert('우편번호는 형식이 잘못되었습니다.');history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "ADDR" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^!#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			'response.write "<script type='text/javascript'>alert('지역을 입력해주세요');history.back();</script>"
			'sReturn = "False" 
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('지역에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 25 then
			response.write "<script type='text/javascript'>alert('지역을 25자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "ADDRESS" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^!#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('주소를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('주소에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 50 then
			response.write "<script type='text/javascript'>alert('주소를 50자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "ADDRESS2" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^!#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('상세주소에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 50 then
			response.write "<script type='text/javascript'>alert('상세주소를 50자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "OTIME" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^!#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			sReturn = "True"
		'elseif fnRegChk(sPattern, sArgValue) = "False" then				
		'	response.write "<script type='text/javascript'>alert('영업시간에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
		'	sReturn = "False" 
		elseif nLength > 50 then
			response.write "<script type='text/javascript'>alert('영업시간을 50자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "CLOSE_DAY" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^!#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			sReturn = "True"
		'elseif fnRegChk(sPattern, sArgValue) = "False" then				
		'	response.write "<script type='text/javascript'>alert('휴무일에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
		'	sReturn = "False" 
		elseif nLength > 50 then
			response.write "<script type='text/javascript'>alert('휴무일을 50자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "LOCATION_DES" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^!#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			sReturn = "True"
		'elseif fnRegChk(sPattern, sArgValue) = "False" then				
		'	response.write "<script type='text/javascript'>alert('위치에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
		'	sReturn = "False" 
		elseif nLength > 50 then
			response.write "<script type='text/javascript'>alert('위치를 50자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "CAPACITY" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^!#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			sReturn = "True"
		'elseif fnRegChk(sPattern, sArgValue) = "False" then				
		'	response.write "<script type='text/javascript'>alert('최대수용인원에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
		'	sReturn = "False" 
		elseif nLength > 50 then
			response.write "<script type='text/javascript'>alert('최대수용인원을 50자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "TEL1" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('전화번호를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" or nLength < 2 or nByte > 4 then
			response.write "<script type='text/javascript'>alert('전화번호 형식이 잘못되었습니다.');history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "TEL2" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('전화번호를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" or nLength < 3 or nByte > 4 then
			response.write "<script type='text/javascript'>alert('전화번호 형식이 잘못되었습니다.');history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "TEL3" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('전화번호를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" or nLength < 4 or nByte > 4 then
			response.write "<script type='text/javascript'>alert('전화번호 형식이 잘못되었습니다.');history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "FAX1" then
		sPattern = "[^0-9]"
		if sValue = "" then
			sReturn = "True" 
		elseif fnRegChk(sPattern, sArgValue) = "False" or nLength < 2 or nByte > 4 then
			response.write "<script type='text/javascript'>alert('팩스번호 형식이 잘못되었습니다.');history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "FAX2" then
		sPattern = "[^0-9]"
		if sValue = "" then
			sReturn = "True" 
		elseif fnRegChk(sPattern, sArgValue) = "False" or nLength < 3 or nByte > 4 then
			response.write "<script type='text/javascript'>alert('팩스번호 형식이 잘못되었습니다.');history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"		
		end if
	elseif sArgType = "FAX3" then
		sPattern = "[^0-9]"
		if sValue = "" then
			sReturn = "True" 
		elseif fnRegChk(sPattern, sArgValue) = "False" or nLength < 4 or nByte > 4 then
			response.write "<script type='text/javascript'>alert('팩스번호 형식이 잘못되었습니다.');history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "EMAIL" then
		sPattern = "[^가-힣ㄱ-ㅎ0-9a-zA-Z*~/@()._-]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('이메일을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('이메일에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 25 then
			response.write "<script type='text/javascript'>alert('이메일을 25자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "EMAIL0" then
		sPattern = "[^가-힣ㄱ-ㅎ0-9a-zA-Z*~/@()._-]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('이메일을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('이메일에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 25 then
			response.write "<script type='text/javascript'>alert('이메일을 25자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "TITLE" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^!#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('제목을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('제목에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 40 then
			response.write "<script type='text/javascript'>alert('제목을 40자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "ADMIN_MSG_TITLE" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "(\/+\/)|(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('인사말 제목을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('인사말 제목에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 40 then
			response.write "<script type='text/javascript'>alert('인사말 제목을 40자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "ADMIN_MSG" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "(\/+\/)|(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('인사말을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('인사말에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 500 then
			response.write "<script type='text/javascript'>alert('인사말을 500자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "MEMO" then
		sPattern = "(\/+\/)|(\-+\-)"
		if sValue = "" then
			sReturn = "True"
			'response.write "<script type='text/javascript'>alert('내용을 입력해주세요');history.back();</script>"
			'sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('내용에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nByte > 255 then
			response.write "<script type='text/javascript'>alert('내용을 125자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "MEMO_ANS" then
		sPattern = "(\/+\/)|(\-+\-)"
		if sValue = "" then
			'response.write "<script type='text/javascript'>alert('답변을 입력해주세요');history.back();</script>"
			'sReturn = "False" 
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('답변에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nByte > 255 then
			response.write "<script type='text/javascript'>alert('답변을 125자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if 
	elseif sArgType = "CONTENT" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('내용을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('내용에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 2000 then
			response.write "<script type='text/javascript'>alert('내용을 2000자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "HTML_CONTENT" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('내용을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('내용에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nByte > 8000 then
			response.write "<script type='text/javascript'>alert('내용을 한글4,000자/영문8,000자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "DRIVER_ID" then
		sPattern = "[^a-zA-Z0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('운전자 아이디를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('운전자 아이디는 영문 또는 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 10 then
			response.write "<script type='text/javascript'>alert('운전자 아이디를 영문/숫자조합 2~10자 이내로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "CEO" then
		sPattern = "[^ㄱ-ㅎ가-힣A-Z0-9()]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('대표자명을 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('대표자명은 한글/영문만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 10 then
			response.write "<script type='text/javascript'>alert('대표자명은 2자 이상 10자 이하로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "DRIVER_JUMIN" then
		sPattern = "[^0-9-]"
		if sValue = "" then
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('주민등록번호 형식이 잘못되었습니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength <> 13 then
			response.write "<script type='text/javascript'>alert('주민등록번호를 13자리로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "DRIVER_TEL" then
		sPattern = "[^가-힣A-Z0-9-@*~/(),._ ]"

		if sValue = "" then
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('전화번호에 입력불가능한 문자가 포함되었습니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 15 then
			response.write "<script type='text/javascript'>alert('전화번호를 15자리 이내로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "NICKNAME" then
		sPattern = "[^가-힣A-Z0-9-@*~/(),._ ]"
		if sValue = "" then
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('휴대폰번호에 입력불가능한 문자가 포함되었습니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 15 then
			response.write "<script type='text/javascript'>alert('휴대폰번호를 15자리 이내로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "DRIVER_PHONE" then
		sPattern = "[^가-힣A-Z0-9-@*~/(),._ ]"
		if sValue = "" then
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('휴대폰번호에 입력불가능한 문자가 포함되었습니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 15 then
			response.write "<script type='text/javascript'>alert('휴대폰번호를 15자리 이내로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "PRICE" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('가격을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then
			response.write "<script type='text/javascript'>alert('가격은 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "SELLERID" then
		sPattern = "[^0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('SELLERID를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then
			response.write "<script type='text/javascript'>alert('SELLERID는 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 10 then
			response.write "<script type='text/javascript'>alert('SELLERID를 10자리 이내로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "RATE" then
		sPattern = "[^0-9.]"
		If IsNumeric(sValue) Then
			nValue = CDbl(sValue)
		End if
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('수수료를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then
			response.write "<script type='text/javascript'>alert('수수료는 숫자만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nValue > 50 then
			response.write "<script type='text/javascript'>alert('수수료를 50 이하로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "BCODE" then
		sPattern = "[^A-Za-z0-9]"
		If IsNumeric(sValue) Then
			nValue = CDbl(sValue)
		End if
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('대분류코드를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" OR nLength > 2 then
			response.write "<script type='text/javascript'>alert('대분류코드는 (영문, 숫자) 2자 이하로 입력해주세요.'); history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "SCODE" then
		sPattern = "[^0-9]"
		If IsNumeric(sValue) Then
			nValue = CDbl(sValue)
		End if
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('소분류코드를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" OR nValue > 2147483647 then
			response.write "<script type='text/javascript'>alert('소분류코드를 숫자(2147483647 이하)로 입력해주세요.'); history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "MENU_NAME" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('메뉴명을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('메뉴명에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nByte > 50 then
			response.write "<script type='text/javascript'>alert('메뉴명을 25자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "CODE_NOTE" then
		'sPattern = '[^0-9a-zA-Z가-힝 *&~()._-/]|(\/+\/)|(\-+\-)'
		sPattern = "[=+%^!#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('비고에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nByte > 50 then
			response.write "<script type='text/javascript'>alert('비고를 25자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end if
	elseif sArgType = "BRANDCODE" then
		sPattern = "[^0-9]"
		If IsNumeric(sValue) Then
			nValue = CDbl(sValue)
		End if
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('브랜드코드를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" OR nValue > 2147483647 then
			response.write "<script type='text/javascript'>alert('브랜드코드를 숫자(2147483647 이하)로 입력해주세요.'); history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "DONG" Then
		sPattern = "[^A-Za-z0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('동을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" OR nLength > 5 then
			response.write "<script type='text/javascript'>alert('동 정보는 (영문, 숫자) 5자 이하로 입력해주세요.'); history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "HOSU" Then
		sPattern = "[^A-Za-z0-9]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('호수를 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" OR nLength > 5 then
			response.write "<script type='text/javascript'>alert('호수 정보는 (영문, 숫자) 5자 이하로 입력해주세요.'); history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "NAME" Then
		sPattern = "[^ㄱ-ㅎ가-힣A-Z0-9()]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('이름을 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('이름은 한글/영문만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 10 then
			response.write "<script type='text/javascript'>alert('이름은 2자 이상 10자 이하로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "COUNT" Then
		sPattern = "[^0-9]"
		If IsNumeric(sValue) Then
			nValue = CDbl(sValue)
		End if
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('수량을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" OR nValue > 2147483647 then
			response.write "<script type='text/javascript'>alert('수량을 숫자(2,000,000,000이하)로 입력해주세요.'); history.back();</script>" 
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "HANDPHONE" then
		sPattern = "[^가-힣A-Z0-9-@*~/(),._ ]"
		if sValue = "" then
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('휴대폰번호에 입력불가능한 문자가 포함되었습니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength > 15 then
			response.write "<script type='text/javascript'>alert('휴대폰번호를 15자리 이내로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "MEMO" then
		sPattern = "[=+%^#'|<>]|(\/+\/)|(\-+\-)"
		if sValue = "" then
			'response.write "<script type='text/javascript'>alert('답변을 입력해주세요');history.back();</script>"
			'sReturn = "False" 
			sReturn = "True"
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('메모에 입력불가능한 문자가 포함되어있습니다.');history.back();</script>" 
			sReturn = "False" 
		elseif nByte > 255 then
			response.write "<script type='text/javascript'>alert('메모를 125자 이내로 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If

	elseif sArgType = "TYPE" Then
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('구분을 선택해주세요');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If

	elseif sArgType = "SENDCOMPANY" Then
		sPattern = "[^ㄱ-ㅎ가-힣A-Z0-9()]"
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('업체명을 입력해주세요.');history.back();</script>"
			sReturn = "False" 
		elseif fnRegChk(sPattern, sArgValue) = "False" then				
			response.write "<script type='text/javascript'>alert('업체명은 한글/영문만 입력가능합니다.'); history.back();</script>" 
			sReturn = "False" 
		elseif nLength < 2 or nLength > 15 then
			response.write "<script type='text/javascript'>alert('업체명은 2자 이상 15자 이하로 입력해주세요.'); history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If

	elseif sArgType = "RECEIVECHECK" Then
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('수령여부를 선택해주세요');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "ARRIVALDATE" Then
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('도착일을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif not(isDate(sValue)) then
			response.write "<script type='text/javascript'>alert('입력하신 도착일은 유효한 날짜가 아닙니다.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "SDATE" Then
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('시작일을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif not(isDate(sValue)) then
			response.write "<script type='text/javascript'>alert('입력하신 시작일은 유효한 날짜가 아닙니다.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "EDATE" Then
		if sValue = "" then
			response.write "<script type='text/javascript'>alert('종료일을 입력해주세요');history.back();</script>"
			sReturn = "False" 
		elseif not(isDate(sValue)) then
			response.write "<script type='text/javascript'>alert('입력하신 종료일은 유효한 날짜가 아닙니다.');history.back();</script>"
			sReturn = "False" 
		else
			sReturn = "True"
		end If
	elseif sArgType = "RECEIVEDATE" Then
		if sValue = "" then
			sReturn = "True"
		else
			sReturn = "True"
		end If

	else
		response.write "<script type='text/javascript'>alert('체크 할 데이터가 없습니다.');history.back();</script>"
		sReturn = "False"
	end if

	fnDataChk = sReturn
End Function

'DB입력 데이터 특수문자 제거('"' => '´')
function fnCharChk(sArgString)
	Dim sNewStr
	sNewStr = sArgString
	if isNull(sNewStr) and sNewStr = "" then
		sNewStr = replace(sNewStr, "'", "´")
		sNewStr = replace(sNewStr, ">", "&gt;")
		sNewStr = replace(sNewStr, "<", "&lt;")
	end if
	fnCharChk = sNewStr
End Function

'데이터가 있는경우에만 포맷체크
function fnDataChk0(sArgValue, sArgType)
	if fnNullInit(sArgValue) <> "" then
		fnDataChk0 = fnDataChk(sArgValue, sArgType)
	else
		fnDataChk0 = "True"
	end if
end function
%>