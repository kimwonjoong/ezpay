<% 
Response.AddHeader "Cache-Control", "no-cache" 
Response.AddHeader "Expires", "0" 
Response.AddHeader "Pragma", "no-cache" 

Dim company, browser, markup, sw 
Dim param, arr_param 
Dim tag_start, tag_end 

param = Request.ServerVariables("QUERY_STRING") 
arr_param = split(param, "&", -1, 1) 

company = Request("company") 
browser = Request("browser") 
markup  = Request("markup") 
sw      = Request("screen_width") 

if company ="SKT" or company = "LGT" then 

 Dim wml_dtd 
 wml_dtd = "<?xml version=""1.0"" encoding=""ISO-8859-1""?> "& chr(10) 
 wml_dtd = wml_dtd &"<!DOCTYPE wml PUBLIC ""-//WAPFORUM//DTD WML 1.1//EN"" ""http://www.wapforum.org/DTD/wml.wml"">" & chr(10) 
 tag_start = "<wml>"& chr(10) &"<card>"& chr(10) 
 tag_end = "</card>"& chr(10) &"</wml>" 

 Response.ContentType = "text/vnd.wap.wml" 'SKT/LGT폰일 경우 ContentType 추가 
 Response.Write wml_dtd 
else 

 Response.ContentType = "text/html; charset=euc-kr" 
 tag_start = "<html>"& chr(10) &"<body>"& chr(10) 
 tag_end = "</body>"& chr(10) &"</html>" 
  
end if 

'------------ 
'내용 출력 
'------------ 
 Response.write tag_start 
 Response.Write "<p>"& chr(10) 
 Response.Write "고객님의 폰은 본 서비스를 이용하실 수 없는 단말입니다.<br/>"& chr(10) 

 if UBound(arr_param) > 0 then    
  Response.Write "폰 정보 : <br/>"  
  Response.Write arr_param(0)&"<br/>" 
  Response.Write arr_param(1)&"<br/>" 
  Response.Write arr_param(2)&"<br/>" 
  Response.Write arr_param(3)&"<br/>" 
 end if 

 '아래 코딩은 LGT WAP1.0단말 브라우저에서 디코딩을 못함 **************** 
 'if UBound(arr_param) > 0 then 
 ' With Response 
 '  .Write "통신사 : "& arr_param(0)&"<br/>" 
 '  .Write "Browser : "& arr_param(1) & 
 '  .Write "Markup : "& arr_param(2)&"<br/>" 
 '  .Write "Screen Width : "& arr_param(3)&"<br/>" 
 '  .Write "ALL_HTTP<br/>" 
 '  .Write Request.ServerVariables("ALL_HTTP") &"<br/>" 
 ' End With 
 'end if 
 '***************************************************************** 

 Response.Write "</p>"& chr(10) 
 Response.Write tag_end 
%> 