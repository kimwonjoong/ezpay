<% Option Explicit %>
<!--#include file="../inc/inc_top_common.asp"-->
<%Call sbMemberAuthChk("E")%>
<!--#include file="../inc/inc_db_lib.asp"-->
<%
Response.Buffer = False

Dim objstream, oFs, myfile, download
Dim sFilePath, sFileName, nNum
	
nNum = fnNullInit(Request.Form("hdNum"))
sPageCode = fnNullInit(Request.Form("hdPageCode"))

If Not(IsNumeric(nNum)) Then
	fnErrScript "게시물정보가 없습니다.", "location.href='temp.asp'"
	response.end
End If

If sPageCode <> "notice" Then
	fnErrScript "게시판정보가 없습니다.", "location.href='temp.asp'"
	response.end
End If

fnDBConn()

Set rs = Server.CreateObject("ADODB.Recordset")
sql = " select bi_filename1 from mp_" & sPageCode & " where bi_num = ? "
if SS_LEVEL <> "A" then
	sql = sql & " and bi_sec <>  'Y' "
end if
cmd.Parameters.Append cmd.CreateParameter("@bi_num", adInteger, adParamInput, 4, nNum)
cmd.CommandText = sql
Set rs = cmd.execute()
if not(rs.eof or rs.bof) Then
	sFileName = fnNullInit(rs("bi_filename1"))
end if
rs.close
Set rs = Nothing
fnDBClose()

If sFileName = "" Then
	fnErrScript "게시물이 없거나 첨부된 파일이 없습니다..", "location.href='temp.asp'"
Else		
	sFilePath = server.MapPath("..") & "\files\" & sPageCode & "\" & sFileName

	Set oFs = CreateObject("Scripting.FileSystemObject")
	If oFs.FileExists(sFilePath) then
		Set myfile = oFs.GetFile(sFilePath)
		if myfile.size > 0 then 
			Response.ContentType = "application/unknown"
			Response.AddHeader "Content-Disposition","attachment; filename=" & sFileName
			Set objStream = Server.CreateObject("ADODB.Stream")
			objStream.Open
	
			objStream.Type = 1
			objStream.LoadFromFile sFilePath
	
			download = objStream.Read
			Response.BinaryWrite download 
	
			Set objstream = nothing 
		else
			fnErrScript "사이즈가 '0'인 파일은 다운받을 수 없습니다.!", "location.href='temp.asp'"
		end if
	Else
		fnErrScript "잘못된 파일경로입니다.", "location.href='temp.asp'"
	End If	
	Set oFs = nothing	
End If
%>