<%
Class ImageClass

	   Private m_Width
	   Private m_Height
	   Private m_ImageType
	   Private m_Depth
	   Private BinFile
	   Private depth

	   Private BUFFERSIZE
	   Private objStream

	   Private Sub class_initialize()

	   	   BUFFERSIZE = 65535

	   	   ' Set all properties to default values
	   	   m_Width	   = 0
	   	   m_Height	   = 0
	   	   m_Depth	   = 0
	   	   m_ImageType = Null

	   	   Set objStream = Server.CreateObject("ADODB.Stream")

	   End Sub

	   Private Sub class_terminate()

	   	   Set objStream = Nothing

	   End Sub

	   Public Property Get Width()
	   	   Width = m_Width
	   End Property

	   Public Property Get Height()
	   	   Height = m_Height
	   End Property

	   Public Property Get ImageType()
	   	   ImageType = m_ImageType
	   End Property

	   Private Function Mult(lsb, msb)
	   	   Mult = lsb + (msb * CLng(256))
	   End Function

	   Private Function BinToAsc(ipos)
	   	   BinToAsc = AscB(MidB(BinFile, (ipos+1), 1))
	   End Function

	   Public Sub LoadFilePath(strPath)
	   	   If InStr(strPath, ":") = 0 Then
	   	   	   strPath = Server.MapPath(strPath)
	   	   End If

	   	   objStream.Open
	   	   objStream.LoadFromFile(strPath)
	   	   BinFile = objStream.ReadText(-1)

	   End Sub

	   Public Sub LoadBinary(BinaryFile)

	   	   BinFile = BinaryFile

	   End Sub

	   Public Sub ImageRead

	   	   If  BinToAsc(0) = 137 And BinToAsc(1) = 80 And BinToAsc(2) = 78 Then
	   	   	   ' this is a PNG file
	   	   	   m_ImageType = "png"

	   	   	   ' get bit depth
	   	   	   Select Case BinToAsc(25)
	   	   	   	   Case 0
	   	   	   	   ' greyscale
	   	   	   	   	   Depth = BinToAsc(24)
	   	   	   	   Case 2
	   	   	   	   ' RGB encoded
	   	   	   	   	   Depth = BinToAsc(24) * 3
	   	   	   	   Case 3
	   	   	   	   ' Palette based, 8 bpp
	   	   	   	   	   Depth = 8
	   	   	   	   Case 4
	   	   	   	   ' greyscale with alpha
	   	   	   	   	   Depth = BinToAsc(24) * 2
	   	   	   	   Case 6
	   	   	   	   ' RGB encoded with alpha
	   	   	   	   	   Depth = BinToAsc(24) * 4
	   	   	   	   Case Else
	   	   	   	   ' This value is outside of it's normal range, so we'll assume that this is not a valid file
	   	   	   	   	   m_ImageType = Null
	   	   	   End Select

	   	   	   If not IsNull(m_ImageType) Then
	   	   	   	   ' if the image is valid then

	   	   	   	   ' get the width
	   	   	   	   m_Width = Mult(BinToAsc(19), BinToAsc(18))

	   	   	   	   ' get the height
	   	   	   	   m_Height = Mult(BinToAsc(23), BinToAsc(22))
	   	   	   End If
	   	   End If

	   	   If BinToAsc(0) = 71 And BinToAsc(1) = 73 And BinToAsc(2) = 70 Then
	   	   	   ' this is a GIF file
	   	   	   m_ImageType = "gif"

	   	   	   ' get the width
	   	   	   m_Width = Mult(BinToAsc(6), BinToAsc(7))

	   	   	   ' get the height
	   	   	   m_Height = Mult(BinToAsc(8), BinToAsc(9))

	   	   	   ' get bit depth
	   	   	   m_Depth = (BinToAsc(10) And 7) + 1
	   	   End If

	   	   If BinToAsc(0) = 66 And BinToAsc(1) = 77 Then
	   	   	   ' this is a BMP file

	   	   	   m_ImageType = "bmp"

	   	   	   ' get the width
	   	   	   m_Width = Mult(BinToAsc(18), BinToAsc(19))

	                   	    ' get the height
	   	   	   m_Height = Mult(BinToAsc(22), BinToAsc(23))

	   	   	   ' get bit depth
	   	   	   m_Depth = BinToAsc(28)
	   	   End If


	   	   If IsNull(m_ImageType) Then
	   	   	   ' if the file is not one of the above type then
	   	   	   ' check to see if it is a JPEG file
	   	   	   Dim lPos : lPos = 0

	   	   	   Do
	   	   	   	   ' loop through looking for the byte sequence FF,D8,FF
	   	   	   	   ' which marks the begining of a JPEG file
	   	   	   	   ' lPos will be left at the postion of the start
'###########################요기 엔터값 없애야함
	   	   	   	   If (BinToAsc(lPos) = &HFF And BinToAsc(lPos + 1) = &HD8 And BinToAsc(lPos + 2) = &HFF) Or (lPos >= BUFFERSIZE - 10) Then Exit Do

	   	   	   	   	   ' move our pointer up
	   	   	   	   	   lPos = lPos + 1

	   	   	   	   	   ' and continue
	   	   	   Loop

	   	   	   lPos = lPos + 2
	   	   	   If lPos >= BUFFERSIZE - 10 Then Exit Sub


	   	   	   Do
	   	   	   	   ' loop through the markers until we find the one
	   	   	   	   ' starting with FF,C0 which is the block containing the
	   	   	   	   ' image information

	   	   	   	   Do
	   	   	   	   	   ' loop until we find the beginning of the next marker
	   	   	   	   	   If BinToAsc(lPos) = &HFF And BinToAsc(lPos + 1) <> &HFF Then Exit Do
	   	   	   	   	   	   lPos = lPos + 1
	   	   	   	   	   	   If lPos >= BUFFERSIZE - 10 Then Exit Sub
	   	   	   	   Loop

	   	   	   	   ' move pointer up
	   	   	   	   lPos = lPos + 1




'###########################요기 엔터값 없애야함
	   	   	   	   If  (BinToAsc(lPos) >= &HC0 And BinToAsc(lPos) <= &HC3) Or (BinToAsc(lPos) >= &HC5 And BinToAsc(lPos) <= &HC7) Or (BinToAsc(lPos) >= &HC9 And BinToAsc(lPos) <= &HCB) Or (BinToAsc(lPos) >= &HCD And BinToAsc(lPos) <= &HCF) Then
	   	   	   	   	   Exit Do
	   	   	   	   End If

	   	   	   	   ' otherwise keep looking
	   	   	   	   lPos = lPos + Mult(BinToAsc(lPos + 2), BinToAsc(lPos + 1))

	   	   	   	   ' check for end of buffer
	   	   	   	   If lPos >= BUFFERSIZE - 10 Then Exit Sub

	   	   	   Loop

	   	   	   ' If we've gotten this far it is a JPEG and we are ready
	   	   	   ' to grab the information.

	   	   	   m_ImageType = "jpg"

	   	   	   ' get the height
	   	   	   m_Height = Mult(BinToAsc(lPos + 5), BinToAsc(lPos + 4))

	   	   	   ' get the width
	   	   	   m_Width = Mult(BinToAsc(lPos + 7), BinToAsc(lPos + 6))

	   	   	   ' get the color depth
	   	   	   m_Depth = BinToAsc(lPos + 8) * 8

	   	   End If
	   End Sub

End Class

Function fnThumImgView(sImgSrc, nMaxW, nMaxH)

	Dim sImgTag : sImgTag = ""
	Dim sImgPath : sImgPath = ""
	Dim FSO, sFilePath
	sImgSrc = "/upload/" & sImgSrc
	sImgPath = replace(sImgSrc, "/", "\")
	sFilePath = server.MapPath("/") & sImgPath

	Set FSO = CreateObject("Scripting.FileSystemObject")

	If FSO.FileExists(sFilePath) then
		Dim img_size, img_width, img_height
		Dim Image, iType, iWidth, iHeight, FileSize		
		Set Image = new ImageClass
		With Image
			   .LoadFilePath(sFilePath)
					''''''''''''''''''''''''.LoadBinary("바이너리로 읽었을때...")
			   .ImageRead
			   iType = .ImageType
			   img_width = .Width
			   img_height = .Height
		End With
		Set Image = Nothing 

		'최대 크기
		if img_width > nMaxW then
			img_height = CLng(img_height * (nMaxW/img_width))
			'img_height = img_height + 30
			img_width = nMaxW
		end if
		
		if img_height > nMaxH then
			img_width= CLng(img_width * (nMaxH/img_height))
			'img_width = img_width + 30
			img_height = nMaxH
		end if 
		sImgTag = "<div class='lyThumImg'><img src='" & sImgSrc & "' height=" & img_height & " width=" & img_width & " ></div>"
	else
		sImgTag = "<div class='lyThumImgText' style='padding:" & (nMaxH/2-10) & "px 0px;width:"&nMaxW&"px;'>No Img</div>"
	End If	
	Set FSO = nothing	
	fnThumImgView = sImgTag
end  function
Function fnImgView(sImgSrc, nMaxW, nMaxH)

	Dim sImgTag : sImgTag = ""
	Dim sImgPath : sImgPath = ""
	Dim FSO, sFilePath
	sImgSrc = "/upload/" & sImgSrc
	sImgPath = replace(sImgSrc, "/", "\")
	sFilePath = server.MapPath("/") & sImgPath

	Set FSO = CreateObject("Scripting.FileSystemObject")

	If FSO.FileExists(sFilePath) then
		Dim img_size, img_width, img_height
		Dim Image, iType, iWidth, iHeight, FileSize		
		Set Image = new ImageClass
		With Image
			   .LoadFilePath(sFilePath)
					''''''''''''''''''''''''.LoadBinary("바이너리로 읽었을때...")
			   .ImageRead
			   iType = .ImageType
			   img_width = .Width
			   img_height = .Height
		End With
		Set Image = Nothing 

		'최대 크기
		if img_width > nMaxW then
			img_height = CLng(img_height * (nMaxW/img_width))
			'img_height = img_height + 30
			img_width = nMaxW
		end if
		
		if img_height > nMaxH then
			img_width= CLng(img_width * (nMaxH/img_height))
			'img_width = img_width + 30
			img_height = nMaxH
		end if 
		sImgTag = "<div class='lyImg'><img src='" & sImgSrc & "' height=" & img_height & " width=" & img_width & "  style='cursor:pointer;' onclick=""fnPopImg('" & sImgSrc & "');""></div>"
	else
		sImgTag = "<div class='lyImgText' style='padding:" & (nMaxH/2-10) & "px 0px;width:"&nMaxW&"px'>No Img</div>"
	End If	
	Set FSO = nothing	
	fnImgView = sImgTag
end  function

function fnImgHSize(sImgSrc)
	Dim nImgH : nImgH = 0
	Dim sFilePath : sFilePath = sImgSrc
	Dim FSO

	Set FSO = CreateObject("Scripting.FileSystemObject")
	sFilePath = server.mappath("\upload")  & "\" & replace(sFilePath, "/", "\")

	If FSO.FileExists(sFilePath) then
		Dim img_size, img_width, img_height
		Dim Image, iType, iWidth, iHeight, FileSize		
		Set Image = new ImageClass
		With Image
			   .LoadFilePath(sFilePath)					''''''''''''''''''''''''.LoadBinary("바이너리로 읽었을때...")
			   .ImageRead
			   iType = .ImageType
			   img_width = .Width
			   nImgH = .Height
		End With
		Set Image = Nothing 
	End If	
	Set FSO = nothing	
	fnImgHSize = nImgH
end function
%>