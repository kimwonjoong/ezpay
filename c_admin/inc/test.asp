<% 
'---------------------------------------------------------------------------- 
' 프로그램 [ASP] HTTP Header를 이용한 통신사 분기처리 및 기본 폰정보 얻기 
'              신형폰 위주(NATEm, KUN, LION 브라우저)로 개발 / 나머지 비지원 처리 
' 작성자    박유경(Pary Yu Kyung) 
' 작성일자 2008. 3. 31 
'---------------------------------------------------------------------------- 
Response.AddHeader "Cache-Control", "no-cache" 
Response.AddHeader "Expires", "0" 
Response.AddHeader "Pragma", "no-cache" 


Dim userAgent, platformInfo, xup_subNo, deviceInfo, phoneCookie 
Dim browser, company, markup, sw 
Dim company_list, browser_list, markup_list 
Dim phonenumber :   phonenumber = Request.ServerVariables("HTTP_HTTP_PHONE_NUMBER")
Dim phonenumber2 : phonenumber2 = Request.ServerVariables("HTTP_PHONE_NUMBER")

allRaw    = request.ServerVariables("ALL_RAW")      'Header 전체 정보 
userAgent = request.ServerVariables("HTTP_USER-AGENT")    '3사 공통 Header 
phoneCookie = request.ServerVariables("HTTP_COOKIE")    
xup_subNo  = request.ServerVariables("HTTP_X-UP-SUBNO")    'only LGT(모든 단말) 
wapUserInfo = request.ServerVariables("HTTP_WAP-UserInfo")    'only LGT(WAP2.0폰 / 폰번호 및 브라우저정보 추출 가능) 

response.write phonenumber & "<br/>"
response.write phonenumber2 & "<br/>"
response.write allRaw & "<br/>"
response.write phoneCookie & "<br/>"
response.write userAgent & "<br/>"
response.write xup_subNo & "<br/>"
response.write wapUserInfo & "<br/>"

'기본값 세팅 
browser = "etc"  
markup  = "html"  
company = "none" 
sw  = "000"  'screen width 

'서비스 가능조건 목록 
company_list = "SKT; LGT; KTF; SK; KT; LG; " 
browser_list = "NATEm; KUN; LION;" 
markup_list  = "xhtml; khtml;" 
skt_list    = "010; SKT; STI; LGT; KTF; HSP; SK; KT; LG; " 

'----------------- 
'1. 통신사 체크 
'----------------- 
if (xup_subNo <> "" ) then  '(LGT폰) 체크부터 해야함(ua의 첫번째자리가 I인 경우의 LGT 단말도 있음) 

 company = "LGT"                                            
 if mid(userAgent, 2, 1) >= 5  then '서비스 방식 5: WML2.0 
  browser = "LION"  'LION Browser          
        markup  = "xhtml"  
 elseif mid(userAgent, 2, 1) = 3 or mid(userAgent, 2, 1) = 4 then 
  browser = "UP"    'UP 브라우저 단말        
        markup  = "wml"  
 else 
  browser = "etc"    '구버젼 브라우저          
        markup  = "wml"  
 end if 

 'Screen Width 가져오기(정상) 
    if left(userAgent, 3) = "152"  then 'WAP2.0 
    sw = mid(userAgent, 17, 3) 
 elseif left(userAgent, 3) = "122" then 'WAP1.0 
    sw = mid(userAgent, 13, 3) 
 else 
  sw = "000" 
 end if 

elseif (Instr(1, userAgent, "CellPhone")  > 0 ) then '(KTF폰) 

 company = "KTF"            '//모질라 계열을 사용하는 통신사는 KTF 이다 
 if (Instr(1, userAgent, "KUN")  > 0 )  then  'KUN브라우저 
  browser = "KUN"            '//쿤 브라우저 (KUN  Browser) 
        markup  = "khtml"          '//KUN의 html은  khtml 
 elseif (Instr(1, userAgent, "MSMB")  > 0 ) then 'ME브라우저 
  browser = "ME"            '//모바일 익스플로러  (Mobile Explorer) 
        markup  = "chtml"          '//ME의 html은 chtml 
 else 
  browser = "etc"            
        markup  = "none"                                          
 end if 
 '********************************************************************************* 
 'HTTP_DEVICE_INFO 값을 이용해 Screen Width 가져오기(비정상 : asp에선 못가져 옴 ㅡㅡ;) 
 'HTTP_RAW를 이용해 전체를 가져와 파싱해야 함. 
 '********************************************************************************* 
 dim pos1, pos2,http_deviceInfo,temp, LX 
 pos1 = Instr(allRaw, "HTTP_DEVICE_INFO") 
 if pos1 > 0 then 
  pos2 = Instr(pos1, allRaw, chr(13)) 
  http_deviceInfo = mid(allRaw, pos1, pos2 - pos1) 'HTTP_DEVICE_INFO : LX:320, LY:240, CL:16 
  temp = split(http_deviceInfo, ",", -1, 1) 
  if Ubound(temp) > 0 then 
  LX = split(temp(0), ":", -1, 1) 
  sw = lx(2) 
  else 
  sw = "000" 'error 
  end if 
 else  '구형폰은 deviceInfo가 안넘어온다.(구형폰 무시) 
  sw = "000" 

 end if 

elseif (Instr(1, skt_list, left(userAgent, 3)) > 0)  Or (left(userAgent, 1) = "I") then '(SKT폰) 

 company = "SKT"      
 if (mid(userAgent, 10, 2) >= 35 ) then 'Infraware 2.3 이상 
  browser = "NATEm"                                          
  markup  = "xhtml"                                          
 else 
  browser = "etc"                                          
  markup  = "wml"                                          
 end if 

 'Screen Width 가져오기 
 sw = mid(userAgent, 14, 3) 
else 

 '이하 모바일용 브라우저 아님 
    company = "none" 
 browser = "etc"                                    
    markup  = "html"                                          
 sw  = "000" 

end if 

' 컨텐츠 기반 서비스에서 LCD 사이즈별로, 컨텐츠 사이즈를 달리하기 위함 
if sw >= 320 then 
 screenW = "320" 
elseif sw >= 240 and sw < 320 then 
 screenW = "240" 
elseif sw >= 176 and sw < 240 then 
 screenW = "176" 
elseif sw < 176 then 
 screenW = "000" 
end if 

'----------------------- 
'2. 서비스 불가단말 분리 
'----------------------- 
if (Instr(1, company_list, company) = 0) or (Instr(1, browser_list, browser) = 0) or (Instr(1, markup_list, markup) = 0) or (Cint(sw) < 176) then 
  Response.write "company="&company&"&<br/>browser="&browser&"&<br/>markup="&markup&"&<br/>screen_width="&sw 
 'Response.Redirect "./test2.asp?company="&company&"&browser="&browser&"&markup="&markup&"&screen_width="&sw 
 Response.End 
  
'---------------------------------- 
'3. 이후 서비스 가능단말 프로세스 시작 
'---------------------------------- 
else 
 'Response.Redirect "./service_main.asp"  '서비스 메인 페이지로 이동처리 
 'Response.End 
 if markup = "xhtml" then 'xhtml dtd 추가 
  Dim xhtml_dtd 
  xhtml_dtd = "<?xml version=""1.0"" encoding=""KS_C_5601-1987""?> "& chr(10) 
  xhtml_dtd = xhtml_dtd & "<!DOCTYPE html PUBLIC ""-//W3C//DTD XHTML Basic 1.0//EN"" ""http://www.w3.org/TR/xhtml-basic/xhtml-basic10.dtd"">" 
  Response.Write(xhtml_dtd) 
 end if 

  With Response 
  .Write "<html>"& chr(10) 
  .Write "<body title=""폰체크결과보기"">"& chr(10) 
  .Write "<div>"& chr(10) 
  .Write "company : "& company &"<br/>"& chr(10) 
  .Write "browser : "& browser &"<br/>"& chr(10) 
  .Write "markup : "& markup &"<br/>"& chr(10) 
  .Write "screenWidth : "& sw & "<br/><br/>"& chr(10) 
  .Write Request.ServerVariables("ALL_HTTP") &"<br/>"& chr(10) 
  .Write "</div>"& chr(10) 
  .Write "</body>"& chr(10) 
  .Write "</html>" 
  End With 

end if 
%> 