<%
session.CodePage = 65001
response.Charset = "UTF-8"



fnDBConn()

strSQL = " select gcoin,USD,JPY,CNY,PHP,HKD,SGD,MYR from member where wallet_no='" & Session("SS_WALLET") & "'"

cmd.CommandText = strSQL
Set rs = cmd.execute()
if not(rs.eof or rs.bof) then
	gcoin = rs("gcoin")
	USD = rs("USD")
	JPY = rs("JPY")
	CNY = rs("CNY")
	PHP = rs("PHP")
	HKD = rs("HKD")
	SGD = rs("SGD")
	MYR = rs("MYR")
end if
rs.close
Set rs = nothing


fnDBClose()

%>
	<!--div class="container"-->
		<section class="pv-20 stats padding-bottom-clear parallax dark-translucent-bg hovered  hidden-xs">
			<div class="clearfix">
				<div class="col-md-1 col-xs-6 text-center">
					<div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
						<span class="icon dark-bg circle"> <img src="template/images/nations/south-korea.png"></span>	<span class="counter" data-to="<%=gcoin%>" data-speed="2000">0</span> EZ
					</div>
				</div>
				<div class="col-md-1 col-xs-6 text-center">
					<div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
						<span class="icon dark-bg circle"> <img src="template/images/nations/united-states.png"></span></span>
						<span class="counter" data-to="<%=USD%>" data-speed="2000" data-decimals="2">0</span> USD
					</div>
				</div>
				<div class="col-md-1 col-xs-6 text-center">
					<div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
						<span class="icon dark-bg circle"> <img src="template/images/nations/japan.png"></span></span>
						<span class="counter" data-to="<%=JPY%>" data-speed="2000">0</span> JPY
					</div>
				</div>
				<div class="col-md-1 col-xs-6 text-center">
					<div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
						<span class="icon dark-bg circle"> <img src="template/images/nations/china.png"></span></span>
						<span class="counter" data-to="<%=CNY%>" data-speed="2000">0</span> CNY
					</div>
				</div>
				<div class="col-md-1 col-xs-6 text-center">
					<div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
						<span class="icon dark-bg circle"> <img src="template/images/nations/philippines.png"></span></span>
						<span class="counter" data-to="<%=PHP%>" data-speed="2000">0</span> PHP
					</div>
				</div>
				<div class="col-md-1 col-xs-6 text-center">
					<div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
						<span class="icon dark-bg circle"> <img src="template/images/nations/hong-kong.png"></span></span>
						<span class="counter" data-to="<%=HKD%>" data-speed="2000" data-decimals="2">0</span> HKD
					</div>
				</div>
				<div class="col-md-1 col-xs-6 text-center">
					<div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
						<span class="icon dark-bg circle"> <img src="template/images/nations/singapore.png"></span></span>
						<span class="counter" data-to="<%=SGD%>" data-speed="2000" data-decimals="2">0</span> SGD
					</div>
				</div>
				<div class="col-md-1 col-xs-6 text-center">
					<div class="feature-box object-non-visible" data-animation-effect="fadeIn" data-effect-delay="300">
						<span class="icon dark-bg circle"> <img src="template/images/nations/malaysia.png"></span></span>
						<span class="counter" data-to="<%=MYR%>" data-speed="2000">0</span> MYR
					</div>
				</div>
			</div>
		</section>		
	<!--/div-->
