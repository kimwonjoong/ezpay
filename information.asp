<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->
<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 
%>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY | <%=EZ_LAN_75%></title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<script type="text/javascript">
		<!--
		//[] <--문자 범위 [^] <--부정 [0-9] <-- 숫자  
		//[0-9] => \d , [^0-9] => \D
		var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
		var rgx2 = /(\d+)(\d{3})/; 

		function getNumber(obj){
			
			 var num01;
			 var num02;
			 num01 = obj.value;
			 num02 = num01.replace(rgx1,"");
			 num01 = setComma(num02);
			 obj.value =  num01;

		}

		function setComma(inNum){
			 
			 var outNum;
			 outNum = inNum; 
			 while (rgx2.test(outNum)) {
				  outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
			  }
			 return outNum;

		}
		//-->
		</script>

		
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active"><%=EZ_LAN_75%></li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- banner start -->
				<!-- section start -->
			
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title"><%=EZ_LAN_75%></h1>
					<div class="separator-2"></div>
					<table class="table table-striped table-colored">
						<thead>
							<tr>
								<th><%=EZ_LAN_70%></th>
								<th><%=EZ_LAN_91%></th>
								<th><%=EZ_LAN_92%></th>
								<th><%=EZ_LAN_93%></th>
								<th><%=EZ_LAN_94%></th>
							</tr>
						</thead>
						<tbody>
						<%

						fnDBConn()

						Dim bank_name,account_no,input_name

						'지금 핀이 사용가능한 핀인지
						strSQL = " select 'charge' as category,wallet_no,gcoin,result,mode,'' as s_mode,'' as change_coin,regist_date from gcoin_buy where wallet_no='" & Session("SS_USERID") & "' union  select 'send' as type,receive_wallet,gcoin,result,mode,'' as s_mode,'' as change_coin,regist_date from gcoin_transfer where send_wallet='" & Session("SS_USERID") & "' union  select 'receive' as type,send_wallet,gcoin,result,mode,'' as s_mode,'' as change_coin,regist_date from gcoin_transfer where receive_wallet='" & Session("SS_USERID") & "' union  select 'settlement' as type,wallet_no,gcoin,result,mode,'' as s_mode,'' as change_coin,regist_date from gcoin_settlement where wallet_no='" & Session("SS_USERID") & "'  union  select 'transfer' as type,wallet_no,gcoin,result,mode,'' as s_mode,'' as change_coin,regist_date from gcoin_settlement2 where wallet_no='" & Session("SS_USERID") & "' union  select 'switch' as type,wallet_no,gcoin,'1' as result,mode,s_mode,change_coin,regist_date from gcoin_transfer_log where wallet_no='" & Session("SS_USERID") & "' order by regist_date desc"
	

						'Response.write strSQL
						cmd.CommandText = strSQL
						Set rs = cmd.execute()

						if not(rs.eof or rs.bof) then
							nData = fnRs2JsonArr(aData, rs)
						end if

						rs.close
							
						fnDBClose()

						if nData > 0 then
							for i=0 to nData
							
						%>
							<tr>
								<td><%=aData("wallet_no")(i)%>
								<% If aData("category")(i) = "send" Or aData("category")(i) = "receive"  then%>
								<%
											'지금 핀이 사용가능한 핀인지
									fnDBConn()

									strSQL = " select s_name from member where wallet_no='"& aData("wallet_no")(i) &"'"
									cmd.CommandText = strSQL
									Set rs_m = cmd.execute()
									If rs_m.eof Or rs_m.bof Then
									else
										If rs_m("s_name") = "" Then
											s_name = ""
										else
											s_name = rs_m("s_name")
										End If
									End If

									rs_m.close
							
									fnDBClose()

								%>
								(<%=s_name%>)
								<% End If %>
								</td>
								<td>
								<%=fnFormatNumber(aData("gcoin")(i),0,0)%>
								<% If aData("s_mode")(i) = "EZ" Or aData("s_mode")(i) = "B" Then %>
								EZ->
								<% elseIf aData("s_mode")(i) = "USD" Or aData("s_mode")(i) = "S" then%>
								USD->
								<% elseIf aData("s_mode")(i) = "JPY" Or aData("s_mode")(i) = "Y" then%>
								JPY->
								<% elseIf aData("s_mode")(i) = "CNY" Or aData("s_mode")(i) = "C" then%>
								CNY->
								<% elseIf aData("s_mode")(i) = "PHP" Or aData("s_mode")(i) = "P" then%>
								PHP->
								<% elseIf aData("s_mode")(i) = "HKD" Or aData("s_mode")(i) = "H" then%>
								HKD->
								<% elseIf aData("s_mode")(i) = "SGD" Or aData("s_mode")(i) = "G" then%>
								SGD->
								<% elseIf aData("s_mode")(i) = "MYR" Or aData("s_mode")(i) = "M" then%>
								MYR->
								<% End If %>
								<% If aData("category")(i) = "switch" then%>
									<%=fnFormatNumber(aData("change_coin")(i),0,0)%>
								<% End If %>
								<% If aData("mode")(i) = "EZ" Or aData("mode")(i) = "B" then%>
								EZ
								<% elseIf aData("mode")(i) = "USD" Or aData("mode")(i) = "S" then%>
								USD
								<% elseIf aData("mode")(i) = "JPY" Or aData("mode")(i) = "Y" then%>
								JPY
								<% elseIf aData("mode")(i) = "CNY" Or aData("mode")(i) = "C" then%>
								CNY
								<% elseIf aData("mode")(i) = "PHP" Or aData("mode")(i) = "P" then%>
								PHP
								<% elseIf aData("mode")(i) = "HKD" Or aData("mode")(i) = "H" then%>
								HKD
								<% elseIf aData("mode")(i) = "SGD" Or aData("mode")(i) = "G" then%>
								SGD
								<% elseIf aData("mode")(i) = "MYR" Or aData("mode")(i) = "M" then%>
								MYR
								<% elseIf aData("mode")(i) = "" then%>
								EZ
								<% End If %>
								</td>
								<td>
								<% If aData("category")(i) = "send" then%>
								<button class="btn-u btn-u-purple" type="button"><i class="fa  fa-chevron-circle-right"> <%=EZ_LAN_95%></i></button>
								<% elseIf aData("category")(i) = "receive" then%>
								<button class="btn-u btn-u-yellow" type="button"><i class="fa  fa-chevron-circle-left"> <%=EZ_LAN_96%></i></button>
								<% elseIf aData("category")(i) = "charge" then%>
								<button class="btn-u btn-u-green" type="button"><i class="fa fa-plus-square"> <%=EZ_LAN_1%></i></button>
								<% elseIf aData("category")(i) = "settlement" then%>
								<button class="btn-u btn-u-aqua" type="button"><i class="fa fa-money"> <%=EZ_LAN_74%></i></button>
								<% elseIf aData("category")(i) = "transfer" then%>
								<button class="btn-u btn-u-blue" type="button"><i class="fa fa-star-o"> <%=EZ_LAN_97%></i></button>
								<% elseIf aData("category")(i) = "switch" then%>
								<button class="btn-u btn-u-green" type="button"><i class="fa fa-plus-square"> <%=EZ_LAN_98%></i></button>
								<% End If %>
								</td>
								<td>
								<% If aData("result")(i) = "1" then%>
								<button class="btn-u btn-u-blue" type="button"><i class="fa fa-check"> <%=EZ_LAN_99%></i></button>
								<% ElseIf aData("result")(i) = "0" then%>
								<button class="btn-u btn-u-red" type="button"><i class="fa fa-bolt"> <%=EZ_LAN_100%></i></button>
								<% Else %>
								<button class="btn-u btn-u-red" type="button"><i class="fa fa-bolt"> <%=EZ_LAN_101%></i></button>
								<% End If %>
								</td>
								<td><%=aData("regist_date")(i)%></td>
								
							</tr>
						<%
							
							Next
						else
						%>
							<tr>
								<td colspan="5"><%=EZ_LAN_102%></td>								
							</tr>
						<%
						End if
						%>
						</tbody>
					</table>
				</div>
			</div>
			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
