<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->
<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 
%>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY | <%=EZ_LAN_73%></title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<script type="text/javascript">
		<!--
		//[] <--문자 범위 [^] <--부정 [0-9] <-- 숫자  
		//[0-9] => \d , [^0-9] => \D
		var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
		var rgx2 = /(\d+)(\d{3})/; 

		function getNumber(obj){
			
			 var num01;
			 var num02;
			 num01 = obj.value;
			 num02 = num01.replace(rgx1,"");
			 num01 = setComma(num02);
			 obj.value =  num01;

		}

		function setComma(inNum){
			 
			 var outNum;
			 outNum = inNum; 
			 while (rgx2.test(outNum)) {
				  outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
			  }
			 return outNum;

		}
		//-->
		</script>

		<script type="text/javascript">
			function member_select(val)
			{
				//alert(val.value);
				var mode = val;
				if (mode.length > 0){

					if (mode == "M"){
						$("#receive_wallet").val("M");
					}
				}
			}

			function wallet_select(val)
			{
				//alert(val.value);
				var mode = val;
				if (mode.length > 0){
					if (mode.substring(0,1) == "M"){
						$("#m_check").val("M");
					}else{
						$("#m_check").val("G");
					}

					$("#receive_wallet").val(mode);
				}
			}
			function send_name() {
				var receive_wallet = $("#receive_wallet").val();
				receive_wallet = receive_wallet.toUpperCase();

				var gcoin_won = $("#gcoin").val();
				if (receive_wallet.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_130%>',
						callback: function(result) {
							//$("#receive_wallet").focus();
						}
					});
					
				}else{
					$.ajax({
						 url: "send_name.asp",
						 type: "POST",
						 data: { receive_wallet: receive_wallet },  

						 success: function(data){
							if (data == "") {
								modal({
									type: 'error',
									title: '<%=EZ_LAN_73%>',
									text: receive_wallet+'<%=EZ_LAN_128%>',
									callback: function(result) {
										if($("#m_check2").prop("checked")) {
											$("#receive_wallet").val('M');
										} else {
											$("#receive_wallet").val('');
										}
									}
								});	
							} else {
								modal({
									type: 'confirm',
									title: '<%=EZ_LAN_73%>',
									text: receive_wallet+'('+data+')' + '<%=EZ_LAN_129%>',
									callback: function(result) {
										//alert(result);
										if(!result){
											$("#receive_wallet").val('');
											$("#receive_wallet").focus();
										}
										$("#receive_wallet").val(receive_wallet);
										$("#r_name").val(data);
									}
								});	
							}
						
						 },
			 
						 complete: function(data){
						//	location.href="index.asp";
						 }
					});
				}
			}
			function send_ok()
			{
				var mode = $("#mode").val();
				var gcoin = $("#gcoin").val();
				gcoin = gcoin.replace(/[^\d]+/g, '');
				var send_wallet = $("#send_wallet").val();
				var receive_wallet = $("#receive_wallet").val();

				//alert(mode);

				if (receive_wallet.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_130%>',
						callback: function(result) {
							$("#receive_wallet").focus();
						}

					});
					
				}else if (mode.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: 'Select Transfer Unit',
						callback: function(result) {
							$("#mode").focus();
						}

					});
					
				}else if (gcoin.length == 0){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_131%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				}else if (mode == "EZ" && gcoin < 10000){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_132%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
				
				}else if (mode == "CNY" && gcoin < 50){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_206%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				
				}else if (mode == "USD" && gcoin < 1){			
			
					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_207%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
				
					
				}else if (mode == "JPY" && gcoin < 100){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_208%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});				

				}else if (mode == "PHP" && gcoin < 50){		

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_209%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					

				}else if (mode == "MYR" && gcoin < 5){
			
					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_210%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
									
				}else if (mode == "HKD" && gcoin < 7){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_211%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					

				}else if (mode == "SGD" && gcoin < 1){

					modal({
						type: 'info',
						title: '<%=EZ_LAN_73%>',
						text: '<%=EZ_LAN_212%>',
						callback: function(result) {
							$("#gcoin").focus();
						}
					});
					
				}else{
					var r_name = $("#r_name").val()
					if (mode=="EZ")
					{
						mode = "EZ";
					}
					modal({
						type: 'confirm',
						title: '<%=EZ_LAN_73%>',
						text: receive_wallet+'('+r_name+')<%=EZ_LAN_133%> ' + gcoin + mode + ' <%=EZ_LAN_133_1%>',
						callback: function(result) {
							//alert(result);
							if(result){			

								$.ajax({
									 url: "send_ok.asp",
									 type: "POST",
									 data: { mode: mode, gcoin: gcoin,send_wallet: send_wallet,receive_wallet: receive_wallet },  

									 success: function(data){
										 if(data == "YES"){						 
											modal({
												type: 'success',
												title: '<%=EZ_LAN_73%>',
												text: '<%=EZ_LAN_134%>',
												callback: function(result) {
													location.href="send.asp";
												}
											});		
										 }else{								 
											modal({
												type: 'error',
												title: '<%=EZ_LAN_73%>',
												text: data,
												callback: function(result) {
													location.href="send.asp";
												}
											});				
										 }
									 },
						 
									 complete: function(data){
									//	location.href="index.asp";
									 }
								});
							}
						}
					});	
				
				}	
			}

			function insert_money(money)
			{
				$("#gcoin").val(money);
			}
			
			function m_chr(flag) {
					$("#receive_wallet").val(flag);
			}
		</script>
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active"><%=EZ_LAN_73%></li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- banner start -->
				<!-- section start -->
			
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title"><%=EZ_LAN_73%></h1>
					<div class="separator-2"></div>
					<form role="form">
						<input type="hidden" name="send_wallet" id="send_wallet" value="<%=Session("SS_USERID")%>">
						<input type="hidden" name="r_name" id="r_name" value="">
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_135%></label>
							<select class="form-control" name="send_list" id="send_list" required onchange="wallet_select(this.value);" style="background-color:#575757;">
								<option value="">===<%=EZ_LAN_136%>===</option>
								<%

									fnDBConn()

									Dim receive_wallet,s_name

									'지금 핀이 사용가능한 핀인지
									strSQL =  "select t.receive_wallet,m.s_name from gcoin_transfer as t,member as m where t.send_wallet='"&Session("SS_USERID")&"' and t.receive_wallet=m.wallet_no"

									
									cmd.CommandText = strSQL
									Set rs = cmd.execute()

									if not(rs.eof or rs.bof) then
										nData = fnRs2JsonArr(aData, rs)
									end if

									rs.close
										
									fnDBClose()

									if nData > 0 then
										for i=0 to nData

								%>
									<option value="<%=aData("receive_wallet")(i)%>"><%=aData("receive_wallet")(i)%>(<%=aData("s_name")(i)%>)</option>
								<%
								
									Next
								else
								%>
									<option value=""><%=EZ_LAN_137%></option>
								<%
								End if
								%>
							</select>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_138%></label>
							<p class="form-control">
							<span style="padding:0 20px 0 0;"><input type="radio" value="G" name="m_check" id="m_check1" checked  onclick="m_chr('')"> <label for="m_check1"><%=EZ_LAN_139%></label></span><span><input type="radio" value="M" name="m_check" id="m_check2" onclick="m_chr('M')"> <label for="m_check2"><%=EZ_LAN_140%></label></span>
							</p>
							<!--
							<select class="form-control" name="m_check" id="m_check" required onchange="member_select(this.value);">
								<option value="">===선택===</option>
								<option value="G">일반회원</option>
								<option value="M">가맹점</option>
							</select>
							-->
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_141%></label>
							<input type="text" class="form-control" name="receive_wallet" id="receive_wallet" required onblur="send_name();">		
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_142%></label>
							<select class="form-control" name="mode" id="mode" required style="background-color:#575757;">
								<option value="EZ">EZ</option>
								<option value="USD">USD</option>
								<option value="JPY">JPY</option>
								<option value="CNY">CNY</option>
								<option value="PHP">PHP</option>
								<option value="HKD">HKD</option>
								<option value="SGD">SGD</option>
								<option value="MYR">MYR</option>
							</select>
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1"><%=EZ_LAN_143%></label>
							<input type="tel" class="form-control" id="gcoin" name="gcoin" required onchange="getNumber(this);" onkeyup="getNumber(this);">
						</div>

						
						
						<div class="alert alert-info alert-dismissible" role="alert">
							<button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">×</span><span class="sr-only">Close</span></button>
							<b style="color:red;"><%If Session("SS_premium") = "Y" Then%><%=EZ_LAN_144%> <%=EZ_LAN_145%><%Else%><%=EZ_LAN_146%><%End If%><%=EZ_LAN_147%></b>
						</div>
					
						<button type="button" class="btn btn-default"  onclick="send_ok();"><%=EZ_LAN_73%></button>
					</form>
				</div>
			</div>
			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
