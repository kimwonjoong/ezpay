<!--METADATA TYPE= "typelib"  NAME= "ADODB Type Library" FILE="C:\Program Files\Common Files\SYSTEM\ADO\msado15.dll"  -->
<%
session.CodePage = 65001
response.charset = "utf-8"
%>
<!--#include virtual="/admin2/inc/inc_db_lib.asp"-->
<!--#include virtual="/admin2/inc/JSON_2.0.4.asp" -->
<script runat="server" Language="Javascript" src="/admin2/inc/JSON_JS.asp"></script>
<script Language="Javascript" Type="Text/Javascript" Runat="Server">
 function fEncodeURI(str)
 {
  return encodeURIComponent(str);
 }


 function fDecodeURI(str)
 {
  return decodeURIComponent(str);
 }
</script>
<%
Dim sMenuCode, PAGE_CODE_, PAGE_TITLE_, sMnStyle
Dim SS_USER_ID, SS_GROUP, SS_USER_NAME, sModiChk, SS_STORE_CODe, SS_STORE_IDX, SS_APP_ID
Dim sSSAdminChk

Dim sql, rs, i, j, sTableName, sqlTable
Dim aData, nData : nData = -1
Dim UPLOADFORM
Dim aFeeType, nFeeType : nFeeType = 0

'페이징
Dim sFDList : sFDList = ""	'SELECT 필드
Dim sListFileName, sMsg, sMode, sModeStr, sFilter, sTitleName
Dim nPageNO, nPageSize, nPageCount, nRecordCount, nPageNO2
Dim nCurrentBlockpage, nBlockSize, nTempPage, sQueryString, sListQueryString

'검색조건
Dim sTableChk
Dim sStartDate, sEndDate, nStartDate, nEndDate

'스타일설정
Dim sVMode, bLayout, sStyleCss, sHtml, sBtmCss
Dim sCaption1, sCaption2
Dim sBTableStyle, sNBTableStyle, sTitleStyle, sCaptionStyle, sColStyle, aDataStyle(4), aSumTxtStyle(3), aSumStyle(3), sListMSG

Dim HOST_, DOMAIN_, TITLE_, MENU_IDX_
Dim SESSION_ID_
Dim MAIN_URL : MAIN_URL = "/index.asp"
Dim LOGIN_URL : LOGIN_URL = "/admin/login.asp"
Dim sEMsg : sEMsg = ""
Dim CURR_DATE_ : CURR_DATE_ = date()
Dim sAppID : sAppID = ""
Dim oItem, sItemKey, sItemValue
Dim CURR_DATE : CURR_DATE = date()

HOST_ = "175.126.62.150:8083"
DOMAIN_ = "175.126.62.150:8083"
TITLE_ = "G코인 APP"
MENU_IDX_ = 0
PAGE_CODE_ = ""
PAGE_TITLE_ = ""
nBlockSize = 10
nPageSize = 20

'세션데이터
SESSION_ID_ = trim(session.sessionid)
SS_APP_ID = fnNullInit(Trim(Session("SS_APPID")))
SS_USER_ID = fnNullInit(Trim(Session("SS_USERID")))
SS_GROUP = fnNullInit(Trim(Session("SS_GROUP")))
SS_USER_NAME = fnNullInit(Trim(Session("SS_USERNAME")))
SS_STORE_CODE = fnNullInit(Trim(Session("SS_STORE_CODE")))
SS_STORE_IDX = fnNullInit(Trim(Session("SS_STORE_IDX")))

sVMode = ""
sListFileName = ""

sMnStyle = "font-weight:bold;"
sStyleCss = "<link href='../css/default.css' rel='stylesheet' type='text/css'>"
sHtml = "<html>"
sCaption1 = "<img src='../images/common/mb_icon01.gif' width='20' height='20'>"
sCaption2 = "<img src='../images/common/mb_icon02.gif' width='8' height='5'>"

sBTableStyle = " border='1' bordercolor='#555555' style='border-collapse:collapse' cellspacing='0' cellpadding='0' "	
sNBTableStyle = " border='0' bordercolor='#555555' style='border-collapse:collapse' cellspacing='0' cellpadding='0' "	
sTitleStyle = " style='font-size:12pt;font-weight:bold;color:#555555;padding:5px 3px 5px 3px;text-align:center;height:50px;height:27.5pt;border-right:0px;border-top:0px;border-left:0px;border-bottom:0px' "
sCaptionStyle = " style='font-size:10pt;font-weight:bold;color:#555555;padding:5px 3px 5px 3px;text-align:left;height:50px;height:17.5pt;border-right:0px;border-top:0px;border-left:0px;border-bottom:0px' "

sColStyle = " style='background:#e1ecfc;font-size:9pt;font-weight:bold;height:24px;height:18.0pt;text-align:center' "

aDataStyle(0) = " style=""text-align:center;background:#ffffff;font-size:9pt;height:20px;height:15.0pt;mso-number-format:'\@'"" "
aDataStyle(1) = " style=""text-align:left;background:#ffffff;font-size:9pt;height:20px;height:15.0pt;padding-left:4px;height:20px;mso-number-format:'\@'"" "
aDataStyle(2) = " style='text-align:right;background:#ffffff;font-size:9pt;height:20px;height:15.0pt;padding-right:4px;' "
aDataStyle(3) = " style='background:#d3e5ff;font-size:9pt;height:20px;height:15.0pt;text-align:center' "

aSumTxtStyle(0) = " style='background:#aeccf4;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:center' "
aSumTxtStyle(1) = " style='background:#d3e5ff;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:center' "
aSumTxtStyle(2) = " style='background:#d6e284;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:center' "
aSumStyle(0) = " style='background:#aeccf4;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:right;padding-right:4px;' "
aSumStyle(1) = " style='background:#d3e5ff;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:right;padding-right:4px;' "
aSumStyle(2) = " style='background:#d6e284;font-size:9pt;height:24px;height:18.0pt;font-weight:bold;text-align:right;padding-right:4px;' "

sListMSG = " style='background:#ffffff;font-size:9pt;height:200px;text-align:center;' "

if sVMode = "print" then
	Response.write "<script defer> "
	Response.write "	function fnPrint(bArgLayout) "
	Response.write "	{ parent.document.getElementById('ifrSaveAs').contentWindow.focus();"
	Response.write "	/*document.getElementById('ly_prt_bottom').style.display = 'none';*/"
	Response.write "	factory.printing.header = ''; /* Header에 들어갈 문장 */ "
	Response.write "	factory.printing.footer = ''; /* Footer에 들어갈 문장 */ "
	Response.write "	factory.printing.portrait = bArgLayout; /* true 면 세로인쇄, false 면 가로 인쇄 */ "
	Response.write "	factory.printing.leftMargin = 10.0; /* 왼쪽 여백 사이즈 */ "
	Response.write "	factory.printing.topMargin = 10.0; /* 위 여백 사이즈 */ "
	Response.write "	factory.printing.rightMargin = 10.0; /* 오른쪽 여백 사이즈 */ "
	Response.write "	factory.printing.bottomMargin = 10.0; /* 아래 여백 사이즈 */ "
	Response.write "	/*factory.printing.paperSize = 'A4';*/ /* 용지 사이즈 */ "
	Response.write "	/*factory.printing.SetMarginMeasure(2);*/ /* 테두리 여백 사이즈 단위를 인치로 설정합니다. */ "
	Response.write "	/*factory.printing.SetPageRange(false, 1, 3);*/ /* True로 설정하고 1, 3이면 1페이지에서 3페이지까지 출력 */ "
	Response.write "	factory.printing.Preview(); /* 미리보기 */ "
	Response.write "	/*document.getElementById('ly_prt_bottom').style.display = 'block';*/"
	Response.write "	/* factory.printing.Print(true, window); /* 출력하기 */ "
	Response.write "	} "
	Response.write "</script>"
elseif sVMode = "xls" then	
	response.buffer = False
	
	Response.ContentType = "application/vnd.ms-excel"
	Response.CacheControl="public"
	Response.AddHeader "Content-disposition","attachment;filename=" & sListFileName & "_" & replace(date(),"-","") & ".xls"
elseif sVMode = "doc" then
	response.buffer = false
	sVMode = "xls"

	sHtml = ""
	sStyleCss = "<style type='text/css'>"
	sStyleCss = sStyleCss & "html, body {font-size: 12px;line-height: 1.5em;font-family: Dotum, 돋움, sans-serif;height:100%;width:100%;}"
	sStyleCss = sStyleCss & "* { margin:0;padding:0; }"
	sStyleCss = sStyleCss & "p, div, th, td,input,select {color: #333333;font-size: 12px;font-family: Dotum, 돋움, sans-serif;}"
	sStyleCss = sStyleCss & "h1{color: #333333;font-size: 14px;font-weight:bold;font-family: Dotum, 돋움, sans-serif;}"	
	sStyleCss = sStyleCss & "table.hnbd2 {}"
	sStyleCss = sStyleCss & "table.hnbd2 th {height:30px; backgroundColor:#eeeeee;}"
	sStyleCss = sStyleCss & "table.hnbd2 td {color:#005f68; font-size:11px; font-weight:bold; text-align:center; line-height:1.2;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.nmgl {margin:0px;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.top {border-top:1px solid #c0dbde;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.lln {border-left:1px solid #c0dbde;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.btm {border-bottom:1px solid #c0dbde; border-right:1px solid #c0dbde;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.btm1 {border-bottom:0;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.bgln {backgroundColor:#dddddd}"
	sStyleCss = sStyleCss & "table.hnbd2 td.bgcl { background:#eaf4f5;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.n {text-align:left;padding-left:10px;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.c {text-align:center;font-size:12px; color:#363636; font-weight:normal;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.r12 {text-align:right;padding-right:5px; color:#363636; font-size:12px; font-weight:normal;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.ro12 {text-align:right;padding-right:5px; color:#d34600; font-size:12px; font-weight:normal;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.l12 {text-align:left;padding-left:20px; color:#363636; font-size:12px; font-weight:normal;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.12b {text-align:left;padding-left:10px; font-size:12px; font-weight:bold;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.c12b {text-align:center;font-size:12px; font-weight:bold;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.r12b {text-align:right;padding-right:5px; font-size:12px; font-weight:bold;}"
	sStyleCss = sStyleCss & "table.hnbd2 td.l12b {text-align:left;padding-left:10px; font-size:12px; font-weight:bold;}"
	sStyleCss = sStyleCss & "</style>"
	Response.ContentType = "application/msword"
	Response.CacheControl="public"
	Response.AddHeader "Content-disposition","attachment;filename=clm_" & sListFileName & "_" & replace(date(),"-","") & ".doc"
end if

sModiChk = "N"
sSSAdminChk = "N"

if SS_GROUP <> "관리자" and SS_GROUP <> "아파트" then
	SS_GROUP = ""
else	
	If SS_GROUP = "관리자" Then
		sSSAdminChk = "Y"
		sModiChk = "Y"
	End If
end if

'사용권한(로그인) 체크
SUB	sbMemberAuthChk(sArgPageLevel)
	Dim sUserID, sGroup
	Dim sPageURL
	
	sPageURL = "/admin/member_logout.asp"

	sUserID = Trim(Session("SS_USERID"))
	sGroup = Trim(Session("SS_GROUP"))

	if isNull(sGroup) Or (sGroup <> "관리자" and sGroup <> "아파트") then
		sGroup = ""
	end if

	if sArgPageLevel = "" then

	elseif (sArgPageLevel <> "관리자" and sArgPageLevel <> "아파트") then
		sArgPageLevel = "관리자"
	end if
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "top.location.href='" & sPageURL & "';"
		response.write "</script>"
		Response.End
	end if	
	
	'전체가능 이거나 관리자인경우
	If sArgPageLevel = "" Or sGroup = "관리자" Then

	'A
	ElseIf sArgPageLevel = "관리자" And sGroup <> "관리자" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	'A + C
	ElseIf sArgPageLevel = "아파트" And sGroup <> "아파트" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	ElseIf sGroup = "" then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	end if

END SUB

'팝업페이지 사용권한(로그인) 체크
SUB	sbPopMemberAuthChk(sArgPageLevel)
	Dim sUserID, sGroup
	Dim sPageURL
		
	sPageURL = "/admin/member_logout.asp"

	sUserID = Trim(Session("SS_USERID"))
	sGroup = Trim(Session("SS_GROUP"))

	if isNull(sGroup) Or (sGroup <> "관리자" and sGroup <> "아파트") then
		sGroup = ""
	end if
	
	if sArgPageLevel = "" then

	elseif (sArgPageLevel <> "관리자" and sArgPageLevel <> "아파트") then
		sArgPageLevel = "관리자"
	end if
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "opener.location.href='" & sPageURL & "';"
		response.write "self.close();"
		response.write "</script>"
		Response.End
	end if
	
	'전체가능 이거나 관리자인경우
	If sArgPageLevel = "" Or sGroup = "관리자" Then

	'A
	ElseIf sArgPageLevel = "관리자" And sGroup <> "관리자" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "opener.location.href='/admin/main.asp';"
		response.write "self.close();"
		response.write "</script>"
		Response.End
	'A + C
	ElseIf sArgPageLevel = "아파트" And sGroup <> "아파트" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "opener.location.href='/admin/main.asp';"
		response.write "self.close();"
		response.write "</script>"
		Response.End
	ElseIf sGroup = "" then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "opener.location.href='/admin/main.asp';"
		response.write "self.close();"
		response.write "</script>"
		Response.End
	end if
END Sub


'사용권한(로그인) 체크 (특정아이디만 사용가능)
SUB	sbMemberIDAuthChk(sArgUserID)
	Dim sUserID, sGroup
	Dim sPageURL
	
	sPageURL = "/admin/member_logout.asp"

	sUserID = Trim(Session("SS_USERID"))
	sGroup = Trim(Session("SS_GROUP"))	
	
	if isNull(sGroup) Or (sGroup <> "A" and sGroup <> "B" and sGroup <> "C" and sGroup <> "E") then
		sGroup = ""
	end If
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "top.location.href='" & sPageURL & "';"
		response.write "</script>"
		Response.End
	end if
	
	If sArgUserID <> sUserID Or sGroup = "" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	end if

END Sub

'사용권한(로그인) 체크 (특정아이디 권한없음)
SUB	sbMemberIDNOAuthChk(sArgUserID)
	Dim sUserID, sGroup
	Dim sPageURL
	
	sPageURL = "/admin/member_logout.asp"

	sUserID = Trim(Session("SS_USERID"))
	sGroup = Trim(Session("SS_GROUP"))	
	
	if isNull(sGroup) Or (sGroup <> "A" and sGroup <> "B" and sGroup <> "C" and sGroup <> "E") then
		sGroup = ""
	end If
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "top.location.href='" & sPageURL & "';"
		response.write "</script>"
		Response.End
	end if
	
	If sArgUserID = sUserID Or sGroup = "" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	end if

END Sub

'사용권한(로그인) 체크 (특정아이디만 사용가능) - 아이디 리스트
SUB	sbMemberIDSAuthChk(sArgUserID)
	Dim sUserID, sGroup
	Dim sPageURL
	Dim aUserList, nUserList, sAuthChk, i
	
	sPageURL = "/admin/member_logout.asp"

	sUserID = LCase(fnNullInit(Session("SS_USERID")))
	sGroup = fnNullInit(Session("SS_GROUP"))	
	
	if isNull(sGroup) Or (sGroup <> "A" and sGroup <> "B" and sGroup <> "C" and sGroup <> "E") then
		sGroup = ""
	end If
	
	aUserList = Split(sArgUserID, ",")
	nUserList = UBound(aUserList)
	sAuthChk = "False"
	
	If isNull(sUserID) or sUserID = "" then
		response.write "<script type='text/javascript'>"
		response.write "top.location.href='" & sPageURL & "';"
		response.write "</script>"
		Response.End
	end if

	For i=0 To nUserList
		If LCase(fnNullInit(aUserList(i))) = sUserID Then
			sAuthChk = "True"
		End if
	Next
	
	If sAuthChk = "False" Or sGroup = "" Then
		response.write "<script type='text/javascript'>"
		response.write "alert('사용권한이 없습니다.');"
		response.write "top.location.href='" & MAIN_URL & "';"
		response.write "</script>"
		Response.End
	end if

END Sub

'MUTI PUSH 발송(메세지, 전송 할 REG_ID(','구분), 전송 할 폰번호(','구분), 전송 실패 사유(','구분), 성공한 REG_ID(','구분), 성공한 폰번호(','구분))
Function fnOnePushSend(sMsg, sRegID, sPhone, aAppID)
	Dim APIKEY : APIKEY = "AIzaSyC-eisgmovdUQQgTwQ7f9hOFdrCvKqu0Lo"
	Dim sValue, aValue, nValue, i
	Dim sResultString : sResultString = ""
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)
	
	sRegID = fnNullInit(sRegID)
	sPhone = fnNullInit(sPhone)
	sMsg = fnNullInit(sMsg)
	aAppID = fnNullInit(aAppID)
	if sRegID <> "" and sPhone <> "" and sMsg <> "" and aAppID <> "" then
		Dim SEND_URL
		Dim seneHTTP
		SEND_URL = "https://android.googleapis.com/gcm/send"
		Set seneHTTP = Server.CreateObject("MSXML2.ServerXMLHTTP")
		seneHTTP.Open "POST",""& SEND_URL &"",false
		seneHTTP.SetRequestHeader "POST", ""& SEND_URL &" HTTP/1.0"
		seneHTTP.SetRequestHeader "Authorization", "key=" & APIKEY & ""
		'seneHTTP.SetRequestHeader "Content-Type", "application/json"									'멀티 전송시
		seneHTTP.SetRequestHeader "Content-Type", "application/x-www-form-urlencoded"		'단일 전송시

		If seneHTTP.readyState = 1 Then		
			Dim collapse_key : collapse_key = ""

			for i=1 to 4 '4자리만 생성
				collapse_key = collapse_key & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
			next
			
			'Dim json1 : Set json1 = jsObject()
			'set json1("data") = jsObject()
			'json1("data")("msg") = sMsg			
			'json1("delay_while_idle") = true	'디바이스가 idle상태일때는 전달하지 않음(기본 false)
			'json1("collapse_key") = collapse_key	 '메세지 그룹설정(오프라인에서 온라인 상태가 되면 많은 메세지가 전다되는 것을 막고 그룹내에서 가장 최신의 메시지만 전달)
			'json1("registration_ids") = aRegID
			'json1("dry_run") = true		'전송TEST(기본 false)
			'json1.Flush
			'seneHTTP.send toJSON(json1)	 '멀티 전송시

			seneHTTP.send  "registration_id=" & sRegID & "&collapse_key=" & collapse_key & "&delay_while_idle=1&data.msg=" & fEncodeURI(sMsg)	'단일 전송시

			On Error Resume Next
			seneHTTP.waitForResponse 5
			If (seneHTTP.readyState = 4) And (seneHTTP.Status = 200) Then
				sResultString = Trim(seneHTTP.ResponseText)
			else
				sResultString="HTTP_ERR4" & "(" & seneHTTP.readyState & "/"  & seneHTTP.Status & ")"
			End If
			
			Dim sDBChk : sDBChk = "OFF"

			if isObject(dbConn) and not(dbConn is Nothing) then
				sDBChk = "ON"
			else
				fnDBConn()
			end if
				ParamInit()
				'푸쉬발송 로그저장
				sql = " insert into member_msg_log (app_id, msg_idx, div_idx, try_idx, send_cnt, log_data, phone_list, regid_list, send_date, msg_content)values(?, ?, ?, ?, ?, ?, ?, ?, getdate(),?) "
				cmd.Parameters.Append cmd.CreateParameter("@app_id", adVarChar, adParamInput, 20, sAppID)
				cmd.Parameters.Append cmd.CreateParameter("@msg_idx", adInteger, adParamInput, 4, 0)
				cmd.Parameters.Append cmd.CreateParameter("@div_idx", adInteger, adParamInput, 4, 1)
				cmd.Parameters.Append cmd.CreateParameter("@try_idx", adInteger, adParamInput, 4, 1)
				cmd.Parameters.Append cmd.CreateParameter("@send_cnt", adInteger, adParamInput, 4, 1)
				cmd.Parameters.Append cmd.CreateParameter("@log_data", adVarChar, adParamInput, 8000, sResultString)
				cmd.Parameters.Append cmd.CreateParameter("@phone_list", adVarChar, adParamInput, 8000, sPhone)
				cmd.Parameters.Append cmd.CreateParameter("@regid_list", adVarChar, adParamInput, 8000, sRegID)
				cmd.Parameters.Append cmd.CreateParameter("@msg_content", adVarChar, adParamInput, 255, sMsg)
				cmd.CommandText = sql
				cmd.Execute , , adExecuteNoRecords				

				if Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
					fnErrSave()
					'response.end
				end if
			if sDBChk = "OFF" then
				fnDBClose()
			end if
		else
			sResultString="HTTP_ERR1"
		End If

		Set seneHTTP = Nothing
		fnOnePushSend = sResultString
	else		
		fnOnePushSend = "PUSH발송 할 휴대폰번호가 없습니다"
	end if
end Function

Function fnFeeTypeInitUTF8(ByRef aData)

	Set aData = jsObject()

	Set aData("name") = jsObject()
		aData("name")("1") = "경비비"
		aData("name")("2") = "공동수도료"
		aData("name")("3") = "공동전기료"
		aData("name")("4") = "대표회의운영비"
		aData("name")("5") = "부과 총괄표"
		aData("name")("6") = "산업용전기료"
		aData("name")("7") = "생활폐기물수수료"
		aData("name")("8") = "승강기유지비"
		aData("name")("9") = "승강기전기료"
		aData("name")("10") = "위탁관리수수료"
		aData("name")("11") = "일반관리비"
		aData("name")("12") = "장기수선충당금"
		aData("name")("13") = "청소비"
		aData("name")("14") = "케이블TV시청료"
		aData("name")("15") = "화재보험료"
		aData("name")("16") = "수도료"
		aData("name")("17") = "전기료"

	Set aData("table_name") = jsObject()
		aData("table_name")("1") = "apt_fee_01"			'경비비
		aData("table_name")("2") = "apt_fee_02"			'공동수도료
		aData("table_name")("3") = "apt_fee_03"			'공동전기료
		aData("table_name")("4") = "apt_fee_04"			'대표회의운영비
		aData("table_name")("5") = "apt_fee_05"			'부과 총괄표
		aData("table_name")("6") = "apt_fee_06"			'산업용전기료
		aData("table_name")("7") = "apt_fee_07"			'생활폐기물수수료
		aData("table_name")("8") = "apt_fee_08"			'승강기유지비
		aData("table_name")("9") = "apt_fee_09"			'승강기전기료
		aData("table_name")("10") = "apt_fee_10"		'위탁관리수수료
		aData("table_name")("11") = "apt_fee_11"		'일반관리비
		aData("table_name")("12") = "apt_fee_12"		'장기수선충당금
		aData("table_name")("13") = "apt_fee_13"		'청소비
		aData("table_name")("14") = "apt_fee_14"		'케이블TV시청료
		aData("table_name")("15") = "apt_fee_15"		'화재보험료
		aData("table_name")("16") = "apt_fee_16"	'수도료
		aData("table_name")("17") = "apt_fee_17"	'전기료

	Set aData("sel_col") = jsObject()
		aData("sel_col")("1") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'경비비
		aData("sel_col")("2") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'공동수도료
		aData("sel_col")("3") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'공동전기료
		aData("sel_col")("4") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'대표회의운영비

		aData("sel_col")("5") = "gubn as 구분, amt as 금액, pay_width as 부과면적, pay_amt as 부과금액, diff_amt as 절상차액"		'부과 총괄표

		aData("sel_col")("6") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'산업용전기료
		aData("sel_col")("7") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'생활폐기물수수료
		aData("sel_col")("8") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'승강기유지비
		aData("sel_col")("9") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'승강기전기료
		aData("sel_col")("10") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'위탁관리수수료
		aData("sel_col")("11") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'일반관리비
		aData("sel_col")("12") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'장기수선충당금
		aData("sel_col")("13") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'청소비
		aData("sel_col")("14") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'케이블TV시청료
		aData("sel_col")("15") = "dong as 동, hosu as 호수, width as 평수, amt as 금액"		'화재보험료

		aData("sel_col")("16") = "dong as 동, hosu as 호수, amt1 + amt2 as 금액"		'수도료
		aData("sel_col")("17") = "dong as 동, hosu as 호수, amt1 + amt2 as 금액"		'전기료

	Set aData("col_txt") = jsObject()
		aData("col_txt")("1") = ""		'경비비
		aData("col_txt")("2") = ""		'공동수도료
		aData("col_txt")("3") = ""		'공동전기료
		aData("col_txt")("4") = ""		'대표회의운영비
		aData("col_txt")("5") = ""		'부과 총괄표
		aData("col_txt")("6") = ""		'산업용전기료
		aData("col_txt")("7") = ""		'생활폐기물수수료
		aData("col_txt")("8") = ""		'승강기유지비
		aData("col_txt")("9") = ""		'승강기전기료
		aData("col_txt")("10") = ""		'위탁관리수수료
		aData("col_txt")("11") = ""		'일반관리비
		aData("col_txt")("12") = ""		'장기수선충당금
		aData("col_txt")("13") = ""		'청소비
		aData("col_txt")("14") = ""		'케이블TV시청료
		aData("col_txt")("15") = ""		'화재보험료
		aData("col_txt")("16") = ""		'수도료
		aData("col_txt")("17") = ""		'전기료

	fnFeeTypeInitUTF8 = 17
End Function
%>