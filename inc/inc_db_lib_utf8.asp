<!--METADATA TYPE= "typelib"  NAME= "ADODB Type Library"
      FILE="C:\Program Files\Common Files\SYSTEM\ADO\msado15.dll"  -->
 <!--#include virtual="./inc/JSON_2.0.4.asp" -->
<%
Dim dbConn, cmd, cmd2
Dim DefaultConnString

Set dbConn = Nothing
Set cmd = Nothing
Set cmd2 = Nothing

'---------------------------------------------------
' DB연결 및 Command개체 초기화
'---------------------------------------------------
Sub fnDBConn()
	If dbConn is Nothing Then
		Dim sConnect, fnDBConn, sDBIP, sDB, sDBID, sDBPassword
		
		'sDBIP="yy7910ae.cafe24.com"
		'sDB="yy7910ae"
		'sDBID="yy7910ae"
		'sDBPassword="cjstk@1004"

		sDBIP="45.77.25.42"
		sDB="ezpay"
		sDBID="sa"
		sDBPassword="dlwlms!)@(#*$&2017"

		sConnect = "Provider=SQLOLEDB.1;Password=" & sDBPassword & ";Persist Security Info=False;User ID=" & sDBID & ";Initial Catalog=" & sDB & ";Data Source=" & sDBIP	
		DefaultConnString = sConnect
		Set dbConn = Server.CreateObject("ADODB.Connection")
		dbConn.Open sConnect

		'Server.ScriptTimeOut = 10000 'asp스크립트 타임아웃 시간을 늘려준다.
		dbConn.CommandTimeout=180
	End If

	If cmd is Nothing Then
		Set cmd = Server.CreateObject("ADODB.Command")
		cmd.ActiveConnection = dbConn
		cmd.CommandType = adCmdText
		cmd.CommandTimeout=180	'초단위
	End If

End Sub

Sub fnCmd2Init()
	If cmd2 is Nothing Then
		Set cmd2 = Server.CreateObject("ADODB.Command")
		cmd2.ActiveConnection = dbConn
		cmd2.CommandType = adCmdText
	End If
End Sub

'---------------------------------------------------
' DB연결 해제 Command개체 반환
'---------------------------------------------------
Sub fnDBClose()
	if (Not dbConn is Nothing) Then 
		if (dbConn.State = adStateOpen) Then dbConn.Close
		Set dbConn = Nothing
	End if

	if (Not cmd is Nothing) Then 
		Set cmd.ActiveConnection = Nothing
	  Set cmd = Nothing
	End If
	
	if (Not cmd2 is Nothing) Then 
		Set cmd2.ActiveConnection = Nothing
	  Set cmd2 = Nothing
	End if
End Sub

'---------------------------------------------------
' 트랜잭션을 시작하고, Connetion 개체를 반환한다.
'---------------------------------------------------
Function BeginTrans(dbConnectionString)
	If IsObject(dbConnectionString) Then
		If dbConnectionString is Nothing Then
			dbConnectionString = DefaultConnString
		End If
	End If

	Set dbConn = Server.CreateObject("ADODB.Connection")
	dbConn.Open dbConnectionString
	dbConn.BeginTrans
	Set BeginTrans = dbConn
End Function

'---------------------------------------------------
' 활성화된 트랜잭션을 커밋한다.
'---------------------------------------------------
Sub CommitTrans(dbConnectionObj)
	If Not dbConnectionObj Is Nothing Then
		dbConnectionObj.CommitTrans
		dbConnectionObj.Close
		Set ConnectionObj = Nothing
	End If
End Sub

'---------------------------------------------------
' 활성화된 트랜잭션을 롤백한다.
'---------------------------------------------------
Sub RollbackTrans(dbConnectionObj)
	If Not dbConnectionObj Is Nothing Then
		dbConnectionObj.RollbackTrans
		dbConnectionObj.Close
		Set ConnectionObj = Nothing
	End If
End Sub

Function fnInputFilter(str)
	str = trim(str)
	'str = replace(str,"'","")
	'str = replace(str,";","")
	'str = replace(str,"--","")
	fnInputFilter = str
End Function

Function fnKeywordFilter(str)
	str = fnNullInit(str)
	str = replace(str,"'","")
	str = replace(str,";","")
	str = replace(str,"--","")
	fnKeywordFilter = str
End Function

Function fnKeyworeFilter(str)
	str = fnNullInit(str)
	str = replace(str,"'","")
	str = replace(str,";","")
	str = replace(str,"--","")
	fnKeyworeFilter = str
End Function

'금액 데이터 처리1
Function fnFormatNumber(sArgString, sArgReturnString, nArgIdx)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) then
		sNewStr = FormatNumber(sNewStr, nArgIdx)
	else
		sNewStr = sArgReturnString
	end if
	
	fnFormatNumber = sNewStr
	
End Function

'금액 데이터 처리2
Function fnFormatNumber2(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) then
		sNewStr = sNewStr
	else
		sNewStr = 0
	end if
	
	fnFormatNumber2 = sNewStr
	
End Function

'비율
Function fnRate(sArgNum1, sArgNum2)
	Dim nNum1, nNum2
	Dim nRate

	nNum1 = sArgNum1
	nNum2 = sArgNum2
	
	nNum1 = Trim(nNum1)
	nNum2 = Trim(nNum2)
	
	nRate = 0
	if isNumeric(nNum1) And IsNumeric(nNum2) Then
		nNum1 = CDbl(nNum1)
		nNum2 = CDbl(nNum2)
		If nNum1 > 0 and nNum2 > 0 then
			nRate = FormatNumber((nNum1/nNum2)*100,2)
		End If
	end if
	
	fnRate = nRate
End Function 

'널값 공백으로 초기화
Function fnNullInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	if isNull(sNewStr) then
		sNewStr = ""
	end If	
	
	sNewStr = Trim(sNewStr)
	
	fnNullInit = sNewStr
End Function

'만 나이 구하는 함수
Function FuNnAge(dAgeDate) 
	DIM	nAge, nTemp

	If IsDate(dAgeDate) then

		nAge = DateDiff("YYYY",dAgeDate ,FormatDateTime(Now, 2)) 
				 
		If Right(dAgeDate, 6) = "-02-29" Then   '윤달
			nTemp = "-02-28"
		Else
			nTemp = dAgeDate
		End If
		
		nTemp = Left(DateAdd("YYYY", -1, FormatDateTime(Now, 2)), 4) & Right(nTemp, 6)   '현재년도의 전년도에 회원탄생을을 구한다
		nTemp = DateDiff("D", nTemp, FormatDateTime(Now, 2)) * 1

		If nTemp < 365 Then                                                              ' 일수로 일년이 지나지 않았으면....
			nAge = nAge - 1
		End If
	Else
		nAge = 0
	End If
	
	FuNnAge = nAge
End Function

Function fnBackScript(sArgMsg, sArgScript)
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('" & sArgMsg & "');"
	Response.Write sArgScript
	Response.Write "</script>"
End Function

'널데이터 처리
Function fnNullChk(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNull(sNewStr) or sNewStr = "" then
		sNewStr = "&nbsp;"
		sNewStr = "-"
	end if
	
	fnNullChk = sNewStr
	
End Function

Sub ParamInit()
	Dim nParameter, i
	if (Not cmd is Nothing) Then 
		nParameter = cmd.Parameters.Count
		for i=1 to nParameter
			cmd.Parameters.Delete(0)
		next
	End if
End Sub

Sub ParamInit2(commandCmd)
	Dim nParameter, i
	if (Not commandCmd is Nothing) Then 
		nParameter = commandCmd.Parameters.Count
		for i=1 to nParameter
			commandCmd.Parameters.Delete(0)
		next
	End if
End Sub

Sub printParam()
	Dim nParameter, i
	
	if (Not cmd is Nothing) Then 
		nParameter = cmd.Parameters.Count
		if nParameter >= 1 then
			response.write "Parameter List<br/>"
		else
			response.write "No Parameter<br/>"
		end if
		for i=1 to nParameter
			response.write cmd.Parameters(i-1).Name & ": [" & cmd.Parameters(i-1).Value &"]<br/>"
		next
	else
		response.write "cmd is Nothing!<br/>"
	End if
End Sub

'올림함수
Function fnCeiling(nArgNum)
	If CLng(nArgNum) < nArgNum Then
		fnCeiling = CLng(nArgNum) + 1
	Else
		fnCeiling = CLng(nArgNum)
    End If
    
End Function

'리스트 새글 아이콘
Function fnNewIcon(sArgDate)
	
	Dim sNewIcon
							
	If datediff ("h", CDate(sArgDate), Now()) < 48 Then
		sNewIcon = "&nbsp;<img src='/g_admin/images/common/icon_new.gif' width='26' height='13' border='0' align='absmiddle'>"
	Else
		sNewIcon = ""
	End If
	
	fnNewIcon = sNewIcon
	
End Function

'조회수쿠키체크
Function fnVisitedChk(sArgBID, sArgTableName)

	Dim sIP, sValue, sCookie
	Dim bChk
	
	sIP = Request.ServerVariables("REMOTE_ADDR")	'IP주소
	sValue = "@" & sIP & "_" & sArgBID
	sCookie = Trim(request.cookies("ck_" & sArgTableName))
	
	bChk = False		
	if InStr(1, sCookie, sValue, 1) = 0 then
		response.cookies("ck_" & sArgTableName) = sCookie & sValue
		bChk = True				'조회수 증가
	end if

	fnVisitedChk = bChk

End Function

'문자열 바이트수로 자르기(문자열,최대바이트수,대체문자)
Function fnStrCut(sArgStr, nArgMaxByte, sArgTail)
	
	Dim sResult, cTemp, nByteSum, nIdx, nLen
	
	nByteSum=1
	nIdx=1
	nLen=Len(sArgStr)
	Do While nByteSum<=nArgMaxByte and nIdx<=nLen
		cTemp = Mid(sArgStr, nIdx, 1)
		If Asc(cTemp) > 0 and Asc(cTemp) < 255 Then
			nByteSum = nByteSum + 1
		Else
			nByteSum = nByteSum + 2
		End If
		nIdx = nIdx + 1
		sResult = sResult & cTemp
	Loop
	
	nIdx = nIdx - 1
	If nIdx = nLen Then
		fnStrCut = sArgStr
	Else
		fnStrCut = sResult & sArgTail
	End If
End Function

function fnStrChk(sStr)
	Dim sResult : sResult = fnNullInit(sStr)
	
	Dim objRegExp, strOutput
	Set objRegExp = New Regexp

	objRegExp.IgnoreCase = True                              '// 대소문자 구분 여부
	objRegExp.Global = True                                      '// 전체 검색여부
	objRegExp.Pattern = "[^ㄱ-ㅎ가-?A-Za-z0-9-!$%~`'()*^,-.?ㅜㅠ&_#+=:-;@~/,._  ]"  

	sResult = objRegExp.Replace(sResult, "") 

	Set objRegExp = Nothing

	fnStrChk = sResult
end function

Function fnHTMLConvertR(sArgContent)
	Dim sResult : sResult = sArgContent
	sResult = Replace(sResult,"<br/>",chr(13)&chr(10))
	sResult = Replace(sResult,"<br>",chr(13)&chr(10))
	fnHTMLConvertR = sResult
end function

Function fnHTMLConvertI(sArgContent)
	Dim sResult : sResult = sArgContent
	sResult = Replace(sResult,chr(13)&chr(10),"<br/>")
	fnHTMLConvertI = sResult
end function

'HTML 변환 함수
Function fnHTMLConvert(sArgContent, cArgHtml)
	Dim sResult
	sResult = sArgContent
	
	if isNull(sResult) then
		sResult = ""
	end if

	if cArgHtml="Y" then 'html 허용
			
		sResult=Replace(sResult,chr(13)&chr(10),"<br>") '엔터는 한줄띄움
		sResult=RePlace(sResult,"  ","&nbsp;&nbsp;")
	else
		sResult=Replace(sResult, "&" , "&amp;")
		sResult=Replace(sResult, "%" , "&#37")	
		sResult=Replace(sResult, "<" , "&lt;")
		sResult=Replace(sResult, ">" , "&gt;")
		sResult=Replace(sResult,chr(13)&chr(10),"<br>") '엔터는 한줄띄움
	end if
		
	fnHtmlConvert = sResult
	
End Function

Function fnGetIDNO(sArgRegNO)
	Dim aRegNO(1), aTmpRegNO, i
	Dim nRegNO

	if isNumeric(sArgRegNO) and len(sArgRegNO) = 13 then
		aRegNO(0) = left(sArgRegNO, 6)
		aRegNO(1) = right(sArgRegNO, 7)
	else
		aTmpRegNO = split(sArgRegNO, "-")
		nRegNO = ubound(aTmpRegNO)	
		for i=0 to 1
			aRegNO(i) = ""
			if nRegNO >= i then
				aRegNO(i) = aTmpRegNO(i)
			end if
		next
	end if
	fnGetIDNO = aRegNO
End Function

Sub fnErrChk()
	Dim i		
	Dim ASPerror 
	
	if isObject(dbConn) and not(dbConn is Nothing) then
		for i=0 to dbConn.Errors.Count-1
			response.write "DB 에러 " & i & " : "& dbConn.Errors.item(i).description & "<= 1<br>"
		next
	end if

	if err then   '또는 err.Number<>0
		response.write "<br>err.Description : " & err.Description		'에러에 대한 자세한 설명
		response.write "<br>err.Number : " & err.Number				'에러번호
		response.write "<br>err.Source : " & err.Source					'어떤 에러인지 간략한 설명
		response.write "<br>err.HelpFile : " & err.HelpFile					'도움말파일
		response.write "<br>err.HelpContext : " & err.HelpContext		'출력내용
	end if
						
	SET ASPerror = server.getlasterror					
	if isObject(ASPerror) and ASPerror.Line > 0 then
		response.write "<br>에러난파일="&ASPerror.File&"<br>"							 
		response.write "에러난줄="&ASPerror.Line&"<br>"							 
		response.write "에러상세내역="&ASPerror.category&"<br><br>"			
	end if
	SET ASPerror = nothing
End Sub

Function fnGetErr()
	Dim i
	Dim sEMsg : sEMsg = ""

	if isObject(dbConn) and not(dbConn is Nothing) then
		for i=0 to dbConn.Errors.Count-1
			'response.write "DB 에러 " & i & " : "& dbConn.Errors.item(i).description & "<= 1<br>"
			sEMsg = sEMsg & "DB 에러 " & i & " : "& dbConn.Errors.item(i).description & "<= 1<br>\n"
		next
	end if

	if err then   '또는 err.Number<>0
		response.write "<br>err.Description : " & err.Description		'에러에 대한 자세한 설명
		response.write "<br>err.Number : " & err.Number				'에러번호
		'response.write "<br>err.Source : " & err.Source					'어떤 에러인지 간략한 설명
		'response.write "<br>err.HelpFile : " & err.HelpFile					'도움말파일
		'response.write "<br>err.HelpContext : " & err.HelpContext		'출력내용
		'response.write "<br>err.NativeError : " & err.NativeError		'DB 에러번호
		sEMsg = sEMsg &  "\n<br>err.Description : " & err.Description		'에러에 대한 자세한 설명
		sEMsg = sEMsg &  "\n<br>err.Number : " & err.Number				'에러번호
		sEMsg = sEMsg &  "\n<br>err.Source : " & err.Source					'어떤 에러인지 간략한 설명
		sEMsg = sEMsg &  "\n<br>err.HelpFile : " & err.HelpFile					'도움말파일
		sEMsg = sEMsg &  "\n<br>err.HelpContext : " & err.HelpContext		'출력내용
	end if
	'response.end
	fnGetErr = sEMsg
End Function

'거래일시 데이터 처리
Function fnFormatDateTime(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) and len(sNewStr) = 14 then
		'20090601192430 => 2009.06.01.19:24:30
		sNewStr = left(sNewStr, 4) & "-" & mid(sNewStr, 5,2)  & "-" & mid(sNewStr, 7,2) & " " & mid(sNewStr, 9,2) & ":" & mid(sNewStr, 11,2) & ":" & mid(sNewStr, 13,2)
	elseIf Not(IsNull(sNewStr)) And IsDate(sNewStr) Then
		sNewStr = FormatDateTime(sNewStr,0)
		'sNewStr = FormatDateTime(sNewStr,2) & " " & right("0" & hour(sNewStr),2) & ":" & right("0" & minute(sNewStr),2) & ":" & right("0" & second(sNewStr),2)
	else
		sNewStr = "-"
	end if
	
	fnFormatDateTime = sNewStr
	
End Function


'날짜 데이터 처리
Function fnFormatDate(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) and len(sNewStr) = 8 then
		'20090930
		sNewStr = left(sNewStr, 4) & "-" & mid(sNewStr, 5,2)  & "-" & mid(sNewStr, 7,2)
	elseIf Not(IsNull(sNewStr)) And IsDate(sNewStr) Then
		sNewStr = FormatDateTime(sNewStr,2)
	Else
		sNewStr = "-"
	End If
	
	fnFormatDate = sNewStr
	
End Function


'거래시간 데이터 처리
Function fnFormatTime(sArgString)
	
	Dim sNewStr
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if isNumeric(sNewStr) and len(sNewStr) = 6 then
		'01192430 => 19:24:30
		sNewStr = left(sNewStr, 2) & ":" & mid(sNewStr, 3,2)  & ":" & mid(sNewStr, 5,2)
	elseIf Not(IsNull(sNewStr)) And IsDate(sNewStr) Then
		sNewStr = FormatDateTime(sNewStr,0)
	else
		sNewStr = "-"
	end if
	
	fnFormatTime = sNewStr
	
End Function

Function fnIntInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	else
		sNewStr = trim(sNewStr)
		sNewStr = Replace(sNewStr, "," , "")
	End If
	
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = CDbl(sNewStr)
	else
		sNewStr = 0
	end If	
	fnIntInit = sNewStr
End Function
Function fnLngInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	else
		sNewStr = trim(sNewStr)
		sNewStr = Replace(sNewStr, "," , "")
	End If
	
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = CLng(sNewStr)
	else
		sNewStr = 0
	end If	
	fnLngInit = sNewStr
End Function

Function fnDongInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	else
		sNewStr = trim(sNewStr)
	End If
	
	'숫자 or 알파벳
	if fnRegChk("[^a-z0-9]", sNewStr) = "False" then
		sNewStr = ""
	end If	
	
	fnDongInit = sNewStr
End Function

Function fnHosuInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	else
		sNewStr = trim(sNewStr)
	End If
	
	'숫자타입
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = CInt(sNewStr)
	'알파벳타입
	else
		if fnRegChk("[^a-z0-9]", sNewStr) = "False" then
			sNewStr = ""
		end if
	end If	
	
	fnHosuInit = sNewStr
End Function


Function fnPagingInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	End If

	sNewStr=Replace(sNewStr, "," , "")
	
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = CInt(sNewStr)
	else
		sNewStr = 0
	end If	

	if sNewStr < 1 then
		sNewStr = 1
	end if
	
	fnPagingInit = sNewStr
End Function

Function fnIdxInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	End If

	sNewStr=Replace(sNewStr, "," , "")
	
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = CDbl(sNewStr)
	else
		sNewStr = ""
	end If	
	
	fnIdxInit = sNewStr
End Function

Function fnDblInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNumeric(sNewStr) Then
		sNewStr = CDbl(sNewStr)
	else
		sNewStr = 0
	end If	
	
	fnDblInit = sNewStr
End Function

Function fnGeoInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = round(CDbl(sNewStr), 7)
	else
		sNewStr = 0.0
	end If	
	
	fnGeoInit = sNewStr
End Function

Function fnFloatInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNumeric(sNewStr) and len(sNewStr) >= 1 Then
		sNewStr = round(CDbl(sNewStr), 2)
	else
		sNewStr = 0.0
	end If	
	
	fnFloatInit = sNewStr
End Function

'게시물건수(페이지번호/총페이지수)
Function fnRecordCnt(nArgRecordCnt, nArgPageNO, nArgPageCnt)
	if isNumeric(nArgRecordCnt) then
%>
	<div class="lyRecordCnt">총 <%=nArgRecordCnt%>건 (<%=nArgPageNO%>/<%=nArgPageCnt%>)</div>
<%
	end if
End Function 

'게시물건수(페이지번호/총페이지수)
Function fnRecordCnt2(nArgRecordCnt)
	if isNumeric(nArgRecordCnt) then
%>
	<div class="lyRecordCnt">총 <%=nArgRecordCnt%>건</div>
<%
	end if
End Function 

'페이징
Function fnPaging(sListFileName, nPageNO, nBlockSize, nPageCount, sQueryString)
	nCurrentBlockpage = fnCeiling(nPageNO/nBlockSize)					'현재 페이지의 블럭카운트
	nTempPage = CLng(nBlockSize*(nCurrentBlockpage-1)+1)			'현재 블럭의 첫번째 페이지카운트
	if sQueryString <> "" then
		sQueryString = "&" & sQueryString
	end if
%>
	<div class="lyPaging">
<%
		'<<(-10) : nCurrentBlockpage > 1
		if nTempPage > 1 then
			response.write "<a href='" & sListFileName & "?pageNO=" & (nTempPage-nBlockSize) & sQueryString & "'><img src='/g_admin/images/common/prev10.jpg' width='16' height='13' align='absmiddle'></a>&nbsp;"
		else
			response.write "<img src='/g_admin/images/common/prev10.jpg' width='16' height='13' align='absmiddle'>&nbsp;"
		end if

		' <(-1) : nCurrentBlockpage < blocksize
		if nPageNO > 1 then
			response.write "<a href='" & sListFileName & "?pageNO=" & (nPageNO-1) & sQueryString & "'><img src='/g_admin/images/common/prev.jpg' width='13' height='13' align='absmiddle'></a>&nbsp;&nbsp;"
		else
			response.write "<img src='/g_admin/images/common/prev.jpg' width='13' height='13' align='absmiddle'>&nbsp;&nbsp;"
		end if

		' [n1,n2,3,n4,...] : (blocksize*current_blockpage) ~ nBlockSize
		i = 1
		Do until i > nBlockSize or nTempPage > nPageCount
			if nTempPage = nPageNO then
				response.write "<a href='" & sListFileName & "?pageNO=" & nTempPage & sQueryString &  "' class='curr_no'>" & nTempPage & "</a>&nbsp;"
			else
				response.write "<a href='" & sListFileName & "?pageNO=" & nTempPage & sQueryString &  "'>" & nTempPage & "</a>&nbsp;"
			end if
			nTempPage = nTempPage + 1
			i = i + 1
		Loop

		' >(+1) : nCurrentBlockpage < blocksize 
		if nPageNO < nPageCount then
			response.write "&nbsp;<a href='" & sListFileName & "?pageNO=" & (nPageNO+1) & sQueryString & "'><img src='/g_admin/images/common/next.jpg' width='13' height='13' align='absmiddle'></a>&nbsp;"
		else
			response.write "&nbsp;<img src='/g_admin/images/common/next.jpg' width='13' height='13' align='absmiddle'>&nbsp;"
		end if

		' >>(+10) : nTempPage < last_blockpage 
		if nTempPage <= nPageCount then
			response.write "<a href='" & sListFileName & "?pageNO=" & nTempPage & sQueryString &  "'><img src='/g_admin/images/common/next10.jpg' width='16' height='13' align='absmiddle'></a> "
		else
			response.write "<img src='/g_admin/images/common/next10.jpg' width='16' height='13' align='absmiddle'>"
		end if
%>   	 
	</div>
<%
End Function

'페이징2(상세페이지 내에 리스트 페이징)
Function fnPaging2(sListFileName, nPageNO, nBlockSize, nPageCount, sQueryString)
	nCurrentBlockpage = fnCeiling(nPageNO/nBlockSize)					'현재 페이지의 블럭카운트
	nTempPage = CLng(nBlockSize*(nCurrentBlockpage-1)+1)			'현재 블럭의 첫번째 페이지카운트
	if sQueryString <> "" then
		sQueryString = "&" & sQueryString
	end if
%>
	<div class="lyPaging">
<%
		'<<(-10) : nCurrentBlockpage > 1
		if nTempPage > 1 then
			response.write "<a href='" & sListFileName & "?pageNO2=" & (nTempPage-nBlockSize) & sQueryString & "'><img src='/g_admin/images/common/prev10.jpg' width='16' height='13' align='absmiddle'></a>&nbsp;"
		else
			response.write "<img src='/g_admin/images/common/prev10.jpg' width='16' height='13' align='absmiddle'>&nbsp;"
		end if

		' <(-1) : nCurrentBlockpage < blocksize
		if nPageNO > 1 then
			response.write "<a href='" & sListFileName & "?pageNO2=" & (nPageNO-1) & sQueryString & "'><img src='/g_admin/images/common/prev.jpg' width='13' height='13' align='absmiddle'></a>&nbsp;&nbsp;"
		else
			response.write "<img src='/g_admin/images/common/prev.jpg' width='13' height='13' align='absmiddle'>&nbsp;&nbsp;"
		end if

		' [n1,n2,3,n4,...] : (blocksize*current_blockpage) ~ nBlockSize
		i = 1
		Do until i > nBlockSize or nTempPage > nPageCount
			if nTempPage = nPageNO then
				response.write "<a href='" & sListFileName & "?pageNO2=" & nTempPage & sQueryString &  "' class='curr_no'>" & nTempPage & "</a>&nbsp;"
			else
				response.write "<a href='" & sListFileName & "?pageNO2=" & nTempPage & sQueryString &  "'>" & nTempPage & "</a>&nbsp;"
			end if
			nTempPage = nTempPage + 1
			i = i + 1
		Loop

		' >(+1) : nCurrentBlockpage < blocksize 
		if nPageNO < nPageCount then
			response.write "&nbsp;<a href='" & sListFileName & "?pageNO2=" & (nPageNO+1) & sQueryString & "'><img src='/g_admin/images/common/next.jpg' width='13' height='13' align='absmiddle'></a>&nbsp;"
		else
			response.write "&nbsp;<img src='/g_admin/images/common/next.jpg' width='13' height='13' align='absmiddle'>&nbsp;"
		end if

		' >>(+10) : nTempPage < last_blockpage 
		if nTempPage <= nPageCount then
			response.write "<a href='" & sListFileName & "?pageNO2=" & nTempPage & sQueryString &  "'><img src='/g_admin/images/common/next10.jpg' width='16' height='13' align='absmiddle'></a> "
		else
			response.write "<img src='/g_admin/images/common/next10.jpg' width='16' height='13' align='absmiddle'>"
		end if
%>   	 
	</div>
<%
End Function

'파일명 랜덤 키 생성(중복체크용)
Function fnFileKey()
	Dim sPushKey
	Dim sValue, aValue, nValue
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	sPushKey = ""

	for i=1 to 6 '6자리만 생성
		sPushKey = sPushKey & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next
	fnFileKey = sPushKey
End Function

'PUSH 랜덤 키 생성(중복체크용)
Function fnPushKey()
	Dim sPushKey
	Dim sValue, aValue, nValue
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	sPushKey = ""

	for i=1 to 8 '8자리만 생성
		sPushKey = sPushKey & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next
	sPushKey = Replace(Date(), "-", "") & hour(time()) & minute(time()) & second(time()) & sPushKey
	fnPushKey = sPushKey
End Function

'쿠폰번호 생성
Function fnCouponNO()
	Dim coupon_number
	Dim sValue, aValue, nValue
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "A,B,C,D,E,F,G,H,I,J,K,L,N,M,O,P,Q,R,S,T,U,V,W,X,Y,Z,0,1,2,3,4,5,6,7,8,9"
	'sValue = "0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	coupon_number = ""

	for i=1 to 16 '8자리만 생성
		coupon_number = coupon_number & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next

	'12+4=16자리
	'coupon_number = second(time()) & coupon_number & mid(Replace(Date(), "-", ""),3) & hour(time())

	fnCouponNO = coupon_number
End Function

'PUSH 랜덤 키 생성(중복체크용)
Function fnOrderNO()
	Dim sOrderNO
	Dim sValue, aValue, nValue
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	sOrderNO = ""

	for i=1 to 8 '8자리만 생성
		sOrderNO = sOrderNO & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next
	sOrderNO = Replace(Date(), "-", "") & hour(time()) & minute(time()) & second(time()) & sOrderNO
	fnOrderNO = sOrderNO
End Function


Function fnPushType(sArgType)
	Dim sType : sType = trim(sArgType)
	if sType = "p" then
		fnPushType = "PUSH"
	elseif sType = "m" then
		fnPushType = "알림"
	else
		fnPushType = "-"
	end if
End Function

Function fnSendStatus(sArgType)
	Dim sType : sType = trim(sArgType)
	if sType = "0" then
		fnSendStatus = "미발송"
	elseif sType = "1" then
		fnSendStatus = "일부발송"
	elseif sType = "2" then
		fnSendStatus = "발송완료"
	else
		fnSendStatus = "-"
	end if
End Function

Function fnMsgStatus(sArgType)
	Dim sType : sType = trim(sArgType)
	if sType = "0" then
		fnMsgStatus = "미발송"
	elseif sType = "1" then
		fnMsgStatus = "발송완료"
	elseif sType = "2" then
		fnMsgStatus = "수신확인"
	else
		fnMsgStatus = "-"
	end if
End Function

Function fnRs2Arr(ByRef aData, rs)
	Dim nCnt, i
	nCnt = rs.Fields.count
	redim aData(nCnt, 2)
	for i=0 to nCnt-1
		aData(i, 0) = rs.Fields.item(i).Name
		aData(i, 1) = fnNullInit(rs.Fields.item(i).value)
	next
	fnRs2Arr = nCnt
End Function

Function fnRs2Arr2(ByRef aData, rs)
	Dim nCnt
	aData = rs.getRows()
	if isArray(aData) then
		nCnt = ubound(aData, 2)
	else
		nCnt = -1
	end if

	fnRs2Arr2 = nCnt
End Function

Function fnGetKey(sArgKey)
	fnGetKey = mid(fnNullInit(sArgKey), 2)
End Function

Function fnGetKeyType(sArgKey)
	fnGetKeyType = left(fnNullInit(sArgKey), 1)
End Function

Function fnRs2Json(ByRef aData, rs)
	Dim nCnt, i
	Set aData = jsObject()
	nCnt = rs.Fields.count

	for i=0 to nCnt-1
		aData(lcase(rs.Fields.item(i).Name)) = fnNullInit(rs.Fields.item(i).value)
	next

	fnRs2Json = nCnt
End Function

Function fnRs2JsonArr(ByRef aData, rs)
	Dim nCnt, i, nRsCnt : nRsCnt = -1
	Set aData = jsObject()

	if not(rs.eof or rs.bof) Then
		
		nCnt = rs.Fields.count
		do until rs.eof or rs.bof			
			nRsCnt = nRsCnt + 1
			for i=0 to nCnt-1
				if nRsCnt=0 then 
					Set aData(lcase(rs.Fields.item(i).Name)) = jsObject()
				end if
				aData(lcase(rs.Fields.item(i).Name))(nRsCnt) = fnNullInit(rs.Fields.item(i).value)
			next
			rs.movenext
		loop
	end if	
	fnRs2JsonArr = nRsCnt
End Function

Function fnRs2JsonArrR(ByRef aData, rs)
	Dim nCnt, i, nRsCnt : nRsCnt = -1
	Set aData = jsObject()

	if not(rs.eof or rs.bof) then
		nCnt = rs.Fields.count
		do until rs.eof or rs.bof
			nRsCnt = nRsCnt + 1
			Set aData(nRsCnt) = jsObject()
			for i=0 to nCnt-1
				aData(nRsCnt)(lcase(rs.Fields.item(i).Name)) = fnNullInit(rs.Fields.item(i).value)
			next
			rs.movenext
		loop
	end if

	fnRs2JsonArrR = nRsCnt
End Function

Function fnUpload2Json(ByRef aData, upload)
	Dim nCnt, i, oItem
	Set aData = jsObject()
	nCnt = 0

	if isObject(upload) then
		For each oItem in upload.form
			aData(oItem.name) = fnKeywordFilter(oItem.value)
			nCnt = nCnt  + 1
		Next
	end if
	fnUpload2Json = nCnt
End Function

Function fnForm2Json(ByRef aData)
	Dim nCnt, i, oItem
	Set aData = jsObject()
	nCnt = 0

	For each oItem in request.form
		aData(oItem) = fnKeywordFilter(request.form(oItem))
		nCnt = nCnt  + 1
	Next
	fnForm2Json = nCnt
End Function

Function getJson(json, key)
	if isObject(json) then
		getJson = json(key)
	else
		getJson = ""
	end if
End function

'결제수단
Function fnPayType(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if sNewStr = "11" then
		sNewStr = "신용카드"
	elseif sNewStr = "31" then
		sNewStr = "휴대폰"
	elseif sNewStr = "21" then
		sNewStr = "계좌이체"
	elseif sNewStr = "22" then
		sNewStr = "무통장입금"
	elseif sNewStr = "32" then
		sNewStr = "ARS"
	elseif sNewStr = "41" then
		sNewStr = "포인트"
	elseif sNewStr = "81" then
		sNewStr = "배치"
	elseif sNewStr = "card" then
		sNewStr = "신용카드"
	elseif sNewStr = "bank" then
		sNewStr = "무통장입금"
	else
		sNewStr = "-"
	end if

	fnPayType = sNewStr
	
End Function


Function fnFlagInit(sArgString)
	Dim sNewStr
	
	sNewStr = sArgString
	
	If IsNull(sNewStr) Then
		sNewStr = ""
	else
		sNewStr = trim(sNewStr)
	End If
		
	if sNewStr <> "0" and sNewStr <> "1" and sNewStr <> "2" and sNewStr <> "3" and sNewStr <> "4" then
		sNewStr = ""
	end if
	
	fnFlagInit = sNewStr
End Function

function fnDataSec(sArgStr, nArgLen)
	Dim nLen : nLen = len(sArgStr)
	Dim i
	if sArgStr <> "" then
		sArgStr = left(sArgStr, 4)
		for i=1 to nLen-nArgLen
			sArgStr = sArgStr & "*"
		next
	end if
	fnDataSec = sArgStr
End Function

function fnRDataSec(sArgStr, nArgLen)
	Dim nLen : nLen = len(sArgStr)
	Dim i
	Dim sTmpStr : sTmpStr = sArgStr
	if sTmpStr <> "" and nLen > nArgLen then
		sTmpStr = left(sTmpStr, nLen-nArgLen)
		for i=1 to nArgLen
			sTmpStr = sTmpStr & "*"
		next
	end if
	fnRDataSec = sTmpStr
End Function

Function fnImgView(sImgSrc, nMaxW, nMaxH)
	Dim sImgTag : sImgTag = ""
	Dim sImgPath : sImgPath = ""
	Dim FSO, sFilePath
	sImgSrc = "/upload/" & sImgSrc
	sImgPath = replace(sImgSrc, "/", "\")
	sFilePath = server.MapPath("/") & sImgPath

	Set FSO = CreateObject("Scripting.FileSystemObject")

	If FSO.FileExists(sFilePath) then

		Dim img_size, img_width, img_height 		

		if right(sFilePath, 3) = "png" then
			img_width = nMaxW
			img_height = nMaxH
		else
			'이미지 사이즈
			set img_size = LoadPicture(sFilePath)
			img_width = CLng(CDbl(img_Size.Width)*24/635) 
			img_height = CLng(CDbl(img_Size.Height)*24/635)
			set img_size = nothing
		end if
		
		'최대 크기
		if img_width > nMaxW then
			img_height = CLng(img_height * (nMaxW/img_width))
			'img_height = img_height + 30
			img_width = nMaxW
		end if
		
		if img_height > nMaxH then
			img_width= CLng(img_width * (nMaxH/img_height))
			'img_width = img_width + 30
			img_height = nMaxH
		end if 
			
		sImgTag = "<div class='lyImg'><img src='" & sImgSrc & "' height=" & img_height & " width=" & img_width & " style='cursor:pointer;' onclick=""fnPopImg('" & sImgSrc & "');""></div>"
	else
		sImgTag = "<div class='lyImgText' style='padding:" & (nMaxH/2-10) & "px 0px;width:"&nMaxW&"px'>No Img</div>"
	End If	
	Set FSO = nothing		

	fnImgView = sImgTag
end function

function fnFileSave(upload, obj)
	Dim filename, filepath, fileext, filenameonly, i
	
	filename = fnNullInit(obj.FileName)
	filepath = upload.DefaultPath & "\" & filename
	
	if filename <> "" then
		If InStrRev(filename, ".") <> 0 Then 
			filenameonly = Left(filename, InStrRev(filename, ".") - 1) 
			fileext = Mid(filename, InStrRev(filename, ".")) 
		Else 
			filenameonly = filename 
			fileext = "" 
		End If

		Dim objRegExp
		Set objRegExp = New Regexp
		objRegExp.IgnoreCase = True                               '// 대소문자 구분 여부
		objRegExp.Global = True                                      '// 전체 검색여부
		objRegExp.Pattern = "[^a-zA-Z0-9_-]"
		filenameonly = objRegExp.Replace(filenameonly, "") 
		Set objRegExp = Nothing

		filenameonly = fnCreateFileName(filenameonly)
		filepath = upload.DefaultPath & "\" & filenameonly & fileext

		If upload.FileExists(filepath) Then
			i = 2
			Do While (1)
				filepath = upload.DefaultPath & "\" & filenameonly & "_" & i & "" & fileext
				If Not upload.FileExists(filepath) Then Exit Do 
				i = i + 1
			Loop 
		End If
	end if

	obj.SaveAs filepath
	fnFileSave = obj.LastSavedFileName
end Function

function fnFileSave2(upload, obj)
	Dim filename, filepath, fileext, filenameonly, i
	
	filename = fnNullInit(obj.FileName)
	filepath = upload.DefaultPath & "\" & filename

	
	if filename <> "" then
		If InStrRev(filename, ".") <> 0 Then 
			filenameonly = Left(filename, InStrRev(filename, ".") - 1) 
			fileext = Mid(filename, InStrRev(filename, ".")) 
		Else 
			filenameonly = filename 
			fileext = "" 
		End If

		Dim objRegExp
		Set objRegExp = New Regexp
		objRegExp.IgnoreCase = True                               '// 대소문자 구분 여부
		objRegExp.Global = True                                      '// 전체 검색여부
		objRegExp.Pattern = "[^a-zA-Z0-9_-]"
		filenameonly = objRegExp.Replace(filenameonly, "") 
		Set objRegExp = Nothing

		filenameonly = fnCreateFileName(filenameonly)
		filepath = upload.DefaultPath & "\" & filenameonly & fileext

		

		If upload.FileExists(filepath) Then
			i = 2
			Do While (1)
				filepath = upload.DefaultPath & "\" & filenameonly & "_" & i & "" & fileext
				If Not upload.FileExists(filepath) Then Exit Do 
				i = i + 1
			Loop 
		End If
	end If
	
	obj.SaveAs filepath
	fnFileSave2 = obj.LastSavedFileName
end function

'파일명 생성
Function fnCreateFileName(sKey)
	Dim sOrderNO
	Dim sValue, aValue, nValue, i
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음

	if len(sKey) > 1 then
		sOrderNO = left(sKey, 40)
	else
		sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
		aValue = split(sValue,",")
		nValue = ubound(aValue)

		sOrderNO = ""

		for i=1 to 6 '8자리만 생성
			sOrderNO = sOrderNO & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
		next
		sOrderNO = Replace(Date(), "-", "") & right(sKey,5) & hour(time()) & sOrderNO & minute(time())
	end if
	fnCreateFileName = sOrderNO
End Function

Function fnDateTimeStr2Arr(sArgStr)
	Dim aData(2), aTmpData, i
	Dim nData
	
	aData(0) = ""
	aData(1) = ""
	aData(2) = ""

	aTmpData = split(sArgStr, " ")
	nData = ubound(aTmpData)
	if nData >= 1 then
		aData(0) = aTmpData(0)

		Dim aTmpData2
		aTmpData2 = split(aTmpData(1), ":")
		nData = ubound(aTmpData2)
		if nData >= 1 then
			aData(1) = aTmpData2(0)
			aData(2) = aTmpData2(1)
		end if
	end if

	
	fnDateTimeStr2Arr = aData

End Function

Function fnGetTel(sArgTel)
	Dim aTel(2), aTmpTel, i
	Dim nTel
	aTmpTel = split(sArgTel, "-")
	nTel = ubound(aTmpTel)

	if isNumeric(sArgTel) and len(sArgTel) = 9 then
		aTel(0) = left(sArgTel, 2)
		aTel(1) = mid(sArgTel, 4, 3)
		aTel(2) = right(sArgTel, 4)
	elseif isNumeric(sArgTel) and len(sArgTel) = 10 then
		aTel(0) = left(sArgTel, 3)
		aTel(1) = mid(sArgTel, 4, 3)
		aTel(2) = right(sArgTel, 4)
	elseif isNumeric(sArgTel) and len(sArgTel) = 11 then
		aTel(0) = left(sArgTel, 3)
		aTel(1) = mid(sArgTel, 4, 4)
		aTel(2) = right(sArgTel, 4)
	elseif isNumeric(sArgTel) and len(sArgTel) = 12 then
		aTel(0) = left(sArgTel, 4)
		aTel(1) = mid(sArgTel, 5, 4)
		aTel(2) = right(sArgTel, 4)
	else
		for i=0 to 2
			aTel(i) = ""
			if nTel >= i then
				aTel(i) = aTmpTel(i)
			end if
		next
	end if
	fnGetTel = aTel
End Function

Function fnGetRegNO(sArgRegNO)
	Dim aRegNO(2), aTmpRegNO, i
	Dim nRegNO

	if isNumeric(sArgRegNO) and len(sArgRegNO) = 10 then
		aRegNO(0) = left(sArgRegNO, 3)
		aRegNO(1) = mid(sArgRegNO, 4, 2)
		aRegNO(2) = right(sArgRegNO, 5)
	else
		aTmpRegNO = split(sArgRegNO, "-")
		nRegNO = ubound(aTmpRegNO)	
		for i=0 to 2
			aRegNO(i) = ""
			if nRegNO >= i then
				aRegNO(i) = aTmpRegNO(i)
			end if
		next
	end if
	fnGetRegNO = aRegNO
End Function

Function fnGetEmail(sArgEmail)
	Dim aEmail(1), aTmpEmail, i
	Dim nEmail
	aTmpEmail = split(sArgEmail, "@")
	nEmail = ubound(aTmpEmail)
	
	for i=0 to 1
		aEmail(i) = ""
		if nEmail >= i then
			aEmail(i) = aTmpEmail(i)
		end if
	next
	fnGetEmail = aEmail
End Function

Function fnGetPeriod(sArgPeriod)
	Dim aPeriod(1), aTmpPeriod, i
	Dim nPeriod
	aTmpPeriod = split(sArgPeriod, "~")
	nPeriod = ubound(aTmpPeriod)
	
	for i=0 to 1
		aPeriod(i) = ""
		if nPeriod >= i then
			aPeriod(i) = aTmpPeriod(i)
		end if
	next
	fnGetPeriod = aPeriod
End Function

Function fnGetTime(sArgPeriod)
	Dim aPeriod(1), aTmpPeriod, i
	Dim nPeriod
	aTmpPeriod = split(sArgPeriod, ":")
	nPeriod = ubound(aTmpPeriod)
	
	for i=0 to 1
		aPeriod(i) = ""
		if nPeriod >= i then
			aPeriod(i) = aTmpPeriod(i)
		end if
	next
	fnGetTime = aPeriod
End Function

function fnErrSave()
	
	Dim i, site : site = "MBCTICKET[" & sAppID & SS_APP_ID & "]"
	Dim sDBChk : sDBChk = "OFF"
	
	if isObject(dbConn) and not(dbConn is Nothing) then
		sDBChk = "ON"
	else
		fnDBConn()
	end if

	for i=0 to dbConn.Errors.Count-1
		ParamInit()
		'response.write "* 에러 " & i & " : "& dbConn.Errors.item(i).description & "<= 1<br>"
		'cmd.CommandText="{call dbo.insert_errorlog(?,?,?,?,? ,?,?,?,?,? ,?,?,?,?,?,?)}"
		'cmd.CommandType=1
		cmd.CommandType = adCmdStoredProc
		cmd.CommandText = "dbo.insert_errorlog"
		cmd.Parameters.Append cmd.CreateParameter("@aspcode",adVarChar,adParamInput,50,SS_USER_ID)
		cmd.Parameters.Append cmd.CreateParameter("@errornumber",adVarChar,adParamInput,50,i)
		cmd.Parameters.Append cmd.CreateParameter("@source",adVarChar,adParamInput,200,"")
		cmd.Parameters.Append cmd.CreateParameter("@category",adVarChar,adParamInput,200,SS_GROUP)
		cmd.Parameters.Append cmd.CreateParameter("@errorfile",adVarChar,adParamInput,200,"")

		cmd.Parameters.Append cmd.CreateParameter("@line",adInteger,adParamInput,4,i)
		cmd.Parameters.Append cmd.CreateParameter("@description",adVarChar,adParamInput,500,dbConn.Errors.item(i).description)
		cmd.Parameters.Append cmd.CreateParameter("@aspdescription",adVarChar,adParamInput,500,"")
		cmd.Parameters.Append cmd.CreateParameter("@form",adVarChar,adParamInput,8000,fnForm2Str(4000))
		cmd.Parameters.Append cmd.CreateParameter("@querystring",adVarChar,adParamInput,8000,left(request.servervariables("script_name") & "|" & request.servervariables("query_string"),4000))

		cmd.Parameters.Append cmd.CreateParameter("@cookie",adVarChar,adParamInput,8000,left(request.cookies,4000))
		cmd.Parameters.Append cmd.CreateParameter("@referer",adVarChar,adParamInput,8000,left(request.servervariables("http_referer"),4000))
		cmd.Parameters.Append cmd.CreateParameter("@ip",adVarChar,adParamInput,50,request.servervariables("remote_addr"))
		cmd.Parameters.Append cmd.CreateParameter("@site",adVarChar,adParamInput,50,site)
		cmd.Parameters.Append cmd.CreateParameter("@agent",adVarChar,adParamInput,8000,left(request.servervariables("HTTP_USER_AGENT"),4000))
		cmd.Parameters.Append cmd.CreateParameter("@codepage",adVarChar,adParamInput,10,session.codepage)
		cmd.Execute , , adExecuteNoRecords
	next

	if Err.Number <> 0 or Server.GetLastError.Number then   '또는 err.Number<>0
		ParamInit()
		
		'cmd.CommandText="{call dbo.insert_errorlog(?,?,?,?,? ,?,?,?,?,? ,?,?,?,?,?,?)}"
		'cmd.CommandType=1
		cmd.CommandType = adCmdStoredProc
		cmd.CommandText = "dbo.insert_errorlog"

		cmd.Parameters.Append cmd.CreateParameter("@aspcode",adVarChar,adParamInput,50,SS_USER_ID)
		cmd.Parameters.Append cmd.CreateParameter("@errornumber",adVarChar,adParamInput,50,Err.Number)
		cmd.Parameters.Append cmd.CreateParameter("@source",adVarChar,adParamInput,200,err.Source)
		cmd.Parameters.Append cmd.CreateParameter("@category",adVarChar,adParamInput,200,SS_GROUP)
		cmd.Parameters.Append cmd.CreateParameter("@errorfile",adVarChar,adParamInput,200,err.HelpFile)

		cmd.Parameters.Append cmd.CreateParameter("@line",adInteger,adParamInput,4,0)
		cmd.Parameters.Append cmd.CreateParameter("@description",adVarChar,adParamInput,500,err.Description)
		cmd.Parameters.Append cmd.CreateParameter("@aspdescription",adVarChar,adParamInput,500,err.HelpContext)

		cmd.Parameters.Append cmd.CreateParameter("@form",adVarChar,adParamInput,8000,fnForm2Str(4000))
		cmd.Parameters.Append cmd.CreateParameter("@querystring",adVarChar,adParamInput,8000,left(request.servervariables("script_name") & "|" & request.servervariables("query_string"),4000))

		cmd.Parameters.Append cmd.CreateParameter("@cookie",adVarChar,adParamInput,8000,left(request.cookies,4000))
		cmd.Parameters.Append cmd.CreateParameter("@referer",adVarChar,adParamInput,8000,left(request.servervariables("http_referer"),4000))
		cmd.Parameters.Append cmd.CreateParameter("@ip",adVarChar,adParamInput,50,request.servervariables("remote_addr"))
		cmd.Parameters.Append cmd.CreateParameter("@site",adVarChar,adParamInput,50,site)
		cmd.Parameters.Append cmd.CreateParameter("@agent",adVarChar,adParamInput,8000,left(request.servervariables("HTTP_USER_AGENT"),4000))
		cmd.Parameters.Append cmd.CreateParameter("@codepage",adVarChar,adParamInput,10,session.codepage)
		cmd.Execute , , adExecuteNoRecords

		'response.write "<br>* err.Description : " & err.Description		'에러에 대한 자세한 설명
		'response.write "<br>err.Number : " & err.Number				'에러번호
		'response.write "<br>err.Source : " & err.Source					'어떤 에러인지 간략한 설명
		'response.write "<br>err.HelpFile : " & err.HelpFile					'도움말파일
		'response.write "<br>err.HelpContext : " & err.HelpContext		'출력내용

	end if

	if sDBChk = "OFF" then
		fnDBClose()
	end if

	fnErrSave = "Y"
end function

function fnLogSave()
	
	Dim i, site : site = "BITCOIN"
	Dim sDBChk : sDBChk = "OFF"
	
	if isObject(dbConn) and not(dbConn is Nothing) then
		sDBChk = "ON"
	else
		fnDBConn()
	end if
	ParamInit()

	cmd.CommandType = adCmdStoredProc
	cmd.CommandText = "dbo.insert_errorlog"

	cmd.Parameters.Append cmd.CreateParameter("@aspcode",adVarChar,adParamInput,50,"APP")
	cmd.Parameters.Append cmd.CreateParameter("@errornumber",adVarChar,adParamInput,50,"0")
	cmd.Parameters.Append cmd.CreateParameter("@source",adVarChar,adParamInput,200,"LOG")
	cmd.Parameters.Append cmd.CreateParameter("@category",adVarChar,adParamInput,200,"APP")
	cmd.Parameters.Append cmd.CreateParameter("@errorfile",adVarChar,adParamInput,200,"")

	cmd.Parameters.Append cmd.CreateParameter("@line",adInteger,adParamInput,4,0)
	cmd.Parameters.Append cmd.CreateParameter("@description",adVarChar,adParamInput,500,"")
	cmd.Parameters.Append cmd.CreateParameter("@aspdescription",adVarChar,adParamInput,500,"")

	cmd.Parameters.Append cmd.CreateParameter("@form",adVarChar,adParamInput,8000,fnForm2Str(4000))
	cmd.Parameters.Append cmd.CreateParameter("@querystring",adVarChar,adParamInput,8000,left(request.servervariables("script_name") & "|" & request.servervariables("query_string"),4000))

	cmd.Parameters.Append cmd.CreateParameter("@cookie",adVarChar,adParamInput,8000,left(request.cookies,4000))
	cmd.Parameters.Append cmd.CreateParameter("@referer",adVarChar,adParamInput,8000,left(request.servervariables("http_referer"),4000))
	cmd.Parameters.Append cmd.CreateParameter("@ip",adVarChar,adParamInput,50,request.servervariables("remote_addr"))
	cmd.Parameters.Append cmd.CreateParameter("@site",adVarChar,adParamInput,50,site)
	cmd.Parameters.Append cmd.CreateParameter("@agent",adVarChar,adParamInput,8000,left(request.servervariables("HTTP_USER_AGENT"),4000))
	cmd.Parameters.Append cmd.CreateParameter("@codepage",adVarChar,adParamInput,10,session.codepage)
	cmd.Execute , , adExecuteNoRecords

	fnErrSave()

	if sDBChk = "OFF" then
		fnDBClose()
	end if

	fnLogSave = "Y"
end function

Function fnForm2Str(nMaxLen)
	Dim sResult, oItem
	sResult = ""

	if isObject(uploadform) then
		For each oItem in uploadform.form
			'sResult = sResult & "" & oItem.name & "=" & fnKeywordFilter(oItem.value) & ";"
			if oItem.name <> "password" and oItem.name <> "pass" and oItem.name <> "EP_card_no" and oItem.name <> "EP_expire_date" then
				sResult = sResult & "" & oItem.name & "=" & fnKeywordFilter(oItem.value) & ";"
			end if
		Next
	else
		For each oItem in request.form
			'sResult = sResult & "" & oItem & "=" & fnKeywordFilter(request.form(oItem)) & ";"
			if oItem <> "password" and oItem <> "pass" and oItem <> "EP_card_no" and oItem <> "EP_expire_date" then
				sResult = sResult & "" & oItem & "=" & fnKeywordFilter(request.form(oItem)) & ";"
			end if
		Next
	end if
	fnForm2Str = left(sResult, nMaxLen)
End Function

Function fnGet2Str(nMaxLen)
	Dim sResult, oItem
	sResult = request.servervariables("HTTP_USER_AGENT") & request.servervariables("http_referer") & request.servervariables("query_string")
	fnGet2Str = left(sResult, nMaxLen)
End Function

'환경설정
Function fnAppConfig(sArgString)
	
	Dim sNewStr
	
	sNewStr = sArgString
	
	sNewStr = Trim(sNewStr)
	if sNewStr = "kakao_msg" then
		sNewStr = "카카오톡 초대 메세지"
	else
		sNewStr = "-"
	end if

	fnAppConfig = sNewStr
	
End Function

Function fnMenuCnt(ByRef nArgMenuCnt)
	nArgMenuCnt = nArgMenuCnt + 1
	fnMenuCnt = nArgMenuCnt
end function

Function fnThumImgSave(sOrgImgSrc, nWidth, nHeight, sThumFolder)
	Dim sThumImgName : sThumImgName = ""	
	Dim sOrgPath : sOrgPath = Server.MapPath("\upload\") & "\" & replace(sOrgImgSrc, "/", "\")
	Dim sThumPath : sThumPath = sOrgPath
	Dim nThumPath : nThumPath = instrrev(sThumPath, "\")
	Dim sOrgFolderPath : sOrgFolderPath = left(sThumPath, nThumPath)
	Dim sThumFolderPath : sThumFolderPath = left(sThumPath, nThumPath) & sThumFolder & "\"

	'폴더가 없는 경우 폴더 생성
	Dim oFs : Set oFs = Server.CreateObject("Scripting.FileSystemObject")
	'원본이미지 폴더
	If Not(oFs.FolderExists(sOrgFolderPath)) Then
		oFs.CreateFolder(sOrgFolderPath)
	End If
	'썸네일이미지 폴더
	If Not(oFs.FolderExists(sThumFolderPath)) Then
		oFs.CreateFolder(sThumFolderPath)
	End If
	Set oFs = nothing

	Dim FSO : Set FSO = CreateObject("Scripting.FileSystemObject")
	If FSO.FileExists(sOrgPath) then
		sThumPath = left(sThumPath, nThumPath) & sThumFolder & "\" & mid(sThumPath, nThumPath+1)

		Dim nFileName : nFileName = instrrev(sThumPath, "\")
		Dim sFolderPath : sFolderPath = left(sThumPath, nFileName-1)
		Dim sFileName : sFileName = mid(sThumPath, nFileName+1)
		Dim filenameonly, fileext, i
		
		sThumPath = replace(sThumPath, ";", "")
		If FSO.FileExists(sThumPath) Then
			If InStrRev(sFileName, ".") <> 0 Then 
				filenameonly = Left(sFileName, InStrRev(sFileName, ".") - 1) 
				fileext = Mid(sFileName, InStrRev(sFileName, ".")) 
			Else 
				filenameonly = sFileName 
				fileext = "" 
			End If
			filenameonly = replace(filenameonly, ";", "")

			i = 2
			Do While (1)
				sThumPath = sFolderPath & "\" & filenameonly & "[" & i & "]" & fileext
				If Not FSO.FileExists(sThumPath) Then Exit Do 
				i = i + 1
			Loop 
		End If

		'썸네일 이미지 생성
		if 1=1 then
			Dim objCxImage : Set objCxImage = Server.CreateObject("CxImageATL.CxImage")
			Dim Quality : Quality = 100
			Dim widthOrig, heightOrig, fx, fy, f, widthTh, heightTh
			Dim Fast : Fast = 0
			Dim bStrach : bStrach = true
			Dim BinData
			Dim nImgType : nImgType = GetFileType(sOrgPath)
			
			f=1
			Call objCxImage.Load(sOrgPath,nImgType)
			Call objCxImage.IncreaseBpp(24)
			'determine thumbnail size and resample original image data
			If bStrach Then ' retain aspect ratio
				widthTh = nWidth
				heightTh = nHeight
			Else
				widthOrig = CDbl(objCxImage.GetWidth())
				heightOrig = CDbl(objCxImage.GetHeight())
				fx = widthOrig/nWidth
				fy = heightOrig/nHeight 'subsample factors
				' must fit in thumbnail size
				If fx>fy Then f=fx Else f=fy  ' Max(fx,fy)
				If f<1 Then f=1
				widthTh = Int(widthOrig/f)
				heightTh = Int(heightOrig/f)
			End If
			
			Call objCxImage.Resample(widthTh,heightTh,Fast)

			'이미지가 캔버스보다 작은경우에만 처리
			if f > 1 then
				Dim tmpCxImage : Set tmpCxImage = Server.CreateObject("CxImageATL.CxImage")
				Dim tmpCxImageR : Set tmpCxImageR = Server.CreateObject("CxImageATL.CxImage")
				Call tmpCxImage.Create(nWidth, nHeight, 24, nImgType)
				Call tmpCxImage.ShiftRGB(255,255,255)
				Call tmpCxImageR.Create(nWidth+(nWidth-widthTh)/2, nHeight, 24, nImgType)
				Call tmpCxImageR.ShiftRGB(255,255,255)

				'이미지 블렌딩처리
				Call tmpCxImage.Mix(objCxImage,1,0,0)
				
				'좌우회전
				Call tmpCxImage.Mirror()

				'이미지 블렌딩처리(2차)
				Call tmpCxImageR.Mix(tmpCxImage,1,0,0)

				'좌우회전(원복)
				Call tmpCxImageR.Mirror()

				'이미지 자르기
				Call tmpCxImageR.Crop(0, 0, nWidth, nHeight)	

				'Call tmpCxImage.GrayScale()			
				BinData = tmpCxImageR.ImageForASP(3,Quality)
			else
				BinData = objCxImage.ImageForASP(3,Quality)
			end if

			Dim BinaryStream
			Set BinaryStream = CreateObject("ADODB.Stream") 
			BinaryStream.Type = 1 
			BinaryStream.Open 
			BinaryStream.Write BinData 
			BinaryStream.SaveToFile sThumPath, 2 
			BinaryStream.close
			Set BinaryStream = Nothing

			sThumImgName = sThumPath
		else
			Dim objImage : Set objImage = Server.CreateObject("DEXT.ImageProc")
			IF objImage.SetSourceFile(sOrgPath) Then
				
				Dim nImgW : nImgW = objImage.ImageWidth
				Dim nImgH : nImgH = objImage.ImageHeight
				Dim nSaveW : nSaveW = nWidth
				Dim nSaveH : nSaveH = nHeight
				Dim imgWidthResizeVal

				'상세이미지 인 경우 비율유지
				if sThumFolder = "thum_l" then
					'if nWidth < nImgW then
					'	imgWidthResizeVal = nWidth/nImgW
					'	'nSaveW = Round(imgWidthResizeVal *  nImgW)
					'	nSaveH = Round(imgWidthResizeVal *  nImgH) 
					'end if
					
					'if nSaveH > nHeight then
					'	imgWidthResizeVal = nHeight/nSaveH
					'	nSaveW = Round(imgWidthResizeVal *  nImgW)
					'	nSaveH = nHeight	'Round(imgWidthResizeVal *  nImgH) 
					'end if

					'덮어쓰기, 비율저장
					sThumImgName = objImage.SaveasThumbnail(sThumPath, nSaveW, nSaveH, true, true)
				else
					'덮어쓰기, 비율저장
					sThumImgName = objImage.SaveasThumbnail(sThumPath, nSaveW, nSaveH, true, false)
				end if
			End IF
		end if
		
		sThumImgName = replace(sThumImgName, "\", "/")
		nThumPath = instrrev(sThumImgName, "/upload/")
		sThumImgName = mid(sThumImgName, nThumPath+8)
	end if
	fnThumImgSave = sThumImgName
End Function

Function GetFileType(sFile)
	Dim dot, filetype, sExt
  dot = InStrRev(sFile, ".")
  filetype=2
  If dot > 0 Then sExt = LCase(Mid(sFile, dot + 1, 3))
  If sExt = "bmp" Then filetype = 1
  If sExt = "wmf" Then filetype = 1
  If sExt = "raw" Then filetype = 1
  If sExt = "gif" Then filetype = 2
  If sExt = "jpg" Then filetype = 3
  If sExt = "png" Then filetype = 4
  If sExt = "ico" Then filetype = 5
  If sExt = "tif" Then filetype = 6
  If sExt = "tga" Then filetype = 7
  If sExt = "pcx" Then filetype = 8
  If sExt = "jp2" Then filetype = 11
  If sExt = "jpc" Then filetype = 12
  If sExt = "jpx" Then filetype = 13
  If sExt = "pnm" Then filetype = 14
  If sExt = "ras" Then filetype = 15
  GetFileType=filetype
End Function

'PUSH 랜덤 키 생성(중복체크용)
Function fnCreateID(sKey)
	Dim sOrderNO
	Dim sValue, aValue, nValue, i
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	sOrderNO = ""

	for i=1 to 5 '8자리만 생성
		sOrderNO = sOrderNO & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next
	sOrderNO = Replace(Date(), "-", "") & sOrderNO & sKey
	fnCreateID = sOrderNO
End Function

Function fnCreateInvID(sKey)
	Dim sOrderNO
	Dim sValue, aValue, nValue, i
	Randomize '선언안하면 처음 랜덤값 이후로 랜덤값이 바뀌지 않음
	sValue = "a,b,c,d,e,f,g,h,i,j,k,l,n,m,o,p,q,r,s,t,u,v,w,x,y,z,0,1,2,3,4,5,6,7,8,9"
	aValue = split(sValue,",")
	nValue = ubound(aValue)

	sOrderNO = ""

	for i=1 to 5 '8자리만 생성
		sOrderNO = sOrderNO & aValue(Int((nValue - 1 + 1) * Rnd + 1) )
	next
	sOrderNO = right(sKey,4) & hour(time()) & sOrderNO & minute(time())
	fnCreateInvID = sOrderNO
End Function

function fnErrScript(sMsg, sScript)
	Response.Write "<script type='text/javascript'>"
	Response.Write "alert('" & sMsg & "');"
	Response.Write sScript
	Response.Write "</script>"
end Function


'파일이름 지정
function fnFileSaveAs(upload, obj, sNewFileName)
	Dim filepath : filepath = upload.DefaultPath & "\" & sNewFileName

	obj.SaveAs filepath
	fnFileSaveAs = obj.LastSavedFileName
end function
%>
<!--#include virtual="/inc/Language.asp"-->
