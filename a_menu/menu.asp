	<link rel="stylesheet" href="/a_menu/css/style_.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- header-container start -->
<script>
		function login_ok()
		{
			
			var wallet_no = $("#wallet_no").val();
			var password = $("#password").val();
			var remember = $("#remember").val();
			
			if (wallet_no.length == 0){

				modal({
					type: 'info',
					title: '로그인',
					text: '지갑번호를 입력해 주세요.',
					callback: function(result) {
						$("#wallet_no").focus();
					}

				});
				
			}else if (password.length == 0){

				modal({
					type: 'info',
					title: '로그인',
					text: '패스워드를 입력해 주세요.',
					callback: function(result) {
						$("#password").focus();
					}
				});
				
			}else{
				$.ajax({
					 url: "login_ok.asp",
					 type: "POST",
					 data: { wallet_no: wallet_no, password: password, remember: remember },  

					 success: function(data){
						 
						 if(data == "YES"){						 
							location.href="index.asp";							
						 }else if(data == "NO"){							 
							modal({
								type: 'error',
								title: '로그인',
								text: '아이디 패스워드가 틀렸습니다.',
								callback: function(result) {
									$("#wallet_no").focus();
								}
							});				
						 }else if(data == "ERROR"){							 
							modal({
								type: 'error',
								title: '로그인',
								text: '시스템에러 관리자에게 문의하세요.',
								callback: function(result) {
									$("#wallet_no").focus();
								}
							});				
						 }
					 },
		 
					 complete: function(data){
					//	location.href="index.asp";
					 }
				});
			}	
		}

	</script>

<script type="text/javascript">
	function service()
	{
		modal({
			type: '알림',
			title: 'EZ페이',
			text: '서비스 준비중입니다.'
		});
	}
</script>

			
			
			
			<!-- header-container start -->
			<div class="header-container">

			
				<!-- header-top start -->
				<!-- classes:  -->
				<!-- "dark": dark version of header top e.g. class="header-top dark" -->
				<!-- "colored": colored version of header top e.g. class="header-top colored" -->
				<!-- ================ -->
				<div class="header-top blue ">
					<div class="container">
						<div class="row">
							<div class="col-xs-3 col-sm-6 col-md-9">
								<div class="header-top-first clearfix">
									<ul class="social-links circle small clearfix hidden-xs">
										<li class="twitter"><a href="#"><img src="template/images/nations/south-korea.png"></a></li>
										<li class="twitter"><a href="#"><img src="template/images/nations/united-states.png"></a></li>
										<li class="twitter"><a href="#"><img src="template/images/nations/china.png"></a></li>
										<li class="twitter"><a href="#"><img src="template/images/nations/japan.png"></a></li>
										<li class="twitter"><a href="#"><img src="template/images/nations/philippines.png"></a></li>
										
									</ul>
									<div class="social-links hidden-lg hidden-md hidden-sm circle small">
										<div class="btn-group dropdown">
											<button alt="Language" title="Language" type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-language"></i><span style="font-size:12px;"> Language</span></button>
											<ul class="dropdown-menu dropdown-animation">
												<li class="twitter"><a href="#"><img src="template/images/nations/south-korea.png"></a></li>
												<li class="twitter"><a href="#"><img src="template/images/nations/united-states.png"></a></li>
												<li class="twitter"><a href="#"><img src="template/images/nations/china.png"></a></li>
												<li class="twitter"><a href="#"><img src="template/images/nations/japan.png"></a></li>
												<li class="twitter"><a href="#"><img src="template/images/nations/philippines.png"></a></li>
											</ul>
										</div>
									</div>
									
								</div>
								<!-- header-top-first end -->
							</div>
							<div class="col-xs-9 col-sm-6 col-md-3">

								<!-- header-top-second start -->
								<!-- ================ -->
								<div id="header-top-second"  class="clearfix">

									<!-- header top dropdowns start -->
									<!-- ================ -->
									<div class="header-top-dropdown text-right">
										<% If Session("SS_USERID") = "" Then %>
										<div class="btn-group">
											<a href="join.asp" class="btn btn-default btn-sm"><i class="fa fa-user pr-10"></i> 회원가입</a>
										</div>
										<% End if %>
										<div class="btn-group dropdown">
										<% If Session("SS_USERID") <> "" Then %>
										<button type="button" class="btn btn-default btn-sm" onclick="location.href='logout.asp'"><i class="fa fa-unlock pr-10"></i> 로그아웃</button>
										<% Else %>
											<button type="button" class="btn dropdown-toggle btn-default btn-sm" data-toggle="dropdown"><i class="fa fa-lock pr-10"></i> 로그인</button>
											<ul class="dropdown-menu dropdown-menu-right dropdown-animation">
												<li>
													<form class="login-form margin-clear" method="POST">
													
														<div class="form-group has-feedback">
															<label class="control-label">지갑번호</label>
															<input type="tel" class="form-control" name="wallet_no" id="wallet_no" placeholder="" required>
															<i class="fa fa-user form-control-feedback"></i>
														</div>
														<div class="form-group has-feedback">
															<label class="control-label">패스워드</label>
															<input type="password" class="form-control" placeholder="" required  name="password" id="password">
															<i class="fa fa-lock form-control-feedback"></i>
														</div>
														
														
														<ul>
															<label><input type="checkbox" name="remember" id="remember"> 로그인유지</label>
														</ul>
														<button type="button" class="btn btn-gray btn-sm" onclick="login_ok();">Log In</button>
													</form>
												</li>
											</ul>
											<% End If %>
										</div>
									</div>
									<!--  header top dropdowns end -->
								</div>
								<!-- header-top-second end -->
							</div>
						</div>
					</div>
				</div>
				<!-- header-top end -->
				

				<!-- header start -->
				<!-- classes:  -->
				<!-- "fixed": enables fixed navigation mode (sticky menu) e.g. class="header fixed clearfix" -->
				<!-- "dark": dark version of header e.g. class="header dark clearfix" -->
				<!-- "full-width": mandatory class for the full-width menu layout -->
				<!-- "centered": mandatory class for the centered logo layout -->
				<!-- ================ --> 
				<header class="header  fixed    clearfix">
					
					<div class="container">
						<div class="row">
							<div class="col-md-3 ">
								<!-- header-first start -->
								<!-- ================ -->
								<div class="header-first clearfix">
									<% If Session("SS_USERID") <> "" Then %>
									<!-- header dropdown buttons -->
									<div class="header-dropdown-buttons visible-xs">
										<div class="btn-group dropdown">
											<button type="button" class="btn dropdown-toggle" onclick="javascript:location.reload(true);"><i class="fa fa-refresh"></i></button>
										</div>
										<%											
											Dim sResult : sResult = "N"
											Dim gcoin,strSQL,USD,JPY,CNY,PHP,HKD,SGD,MYR

											fnDBConn()

											strSQL = " select gcoin,USD,JPY,CNY,PHP,HKD,SGD,MYR,primeum from member where wallet_no='" & Session("SS_WALLET") & "'"

											cmd.CommandText = strSQL
											Set rs = cmd.execute()
											if not(rs.eof or rs.bof) then
												gcoin = rs("gcoin")
												USD = rs("USD")
												JPY = rs("JPY")
												CNY = rs("CNY")
												PHP = rs("PHP")
												HKD = rs("HKD")
												SGD = rs("SGD")
												MYR = rs("MYR")
												primeum = rs("primeum")
											end if
											rs.close
											Set rs = nothing


											fnDBClose()

										%>
										<div class="btn-group dropdown">
													
											<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-money"></i></button>
											<ul class="dropdown-menu dropdown-menu-right dropdown-animation cart">
												<li>
													<table class="table table-hover">
														<thead>
															<tr>
																<th class="quantity">단위</th>				
																<th class="amount">보유금액</th>
															</tr>
														</thead>
														<tbody>
															<tr>
																<td class="quantity"><img src="template/images/nations/south-korea.png"></td>															
																<td class="amount"><%=fnFormatNumber(gcoin,0,0)%> EZ</td>
															</tr>
															<tr>
																<td class="quantity"><img src="template/images/nations/united-states.png"></td>															
																<td class="amount"><%=USD%> USD</td>
															</tr>
															<tr>
																<td class="quantity"><img src="template/images/nations/japan.png"></td>															
																<td class="amount"><%=fnFormatNumber(JPY,0,0)%> JPY</td>
															</tr>
															<tr>
																<td class="quantity"><img src="template/images/nations/china.png"></td>															
																<td class="amount"><%=fnFormatNumber(CNY,0,0)%> CNY</td>
															</tr>
															<tr>
																<td class="quantity"><img src="template/images/nations/philippines.png"></td>															
																<td class="amount"><%=fnFormatNumber(PHP,0,0)%> PHP</td>
															</tr>
															<tr>
																<td class="quantity"><img src="template/images/nations/hong-kong.png"></td>															
																<td class="amount"><%=HKD%> HKD</td>
															</tr>
															<tr>
																<td class="quantity"><img src="template/images/nations/singapore.png"></td>															
																<td class="amount"><%=SGD%> SGD</td>
															</tr>
															<tr>
																<td class="quantity"><img src="template/images/nations/malaysia.png"></td>															
																<td class="amount"><%=MYR%> MYR</td>
															</tr>
														</tbody>
													</table>
													
												</li>
											</ul>
											
										</div>
									</div>
									<% End If %>
									
									<div id="logo" class="logo">
										<a href="index.asp"><img id="logo_img" src="template/images/logo.png" alt="The Project"></a>
									</div>

									<!-- name-and-slogan -->
									<div class="site-slogan">
										EAZY PAYMENT
									</div>

								</div>
								<!-- header-first end -->

							</div>
							<div class="col-md-9">
					
								<!-- header-second start -->
								<!-- ================ -->
								<div class="header-second clearfix">
									
								<!-- main-navigation start -->
								<!-- classes: -->
								<!-- "onclick": Makes the dropdowns open on click, this the default bootstrap behavior e.g. class="main-navigation onclick" -->
								<!-- "animated": Enables animations on dropdowns opening e.g. class="main-navigation animated" -->
								<!-- "with-dropdown-buttons": Mandatory class that adds extra space, to the main navigation, for the search and cart dropdowns -->
								<!-- ================ -->
								<div class="main-navigation  animated with-dropdown-buttons">

									<!-- navbar start -->
									<!-- ================ -->
									<nav class="navbar navbar-default" role="navigation">
									<div class="container-fluid">
											<!-- Toggle get grouped for better mobile display -->
											<% If Session("SS_USERID") <> "" Then %>												
												<div class="navbar-gcoin" style="width:100%; margin-top:15px; position:absolute; ">
													<%If primeum = "Y" Then%><span style="margin-left:-15px; text-align:left; color:#3198ff; font-size:14px; font-weight:bold;">PREMIUM MEMBER</span><%Else%><%End If%>
													<span style="margin-left:12px; position:absolute;">
													<img src="template/images/nations/south-korea.png" style="width:20px;">
													<span style="float:left; margin-top:-20px; margin-left:23px;"><%=fnFormatNumber(gcoin,0,0)%> EZ</span>
													</span>
												</div>
											<%End if %>
											<div class="navbar-header">
											<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
													<span class="sr-only">Toggle navigation</span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
													<span class="icon-bar"></span>
												</button>
												
											</div>

											<!-- Collect the nav links, forms, and other content for toggling -->
											<div class="collapse" id="navbar-collapse-1">
												<!-- main-menu -->
	<ul class="mainmenu">
		<li><img src="/a_menu/images/user.png" alt="Key icon" class="icon"><span>HOME<span></li>
		<li><img src="/a_menu/images/envelope.png" alt="Envelope icon" class="icon"><span>EZPAY</span></li>
			<ul class="submenu">
				<div class="expand-triangle"><img src="/a_menu/images/expand.png"></div>
				<li><span>충전하기</span></li>
				<li><span>송금하기</span></li>
				<li><span>청산하기</span></li>
				<li><span>거래내역</span></li>
				<li><span>MY쇼핑포인트 사용하기</span></li>
				<li><span>가맹점가입</span></li>
				<li><span>PREMUME MEMBER SHIP</span></li>
			</ul>
		<li><img src="/a_menu/images/cog.png" alt="Cog icon" class="icon"><span>국제환율정보</span></li>
			<ul class="submenu">
				<div class="expand-triangle"><img src="/a_menu/images/expand.png"></div>
				<li><span>국제환율정보</span></li>
				<li><span>외국환전환</span></li>
			</ul>
		<li><img src="/a_menu/images/cog.png" alt="Cog icon" class="icon"><span>비트코인</span></li>
			<ul class="submenu">
				<div class="expand-triangle"><img src="/a_menu/images/expand.png"></div>
				<li><span>송금하기</span></li>
				<li><span>외국환전환</span></li>
				<li><span>거래내역</span></li>
			</ul>
		<li><img src="/a_menu/images/key.png" alt="Key icon" class="icon"><span>쇼핑몰</span></li>
	</ul>
											</div>	
												<!-- main-menu end -->
												
												<% If Session("SS_USERID") <> "" Then %>
												<!-- header dropdown buttons -->
												<div class="header-dropdown-buttons hidden-xs">
													<div class="btn-group dropdown">
														<button type="button" class="btn dropdown-toggle" onclick="javascript:location.reload(true);"><i class="fa fa-refresh"></i></button>
														
													</div>
													
													
													<div class="btn-group dropdown">
													
														<button type="button" class="btn dropdown-toggle" data-toggle="dropdown"><i class="fa fa-money"></i></button>
														<ul class="dropdown-menu dropdown-menu-right dropdown-animation cart">
															<li>
																<table class="table table-hover">
																	<thead>
																		<tr>
																			<th class="quantity">단위</th>				
																			<th class="amount">보유금액</th>
																		</tr>
																	</thead>
																	<tbody>
																		<tr>
																			<td class="quantity"><img src="template/images/nations/south-korea.png"></td>															
																			<td class="amount"><%=fnFormatNumber(gcoin,0,0)%> EZ</td>
																		</tr>
																		<tr>
																			<td class="quantity"><img src="template/images/nations/united-states.png"></td>															
																			<td class="amount"><%=USD%> USD</td>
																		</tr>
																		<tr>
																			<td class="quantity"><img src="template/images/nations/japan.png"></td>															
																			<td class="amount"><%=fnFormatNumber(JPY,0,0)%> JPY</td>
																		</tr>
																		<tr>
																			<td class="quantity"><img src="template/images/nations/china.png"></td>															
																			<td class="amount"><%=fnFormatNumber(CNY,0,0)%> CNY</td>
																		</tr>
																		<tr>
																			<td class="quantity"><img src="template/images/nations/philippines.png"></td>															
																			<td class="amount"><%=fnFormatNumber(PHP,0,0)%> PHP</td>
																		</tr>
																		<tr>
																			<td class="quantity"><img src="template/images/nations/hong-kong.png"></td>															
																			<td class="amount"><%=HKD%> HKD</td>
																		</tr>
																		<tr>
																			<td class="quantity"><img src="template/images/nations/singapore.png"></td>															
																			<td class="amount"><%=SGD%> SGD</td>
																		</tr>
																		<tr>
																			<td class="quantity"><img src="template/images/nations/malaysia.png"></td>															
																			<td class="amount"><%=MYR%> MYR</td>
																		</tr>
																	</tbody>
																</table>
																
															</li>
														</ul>
														
													</div>
												</div>
												<!-- header dropdown buttons end-->
												<% End If %>
											</div>

										</div>
									</nav>
									<!-- navbar end -->

								</div>
								<!-- main-navigation end -->
								</div>
								<!-- header-second end -->
					
							</div>
						</div>
					</div>
					
				</header>
				<!-- header end -->

			</div>
			<!-- header-container end -->
<script src="/a_menu/js/script.js"></script>
<script src="/a_menu/js/retina.min.js"></script>