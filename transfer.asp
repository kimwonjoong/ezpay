<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<% 
	If Session("SS_USERID") = "" Then 
		Response.write "<script type='text/javascript'>"
'		Response.write "alert('로그인이 필요합니다.');"
		Response.redirect "login.asp"
		Response.write "</script>"
	End If 
%>
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY</title>
		<meta name="description" content="The Project a Bootstrap-based, Responsive HTML5 Template">
		<meta name="author" content="htmlcoder.me">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

			<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.carousel.min.css" rel="stylesheet">
		<link href="template/plugins/owlcarousel2/assets/owl.theme.default.min.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- The Project's core CSS file -->
		<!-- Use css/rtl_style.css for RTL version -->
		<link href="template/css/style.css" rel="stylesheet" >
		<!-- The Project's Typography CSS file, includes used fonts -->
		<!-- Used font for body: Roboto -->
		<!-- Used font for headings: Raleway -->
		<!-- Use css/rtl_typography-default.css for RTL version -->
		<link href="template/css/typography-default.css" rel="stylesheet" >
		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/light_blue.css" rel="stylesheet">
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		

		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">

		<script type="text/javascript">
		<!--
		//[] <--문자 범위 [^] <--부정 [0-9] <-- 숫자  
		//[0-9] => \d , [^0-9] => \D
		var rgx1 = /\D/g;  // /[^0-9]/g 와 같은 표현
		var rgx2 = /(\d+)(\d{3})/; 

		function getNumber(obj){
			
			 var num01;
			 var num02;
			 num01 = obj.value;
			 num02 = num01.replace(rgx1,"");
			 num01 = setComma(num02);
			 obj.value =  num01;

		}

		function setComma(inNum){
			 
			 var outNum;
			 outNum = inNum; 
			 while (rgx2.test(outNum)) {
				  outNum = outNum.replace(rgx2, '$1' + ',' + '$2');
			  }
			 return outNum;

		}
		//-->
		</script>

		
	<script type="text/javascript">
		function bank_select(val)
		{
			//alert(val.value);
			var mode = val;
			if (mode.length > 0){
				$.ajax({
					 url: "bank_list.asp",
					 type: "POST",
					 data: { mode: mode },  

					 success: function(data){						 
						 $("#bank_name").empty().append(data);
					 },
		 
					 complete: function(data){

					 }
				});
			}
		}
		function transfer_ok()
		{

			var mode = $("#mode").val();
			var gcoin = $("#gcoin").val();
			gcoin = gcoin.replace(/[^\d]+/g, '');
			var wallet_no = $("#wallet_no").val();
			var bank_name = $("#bank_name").val();
			var account_no = $("#account_no").val();
			var input_name = $("#input_name").val();


			//alert(mode);

			if (wallet_no.length == 0){

				modal({
					type: 'info',
					title: '이체하기',
					text: '지갑번호를 입력해주세요.',
					callback: function(result) {
						$("#wallet_no").focus();
					}

				});
				
			}else if (mode.length == 0){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체단위를 선택해 주세요.',
					callback: function(result) {
						$("#mode").focus();
					}

				});
				
			}else if (gcoin.length == 0){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체금액을 입력해 주세요.',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (bank_name.length == 0){

				modal({
					type: 'info',
					title: '이체하기',
					text: '은행명을 입력해 주세요.',
					callback: function(result) {
						$("#bank_name").focus();
					}
				});
				
			}else if (account_no.length == 0){

				modal({
					type: 'info',
					title: '이체하기',
					text: '계좌번호를 입력해 주세요.',
					callback: function(result) {
						$("#account_no").focus();
					}
				});
				
			}else if (input_name.length == 0){

				modal({
					type: 'info',
					title: '이체하기',
					text: '예금주를 입력해 주세요.',
					callback: function(result) {
						$("#input_name").focus();
					}
				});
				
			}else if (gcoin < 1000 && mode == "EZ"){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체금액은 1,000 EZ 이상을 입력해 주세요.',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 1 && mode == "USD"){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체금액은 1 USD 이상을 입력해 주세요.',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 100 && mode == "JPY"){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체금액은 100 JPY 이상을 입력해 주세요.',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 50 && mode == "CNY"){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체금액은 50 CNY 이상을 입력해 주세요.',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 50 && mode == "PHP"){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체금액은 50 PHP 이상을 입력해 주세요.',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 10 && mode == "HKD"){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체금액은 10 HKD 이상을 입력해 주세요.',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 1 && mode == "SGD"){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체금액은 1 SGD 이상을 입력해 주세요.',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else if (gcoin < 5 && mode == "MYR"){

				modal({
					type: 'info',
					title: '이체하기',
					text: '이체금액은 5 MYR 이상을 입력해 주세요.',
					callback: function(result) {
						$("#gcoin").focus();
					}
				});
				
			}else{

//				sMsg = "청산처리하시겠습니까?";
	
//				if(confirm(sMsg))
//				{	
				modal({
					type: 'confirm',
					title: '이체하기',
					text: bank_name+'('+account_no+')' + input_name + '으로 ' + gcoin + '원 이체처리하시겠습니까?',
					callback: function(result) {
						//alert(result);
						if(result){							
							$.ajax({
								 url: "transfer_ok.asp",
								 type: "POST",
								 data: { mode: mode, gcoin: gcoin,wallet_no: wallet_no,bank_name: bank_name,account_no: account_no,input_name: input_name },  

								 success: function(data){
									 if(data == "YES"){						 
										modal({
											type: 'success',
											title: '이체하기',
											text: '이체신청에 성공하였습니다!',
											callback: function(result) {
												location.href="settlement.asp";
											}
										});		
									 }else{								 
										modal({
											type: 'error',
											title: '이체하기',
											text: data,
											callback: function(result) {
												location.href="transfer.asp";
											}
										});				
									 }
								 },
					 
								 complete: function(data){
								//	location.href="index.asp";
								 }
							});

						}
					}
				});	
			
					

	//			}
				
			}	
		}

		function insert_money(money)
		{
			$("#gcoin").val(money);
		}
		
	</script>
	
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
		
			<!--#include file="header.asp"-->

			<!-- breadcrumb start -->
			<!-- ================ -->
			<div class="breadcrumb-container dark">
				<div class="container">
					<ol class="breadcrumb">
						<li><i class="fa fa-home pr-10"></i><a href="index.asp">Home</a></li>
						<li class="active">이체하기</li>
					</ol>
				</div>
			</div>
			<!-- breadcrumb end -->

			<!-- banner start -->
				<!-- section start -->
			<%

				fnDBConn()

				Dim bank_name,account_no,input_name

				'지금 핀이 사용가능한 핀인지
				strSQL =  "  select bank_name,account_no,input_name from gcoin_bank where mode='kr'"

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					bank_name = rs("bank_name")
					account_no = rs("account_no")
					input_name = rs("input_name")
				end If

				rs.close
					
				fnDBClose()

			%>
			<div class="section dark-bg text-muted">
				<div class="container">
					<h1 class="page-title">이체하기</h1>
					<div class="separator-2"></div>
					<form role="form">
					<input type="hidden" name="wallet_no" id="wallet_no" value="<%=Session("SS_USERID")%>">
						<div class="form-group">
							<label for="exampleInputPassword1">이체단위선택</label>
							<select class="form-control" name="mode" id="mode" required onchange="bank_select(this.value);">
								<option value="">===선택===</option>
								<option value="EZ">EZ</option>
								<option value="USD">USD</option>
								<option value="JPY">JPY</option>
								<option value="CNY">CNY</option>
								<option value="PHP">PHP</option>
								<option value="HKD">HKD</option>
								<option value="SGD">SGD</option>
								<option value="MYR">MYR</option>
							</select>
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1">청산금액</label>
							<input type="money" class="form-control" id="gcoin" name="gcoin" required onchange="getNumber(this);" onkeyup="getNumber(this);">
						</div>
						
						<div class="form-group">
							<label for="exampleInputPassword1">은행명</label>
							<select class="form-control" name="bank_name" id="bank_name" required>
								<option value="">===선택===</option>
							</select>
						</div>

						<div class="form-group">
							<label for="exampleInputPassword1">계좌번호</label>
							<input type="number" class="form-control" name="account_no" id="account_no" required>
						</div>
						<div class="form-group">
							<label for="exampleInputPassword1">예금주명</label>
							<input type="text" class="form-control" name="input_name" id="input_name" required>
						</div>

						<div class="alert alert-info alert-dismissible" role="alert">							
							<font color="red"><b>하루 청산+이체+송금 일회한도 200만원 일한도 500만원 이상 불가능합니다.</b></font>
						</div>
						
					
						<button type="button" class="btn btn-default"  onclick="transfer_ok();">이체신청</button>
					</form>
				</div>
			</div>
			<!-- section end -->
			<div class="space"></div>
								
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>
		<!-- Owl carousel javascript -->
		<script type="text/javascript" src="template/plugins/owlcarousel2/owl.carousel.min.js"></script>
		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
