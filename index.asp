<% @CODEPAGE="65001" language="vbscript" %>
<% session.CodePage = "65001" %>
<% Response.CharSet = "utf-8" %>
<% Response.buffer=true %>
<% Response.Expires = 0 %>
<!--#include file="./inc/inc_db_lib_utf8.asp"-->
<!--#include virtual="/inc/Language.asp"-->
<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
		<title>EZ PAY</title>
		<meta name="google-site-verification" content="fgg4kEpnkY1o2lA06uHYNDGklgf6XhKZ_jqhJ1ahkj0" />
		<meta name="description" content="<%=EZ_LAN_81%>">
		<meta name="keywords" content="<%=EZ_LAN_90%>,EZPAY">
		<meta name="viewport" content="width=device-width">
		<meta property="og:type" content="website">
		<meta property="og:title" content="EZPAY">
		<meta property="og:description" content="<%=EZ_LAN_82%>">
		<meta property="og:image" content="http://www.e-zpay.co.kr/template/images/logo.png">
		<meta property="og:url" content="http://www.e-zpay.co.kr">

		<!-- Mobile Meta -->
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<!-- Favicon -->
		<link rel="shortcut icon" href="template/images/favicon.ico">

		<!-- Web Fonts -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic,700,700italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=PT+Serif' rel='stylesheet' type='text/css'>

		<!-- Bootstrap core CSS -->
		<link href="template/bootstrap/css/bootstrap.css" rel="stylesheet">

		<!-- Font Awesome CSS -->
		<link href="template/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">

		<!-- Fontello CSS -->
		<link href="template/fonts/fontello/css/fontello.css" rel="stylesheet">

		<!-- Plugins -->
		<link href="template/plugins/magnific-popup/magnific-popup.css" rel="stylesheet">
		<link href="template/css/animations.css" rel="stylesheet">
		<link href="template/plugins/hover/hover-min.css" rel="stylesheet">		
		
		<!-- the project core CSS file -->
		<link href="template/css/style.css" rel="stylesheet" >
		<link href="template/css/typography-default.css" rel="stylesheet" >

		<!-- Color Scheme (In order to change the color scheme, replace the blue.css with the color scheme that you prefer)-->
		<link href="template/css/skins/dark_cyan.css" rel="stylesheet">

		<link href="template/css/jquery.modal.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-xenon.css" type="text/css" rel="stylesheet" />
		<link href="template/css/jquery.modal.theme-atlant.css" type="text/css" rel="stylesheet" />
		<link rel="canonical" href="http://www.e-zpay.co.kr/index.asp">

		<style>
		#email_templates .btn-blue { margin-bottom: 10px; display: block; border-color: #3697d9; color: #3697d9;  }
		#email_templates .btn-blue:hover,
		#email_templates .btn-blue:focus { background-color: #338dca; color: #fff; border-color: #338dca; }
		#email_templates .btn-brown { margin-bottom: 10px; display: block; border-color: #91633c; color: #91633c;  }
		#email_templates .btn-brown:hover,
		#email_templates .btn-brown:focus { background-color: #795332; color: #fff; border-color: #795332; }
		#email_templates .btn-cool-green { margin-bottom: 10px; display: block; border-color: #21bb9d; color: #21bb9d;  }
		#email_templates .btn-cool-green:hover,
		#email_templates .btn-cool-green:focus { background-color: #1ea88d; color: #fff; border-color: #1ea88d; }
		#email_templates .btn-dark-cyan { margin-bottom: 10px; display: block; border-color: #2aa4a5; color: #2aa4a5;  }
		#email_templates .btn-dark-cyan:hover,
		#email_templates .btn-dark-cyan:focus { background-color: #248d8e; color: #fff; border-color: #248d8e; }
		#email_templates .btn-dark-red { margin-bottom: 10px; display: block; border-color: #992020; color: #992020;  }
		#email_templates .btn-dark-red:hover,
		#email_templates .btn-dark-red:focus { background-color: #681313; color: #fff; border-color: #681313; }
		#email_templates .btn-gold { margin-bottom: 10px; display: block; border-color: #c7ac56; color: #c7ac56;  }
		#email_templates .btn-gold:hover,
		#email_templates .btn-gold:focus { background-color: #b0984d; color: #fff; border-color: #b0984d; }
		#email_templates .btn-gray { margin-bottom: 10px; display: block; border-color: #809398; color: #809398;  background-color: transparent; }
		#email_templates .btn-gray:hover,
		#email_templates .btn-gray:focus { background-color: #6e7e82; color: #fff; border-color: #6e7e82;sparent; text-decoration: none; }
		#email_templates .btn-green { margin-bottom: 10px; display: block; border-color: #24a828; color: #24a828;  }
		#email_templates .btn-green:hover,
		#email_templates .btn-green:focus { background-color: #1f9123; color: #fff; border-color: #1f9123; }
		#email_templates .btn-light-blue { margin-bottom: 10px; display: block; border-color: #09afdf; color: #09afdf;  }
		#email_templates .btn-light-blue:hover,
		#email_templates .btn-light-blue:focus { background-color: #0c9ec7; color: #fff; border-color: #0c9ec7; }
		#email_templates .btn-orange { margin-bottom: 10px; display: block; border-color: #e07e27; color: #e07e27;  }
		#email_templates .btn-orange:hover,
		#email_templates .btn-orange:focus { background-color: #cc7324; color: #fff; border-color: #cc7324; }
		#email_templates .btn-pink { margin-bottom: 10px; display: block; border-color: #dd7b9b; color: #dd7b9b;  }
		#email_templates .btn-pink:hover,
		#email_templates .btn-pink:focus { background-color: #c9718e; color: #fff; border-color: #c9718e; }
		#email_templates .btn-purple { margin-bottom: 10px; display: block; border-color: #ab6cc6; color: #ab6cc6;  }
		#email_templates .btn-purple:hover,
		#email_templates .btn-purple:focus { background-color: #9760af; color: #fff; border-color: #9760af; }
		#email_templates .btn-red { margin-bottom: 10px; display: block; border-color: #dc5b5b; color: #dc5b5b;  }
		#email_templates .btn-red:hover,
		#email_templates .btn-red:focus { background-color: #c15050; color: #fff; border-color: #c15050; }
		#email_templates .btn-vivid-red { margin-bottom: 10px; display: block; border-color: #c82727; color: #c82727;  }
		#email_templates .btn-vivid-red:hover,
		#email_templates .btn-vivid-red:focus { background-color: #b42424; color: #fff; border-color: #b42424; }
		</style>
		<!-- Custom css --> 
		<link href="template/css/custom.css" rel="stylesheet">
	</head>

	<!-- body classes:  -->
	<!-- "boxed": boxed layout mode e.g. <body class="boxed"> -->
	<!-- "pattern-1 ... pattern-9": background patterns for boxed layout mode e.g. <body class="boxed pattern-1"> -->
	<!-- "transparent-header": makes the header transparent and pulls the banner to top -->
	<body class="no-trans">

		<!-- scrollToTop -->
		<!-- ================ -->
		<div class="scrollToTop circle"><i class="icon-up-open-big"></i></div>
		
		<!-- page wrapper start -->
		<!-- ================ -->
		<div class="page-wrapper">
		
			<!--#include file="header.asp"-->

			<!-- banner start -->
			<!-- ================ -->
			<div class="banner dark-translucent-bg" style="background-image:url('intro-images/img.png'); background-position: 50% 32%;">
				<!-- breadcrumb start -->
				<!-- ================ -->
				<div class="breadcrumb-container">
					<div class="container">
						<ol class="breadcrumb">
							<li><i class="fa fa-home pr-10"></i><a class="link-dark" href="index.html">Home</a></li>
							<li class="active">Page Services</li>
						</ol>
					</div>
				</div>
				<!-- breadcrumb end -->
				<div class="container">
					<div class="row">
						<div class="col-md-8 text-center col-md-offset-2 pv-20">
							<h2 class="title object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100"><%=EZ_LAN_89%> <strong><%=EZ_LAN_90%></strong></h2>
							<div class="separator object-non-visible mt-10" data-animation-effect="fadeIn" data-effect-delay="100"></div>
							<p class="text-center object-non-visible" data-animation-effect="fadeIn" data-effect-delay="100"><%=EZ_LAN_83%></p>
						</div>
					</div>
				</div>
			</div>
			<!-- banner end -->

			<!-- main-container start -->
			<!-- ================ -->
			<section class="main-container padding-bottom-clear">

				<div class="container">
					<div class="row">

						<!-- main start -->
						<!-- ================ -->
						<div class="main col-md-12">

							<div class="row">
								<div class="col-md-4 col-sm-6">
									<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="100">
										<span class="icon large default-bg circle"><i class="fa fa-diamond"></i></span>
										<h3><%=EZ_LAN_84%></h3>
										<div class="separator clearfix"></div>
										<p class="text-muted"><%=EZ_LAN_85%></p>
										<a href="charge.asp">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
									</div>
								</div>
								<div class="col-md-4 col-sm-6">
									<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="200">
										<span class="icon large default-bg circle"><i class="fa fa-connectdevelop"></i></span>
										<h3><%=EZ_LAN_86%></h3>
										<div class="separator clearfix"></div>
										<p class="text-muted"><%=EZ_LAN_87%></p>
										<a href="send.asp">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
									</div>
								</div>
								<div class="col-md-4 col-sm-6">
									<div class="ph-20 feature-box text-center object-non-visible" data-animation-effect="fadeInDownSmall" data-effect-delay="300">
										<span class="icon large default-bg circle"><i class="icon-snow"></i></span>
										<h3><%=EZ_LAN_86%></h3>
										<div class="separator clearfix"></div>
										<p class="text-muted"><%=EZ_LAN_87_1%></p>
										<a href="settlement.asp">Read More<i class="pl-5 fa fa-angle-double-right"></i></a>
									</div>
								</div>
								
							</div>

						</div>
						<!-- main end -->

					</div>
				</div>
			</section>
			<!-- main-container end -->

<!--
			<div class="banner dark-translucent-bg" style="background-image:url('intro-images/img.png');">
				
				<div class="container">
					
					<div class="row">
						<div class="col-md-8 text-center col-md-offset-2 space-bottom">
							<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
						</div>
					</div>
					
				</div>
				
			</div>
		-->
						
			<!--#include file="footer.asp"-->
			
		</div>
		<!-- page-wrapper end -->

		<!-- JavaScript files placed at the end of the document so the pages load faster -->
		<!-- ================================================== -->
		<!-- Jquery and Bootstap core js files -->
		<script type="text/javascript" src="template/plugins/jquery.min.js"></script>
		<script type="text/javascript" src="template/bootstrap/js/bootstrap.min.js"></script>

		<!-- Modernizr javascript -->
		<script type="text/javascript" src="template/plugins/modernizr.js"></script>

		<!-- Isotope javascript -->
		<script type="text/javascript" src="template/plugins/isotope/isotope.pkgd.min.js"></script>
		
		<!-- Magnific Popup javascript -->
		<script type="text/javascript" src="template/plugins/magnific-popup/jquery.magnific-popup.min.js"></script>
		
		<!-- Appear javascript -->
		<script type="text/javascript" src="template/plugins/waypoints/jquery.waypoints.min.js"></script>

		<!-- Count To javascript -->
		<script type="text/javascript" src="template/plugins/jquery.countTo.js"></script>
		
		<!-- Parallax javascript -->
		<script src="template/plugins/jquery.parallax-1.1.3.js"></script>

		<!-- Contact form -->
		<script src="template/plugins/jquery.validate.js"></script>

		<!-- Background Video -->
		<script src="template/plugins/vide/jquery.vide.js"></script>

		<!-- SmoothScroll javascript -->
		<script type="text/javascript" src="template/plugins/jquery.browser.js"></script>
		<script type="text/javascript" src="template/plugins/SmoothScroll.js"></script>

		<!-- Initialization of Plugins -->
		<script type="text/javascript" src="template/js/template.js"></script>

		<!-- Custom Scripts -->
		<script type="text/javascript" src="template/js/custom.js"></script>
		<script type="text/javascript" src="template/js/jquery.modal.js"></script>
	</body>
</html>
