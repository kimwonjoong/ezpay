<!--#include file="./inc/inc_db_lib.asp"-->

<%
session.CodePage = 65001
response.Charset = "UTF-8"
on error resume next

Dim sResult : sResult = "N"
Dim nResultCnt : nResultCnt = 0
Dim g_sum : g_sum = 0

Dim wallet_no : wallet_no = Trim(Request("wallet_no"))
Dim gcoin : gcoin = Trim(Request("gcoin"))
Dim mode : mode = Trim(Request("mode"))
Dim bank_name : bank_name = Trim(Request("bank_name"))
Dim account_no : account_no = Trim(Request("account_no"))
Dim input_name : input_name = Trim(Request("input_name"))
Dim SS_premium : SS_premium = Session("SS_premium")
gcoin = Replace(gcoin,",","")

'If wallet_no = "M1008" Then
'	Response.write "9"
'Else

	Dim bank_code

	If bank_name = "우리은행" Or bank_name = "우리" Then
		bank_code = "020"
	ElseIf bank_name = "국민은행" Or bank_name = "국민" Then
		bank_code = "004"
	ElseIf bank_name = "기업은행" Or bank_name = "기업" Then
		bank_code = "003"
	ElseIf bank_name = "농협" Then
		bank_code = "011"
	ElseIf bank_name = "SC" Then
		bank_code = "023"
	ElseIf bank_name = "우체국" Then
		bank_code = "071"
	ElseIf bank_name = "신한은행" Or bank_name = "신한" Then
		bank_code = "088"
	ElseIf bank_name = "하나은행" Or bank_name = "하나" Then
		bank_code = "081"
	ElseIf bank_name = "KDB산업" Or bank_name = "산업" Then
		bank_code = "002"
	ElseIf bank_name = "새마을금고" Then
		bank_code = "045"
	ElseIf bank_name = "경남" Or bank_name = "경남은행" Then
		bank_code = "039"
	ElseIf bank_name = "부산" Or bank_name = "부산은행" Then
		bank_code = "032"
	ElseIf bank_name = "외환" Or bank_name = "외환은행" Then
		bank_code = "005"
	ElseIf bank_name = "광주" Or bank_name = "광주은행" Then
		bank_code = "034"
	ElseIf bank_name = "씨티" Or bank_name = "씨티은행" Then
		bank_code = "027"
	ElseIf bank_name = "대구" Or bank_name = "대구은행" Then
		bank_code = "031"
	ElseIf bank_name = "제주" Or bank_name = "제주은행" Then
		bank_code = "035"
	ElseIf bank_name = "신협" Then
		bank_code = "048"
	ElseIf bank_name = "수협" Then
		bank_code = "007"
	ElseIf bank_name = "전북은행" Or bank_name = "전북" Then
		bank_code = "037"
	ElseIf bank_name = "산업" Then
		bank_code = "002"	
	End if

	fnDBConn()

	sCheck = "NO"
	ssCheck = "YES"

		'지금 핀이 사용가능한 핀인지
		strSQL =  "  select wallet_no, gcoin from member where wallet_no='" & wallet_no & "'"
	'Response.write strSQL
		cmd.CommandText = strSQL
		Set rs = cmd.execute()

		if not(rs.eof or rs.bof) then
			If mode = "EZ" then 
				now_gcoin = rs("gcoin")
			ElseIf mode = "USD" then 
				now_gcoin = rs("USD")
			ElseIf mode = "JPY" then 
				now_gcoin = rs("JPY")
			ElseIf mode = "CNY" then 
				now_gcoin = rs("CNY")
			ElseIf mode = "PHP" then 
				now_gcoin = rs("PHP")
			ElseIf mode = "HKD" then 
				now_gcoin = rs("HKD")
			Else
				now_gcoin = rs("gcoin")
			End If 
			If CCur(now_gcoin) >= CCur(gcoin) Then
				sCheck = "YES"
			Else 
				sCheck = "NO"
				sResult = "0"
				'msg = EZ_LAN_148
			End if
		end If

		rs.close

		If(sCheck="YES") Then

			strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_settlement where wallet_no='" & wallet_no & "' and (result='0' or result='1') and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				g_sum = CCur(rs("g_sum"))
				
			end If
			
			rs.close


			strSQL =  "  select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_settlement2 where wallet_no='" & wallet_no & "' and (result='0' or result='1') and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				g_sum = g_sum + CCur(rs("g_sum"))
				
			end If
			
			rs.close


			strSQL =  " select  isnull(sum( case isnumeric ( gcoin ) when 1 then convert ( int, gcoin ) else 0 end),0) as g_sum  from gcoin_transfer where send_wallet='" & wallet_no & "' and receive_wallet not like 'M%' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, 0, GETDATE()),  23) AS DATETIME) "

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				g_sum = g_sum + CCur(rs("g_sum"))
				
			end If
			
				rs.close

			'특정 지갑회원
			If wallet_no = "521004" Or wallet_no = "407000" Or wallet_no="M1008"  Or wallet_no="M1009" Then
				sCheck = "YES"
			else
			'특정 지갑회원

				'1일 청산금액 체크
				If SS_premium <> "Y" then
					If CCur(g_sum)+CCur(gcoin) <= 5000000 Then
						sCheck = "YES"
					Else
						sCheck = "NO"
						sResult = "1"
						'msg = EZ_LAN_194 & 5000000-CCur(g_sum) & EZ_LAN_194_1
					End if
				End If

				'1회 청산금액 체크
				If SS_premium <> "Y" then
					If(sCheck="YES") Then		
						If CCur(gcoin) <= 2000000 Then
							sCheck = "YES"
						Else
							sCheck = "NO"
							sResult = "2"
							'msg = EZ_LAN_195 & 2000000-CCur(g_sum) & EZ_LAN_195_1
						end If	
					End if	
				Else
					If CCur(gcoin) <= 30000000 Then
						sCheck = "YES"
					Else
						sCheck = "NO"
						sResult = "3"
						'msg = EZ_LAN_154
					end If	
				End If

			End if
		End if	

		If(sCheck="YES") Then
			
			strSQL =  "  select count(*) as cnt from gcoin_buy where wallet_no ='" & wallet_no & "' and result='1' and regist_date >= CAST(CONVERT(VARCHAR, DATEADD(DAY, -1, GETDATE()),  23) AS DATETIME) "

			cmd.CommandText = strSQL
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				b_cnt = rs("cnt")
				If b_cnt = 0 Then
					sCheck = "YES"
				Else
					sCheck = "NO"
					sResult = "4"
					'msg = EZ_LAN_155
				End if
			end If
			
			rs.close



			If(sCheck="NO") Then

				'지금 핀이 사용가능한 핀인지
				strSQL =  "  select count(*) as cnt from gcoin_transfer where send_wallet ='" & wallet_no & "' or receive_wallet='" & wallet_no & "'"

				cmd.CommandText = strSQL
				Set rs = cmd.execute()

				if not(rs.eof or rs.bof) then
					s_cnt = rs("cnt")
					If s_cnt > 0 Then
						sCheck = "YES"
					Else
						sCheck = "NO"
						ssCheck = "NO"
						sResult = "4"
						'msg = EZ_LAN_155
					End if
				end If
				
				rs.close

			End if

		End if
		
		If(sCheck="YES") Then

			strSQL=""
		
			dbConn.BeginTrans

			Dim now_date,now_time
			now_date = Replace(date, "-", "") 
			now_time = Year(Now)&Month(Now)&Day(Now)&Hour(Now)&Minute(Now)&Second(Now)


			'지금 핀이 사용가능한 핀인지
			sql =  "  select TR_SEQ from TB_CTBX_SEQ where datediff(day,TR_DATE,'" & now_date & "')=0"

			cmd.CommandText = sql
			Set rs = cmd.execute()

			if not(rs.eof or rs.bof) then
				TR_SEQ = rs("TR_SEQ")
			Else
				sql2 = "	insert into TB_CTBX_SEQ(TR_DATE,SEND_ID,TR_SEQ) values"
				sql2 = sql2 & "	( '" & now_date &"','', 1 ) "
				

				cmd.CommandText = sql2

				cmd.Execute nResultCnt, , adExecuteNoRecords

			end If
			
			rs.close

			If mode = "EZ" then 
				strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
			ElseIf mode = "USD" then 
				strSQL = strSQL & "	update  member set USD=convert(money,USD)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
			ElseIf mode = "JPY" then 
				strSQL = strSQL & "	update  member set JPY=convert(money,JPY)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
			ElseIf mode = "CNY" then 
				strSQL = strSQL & "	update  member set CNY=convert(money,CNY)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
			ElseIf mode = "PHP" then 
				strSQL = strSQL & "	update  member set PHP=convert(money,PHP)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
			ElseIf mode = "HKD" then 
				strSQL = strSQL & "	update  member set HKD=convert(money,HKD)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
			Else
				strSQL = strSQL & "	update  member set gcoin=convert(money,gcoin)-convert(money,'"&gcoin&"')  where wallet_no='" & wallet_no & "'"	
			End If 
			
			strSQL = strSQL & "	update  TB_CTBX_SEQ set TR_SEQ=TR_SEQ+1 where TR_DATE='" & now_date & "'"

			strSQL = strSQL & "	insert into gcoin_settlement(wallet_no,gcoin,bank_name,bank_code,account_no,input_name,mode,TR_DATE,SEQ) values"
			strSQL = strSQL & "	( '" & wallet_no &"','" & gcoin &"','" & bank_name &"','" & bank_code &"','" & account_no &"','" & input_name &"','" & mode &"','" & now_date & "',dbo.rtn_seq())"

			If mode = "B" Or mode = "EZ" Then
			
				strSQL = strSQL & "	insert into TB_BANK_TRAN(TR_DATE,SEQ,BANK_CD,ORG_CD,OUT_BANK_CD,OUT_ACCT_NO,IN_BANK_CD,IN_ACCT_NO,TR_AMT,OUT_REMARK,IN_REMARK) values"
				strSQL = strSQL & "	( '" & now_date &"',dbo.rtn_seq(),'003','20034321','003','20911827301016','"& bank_code &"','"& account_no &"',"& gcoin &",'"& input_name &"','이지페이')"

			End if



			
			cmd.CommandText = strSQL
			cmd.Execute nResultCnt, , adExecuteNoRecords

			if nResultCnt <> 1 or Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
				dbConn.RollBackTrans
				fnErrSave()
				'msg = EZ_LAN_9
				sResult = "5"
			else
				dbConn.CommitTrans
				sResult = "Y"
			'	fnLogSave()
			end If

		End If
		
		If (ssCheck="NO") Then


			strSQL=""
		
			dbConn.BeginTrans

			strSQL = strSQL & "	insert into gcoin_settlement(wallet_no,gcoin,bank_name,bank_code,account_no,input_name,result) values"
			strSQL = strSQL & "	( '" & wallet_no &"','" & gcoin &"','" & bank_name &"','" & bank_code &"','" & account_no &"','" & input_name &"','3')"
			
			cmd.CommandText = strSQL
			cmd.Execute nResultCnt, , adExecuteNoRecords

			if nResultCnt <> 1 or Err.Number <> 0 or dbConn.Errors.Count > 0 or Server.GetLastError.Number then
				dbConn.RollBackTrans
				fnErrSave()
				'msg = EZ_LAN_9
				sResult = "5"
			else
				dbConn.CommitTrans
				sResult = "Y"
			'	fnLogSave()
			end If

		End if
		


	fnDBClose()

	If sResult <> "Y" Then
		
		sResult = msg

	End if
	%>

	<% 
	If sResult = "Y" Then
		Response.write "YES"
	ElseIf sResult = "0" Then
		Response.write "0"
	ElseIf sResult = "1" Then
		Response.write "1"
	' 프리미엄회원 3천만원 까지
	ElseIf sResult = "2" Then
		Response.write "2"
	' 1일 500만원 까지 (프리미엄 회원이 아닐경우)
	ElseIf sResult = "3" Then
		Response.write "3"
	ElseIf sResult = "4" Then
		Response.write "4"
	ElseIf sResult = "5" Then
		Response.write "5"
	Else
		Response.write "NO"
		'Response.write msg
	End If

'End If
%>